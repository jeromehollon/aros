﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ExpenseRepository.Interface;

namespace ExpenseRepository.Sql.Archive
{
    public class Detail : IDetail
    {
        public string AccountId
        { get; set; }
        

       List<IDetail> IDetail.GetDetails(string branch, int month, int year, string accountId)
        {
            ExpensesArchive context = new ExpensesArchive();

            var details = context.GetAcctDetails(accountId, branch, month, year);

            List<IDetail> detailList = new List<IDetail>();

            foreach (var item in details)
            {
                IDetail detail = new Detail();

                detail.AccountId = item.Account;
                detail.AccountName = item.FACCOUNT;
                detail.Amount = double.Parse(item.FAMOUNT, NumberStyles.Currency);
                detail.Payee = item.FNAME;
                detail.Memo = item.FMEMO;
                detail.TransactionDate = DateTime.Parse(item.FDATE);

                detailList.Add(detail);
            }

            return detailList;
        }

        public int Month
        { get; set; }

        public int Year
        { get; set; }

        public DateTime TransactionDate
        { get; set; }

        public double Amount
        { get; set; }

        public string AccountName
        { get; set; }
        
        public string Payee
        { get; set; }

        public string Memo
        { get; set; }
    }
}
