﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseRepository.Interface;

namespace ExpenseRepository.Sql.Archive
{
    public class Expenses : IExpenses
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public string AccountId { get; set; }
        public double TotalAmt { get; set; }
        public string BranchName { get; set; }

        List<IExpenses> IExpenses.GetSummaryByBranch(string branchId, int year)
        {
            ExpensesArchive db = new ExpensesArchive();
            db.Database.CommandTimeout = 180;
            var summaries = db.AcctSummaryByBranch(branchId, year);

            List<IExpenses> summaryList = GetExpenses(summaries);

            return summaryList;
        }


        private static List<IExpenses> GetExpenses(System.Data.Entity.Core.Objects.ObjectResult<AcctSummaryByBranch_Result> summaries)
        {
            List<IExpenses> summaryList = new List<IExpenses>();

            foreach (var item in summaries)
            {
                Expenses summary = new Expenses();

                summary.AccountId = item.Account;
                summary.Month = (int)item.Mnth;
                summary.Year = (int)item.Yr; 
                summary.TotalAmt = (double)item.Tot_Amt;
                summary.BranchName = item.Branch;

                summaryList.Add(summary);
            }
            return summaryList;
        }
        
     
        DateTime IExpenses.DateRefreshed
        {
            get
            {               
                ExpensesArchive db = new ExpensesArchive();
                var lastUpdate = db.ProfitAndLossDetailLastUpdates.Max(d => d.LastUpdate);
                
                DateTime dateRefreshed = lastUpdate;
              
                return dateRefreshed;
            }
        }


        
        DateTime IExpenses.AcctClosingDate
        {
            get
            {
                ExpensesArchive db = new ExpensesArchive();
                var lastUpdate = db.AcctClosings.Where(d => d.AcctClosingDate.Year == Year).Max(d => d.AcctClosingDate);

                DateTime dateRefreshed = lastUpdate;
              
                return dateRefreshed;
            }
        }
    }
}

