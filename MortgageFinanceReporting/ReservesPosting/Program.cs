﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using _5XUtilities;
using BranchRepository.Interface;
using dotNetTips.Utility.Logging;
using WatiN.Core;

namespace ReservesPosting
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            // log starting event
            LoggingHelper.WriteEntry("Branch Beginning Balances Started", TraceEventType.Information);

            string uriPath = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(Program)).CodeBase) + "/ErrLog.txt";
            string localPath = new Uri(uriPath).LocalPath;
            IExceptionManagement exceptionMgt = new ExceptionManagement(ExceptionManagement.AlertMethod.File, localPath, "", "", "");
            string branchName = "";

            try
            {
                // Get Branches With Ending Balances - find current branches that also existed last year - use branch name
                List<IBranch> branchesRepo = BranchRepository.Sql.Branches.GetBranches(DateTime.Now.Year);

                var branches = from b in branchesRepo
                               select new { b.BranchId, b.BranchName };

                // get last year string
                string yearStr = (DateTime.Now.Year - 1).ToString();

                foreach (var branch in branches)
                {
                    branchName = branch.BranchName;
                    IBranch br = new BranchRepository.Sql.Branches();
                    br.BranchId = branch.BranchId;
                    br.BranchName = branch.BranchName;
                    
                    IBranch branchUpdated = ReserveReport(yearStr, br);

                    //using (var context = new AROSReportsEntities())
                    //{
                    //    // use ids - they are unlikely to change from year to year unless a dba does it
                    //    var branchRepo = context.Branches.Find(branch.BranchId);

                    //    // last year's branches may not exist for this year's branch
                    //    if (branchRepo != null)
                    //    {
                    //        branchRepo.BegAroAccountBal = branchUpdated.BegAroAccountBal;
                    //        branchRepo.BegBranchReserveBal = branchUpdated.BegBranchReserveBal;
                    //        branchRepo.BegLoanLossReserveBal = branchUpdated.BegLoanLossReserveBal;

                    //        context.SaveChanges();
                    //    }
                    //}
                   
                }
            }
            catch (Exception ex)
            {
                exceptionMgt.logException(ex, "Venta", "", "Branch Beginning Balances", branchName);
                LoggingHelper.WriteEntry("Error: " + ex.Message, TraceEventType.Error);
            }

            LoggingHelper.WriteEntry("Branch Beginning Balances Ended", TraceEventType.Information);
        }



        public static IBranch ReserveReport(string year, IBranch branch)
        {
            // create web session
            string arosUrl = ReservesPosting.Properties.Settings.Default.ArosUrl.ToString();

            using (var browser = new IE(arosUrl))
            {
                browser.TextField(Find.ById("UserName")).TypeText("admin");
                browser.TextField(Find.ById("Password")).TypeText(ReservesPosting.Properties.Settings.Default.AdminPw.ToString());
                browser.Button(Find.ByValue("Log in")).Focus();
                browser.Button(Find.ByValue("Log in")).ClickNoWait();
                browser.WaitForComplete();
                Thread.Sleep(1000);

                browser.Link(link => link.Text == "View Reserve Accounts Report »").Click();
                Thread.Sleep(1000);

                var yearList = browser.SelectList(Find.ById("lstYear"));
                yearList.Select(year);
                Thread.Sleep(5000);

                var branchList = browser.SelectList(Find.ById("lstBranchId"));
                branchList.Select(branch.BranchName);

                browser.WaitForComplete();
                browser.Button(Find.ByValue("Ok")).ClickNoWait();
                browser.WaitForComplete();
                Thread.Sleep(1000);

                if (browser.Body.Element("RAS22").InnerHtml == "-")
                    {
                        branch.BegBranchReserveBal = 0;
                    }
                    else
                    {
                        branch.BegBranchReserveBal = decimal.Parse(Sanitize(browser.Body.Element("RAS22").InnerHtml));
                    }
                
                    if (browser.Body.Element("RAS8").InnerHtml == "-")
                    {
                        branch.BegLoanLossReserveBal = 0;
                    }
                    else
                    {
                        branch.BegLoanLossReserveBal = decimal.Parse(Sanitize(browser.Body.Element("RAS8").InnerHtml));
                    }
                
                    if (browser.Body.Element("RAS29").InnerHtml == "-")
                    {
                        branch.BegAroAccountBal = 0;
                    }
                    else
                    {
                        branch.BegAroAccountBal = decimal.Parse(Sanitize(browser.Body.Element("RAS29").InnerHtml));
                    }                
            }

            return branch;
        }

        private static string Sanitize(string cellVal)
        {
            cellVal = cellVal.Replace("<span style=\"color: red;\">", "");
            cellVal = cellVal.Replace("</span>", "");
            return cellVal;
        }
    }
}
