﻿using System;
using System.Collections.Generic;
using System.Linq;
using BranchRepository.Interface;

namespace Reports
{
    internal class BranchInfo
    {   
        public string BranchId { get; set; }

        public IBranch Branch { get; set; }

        public IBpsRankings BpsRanking { get; set; }

        public BranchInfo(IBranch branch, IBpsRankings bpsRankings)
        {
            Branch = branch;
            BpsRanking = bpsRankings;
        }

          public BranchInfo()
        {
            Branch = new BranchRepository.Sql.Branches();

            BpsRanking = new BranchRepository.Sql.BpsRankings();
        }

        #region constants
        const int ArRateCell = 6;
        const int ArRateHistCell = 8;
        const int BranchMarginCell = 8;
        const int LoMarginCell = 9;
        const int ProcessingFeeCell = 1;
        const int LoanLossRsrvRateCell = 121;
        #endregion

        internal IBranch GetBranchData(string branchId)
        {            
            IBranch branch = Branch.GetBranchData(branchId);

            return branch;
        }

        #region AroReport
        internal string AddBranchToAroRpt(string aroTemplate, IBranch branch, List<IBranchHistory> branchHist)
        {
            string aRRateStr = "0";
            string aroRpt = aroTemplate;

            aroRpt = aroRpt.Replace("@BRANCH_NAME", branch.BranchName);

            aroRpt = AddBranchHistory(branchHist, aroRpt);

            // AddArRateToAroRpt is a legacy function for 2014. AR Rate can change over time but only 2014 is static over the year
            aroRpt = AddArRateToAroRpt(branch, aRRateStr, aroRpt);

            aroRpt = AddLoanLossRsrvToAroRpt(branch, aroRpt);

            aroRpt = AddBranchMarginToAroRpt(branch, aroRpt); 
 
            aroRpt = AddLoMarginToAroRpt(branch, aroRpt); 

            aroRpt = AddProessingFeeToAroRpt(branch, aroRpt); 

            return aroRpt;
        }
        

        private static string AddBranchMarginToAroRpt(IBranch branch, string aroRpt)
        {
            string branchMarginStr = "";
            if (branch.BranchMargin > 0)
            {
                branchMarginStr = string.Format("{0:0.000%}", (double)branch.BranchMargin * .01);
            }
            aroRpt = aroRpt.Replace("$G$" + BranchMarginCell, branchMarginStr);

            return aroRpt;
        }


        private static string AddLoMarginToAroRpt(IBranch branch, string aroRpt)
        {
            string loMarginStr = "";
            if (branch.LoMargin > 0)
            {
                loMarginStr = string.Format("{0:0.000%}", (double)branch.LoMargin * .01);
            }
            aroRpt = aroRpt.Replace("$G$" + LoMarginCell, loMarginStr);

            return aroRpt;
        }
        

        private static string AddProessingFeeToAroRpt(IBranch branch, string aroRpt)
        {
            string proessingFeeStr = "";
            if (branch.ProcessingFee > 0)
            {
                proessingFeeStr = string.Format("{0:C}", (double)branch.ProcessingFee);
            }
            aroRpt = aroRpt.Replace("$G$" + ProcessingFeeCell, proessingFeeStr);

            return aroRpt;
        }


        // ar rate history function - introduced in 2/2016
        private static string AddBranchHistory(List<IBranchHistory> branchHist, string aroRpt)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();

            // get month array
            string[] monthCols = Common.GetMonthCols();

            // add token/amount to the dictionary for each month by cycling through 0-based array of columns that correspond to months
            int i = 0;
            do
            {
                AddArrBranchHistoryCells(branchHist, d, monthCols, i);
                i += 1;

            } while (i < 12);

            // replace tokens
            aroRpt = ReplaceTokenInTemplate(aroRpt, d);

            return aroRpt;
        }


        private static string AddArRateToAroRpt(IBranch branch, string aRRateStr, string aroRpt)
        {

            if (branch.ARRate > 0)
            {
                aRRateStr = string.Format("{0:0.000%}", (double)branch.ARRate * .01);
            }
            aroRpt = aroRpt.Replace("$G$" + ArRateCell, aRRateStr);

            return aroRpt;
        }


        internal static string ReplaceTokenInTemplate(string aroTemplate, Dictionary<string, string> d)
        {
            // replace tokens without id spans
            foreach (var pair in d)
            {                
                aroTemplate = aroTemplate.Replace(String.Format("{0}", pair.Key), String.Format("{0}", pair.Value));
            }

            return aroTemplate;
        }


        private static void AddArrBranchHistoryCells(List<IBranchHistory> branchHist, Dictionary<string, string> d, string[] monthCols, int i)
        {
            var arRate = branchHist.Where(s => s.MonthNum == i + 1).Select(s => s.ARRate).FirstOrDefault();
            string arRateStr = string.Format("{0:0.000%}", (double)arRate * .01);
            d.Add(String.Format("${0}${1}</", monthCols[i], ArRateHistCell), arRateStr + "</");
        }


        private static string AddLoanLossRsrvToAroRpt(IBranch branch, string aroRpt)
        {
            string loanLossRsrveRate;
            if (branch.LoanLossReserveRate > 0)
                loanLossRsrveRate = string.Format("{0:0.00%}", (double)branch.LoanLossReserveRate * .01);
            else
                loanLossRsrveRate = "0";
            aroRpt = aroRpt.Replace("$G$" + LoanLossRsrvRateCell, loanLossRsrveRate);

            return aroRpt;
        }        
        #endregion

        #region ReserveAcctRpt
        internal static string AddBranchToReserveAcctRpt(string reserveAcctTemplate, IBranch branch)
        {
            string reserveAcctRpt = "";
            
            if (string.IsNullOrEmpty(branch.BranchName) == false) {
                reserveAcctRpt = reserveAcctTemplate;

                reserveAcctRpt = reserveAcctRpt.Replace("@BRANCH_NAME", branch.BranchName);
            }
            else {
               reserveAcctRpt = "Branch Does Not Exist";                
            }

            return reserveAcctRpt;
        }
        #endregion      
    }
}
