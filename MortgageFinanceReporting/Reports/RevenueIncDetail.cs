﻿
namespace Reports
{
    class RevenueIncDetail : ExpenseDetail
    {
        public RevenueIncDetail(string acctMap)
        {
            ExpenseMapPath = acctMap;
        }

        public string ExpenseMapPath { get; set; }

    }
}
