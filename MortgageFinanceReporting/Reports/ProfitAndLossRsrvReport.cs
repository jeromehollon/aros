﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BranchRepository.Interface;
using ExpenseRepository.Interface;
using RevenueExtractor.Interface;
using RevenueRepository.Interface;

namespace Reports
{
    public class ProfitAndLossRsrvReport : ReserveAccounts
    {
        #region Properties

        // renvenue map is unique to the profit and loss report - other properties are inherited from the base class
        public string RevenueIncMapPath { get; set; }
        public string RevenueIncTotalMapPath { get; set; }
        public IBranchHistory BranchHistory { get; private set; }
        public string ExpenseAcctsPath { get; set; }
        #endregion


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the AroReport class.
        /// </summary>
        public ProfitAndLossRsrvReport(string templateFilePath, IBranch branch, IBranchHistory branchHist, IBpsRankings bpsRankings, IExpenses expenses, IRevenue revenue, ExpenseRepository.Interface.IDetail expenseDtl, int year)
        {
            // explicitly set template path
            TemplateFilePath = templateFilePath;
            Year = year;

            Branch = branch;
            BranchHistory = branchHist;
            Expenses = expenses;
            Revenue = revenue;
        }


        /// <summary>
        /// Initializes a new instance of the AroReport class.
        /// </summary>
        public ProfitAndLossRsrvReport(string templateFilePath, int year)
        {
            // explicitly set template path
            TemplateFilePath = templateFilePath;
            Year = year;

            BranchHistory = new BranchRepository.Sql.BranchHistories();

            if (Year == DateTime.Now.Year)
            {
                Branch = new BranchRepository.Sql.Branches();                
                Revenue = new RevenueRepository.Sql.Pipeline();
            }
            else
            {
                Branch = new BranchRepository.Sql.Archive.Branches();
                Revenue = new RevenueRepository.Sql.Archive.Pipeline();
            }

            Expenses = new ExpenseRepository.Sql.Expenses();
        }


        /// <summary>
        /// Initializes a new instance of the AroReport class.
        /// </summary>
        public ProfitAndLossRsrvReport()
        {
            BranchHistory = new BranchRepository.Sql.BranchHistories();

            // default composition root 
            if (Year == DateTime.Now.Year)
            {
                Branch = new BranchRepository.Sql.Branches();                
                Revenue = new RevenueRepository.Sql.Pipeline();
            }
            else
            {
                Branch = new BranchRepository.Sql.Archive.Branches();
                Revenue = new RevenueRepository.Sql.Archive.Pipeline();
            }
                        
            Expenses = new ExpenseRepository.Sql.Expenses();            
        }
        #endregion


        #region Public

        public string GetProfitAndLossReport()
        {
            string plRpt = Common.GetRptTemplate(TemplateFilePath);            

            Expenses.Year = Year;
            Revenue.Year = Year;

            List<IExpenses> summary = Expenses.GetSummaryByBranch(BranchId, Year);

            // revenue income uses the expense logic and data
            Reports.Expenses.ExpenseMapPath = RevenueIncMapPath;
            Reports.Expenses.ExpenseTotalMapPath = RevenueIncTotalMapPath;

            plRpt = GetReserveAcctReport();
            plRpt = plRpt.Replace("�", " ");            

            // date stamp of last expense update   
            DateTime dateRefreshed = Expenses.DateRefreshed;
            DateTime acctClosingDate = Expenses.AcctClosingDate;
           
            // branch info
            Branch.Year = Year;

            IBranch branch = Branch.GetBranchData(BranchId);

            List<IBranchHistory> branchHist = BranchHistory.GetBranchHistory(Year, BranchId);

            BranchInfo branchInfo = new BranchInfo();
            plRpt = branchInfo.AddBranchToAroRpt(plRpt, branch, branchHist);

            plRpt = Reports.Expenses.AddExpensesToTemplate(plRpt, BranchId, summary, dateRefreshed, acctClosingDate, Year, SectionType.Expense);

            // revenue           
            // year support - go to archive when not current year            
            Revenue.Year = Year;

            return plRpt;
        }


        public string GetAllocationsReport()
        {
            string plRpt = Common.GetRptTemplate(TemplateFilePath);
           
            // branch info
            Branch.Year = Year;
            IBranch branch = Branch.GetBranchData(BranchId);
            List<IExpenses> expenses = Expenses.GetSummaryByBranch(BranchId, Year);
            Revenue.Year = Year;
            List<IRevenue> revenue = Reports.Revenue.GetRevenueData(BranchId, Revenue);


            Allocations allocations = new Allocations();
            allocations.CogsMapPath = CogsMapPath;
            allocations.ExpenseAcctsPath = ExpenseAcctsPath;
            allocations.RevenueIncMapPath = RevenueIncMapPath;
            allocations.RptType = "PL";
            allocations.BonusPaidAcct = "6025";

            Allocations.AllocationsReserves rsrvs = allocations.Main(branch, 2016, revenue, expenses);


            // add date stamp
            plRpt = AddDateStamps(plRpt);

            // get month array
            string[] monthCols = Common.GetMonthCols();

            int i = 0;
            do
            {
                // loan loss reserve
                plRpt = plRpt.Replace("$" + monthCols[i] + "$6", Common.GetDecimalAmtTotStr(rsrvs.LoanLossReserveBeginning[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$7", Common.GetDecimalAmtTotStr(rsrvs.LoanLossReserveContribution[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$800", Common.GetDecimalAmtTotStr(rsrvs.LoanLossReserveUtilized[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$805", Common.GetDecimalAmtTotStr(rsrvs.EarlyPayoff[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$810", Common.GetDecimalAmtTotStr(rsrvs.EarlyPaymentDefault[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$8", Common.GetDecimalAmtTotStr(rsrvs.LoanLossReserveEndingbalance[i]));

                // Corporate Investment Account (aka Operating Deficit
                plRpt = plRpt.Replace("$" + monthCols[i] + "$11", Common.GetDecimalAmtTotStr(rsrvs.OperatingLoss[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$12", Common.GetDecimalAmtTotStr(rsrvs.PaidBackToCorporate[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$14", Common.GetDecimalAmtTotStr(rsrvs.AdvanceFromCorporatetoBranch[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$15", Common.GetDecimalAmtTotStr(rsrvs.CorpInvestAcctBal[i]));

                // Branch Reserves
                plRpt = plRpt.Replace("$" + monthCols[i] + "$18", Common.GetDecimalAmtTotStr(rsrvs.BranchReserveBeginning[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$19", Common.GetDecimalAmtTotStr(rsrvs.PaidIntoAccountfromBranch[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$21", Common.GetDecimalAmtTotStr(rsrvs.AppliedtoCoverLoss[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$22", Common.GetDecimalAmtTotStr(rsrvs.BranchReserveEnding[i]));

                // Available Funds To Manager
                plRpt = plRpt.Replace("$" + monthCols[i] + "$26", Common.GetDecimalAmtTotStr(rsrvs.AvailableFundsToMgrBeg[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$27", Common.GetDecimalAmtTotStr(rsrvs.AvailableFundsToManagerIncreases[i]));
                plRpt = plRpt.Replace("$" + monthCols[i] + "$25", Common.GetDecimalAmtTotStr(rsrvs.AppliedtoCoverLosses[i]));

                string link = GetDetailLink(rsrvs, monthCols, i);
                plRpt = plRpt.Replace("$" + monthCols[i] + "$28", link);
                plRpt = plRpt.Replace("$" + monthCols[i] + "$29", Common.GetDecimalAmtTotStr(rsrvs.AvailableFundsToMgrEnd[i]));

                i++;
            } while (i < 12);

            // order counts here because of the token wildcard
            plRpt = plRpt.Replace("$G$11", Common.GetDecimalAmtTotStr(rsrvs.OperatingLoss[0]));
            plRpt = plRpt.Replace("$G$18", Common.GetDecimalAmtTotStr(rsrvs.BranchReserveBeginning[0]));
            plRpt = plRpt.Replace("$G$1", Common.GetDecimalAmtTotStr(rsrvs.LoanLossReserveBeginning[0]));
            plRpt = plRpt.Replace("$G$26", Common.GetDecimalAmtTotStr(rsrvs.AvailableFundsToMgrBeg[0]));

            plRpt = BranchInfo.AddBranchToReserveAcctRpt(plRpt, branch);

            return plRpt;
        }

        private string GetDetailLink(Allocations.AllocationsReserves rsrvs, string[] monthCols, int i)
        {
            return string.Format("</span><a id='{0}' href='../ExpenseDetail?id={0}&branchId=" + BranchId +
                                           "&year=" + Year + "&rptType=PLRsrv" +
                                            "' target='_blank'>{1}</a><span",
                                           monthCols[i] + "28",
                                            Common.GetDecimalAmtTotStr(rsrvs.NonProducingMgrDraw[i])
                                        );
        }

        private string AddDateStamps(string reportStr)
        {
            Expenses.Year = Year;
            Revenue.Year = Year;

            // date stamp of last expense update
            DateTime dateRefreshed = Expenses.DateRefreshed;
            reportStr =reportStr.Replace("#LastUpdated", dateRefreshed.ToShortDateString());

            // date stamp of accounting close
            DateTime acctCloseDate = Expenses.AcctClosingDate;
            reportStr = reportStr.Replace("#AcctCloseDate", acctCloseDate.ToShortDateString());

            return reportStr;
        }
    }
    #endregion
}
    

