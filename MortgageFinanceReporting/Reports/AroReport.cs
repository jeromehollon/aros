﻿using System;
using System.Collections.Generic;
using BranchRepository.Interface;
using ExpenseRepository.Interface;
using RevenueRepository.Interface;

namespace Reports
{
    public class AroReport
    {
        #region Properties
        public string TemplateFilePath { get; set; }

        public int Year { get; set; }

        public string BranchId { get; set; }

        public IBranch Branch { get; set; }

        public IBranch BranchInfo { get; set; }

        public IBranchHistory BranchHistory { get; set; }

        public IBpsRankings BpsRankings { get; set; }

        public IExpenses Expenses { get; set; }

        public ExpenseRepository.Interface.IDetail ExpenseDetails { get; set; }

        public IRevenue Revenue { get; set; }

        public string ExpenseMapPath { get; set; }

        public string ExpenseTotalPath { get; set; }

        public string BonusMapPath { get; set; }

        public string RevenueIncMapPath { get; set; }
        #endregion

       
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the AroReport class.
        /// </summary>
        public AroReport(string templateFilePath, IBranch branch, IBranchHistory branchHist, IBpsRankings bpsRankings, IExpenses expenses, IRevenue revenue, ExpenseRepository.Interface.IDetail expenseDtl, int year)
        {
            // explicitly set template path
            TemplateFilePath = templateFilePath;
            Year = year;

            Branch = branch;
            BranchHistory = branchHist;
            BpsRankings = bpsRankings;
            Expenses = expenses;
            ExpenseDetails = expenseDtl;
            Revenue = revenue;
        }


        /// <summary>
        /// Initializes a new instance of the AroReport class.
        /// </summary>
        public AroReport(string templateFilePath, int year)
        {
            // explicitly set template path
            TemplateFilePath = templateFilePath;
            Year = year;

            // default composition root 
            BpsRankings = new BranchRepository.Sql.BpsRankings();
            BranchHistory = new BranchRepository.Sql.BranchHistories();

            if (Year == DateTime.Now.Year)
            {
                Branch = new BranchRepository.Sql.Branches();                         
                Revenue = new RevenueRepository.Sql.Pipeline();
            }
            else
            {
                Branch = new BranchRepository.Sql.Archive.Branches();
                Revenue = new RevenueRepository.Sql.Archive.Pipeline();
            }

            if (Year > 2014)
            {
                Expenses = new ExpenseRepository.Sql.Expenses();
                ExpenseDetails = new ExpenseRepository.Sql.Detail();
            }
            else
            {               
                Expenses = new ExpenseRepository.Sql.Archive.Expenses();
                ExpenseDetails = new ExpenseRepository.Sql.Archive.Detail();
            }
        }


        /// <summary>
        /// Initializes a new instance of the AroReport class.
        /// </summary>
        public AroReport()
        {
            // default composition root 
            BpsRankings = new BranchRepository.Sql.BpsRankings();
            BranchHistory = new BranchRepository.Sql.BranchHistories();

            // default composition root 
            if (Year == DateTime.Now.Year)
            {
                Branch = new BranchRepository.Sql.Branches();                
                Revenue = new RevenueRepository.Sql.Pipeline();
            }
            else
            {
                Branch = new BranchRepository.Sql.Archive.Branches();                
                Revenue = new RevenueRepository.Sql.Archive.Pipeline();
            }

            if (Year > 2014)
            {
                Expenses = new ExpenseRepository.Sql.Expenses();
                ExpenseDetails = new ExpenseRepository.Sql.Detail();
            }
            else
            {
                Expenses = new ExpenseRepository.Sql.Archive.Expenses();
                ExpenseDetails = new ExpenseRepository.Sql.Archive.Detail();
            }
        }
        #endregion


        #region Public
        public string GetAroReport()
        {
            string aroRpt = Common.GetRptTemplate(TemplateFilePath);
            aroRpt = aroRpt.Replace("�", " ");

            Expenses.Year = Year;
            Revenue.Year = Year;

            ExpenseDetail expenseDetail = new ExpenseDetail();
            expenseDetail.ExpenseMapPath = ExpenseMapPath;

            // date stamp of last expense update   
            DateTime dateRefreshed = Expenses.DateRefreshed;
            DateTime acctClosingDate = Expenses.AcctClosingDate;

            // branch info
            Branch.Year = Year;

            IBranch branch = Branch.GetBranchData(BranchId);
            List<IBranchHistory> branchHist = BranchHistory.GetBranchHistory(Year, BranchId);
            BranchInfo branchInfo = new BranchInfo();

            aroRpt = branchInfo.AddBranchToAroRpt(aroRpt, branch, branchHist);

            // expenses            
            List<IExpenses> summary = Expenses.GetSummaryByBranch(BranchId, Year);
            Reports.Expenses.ExpenseMapPath = ExpenseMapPath;
            Reports.Expenses.ExpenseTotalMapPath = ExpenseTotalPath;
            expenseDetail.BonusMapPath = BonusMapPath;


            aroRpt = Reports.Expenses.AddExpensesToTemplate(aroRpt, BranchId, summary, dateRefreshed, acctClosingDate, Year, SectionType.Expense);

            // revenue           
            // year support - go to archive when not current year            
            Revenue.Year = Year;

            List<IRevenue> revenue = Reports.Revenue.GetRevenueData(BranchId, Revenue);
            aroRpt = Reports.Revenue.AddRevenueToTemplate(aroRpt, BranchId, revenue, Year);

            return aroRpt;
        }


        public string GetExpenseDetailRpt(string branchId, string monthLetter, int year, string acctNum, string expenseMapPath = "")
        {
            ExpenseDetail expenseDtls;
            expenseDtls = new ExpenseDetail();

            return expenseDtls.GetExpenseDetailRpt(ExpenseDetails, branchId, monthLetter, year, acctNum, expenseMapPath);
        }


        public string GetRevenueDetailRpt(string branchId, string monthLetter, int year)
        {
            RevenueDetail revenueDetail = new RevenueDetail();
            return revenueDetail.GetRevenueDetailRpt(branchId, monthLetter, year);
        }


        public string GetBpsRankingReport()
        {
            BpsRankings branchInfo = new BpsRankings();
            return branchInfo.GetBpsRankReport(BpsRankings);
        }
        
    }
        #endregion
}
