﻿using System;
using System.Collections.Generic;
using BranchRepository.Interface;
using ExpenseRepository.Interface;
using RevenueRepository.Interface;

namespace Reports
{
    public class ProfitAndLossReport : AroReport
    {
        #region Properties
        // renvenue map is unique to the profit and loss report - other properties are inherited from the base class
        public string RevenueIncMapPath { get; set; }
        public string RevenueIncTotalMapPath { get; set; }

        #endregion


        #region Constructors
        /// <summary>
        /// Initializes a new instance of the AroReport class.
        /// </summary>
        public ProfitAndLossReport(string templateFilePath, IBranch branch, IBranchHistory branchHist, IBpsRankings bpsRankings, IExpenses expenses, IRevenue revenue, ExpenseRepository.Interface.IDetail expenseDtl, int year)
        {
            // explicitly set template path
            TemplateFilePath = templateFilePath;
            Year = year;

            Branch = branch;
            BranchHistory = branchHist;
            Expenses = expenses;
            ExpenseDetails = expenseDtl;
            Revenue = revenue;
        }


        /// <summary>
        /// Initializes a new instance of the AroReport class.
        /// </summary>
        public ProfitAndLossReport(string templateFilePath, int year)
        {
            // explicitly set template path
            TemplateFilePath = templateFilePath;
            Year = year;

            BranchHistory = new BranchRepository.Sql.BranchHistories();

            if (Year == DateTime.Now.Year)
            {
                Branch = new BranchRepository.Sql.Branches();                
                Revenue = new RevenueRepository.Sql.Pipeline();
            }
            else
            {
                Branch = new BranchRepository.Sql.Archive.Branches();
                Revenue = new RevenueRepository.Sql.Archive.Pipeline();
            }
           
            Expenses = new ExpenseRepository.Sql.Expenses();
            ExpenseDetails = new ExpenseRepository.Sql.Detail();
        }


        /// <summary>
        /// Initializes a new instance of the AroReport class.
        /// </summary>
        public ProfitAndLossReport()
        {
            BranchHistory = new BranchRepository.Sql.BranchHistories();

            if (Year == DateTime.Now.Year)
            {
                Branch = new BranchRepository.Sql.Branches();                
                Revenue = new RevenueRepository.Sql.Pipeline();
            }
            else
            {
                Branch = new BranchRepository.Sql.Archive.Branches();
                Revenue = new RevenueRepository.Sql.Archive.Pipeline();
            }
            
            Expenses = new ExpenseRepository.Sql.Expenses();
            ExpenseDetails = new ExpenseRepository.Sql.Detail();            
        }
        #endregion


        #region Public
        public string GetProfitAndLossReport()
        {
            string plRpt = Common.GetRptTemplate(TemplateFilePath);
            plRpt = plRpt.Replace("�", " ");

            Expenses.Year = Year;
            Revenue.Year = Year;

            List<IExpenses> summary = Expenses.GetSummaryByBranch(BranchId, Year);

            // revenue income uses the expense logic and data
            Reports.Expenses.ExpenseMapPath = RevenueIncMapPath;
            Reports.Expenses.ExpenseTotalMapPath = RevenueIncTotalMapPath;
   
            // date stamp of last expense update   
            DateTime dateRefreshed = Expenses.DateRefreshed;
            DateTime acctClosingDate = Expenses.AcctClosingDate;
            
            plRpt = Reports.RevenueIncome.AddExpensesToTemplate(plRpt, BranchId, summary, dateRefreshed, acctClosingDate, Year, SectionType.RevenueIncome);
         
            // branch info
            Branch.Year = Year;

            IBranch branch = Branch.GetBranchData(BranchId);

            List<IBranchHistory> branchHist = BranchHistory.GetBranchHistory(Year, BranchId);

            BranchInfo branchInfo = new BranchInfo();
            plRpt = branchInfo.AddBranchToAroRpt(plRpt, branch, branchHist);

            // expenses
            ExpenseDetail expenseDetail = new ExpenseDetail();
            expenseDetail.ExpenseMapPath = ExpenseMapPath;
            Reports.Expenses.ExpenseMapPath = ExpenseMapPath;
            Reports.Expenses.ExpenseTotalMapPath = ExpenseTotalPath;
            expenseDetail.BonusMapPath = BonusMapPath;


            plRpt = Reports.Expenses.AddExpensesToTemplate(plRpt, BranchId, summary, dateRefreshed, acctClosingDate, Year, SectionType.Expense);

            // revenue           
            // year support - go to archive when not current year            
            Revenue.Year = Year;

            List<IRevenue> revenue = Reports.Revenue.GetRevenueData(BranchId, Revenue);
            plRpt = Reports.Revenue.AddRevenueToTemplate(plRpt, BranchId, revenue, Year);


            return plRpt;
        }      
    }
        #endregion
}
