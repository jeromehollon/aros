﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseRepository.Interface;

namespace Reports
{
    class Expenses
    {
        #region Expenses

        public static int Year { get; set; }

        public static string TemplatePath { get; set; }

        public static string ExpenseMapPath { get; set; }

        public static string ExpenseTotalMapPath { get; set; }


        internal static string AddExpensesToTemplate(string aroTemplate, string branchId, List<IExpenses> summary, DateTime dateRefreshed, DateTime acctClosingDate, int year, SectionType sectionType)
        {
            Year = year;

            Dictionary<string, string> d = GetDictionaryOfExpenses(summary);

            aroTemplate = Common.ReplaceTokenInTemplate(aroTemplate, branchId, d, sectionType, year);

            Dictionary<string, int> acctRows = ExpenseAcctMap.GetAccountRows(ExpenseMapPath);

            // get map of expense total rows and accounts numbers
            string[] acctMap = ExpenseTotalMap.GetExpenseTotalMap(ExpenseTotalMapPath);

            aroTemplate = AddAllExpensesTotalsToTemplate(aroTemplate, summary, acctRows, acctMap);

            if (year == 2014)
            {
                // 2014 had some noncontiguous rows so use a dedicated function 
                aroTemplate = UpdateMktingTotals2014(aroTemplate, summary, acctRows);
            }

            aroTemplate = aroTemplate.Replace("#LastUpdated", dateRefreshed.ToShortDateString());
            aroTemplate = aroTemplate.Replace("#AcctCloseDate", acctClosingDate.ToShortDateString());

            return aroTemplate;
        }


        private static string AddAllExpensesTotalsToTemplate(string aroTemplate, List<IExpenses> summary, Dictionary<string, int> acctRows, string[] acctMap)
        {
            foreach (string item in acctMap)
            {
                string[] accounts = item.Split(',');

                int startRow = int.Parse(accounts[0]);
                int endRow = int.Parse(accounts[1]);
                string totalRow = accounts[2].Replace("\"", "").Trim();

                acctRows.Add(accounts[0].Replace("\"", ""), endRow);

                aroTemplate = UpdateArosTemplateWithRowTotals(startRow, endRow, totalRow, aroTemplate, summary, acctRows);
            }

            return aroTemplate;
        }


        private static string UpdateArosTemplateWithRowTotals(int startRow, int endRow, string totalRow, string aroTemplate, List<IExpenses> summary, Dictionary<string, int> acctRows)
        {
            // totals rows
            double[] rowTotal = GetColumnTotals(startRow, endRow, summary, acctRows);
            aroTemplate = AddColumnTotals(totalRow, rowTotal, aroTemplate);

            return aroTemplate;
        }


        private static string UpdateMktingTotals2014(string aroTemplate, List<IExpenses> summary, Dictionary<string, int> acctRows)
        {
            // this is specific to 2014 because of non-contiguous rows that were added after the template was implmented
            double[] rowTotal = GetColumnTotals(66, 78, summary, acctRows);
            double[] rowTotalNew = GetColumnTotals(131, 132, summary, acctRows);

            int i = 0;
            do
            {
                rowTotal[i] = rowTotal[i] + rowTotalNew[i];

                i += 1;
            } while (i < 12);

            aroTemplate = AddColumnTotals("79", rowTotal, aroTemplate);

            return aroTemplate;
        }


        private static Dictionary<string, string> MapExpensesToCells(List<IExpenses> expenses, Dictionary<string, int> acctRows)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();

            // get month array
            string[] monthCols = Common.GetMonthCols();

            // add token/amount to the dictionary for each month by cycling through 0-based array of columns that correspond to months
            int i = 0;
            do
            {
                UpdateExpenseCells(expenses, d, acctRows, monthCols, i);
                i += 1;

            } while (i < 12);
            return d;
        }


        private static Dictionary<string, string> GetDictionaryOfExpenses(List<IExpenses> expenses)
        {
            // map account id to row number
            Dictionary<string, int> acctRows = ExpenseAcctMap.GetAccountRows(ExpenseMapPath);

            // create a dictionary to replace tokens with data
            Dictionary<string, string> d = MapExpensesToCells(expenses, acctRows);
            return d;
        }


        private static void UpdateExpenseCells(List<IExpenses> expenses, Dictionary<string, string> d, Dictionary<string, int> acctRows, string[] monthCols, int i)
        {
            foreach (KeyValuePair<string, int> pair in acctRows)
            {
                var amtTot = expenses.Where(s => s.Month == i + 1 && s.AccountId == pair.Key).Select(s => s.TotalAmt).FirstOrDefault();

                string amtTotStr = Common.GetAmtTotStr(amtTot);

                d.Add(String.Format("${0}${1}", monthCols[i], pair.Value), amtTotStr);
            }
        }


        private static string AddColumnTotals(string targetCell, double[] acctTotal, string aroTemplate)
        {
            // get month array
            string[] monthCols = Common.GetMonthCols();

            // add token/amount to the dictionary for each month by cycling through 0-based array of columns that correspond to months
            int i = 0;
            int monthNum = 1;
            do
            {
                string acctTotStr = Common.GetAmtTotStr(acctTotal[i]);

                aroTemplate = aroTemplate.Replace(String.Format("</span>${0}${1}<span", monthCols[i], targetCell), String.Format("</span><span id='{0}'>{1}</span><span", (monthCols[i] + targetCell), acctTotStr));

                i += 1;
                monthNum += 1;
            } while (i < 12);

            return aroTemplate;
        }


        private static double[] GetColumnTotals(int startRow, int endRow, List<IExpenses> summary, Dictionary<string, int> acctRows)
        {
            double[] acctTotal = new double[12];

            // add token/amount to the dictionary for each month by cycling through 0-based array of columns that correspond to months
            int i = 0;
            int monthNum = 1;
            do
            {
                acctTotal[i] = GetAcctTotals(monthNum, startRow, endRow, acctRows, summary);

                i += 1;
                monthNum += 1;
            } while (i < 12);

            return acctTotal;
        }


        internal static double GetAcctTotals(int monthNum, int startRow, int endRow, Dictionary<string, int> acctRows, List<IExpenses> expenses)
        {
            Dictionary<string, int> rowsToSum = Common.GetRowsForTotalCalcs(startRow, endRow, acctRows);

            double amtTot = 0;

            foreach (KeyValuePair<string, int> pair in rowsToSum)
            {
                amtTot = amtTot + expenses.Where(e => e.Month == monthNum && e.AccountId == pair.Key).Select(e => e.TotalAmt).FirstOrDefault();
            }

            return amtTot;
        }
        #endregion
    }
}
