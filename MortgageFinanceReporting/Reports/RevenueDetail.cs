﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BranchRepository.Interface;
using RevenueRepository.Interface;
using RevenueRepository.Sql;

namespace Reports
{
    class RevenueDetail
    {
        public IBranch Branch { get; set; }

        public IRevenue Revenue { get; set; }

        public IDetail RevenueDtls { get; set; }


        public RevenueDetail()
        {
            // default composition root 
            Branch = new BranchRepository.Sql.Branches();

            Revenue = new Pipeline();

            RevenueDtls = new Detail();
        }


        public RevenueDetail(IBranch branch, IRevenue revenue, IDetail revenueDtls)
        {
             // default composition root 
            Branch = branch;

            Revenue = revenue;

            RevenueDtls = revenueDtls;
        }

        List<IDetail> GetRevenueDetails(string branchId, string monthLetter, int year)
        {
            // month
            string[] monthCols = Common.GetMonthCols();
            int monthNum = Array.IndexOf(monthCols, monthLetter) + 1;

            List<IDetail> details = RevenueDtls.GetDetails(branchId, monthNum, year);

            return details;
        }

        public string GetRevenueDetailRpt(string branchId, string monthLetter, int year)
        {
            List<IDetail> details = GetRevenueDetails(branchId, monthLetter, year);

            string detailRpt = "";
            StringBuilder sb = new StringBuilder();

            // branch info

            // concrete implementation set in constructor
            IBranch branch = Branch.GetBranchData(branchId);

            sb.Append("<h2>" + branch.BranchName + "</h2>");
            
            sb.Append("<table class='table table-striped'>");            
            sb.Append("<th>Loan Num</th>");
            sb.Append("<th>Type</th>");
            sb.Append("<th>Last Name</th>");
            sb.Append("<th class='amtCell'>Loan Amt</th>");
            sb.Append("<th class='amtCell'>Funded Date</th>");
            sb.Append("<th>Loan Officer</th>");

            decimal totalAmount = 0;
            int totalUnits = 0;

            foreach (var item in details)
            {
                sb.Append("<tr>");
                sb.Append("<td>" + item.LoanNumber + "</td>");
                sb.Append("<td>" + item.LoanType + "</td>");
                sb.Append("<td>" + item.BorrowerLastName + "</td>");
                sb.Append("<td class='amtCell'>" + String.Format("{0:c}", item.TotalLoanAmount) + "</td>");
                sb.Append("<td class='amtCell'>" + item.FundedDate.ToShortDateString() + "</td>");
                sb.Append("<td>" + item.AssignedLoanOfficerName + "</td>");
                sb.Append("</tr>");

                totalAmount += item.TotalLoanAmount;
                totalUnits += 1;
            }

            // total amount
            sb.Append("<tr>");
            sb.Append("<td class='rowLabel'>Total</td>");
            sb.Append("<td class='total'>" + totalUnits + "</td>");
            sb.Append("<td colspan='5' class='total'>" + String.Format("{0:c}", totalAmount) + "</td>");
            sb.Append("</tr>");

            sb.Append("</table>");

            detailRpt = sb.ToString();

            return detailRpt;
        }
    }
}
