﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BranchRepository.Interface;

namespace Reports
{
    class BpsRankings
    {
        enum VolType
        {
            Small,
            Med,
            Large
        }

        #region BpsRanking        
        internal string GetBpsRankReport(IBpsRankings bpsRanking)
        {
            // the concrete implementation is set in constructor or defaults
            List<IBpsRankings> bpsRankings = bpsRanking.GetBpsRankings();

            return GetBpsRankReportString(bpsRankings);     
        }
        
        
        internal string GetBpsRankReport(IBpsRankings bpsRanking, int regionId)
        {
            // the concrete implementation is set in constructor or defaults
            List<IBpsRankings> bpsRankings = bpsRanking.GetBpsRankings(regionId);

            return GetBpsRankReportString(bpsRankings);     
        }


        private static string GetBpsRankReportString(List<IBpsRankings> bpsRankings)
        {
            StringBuilder sb = new StringBuilder();
            string rptDate = GetRptDate(bpsRankings);
           
            sb.Append("<h2>BPS Rankings</h2 >");
            sb.Append("As of " + rptDate + "<br/>");
            sb.Append("(Updated nightly. Rankings are for last month when today is the 20th or later - otherwise, the month before last)");
                       
            sb.Append("<div class='row'>");
            sb.Append("<div class='col-md-4 bpsTable'>");
            sb.Append("0 to 2.5 mil monthly volume");
            sb.Append(GetBpsRankReportByVolString(bpsRankings, VolType.Small));
            sb.Append("</div>");
            sb.Append("<div class='col-md-4 bpsTable'>");
            sb.Append("> 2.5 mil to	5.0 mil monthly volume");
            sb.Append(GetBpsRankReportByVolString(bpsRankings, VolType.Med));
            sb.Append("</div>");
            sb.Append("<div class='col-md-4 bpsTable'>");
            sb.Append("5.0+ mil monthly volume");
            sb.Append(GetBpsRankReportByVolString(bpsRankings, VolType.Large));
            sb.Append("</div>");
            sb.Append("</div>");

            return sb.ToString();
        }


        private static string GetBpsRankReportByVolString(List<IBpsRankings> bpsRankings, VolType volType)
        {
            StringBuilder sb = new StringBuilder();

            string branchNm = "";

            sb.Append("<table class='table table-striped table-condensed table-hover' style='outline: 1px solid black;'><thead style='bgcolor: gray; font-weight: bold; outline: 1px solid black;'>");
            sb.Append("<tr><td>Branch</td><td style='border-right: 1px solid black; text-align: right'>Volume</td>");
            sb.Append("<td style='border-left: 1px solid black;'>Loan Cost<br />(BPS)</td>");
            sb.Append("</tr></thead>");

            int cellCount = 0;
            double minVolume = 0;
            double maxVolume = 0;

            if (volType == VolType.Small)
            {
                minVolume = 0;
                maxVolume = 2500000;
            }
            else if (volType == VolType.Med)
            {
                minVolume = 2500001;
                maxVolume = 5000000;
            }
            else if (volType == VolType.Large)
            {
                minVolume = 5000001;
                maxVolume = 99000000;
            }

            int recordCnt = 0;
            string rowClass = "";

            foreach (var item in bpsRankings)
            {
                if (branchNm != item.BranchId && item.TotalAmt >= minVolume && item.TotalAmt <= maxVolume)
                {
                    // branch rank, name, and amount
                    cellCount = 1;
                    recordCnt = recordCnt + 1;

                    decimal avgCostPerLoan = getAvgCostPeLoanStr(item);

                    sb.Append("<tr>");

                    if (recordCnt == 1)
                    {
                        rowClass = "bpsFirstPlace";
                    }
                    else if (recordCnt <= 3)
                    {
                        rowClass = "bps2nd3rd";
                    }
                    else
                    {
                        rowClass = "";
                    }

                    sb.Append(String.Format("<td class='" + rowClass + "'>{0}</td>", item.Branch));
                    sb.Append(String.Format("<td class='" + rowClass + "' style='text-align: right'>{0}</td>", String.Format("{0:c}", item.TotalAmt)));
                    sb.Append(String.Format("<td class='" + rowClass + "' style='text-align: right'; border-right: 1px solid black;'>{0}</td>", String.Format("{0:0}", item.TotalBps * 10000)));
                }
                else
                {
                    cellCount += 1;
                }

                branchNm = item.BranchId;
            }

            sb.Append("</table>");

            return sb.ToString();
        }


        private static decimal getAvgCostPeLoanStr(IBpsRankings item)
        {
            decimal avgCostPerLoan = 0;
            if (item.YrAmt > 0 && item.YrTotalVol > 0)
            {
                avgCostPerLoan = (item.YrAmt / item.YrTotalVol);
            }

            return avgCostPerLoan;
        }
        #endregion


        #region Private
        private static string GetRptDate(List<IBpsRankings> bpsRanking)
        {
            // get the date (month & year) of the bps ranking that is recorded in the table
            // the usp that populates this table contains the rules for establishing dates:
            //  - go back the month before last when current date is before or on the 20th
            //  - otherwise go back one month
            var firstBpsRank = bpsRanking.Select(b => new { b.Mnth, b.Yr }).FirstOrDefault();
            string rptDateFirst = string.Format("{0}/1/{1}", firstBpsRank.Mnth, firstBpsRank.Yr);

            DateTime rptDate = DateTime.Parse(rptDateFirst).AddMonths(1).AddDays(-1);
            string rptDateStr = rptDate.ToShortDateString();

            return rptDateStr;
        }
        #endregion
    }
}
