﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BranchRepository.Interface;
using ExpenseRepository.Interface;

namespace Reports
{
    class ExpenseDetail
    {
        public IBranch Branch { get; set; }

        public IExpenses Expenses { get; set; }

        public string ExpenseMapPath { get; set; }

        public string BonusMapPath { get; set; }

        public ExpenseDetail()
        {
             // default composition root 
            Branch = new BranchRepository.Sql.Branches();

            Expenses = new ExpenseRepository.Sql.Expenses();
        }


        public ExpenseDetail(IBranch branch, IExpenses expenses)
        {
             // default composition root 
            Branch = branch;

            Expenses = expenses;
        }


        List<IDetail> GetExpenseDetails(IDetail expenseDtl, string branchId, string monthLetter, int year, string acctId, string expenseMapPath)
        {
            // month
            string[] monthCols = Common.GetMonthCols();
            int monthNum = Array.IndexOf(monthCols, monthLetter) + 1;

            // lookup account number based on id from link           
            Dictionary<string, int> acctRows = ExpenseAcctMap.GetAccountRows(expenseMapPath);
            string acctNum = (from a in acctRows
                             where a.Value == int.Parse(acctId)
                             select a.Key).FirstOrDefault();

            // concrete implementation is set in report constructor or default           
            List<IDetail> details = expenseDtl.GetDetails(branchId, monthNum, year, acctNum);

            return details;
        }

        public string GetExpenseDetailRpt(IDetail expenseDtl, string branchId, string monthLetter, int year, string acctId, string expenseMapPath = "")
        {
            ExpenseDetail expenseDetail = new ExpenseDetail();
           
                if (expenseMapPath.Length > 0)
                {
                    expenseDetail.ExpenseMapPath = expenseMapPath;
                }
                else
                {
                    expenseMapPath = expenseDetail.ExpenseMapPath;
                }
           
            List<IDetail> details = GetExpenseDetails(expenseDtl, branchId, monthLetter, year, acctId, expenseMapPath);

            string detailRpt = "";
            StringBuilder sb = new StringBuilder();

            // branch info
            // set concrete implementation in constructor or simply pass the branch name as a parameter
           
            IBranch branch = Branch.GetBranchData(branchId);

            sb.Append(String.Format("<h2>{0}</h2>", branch.BranchName));

            sb.Append("<table class='table table-striped'>");
            sb.Append("<th>Account Id</th>");
            sb.Append("<th>Account Name</th>");
            sb.Append("<th>Transaction Date</th>");
            sb.Append("<th>Payee</th>");
            sb.Append("<th>Memo</th>");
            sb.Append("<th class='detailHeader amtCell'>Amount</th>");

            double totalAmount = 0;

            foreach (var item in details)
            {
                sb.Append("<tr>");
                sb.Append(String.Format("<td>{0}</td>", item.AccountId));
                sb.Append(String.Format("<td>{0}</td>", item.AccountName));
                sb.Append(String.Format("<td class='amtCell'>{0}</td>", item.TransactionDate.ToShortDateString()));
                sb.Append(String.Format("<td>{0}</td>", item.Payee));
                sb.Append(String.Format("<td>{0}</td>", item.Memo));
                sb.Append(String.Format("<td class='amtCell'>{0}</td>", String.Format("{0:c}", item.Amount)));
                sb.Append("</tr>");

                totalAmount += item.Amount;
            }

            // total amount
            sb.Append("<tr>");
            sb.Append("<td colspan='5' class='rowLabel'>Total</td>");
            sb.Append(String.Format("<td class='total'>{0}</td>", String.Format("{0:c}", totalAmount)));
            sb.Append("</tr>");

            sb.Append("</table>");

            detailRpt = sb.ToString();

            return detailRpt;
        }
    }
}
