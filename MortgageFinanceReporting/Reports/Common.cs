﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Reports
{
    static class Common
    {
        #region Common

        internal static string[] GetMonthCols()
        {
            // month array of column letters
            string[] monthCols = new string[] { "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S" };
            return monthCols;
        }


        internal static string GetAmtTotStr(double amtTot, int decimalPlaces = 2)
        {
            string amtTotStr;

            if (amtTot.ToString() != "")
                if (decimalPlaces == 0)
                {
                    // zero places for integer
                    amtTotStr = amtTot.ToString("N0", new CultureInfo("en-US"));
                }
                else
                {
                    // two places for money
                    amtTotStr = amtTot.ToString("N", new CultureInfo("en-US"));
                }
            else
                amtTotStr = "-";

            if (amtTot == 0)
            {
                amtTotStr = "-";
            }

            if (amtTotStr == "0.00")
            {
                amtTotStr = "-";
            }

            if (amtTot < 0)
            {
                amtTotStr = String.Format("<span class='negative'>{0}</span>", amtTotStr);
            }

            return amtTotStr;
        }


        internal static string GetDecimalAmtTotStr(decimal amtTot, int decimalPlaces = 2)
        {
            string amtTotStr;

            if (amtTot.ToString() != "")
                if (decimalPlaces == 0)
                {
                    // zero places for integer
                    amtTotStr = amtTot.ToString("N0", new CultureInfo("en-US"));
                }
                else
                {
                    // two places for money
                    amtTotStr = amtTot.ToString("N", new CultureInfo("en-US"));
                }
            else
                amtTotStr = "-";

            if (amtTot == 0)
            {
                amtTotStr = "-";
            }

            if (amtTotStr == "0.00")
            {
                amtTotStr = "-";
            }

            if (amtTot < 0)
            {
                amtTotStr = String.Format("<span class='negative'>{0}</span>", amtTotStr);
            }

            return amtTotStr;
        }
        internal static string ReplaceTokenInTemplate(string aroTemplate, string branchId, Dictionary<string, string> d, SectionType sectionType, int year)
        {
            // replace tokens without id spans
            foreach (var pair in d)
            {
                if (pair.Value == "-")
                {
                    aroTemplate = aroTemplate.Replace(String.Format("</span>{0}<span", pair.Key), String.Format("</span>{0}<span", pair.Value));
                }
                else
                {
                    if (sectionType == SectionType.Expense)
                    {
                        aroTemplate = aroTemplate.Replace(
                            String.Format("</span>{0}<span", pair.Key),
                            String.Format("</span><a id='{0}' href='../ExpenseDetail?id={0}&branchId=" + branchId +
                               "&year=" + year +
                                "' target='_blank'>{1}</a><span",
                                pair.Key.Replace("$", ""),
                                pair.Value
                            )
                        );
                    }
                    else if (sectionType == SectionType.RevenueIncome)
                    {
                        aroTemplate = aroTemplate.Replace(
                            String.Format("</span>{0}<span", pair.Key),
                            String.Format("</span><a id='{0}' href='../ExpenseDetail?id={0}&branchId=" + branchId +
                               "&year=" + year +
                                "&rptType=PL' target='_blank'>{1}</a><span",
                                pair.Key.Replace("$", ""),
                                pair.Value
                            )
                        );
                    }
                    else if (sectionType == SectionType.Revenue)
                    {
                        aroTemplate = aroTemplate.Replace(
                            String.Format("</span>{0}<span", pair.Key),
                            String.Format("</span><a id='{0}' href='../RevenueDetail?id={0}&branchId=" + branchId +
                            "&year=" + year +
                            "' target='_blank'>{1}</a><span", pair.Key.Replace("$", ""), pair.Value));
                    }
                }
            }

            return aroTemplate;
        }


        internal static string GetRptTemplate(string filePath)
        {
            string myContent = "";

            using (StreamReader sr = new StreamReader(filePath))
            {
                myContent = sr.ReadToEnd();
            }
            myContent = myContent.Replace("�", " ");

            return myContent;
        }


        internal static Dictionary<string, int> GetRowsForTotalCalcs(int startRow, int endRow, Dictionary<string, int> allRows)
        {
            // map account id to row number
            Dictionary<string, int> acctRows = new Dictionary<string, int>();

            string accountId = "";
            int i = startRow;
            do
            {
                accountId = allRows.Where(a => a.Value == i).Select(a => a.Key).FirstOrDefault();

                if (accountId != null)
                {
                    acctRows.Add(accountId, i);
                }
                i++;
            } while (i <= endRow);


            return acctRows;
        }

        #endregion
    }
}
