﻿using System;
using System.Collections.Generic;
using System.Linq;
using RevenueRepository.Interface;

namespace Reports
{
    class Revenue
    {
        #region Revenue

        internal static string AddRevenueToTemplate(string aroTemplate, string branchId, List<IRevenue> revenue, int year)
        {
            Dictionary<string, string> d = GetDictionaryOfRevenue(revenue);

            aroTemplate = Common.ReplaceTokenInTemplate(aroTemplate, branchId, d, SectionType.Revenue, year);

            return aroTemplate;
        }

        private static Dictionary<string, string> MapRevenueToCells(List<IRevenue> revenue)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();

            // get month array
            string[] monthCols = Common.GetMonthCols();

            // add token/amount to the dictionary for each month by cycling through 0-based array of columns that correspond to months
            int i = 0;
            do
            {
                UpdateRevenueCells(revenue, d, monthCols, i);
                i += 1;

            } while (i < 12);
            return d;
        }


        private static void UpdateRevenueCells(List<IRevenue> revenue, Dictionary<string, string> d, string[] monthCols, int i)
        {
            // Total Closed Units
            int totalLoans = GetTotalLoans(revenue, d, monthCols, i);

            // Total Closed Volume
            double amtTot = GetClosedVolume(revenue, d, monthCols, i);

            // Average Loan Amount
            AverageLoanAmount(d, monthCols, i, totalLoans, amtTot);
        }


        private static int GetTotalLoans(List<IRevenue> revenue, Dictionary<string, string> d, string[] monthCols, int i)
        {
            // Total Closed Units
            var totalLoans = revenue.Where(s => s.Month == i + 1).Select(s => s.TotalLoans).FirstOrDefault();
            d.Add(String.Format("${0}${1}", monthCols[i], 4), Common.GetAmtTotStr(double.Parse(totalLoans.ToString()), 0));
            return totalLoans;
        }


        private static double GetClosedVolume(List<IRevenue> revenue, Dictionary<string, string> d, string[] monthCols, int i)
        {
            // Total Closed Volume
            var amtTot = revenue.Where(s => s.Month == i + 1).Select(s => s.TotalAmt).FirstOrDefault();
            string amtTotStr = Common.GetAmtTotStr(amtTot, 0);
            d.Add(String.Format("${0}${1}", monthCols[i], 5), amtTotStr);
            return amtTot;
        }


        private static void AverageLoanAmount(Dictionary<string, string> d, string[] monthCols, int i, int totalLoans, double amtTot)
        {
            // Average Loan Amount
            double avgLoanAmt;
            if (amtTot > 0 && totalLoans > 0)
                avgLoanAmt = amtTot / totalLoans;
            else
                avgLoanAmt = 0;

            //Average Loan Amount
            string avgLoanAmtStr = Common.GetAmtTotStr(avgLoanAmt);
            d.Add(String.Format("${0}${1}", monthCols[i], 3), avgLoanAmtStr);
        }


        private static Dictionary<string, string> GetDictionaryOfRevenue(List<IRevenue> revenue)
        {
            // create a dictionary to replace tokens with data
            Dictionary<string, string> d = MapRevenueToCells(revenue);
            return d;
        }


        public static List<IRevenue> GetRevenueData(string branchId, IRevenue revenue)
        {
            List<IRevenue> summaries = revenue.GetSummary(branchId);

            return summaries;
        }

        #endregion
    }
}
