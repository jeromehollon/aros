﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Reports
{

	// get an array of cell tokens and account numbers
	static class ExpenseAcctMap
	{
			public static Dictionary<string, int> GetAccountRows(string fileMapPath)
		{
			// map account id to row number
			Dictionary<string, int> acctRows = new Dictionary<string, int>();

			// get map of expense rows and accounts numbers
			string[] acctMap = GetAcctMap(fileMapPath);

			foreach (string item in acctMap)
			{
				string[] accounts = item.Split(','); 
				acctRows.Add(accounts[0].Replace("\"", ""), int.Parse(accounts[1]));
			}
		   
			return acctRows;
		}


			public static string[] GetAccounts(string fileMapPath)
		{			
			// get map of expense rows
			string[] accounts = GetAcctMap(fileMapPath);
		   
			return accounts;
		}


		private static string[] GetAcctMap(string mapPath)
		{
			string[] acctMap = File.ReadAllLines(mapPath);

			return acctMap;
		}
	}
}
