﻿using BranchRepository.Interface;
using ExpenseRepository.Interface;
using RevenueRepository.Interface;
using System.Collections.Generic;
using System.Linq;
using System;
using Reports.Properties;

namespace Reports
{
	public class Allocations
	{
		#region properties
		public string TemplateFilePath { get; set; }

		public int Year { get; set; }

		public string BranchId { get; set; }

		public IBranch Branch { get; set; }

		public IBranchHistory BranchHistory { get; set; }

		public IExpenses Expenses { get; set; }

		public IRevenue Revenue { get; set; }

		public  string CogsMapPath { get; set; }

		public  string ExpenseAcctsPath { get; set; }
		public  string BonusPaidAcct { get; set; }
		public  string BonusVolAcct { get; set; }
		public string LoanLossRsrvUtilized { get; set; }
		public  string RevenueIncMapPath { get; set; }

		public  string RptType { get; set; }
		#endregion

		#region GlobalFlds
		private  decimal[] _loanLossBalBeg = new decimal[12];
		private  decimal[] _loanLossBalEnd = new decimal[12]; 
		private  decimal[] _loanLossRsrvIncPrev = new decimal[12]; 
		private  decimal[] _paidBackToCorpPrev = new decimal[12]; 
		private  decimal[] _branchRsvrBalBeg = new decimal[12]; 
		private  decimal[] _branchRsvrBalEnd = new decimal[12]; 
		private  decimal[] _corpInvestAcctBalBeg = new decimal[12]; 
		private  decimal[] _corpInvestAcctBalEnd = new decimal[12]; 
		private  decimal[] _paidIntoAcctFromBranchPrev = new decimal[12]; 
		private  decimal[] _borrowedFromRsrvAcctsPrev = new decimal[12]; 
		private  decimal[] _appliedtoCoverLoss = new decimal[12]; 
		private  decimal[] _appliedtoCoverLosses = new decimal[12]; 
		private  decimal[] _netIncometoReserveAcct = new decimal[12];
		private   decimal[] _availableFundsToManagerIncreases = new decimal[12];
		private  decimal[] _availableFundsToMrgPrev = new decimal[12];
		private  decimal[] _availableFundsToMrgEnd = new decimal[12];
		private  decimal[] _arOverridePaid = new decimal[12];
		
		#endregion

		#region constructors
		public Allocations(string templateFilePath, IBranch branch, IBranchHistory branchHist, IExpenses expenses, IRevenue revenue, string cogsMapPath)
		{
			// composition root constructor injection
			TemplateFilePath = templateFilePath;

			Branch = branch;
			BranchHistory = branchHist;
			Expenses = expenses;
			Revenue = revenue;
			CogsMapPath = cogsMapPath;
		}

		
		public Allocations(string templateFilePath, string cogsMapPath, int year)
		{
			// default to value in configuration file when not explicitly set
			TemplateFilePath = templateFilePath;
			CogsMapPath = cogsMapPath;
			Year = year;
			BranchHistory = new BranchRepository.Sql.BranchHistories();

			// default composition root    
			if (Year == DateTime.Now.Year)
			{
				Branch = new BranchRepository.Sql.Branches();
				Revenue = new RevenueRepository.Sql.Pipeline();
			}
			else
			{
				Branch = new BranchRepository.Sql.Archive.Branches();
				Revenue = new RevenueRepository.Sql.Archive.Pipeline();
			}

			if (Year > 2014)
			{
				Expenses = new ExpenseRepository.Sql.Expenses();
			}
			else
			{
				Expenses = new ExpenseRepository.Sql.Archive.Expenses();
			}
		}

		public Allocations()
		{
			// assume latest year when called with this constructor

			// default to value in configuration file when not explicitly set
			TemplateFilePath = Settings.Default.ReserveAcctTemplateFilePath;

			CogsMapPath = Settings.Default.ReserveAcctTemplateFilePath;

			// default composition root 
			Branch = new BranchRepository.Sql.Branches();

			BranchHistory = new BranchRepository.Sql.BranchHistories();

			Expenses = new ExpenseRepository.Sql.Expenses();

			Revenue = new RevenueRepository.Sql.Pipeline();
		}
		#endregion


		#region public

		public  AllocationsReserves Main(IBranch branch, int year, List<IRevenue> revenue, List<IExpenses> expenses, List<IBranchHistory> branchHist = null)
		{
			AllocationsReserves rsrvs = new AllocationsReserves();
			decimal corpInvestAcctPrevBal;
			decimal loanLossReserveBal;


			int i = 0;
			do
			{
				// set the previous balances to the admin val in the db when i = 0
				if (i == 0)
				{
					if (branch.BegLoanLossReserveBal != null) _loanLossBalBeg[i] = (decimal) branch.BegLoanLossReserveBal;
					if (branch.BegBranchReserveBal != null) _branchRsvrBalBeg[i] = (decimal) branch.BegBranchReserveBal;
					_corpInvestAcctBalBeg[i] = branch.CorpInvestBalDueCap; 

					if (branch.BegAroAccountBal != null)
					{
						_availableFundsToMrgPrev[i] = (decimal) branch.BegAroAccountBal;
					}

					corpInvestAcctPrevBal = _corpInvestAcctBalEnd[i];
					loanLossReserveBal = _loanLossBalEnd[i];
				}
				else
				{
					_loanLossBalBeg[i] = _loanLossBalEnd[i - 1];
					_corpInvestAcctBalBeg[i] = _corpInvestAcctBalEnd[i - 1];
					_branchRsvrBalBeg[i] = _branchRsvrBalEnd[i - 1];
					_availableFundsToMrgPrev[i] = _availableFundsToMrgEnd[i-1];

				   corpInvestAcctPrevBal = _corpInvestAcctBalEnd[i - 1];
					loanLossReserveBal = _loanLossBalEnd[i-1];
					
				  
				}

				// allocations
				rsrvs.BranchLoanLossReserveIncrease[i] = LoanLossReserveIncrease(i, loanLossReserveBal, branch, 2016, revenue, expenses);
				rsrvs.PaidBackToCorporate[i] = PaidBackToCorp(i, rsrvs.BranchLoanLossReserveIncrease[i], corpInvestAcctPrevBal, branch, year, revenue, expenses);
				rsrvs.PaidIntoAccountfromBranch[i] = _paidIntoAcctFromBranchPrev[i];
				rsrvs.BorrowedFromReservAccounts[i] = _borrowedFromRsrvAcctsPrev[i];
				rsrvs.TotalRsrvAccts[i] = rsrvs.BranchLoanLossReserveIncrease[i]
										  + rsrvs.PaidBackToCorporate[i]
										  + rsrvs.PaidIntoAccountfromBranch[i]
										  + rsrvs.BorrowedFromReservAccounts[i];

				decimal expenseSubTotal = GetExpenseSubTotal(i, expenses);
				rsrvs.TotalExpensesAndAllocationsToCapital[i] = GetTotalExpensesAndAllocationsToCapital(expenseSubTotal,
					rsrvs.TotalRsrvAccts[i]);

				// Net Income to Reserve Account is Operation Income - TotalExpensesAndAllocationsToCapital
				rsrvs.NetIncomeToReserveAccount[i] = GetNetIncomeToReserveAccount(i, expenses, revenue, branch, rsrvs.TotalExpensesAndAllocationsToCapital[i]);

				// available funds to manager increases is the same as the net income to reserves in the allocations table
				_availableFundsToManagerIncreases[i] = _netIncometoReserveAcct[i];
				rsrvs.AvailableFundsToManagerIncreases[i] = _availableFundsToManagerIncreases[i];

				rsrvs.AppliedtoCoverLosses[i] = _appliedtoCoverLosses[i];

				// loan loss reserves - allocations are set above
				rsrvs.CorpInvestAcctBal[i] = _corpInvestAcctBalEnd[i];
				rsrvs.LoanLossReserveBeginning[i] = _loanLossBalBeg[i];
				rsrvs.LoanLossReserveContribution[i] = rsrvs.BranchLoanLossReserveIncrease[i];
				rsrvs.LoanLossReserveUtilized[i] = GetLoanLossRsrvPaymentsAcct(i, "2706", expenses);
				rsrvs.EarlyPayoff[i] = GetLoanLossRsrvPaymentsAcct(i, "2707", expenses);
				rsrvs.EarlyPaymentDefault[i] = GetLoanLossRsrvPaymentsAcct(i, "2708", expenses);
				rsrvs.LoanLossReserveEndingbalance[i] = _loanLossBalEnd[i];

				// corp investment acct - allocations are set above
				rsrvs.OperatingLoss[i] = _corpInvestAcctBalBeg[i];
				rsrvs.AdvanceFromCorporatetoBranch[i] = GetAdvanceFromCorpToBranch(i, _borrowedFromRsrvAcctsPrev[i], _branchRsvrBalBeg[i]);
				rsrvs.CorpInvestAcctBal[i] = _corpInvestAcctBalEnd[i];

				// branch reserve account - allocations are set above
				rsrvs.BranchReserveBeginning[i] = _branchRsvrBalBeg[i];
				rsrvs.AppliedtoCoverLoss[i] = _appliedtoCoverLoss[i];
				rsrvs.BranchReserveEnding[i] = _branchRsvrBalEnd[i];

				// available funds to managers
				rsrvs.AvailableFundsToMgrEnd[i] = GetAvailableFundsToMrg(i, expenses);
				rsrvs.AvailableFundsToMgrBeg[i] = _availableFundsToMrgPrev[i];

				// note: 6025 removed from PLRevneueBonusAcntMap.txt - it was originally included for the expense detail drill-down
				rsrvs.ArOverridePaid[i] = _arOverridePaid[i]; // "Non Producing Managers draw" in P & L
				// this is the P&L version - same place but different label
				rsrvs.NonProducingMgrDraw[i] = _arOverridePaid[i];

				i++;
			} while (i < 12);

			return rsrvs;
		}

		private  decimal GetNetIncomeToReserveAccount(int i, List<IExpenses> expenses, List<IRevenue> revenue, IBranch branch, decimal totalExpensesAndAllocationsToCapital)
		{
            decimal operatingIncomeTotal = GetOperatingIncomeTotal(i, expenses, revenue, branch);
            _netIncometoReserveAcct[i] = operatingIncomeTotal - totalExpensesAndAllocationsToCapital;

			return _netIncometoReserveAcct[i];
		}


		public  decimal LoanLossReserveIncrease(int i, decimal loanLossPrevBal, IBranch branch, int year, List<IRevenue> revenue, List<IExpenses> expenses)
		{
			decimal[] loanLossRsrvIncs = new decimal[12];
			
			var netIncomeFromOps = GetNetIncomeFromOps(i, expenses, revenue, branch);

			decimal loanLossRsrvBegBal = GetLoanLossRsrvBegBalance(branch, i, loanLossPrevBal);
		   
			// ---------------------------------------------------------------

			// Branch LOAN LOSS RESERVE Rate
			decimal loanLossRsrvCostPerBps = branch.LoanLossReserveRate * (decimal).01;

			// Branch LOAN LOSS RESERVE cap
			decimal loanLossRsrvCap = branch.LoanLossReserveCap;

			decimal totCloseVal = (decimal)(from e in revenue where e.Month == i + 1 select e.TotalAmt).Sum();
			// -------------------
			
			// ------------------------------------------------------
			// NOTE: (the first section pays back negative balance, the second adds the monthly required amount)
			decimal loanLossRsrvInc1 = GetLoanLossRsrvInc1(loanLossRsrvBegBal, netIncomeFromOps, totCloseVal, loanLossRsrvCostPerBps);

			decimal loanLossRsrvInc2 = GetLoanLossRsrvInc2(loanLossRsrvBegBal, loanLossRsrvCap, totCloseVal, loanLossRsrvCostPerBps);

			decimal loanLossRsrvInc = loanLossRsrvInc1 + loanLossRsrvInc2;
			loanLossRsrvIncs[i] = loanLossRsrvInc;

			//Loan Loss Rsrv Ending Bal:
			//    Loan Loss Reserve Balance (Prev Loan Loss Rsrv Bal)
			//    + Loan Loss Reserve Contribution(loanLossRsrvInc)
			//    + 2706 - Loan Loss Utilized
			//    + 2707 - Early Payoff
			//    + 2708 - Early Payment Default

			// Loan Loss Reserve Ending balance (previous month) - save this for the next month

			// Loan Loss Reserve Utilized
			List<AroBonusesPaid> lLoanLossRsrvUtilized = GetLoanLossRsrvPayments(expenses);
			decimal loanLossRsrvUtilized = (decimal) lLoanLossRsrvUtilized.Where(e => e.Month == i + 1).Sum(e => e.TotalAmt);

			_loanLossBalEnd[i] = loanLossRsrvBegBal + loanLossRsrvInc + loanLossRsrvUtilized;
			
			_loanLossRsrvIncPrev[i] = loanLossRsrvInc;
			
			return loanLossRsrvInc;
		}

		public  decimal PaidBackToCorp(int i, decimal loanLossReserveIncrease, decimal corpInvestAcctBalPrev, IBranch branch, int year, List<IRevenue> revenue, List<IExpenses> expenses)
		{
			decimal paidBackToCorp;
			decimal[] operatingLoss = new decimal[12]; 
			// 
			var netIncomeFromOps = GetNetIncomeFromOps(i, expenses, revenue, branch);

			if (i > 0)
			{
				operatingLoss[i] = corpInvestAcctBalPrev;
			}
			else
			{
				operatingLoss[i] = 0;
			}

			 
			if ((netIncomeFromOps - loanLossReserveIncrease) < 1)
			{
				paidBackToCorp = 0;
			}
			else if (operatingLoss[i] > 0)
			{
				if (netIncomeFromOps - loanLossReserveIncrease > 0)
				{
					if (netIncomeFromOps - loanLossReserveIncrease < operatingLoss[i])
					{
						paidBackToCorp = netIncomeFromOps - loanLossReserveIncrease;
					}
					else 
					{
						paidBackToCorp = operatingLoss[i];
					}
				}
				else
				{
					paidBackToCorp = 0;
				}
			}
			else
			{
				paidBackToCorp = operatingLoss[i];
			}

			_paidBackToCorpPrev[i] = paidBackToCorp;
			_paidIntoAcctFromBranchPrev[i] = GetPaidIntoAcctFromBranch(i, branch, revenue, expenses, paidBackToCorp);
			_corpInvestAcctBalEnd[i] = GetCorpInvestAcctBalEnd(i, branch, revenue, expenses);
			

			return paidBackToCorp;
		}

		#endregion

		#region private
		private  decimal GetNetIncomeFromOps(int i, List<IExpenses> expenses, List<IRevenue> revenue, IBranch branch)
		{
			// get the operating income so that can be removed from the expenses
			var operatingIncomes = GetOperatingIncome(i, expenses, revenue);
			var operatingIncome = operatingIncomes.ToList().Where(e => e.Month == (i + 1)).Select(e => new { e.TotalCogsAmt, e.RevenueAmt}).FirstOrDefault();
			decimal netIncomeFromOps = 0;

			decimal subTotExpense = GetExpenseSubTotal(i, expenses);
		
			if (operatingIncome != null)
			{
				if (RptType == "PL")
				{
					// revenue - COGS - sub total of expenses
					netIncomeFromOps = ((decimal)operatingIncome.RevenueAmt - (decimal)operatingIncome.TotalCogsAmt) - subTotExpense;
				}
				else // ARO
				{
					List<AroBonusesPaid> lLoanLossRsrvUtilized = GetLoanLossRsrvPayments(expenses);
					decimal loanLossRsrvUtilized = (decimal)lLoanLossRsrvUtilized.Where(e => e.Month == i + 1).Sum(e => e.TotalAmt);

                    // ARO reserve reports uses Assigned Revenue - its called 'ARO PRIOR TO ALLOCATIONS'
                    // 'ARO PRIOR TO ALLOCATIONS' = Operating Income - Sub Total Expenses

				    decimal aRRate = GetARRate(i);

                    // Assigned Revenue = Total Closed Volume * Assigned Revenue Rate
                    decimal assignedRevenue = (decimal) operatingIncome.RevenueAmt*(aRRate * (decimal) .01);

					// Operating Income = Assigned Revenue - COGS
					decimal operatingIncTotal = assignedRevenue - (decimal) operatingIncome.TotalCogsAmt;

					decimal aroPriorToAllocations = operatingIncTotal - subTotExpense;

					netIncomeFromOps = aroPriorToAllocations;
				}
			}
			return netIncomeFromOps;
		}


		private  decimal GetOperatingIncomeTotal(int i, List<IExpenses> expenses, List<IRevenue> revenue, IBranch branch)
		{
			// get the operating income so that can be removed from the expenses
			var operatingIncomes = GetOperatingIncome(i, expenses, revenue);
			var operatingIncome = operatingIncomes.ToList().Where(e => e.Month == (i + 1)).Select(e => new { e.TotalCogsAmt, e.RevenueAmt }).FirstOrDefault();

			decimal operatingIncomeTotal = 0;


			if (operatingIncome != null)
			{
				if (RptType == "PL")
				{
					operatingIncomeTotal = ((decimal)operatingIncome.RevenueAmt - (decimal)operatingIncome.TotalCogsAmt);
				}
				else
				{
                    // Assigned Revenue = Total Closed Volume * Assigned Revenue Rate
                    decimal aRRate = GetARRate(i);
                    decimal assignedRevenue = (decimal)operatingIncome.RevenueAmt * (aRRate * (decimal).01);
                    // Operating Income = Assigned Revenue - COGS
                    decimal operatingIncTotal = assignedRevenue - (decimal)operatingIncome.TotalCogsAmt;
                    // todo:
                   
                    // ARO reserve reports uses Assigned Revenue
                    operatingIncomeTotal = (decimal)operatingIncome.RevenueAmt * (aRRate * (decimal).01) - (decimal)operatingIncome.TotalCogsAmt;
				}
			}
			return operatingIncomeTotal;
		}



		private  decimal GetExpenseSubTotal(int i, List<IExpenses> expenses)
		{
			List<string> acctNums = ExpenseAcctMap.GetAccounts(ExpenseAcctsPath).ToList();
			decimal expenseTotal = (decimal)(from e in expenses where acctNums.Contains(e.AccountId) && e.Month == i + 1 select e.TotalAmt).Sum();
			
			return expenseTotal;
		}


		private  decimal GetTotalExpensesAndAllocationsToCapital(decimal expenseTotal, decimal totalRsrvAcct)
		{
			decimal totalExpensesAndAllocationsToCapital = expenseTotal + totalRsrvAcct;

			return totalExpensesAndAllocationsToCapital;
		}


		private  decimal GetLoanLossRsrvInc1(decimal loanLossRsrvBegBal, decimal netIncomeFromOps, decimal totCloseVal, decimal loanLossRsrvCostPerBps)
		{
			decimal loanLossRsrvInc1;

			if ((loanLossRsrvBegBal < 0) && netIncomeFromOps > 0)
			{
				if (netIncomeFromOps < (loanLossRsrvBegBal*(-1)) + (totCloseVal*loanLossRsrvCostPerBps))
				{
					loanLossRsrvInc1 = netIncomeFromOps - (totCloseVal*loanLossRsrvCostPerBps);
				}
				else
				{
					loanLossRsrvInc1 = loanLossRsrvBegBal*(-1);
				}
			}
			else
			{
				loanLossRsrvInc1 = 0;
			}
			return loanLossRsrvInc1;
		}


		private  decimal GetLoanLossRsrvInc2(decimal loanLossRsrvBegBal, decimal loanLossRsrvCap, decimal totCloseVal, decimal loanLossRsrvCostPerBps)
		{
			decimal loanLossRsrvInc2;
			if (loanLossRsrvBegBal >= 0 && loanLossRsrvCap <= loanLossRsrvBegBal)
			{
				loanLossRsrvInc2 = 0;
			}
			else if ((loanLossRsrvCap - loanLossRsrvBegBal) > (totCloseVal * loanLossRsrvCostPerBps))
			{
				loanLossRsrvInc2 = totCloseVal * loanLossRsrvCostPerBps;
			}
			else
			{
				loanLossRsrvInc2 = loanLossRsrvCap - loanLossRsrvBegBal;
			}
			return loanLossRsrvInc2;
		}


		private  decimal GetLoanLossRsrvBegBalance(IBranch branch, int i, decimal loanLossPrevBal)
		{
			decimal loanLossBegBal = 0;
			if (i == 0)
			{
				if (branch.BegLoanLossReserveBal != null) loanLossBegBal = (decimal)branch.BegLoanLossReserveBal;
			}
			else
			{
				loanLossBegBal = loanLossPrevBal;
			}

			_loanLossBalEnd[i] = loanLossPrevBal;

			return loanLossBegBal;
		}


		private  decimal GetCorpInvestAcctBalEnd(int i, IBranch branch, List<IRevenue> revenue, List<IExpenses> expenses) // also known as Operating Loss
		{
			decimal operatingLoss;
			decimal paidBackToCorp;
			decimal branchRsvrBalPrev;
			
			branchRsvrBalPrev = _branchRsvrBalBeg[i];

			if (i > 0)
			{
				operatingLoss = _corpInvestAcctBalEnd[i - 1];
			}
			else
			{
				operatingLoss = branch.CorpInvestBalDueRate;
			}

			paidBackToCorp = _paidBackToCorpPrev[i];
			
			_borrowedFromRsrvAcctsPrev[i] = GetBorrowedFromRsrvAccts(i, branch, revenue, expenses);

			decimal advanceFromCorpToBranch = GetAdvanceFromCorpToBranch(i, _borrowedFromRsrvAcctsPrev[i], branchRsvrBalPrev);

			

			decimal corpInvestAcct = operatingLoss - paidBackToCorp + advanceFromCorpToBranch;

			return corpInvestAcct;

		}


		private  decimal GetBorrowedFromRsrvAccts(int i, IBranch branch, List<IRevenue> revenue, List<IExpenses> expenses)
		{
			var netIncomeFromOps = GetNetIncomeFromOps(i, expenses, revenue, branch);

			decimal borrowedFromRsrvAccts = netIncomeFromOps - _loanLossRsrvIncPrev[i] - _paidBackToCorpPrev[i] - _paidIntoAcctFromBranchPrev[i];

			// can never be positive
			if (borrowedFromRsrvAccts > 0)
			{
				borrowedFromRsrvAccts = 0;

			}
			return borrowedFromRsrvAccts;
		}


		private  decimal GetPaidIntoAcctFromBranch(int i, IBranch branch, List<IRevenue> revenue, List<IExpenses> expenses, decimal paidBackToCorp)
		{
			decimal paidIntoAcctFromBranch;

			// Net Income
			var netIncomeFromOps = GetNetIncomeFromOps(i, expenses, revenue, branch);

			// Branch Reserve Balance 
			// this depends on paidIntoAcctFromBranch which is not yet set so get the value before this so use a recursive call below
			// 
			decimal branchRsvrBalance = GetBranchRsvrBalance(i, branch, revenue, expenses); 
			

			// RESERVE REQUIRED FOR BRANCH OPERATIONS (Admin)
			decimal rsrvReqForBranchOps = branch.ReserveRequiredCap;

			if (netIncomeFromOps - _loanLossRsrvIncPrev[i] - paidBackToCorp <= 0)
			{
				paidIntoAcctFromBranch = 0;
			}
			else if (_branchRsvrBalBeg[i] < rsrvReqForBranchOps)
				{
				if (netIncomeFromOps - _loanLossRsrvIncPrev[i] - paidBackToCorp < rsrvReqForBranchOps - _branchRsvrBalBeg[i])
					{
					paidIntoAcctFromBranch = netIncomeFromOps - _loanLossRsrvIncPrev[i] - paidBackToCorp;
					}
					else
					{
						paidIntoAcctFromBranch = rsrvReqForBranchOps - branchRsvrBalance;

					}
				}
			else {
				paidIntoAcctFromBranch = 0;
			}

			_paidIntoAcctFromBranchPrev[i] = paidIntoAcctFromBranch;

			// kludge: this is a recursive call to set the correct branch balance - paid into account from branch depends on branch balance and visa versa
			GetBranchRsvrBalance(i, branch, revenue, expenses);

			return paidIntoAcctFromBranch;
		}


		private  decimal GetBranchRsvrBalance(int i, IBranch branch, List<IRevenue> revenue, List<IExpenses> expenses)
		{
			decimal branchRsvrBalPrev = _branchRsvrBalBeg[i];
			decimal borrowedFromRsrvAccts = GetBorrowedFromRsrvAccts(i, branch, revenue,expenses);
			decimal appliedToCoverLoss;
			appliedToCoverLoss = GetAppliedToCoverLossBranch(borrowedFromRsrvAccts, _branchRsvrBalBeg[i]);
			

			decimal paidIntoAcctFromBranchPrev;
			decimal branchRsvrBal;

			// this is handled by a kludge recursive call in GetPaidIntoAcctFromBranch()
			paidIntoAcctFromBranchPrev = _paidIntoAcctFromBranchPrev[i];
			branchRsvrBal = branchRsvrBalPrev + paidIntoAcctFromBranchPrev + appliedToCoverLoss;
			_branchRsvrBalEnd[i] = branchRsvrBal;

			return branchRsvrBalPrev;
		}


		private  decimal GetAppliedToCoverLossBranch(decimal borrowedFromRsrvAccts, decimal branchRsvrBalPrev)
		{
			decimal appliedToCoverLoss;

			if (borrowedFromRsrvAccts + branchRsvrBalPrev > 0)
			{
				appliedToCoverLoss = borrowedFromRsrvAccts;

			}
			else
			{
				appliedToCoverLoss = -branchRsvrBalPrev;
			}

			return appliedToCoverLoss;
		}


		private  decimal GetAdvanceFromCorpToBranch(int i, 
			decimal borrowedFromRsrvAccts, decimal branchRsvrBalPrev)
		{
			decimal appliedToCoverLossBranch = GetAppliedToCoverLossBranch(borrowedFromRsrvAccts, branchRsvrBalPrev);
			_appliedtoCoverLoss[i] = appliedToCoverLossBranch;

			decimal appliedToCoverLossMgr = GetAppliedToCoverLossMgr(i, borrowedFromRsrvAccts, appliedToCoverLossBranch);
			
			decimal advanceFromCorpToBranch;

			if (borrowedFromRsrvAccts >= 0)
			{
				advanceFromCorpToBranch = 0;
			}
			else if (appliedToCoverLossBranch + appliedToCoverLossMgr > borrowedFromRsrvAccts)
			{
				advanceFromCorpToBranch = -1* (borrowedFromRsrvAccts - appliedToCoverLossBranch - appliedToCoverLossMgr);
			}
			else
			{
				advanceFromCorpToBranch = 0;
			}

			_appliedtoCoverLosses[i] = appliedToCoverLossMgr;

			return advanceFromCorpToBranch;
		}


		private  decimal GetAppliedToCoverLossMgr(int i, decimal borrowedFromRsrvAccts, decimal appliedToCoverLossBranch)
		{
			decimal appliedToCoverLossMgr;
			
			if (borrowedFromRsrvAccts - appliedToCoverLossBranch >= 0 || _availableFundsToMrgPrev[i] <= 0) // Account Beginning (available fund to manager)
			{
				appliedToCoverLossMgr = 0;
			}
			else if (-borrowedFromRsrvAccts + appliedToCoverLossBranch - _availableFundsToMrgPrev[i] > 0)
			{
				appliedToCoverLossMgr = (-1) * _availableFundsToMrgPrev[i];
			}
			else
			{
				appliedToCoverLossMgr = borrowedFromRsrvAccts - appliedToCoverLossBranch;
			}

			return appliedToCoverLossMgr;
		}



		private  decimal GetAvailableFundsToMrg(int i, List<IExpenses> expenses)
		{
			// Account(beginning)
			decimal availableFundsToMrgPrev;

			if (i == 0)
			{
				// _availableFundsToMrgPrev[0] is an ending balance for the previous year set in Main()
				availableFundsToMrgPrev = _availableFundsToMrgPrev[i];
			}
			else
			{
				availableFundsToMrgPrev = _availableFundsToMrgEnd[i - 1];
			}

			//Increases
			decimal availableFundsToManagerIncreases = _availableFundsToManagerIncreases[i];

			//Applied to Cover Losses (Net Income (loss) to Reserve Account)
			decimal appliedtoCoverLosses = _appliedtoCoverLosses[i];

			//Ending Balance(accounting)
			List<AroBonusesPaid> lAroBonusPaid = GetLBonusPaidByType(expenses, BonusPaidAcct);

			//Non Producing Managers draw applies to P & L only
			decimal nonProdMgrDraw = 0;

		    //if (RptType == "PL")
		    //{
		        nonProdMgrDraw = (decimal) lAroBonusPaid.Where(b => b.Month == i + 1).Select(b => b.TotalAmt).FirstOrDefault();
		    //}
		    _arOverridePaid[i] = nonProdMgrDraw;

			decimal availableFundsToMrg = availableFundsToMrgPrev + availableFundsToManagerIncreases + appliedtoCoverLosses - nonProdMgrDraw;

			if (i < 0 )
			{
				// Jan is set from the branch set up using the year-end balance
				_availableFundsToMrgPrev[i-1] = availableFundsToMrg;
			}
			_availableFundsToMrgEnd[i] = availableFundsToMrg;
		  

			return availableFundsToMrg;
		}


		private  List<AroBonusesPaid> GetLoanLossRsrvPayments(List<IExpenses> expenses)
		{
			List<string> acctNums = new List<string> { "2706", "2707", "2708" }.ToList();

			var aroBonusPaid = expenses
				 .Where(w => acctNums.Contains(w.AccountId))
				 .GroupBy(c => new { c.Month, c.AccountId })
				 .Select(m => new
				 {
					 Month = m.Key.Month,
					 Acct = m.Key.AccountId,
					 TotalAmt = m.Sum(g => g.TotalAmt)
				 });

			List<AroBonusesPaid> bonusPaids = new List<AroBonusesPaid>();

			foreach (var item in aroBonusPaid)
			{
				AroBonusesPaid bonusPaid = new AroBonusesPaid();
				bonusPaid.AcctId = item.Acct;
				bonusPaid.Month = Convert.ToInt32(item.Month);
				bonusPaid.TotalAmt = -item.TotalAmt; 

				bonusPaids.Add(bonusPaid);
			}

			return bonusPaids;
		}
		

		private  decimal GetLoanLossRsrvPaymentsAcct(int i, string acctId, List<IExpenses> expenses)
		{
		   var aroBonusPaid = expenses
				 .Where(w => w.AccountId == acctId && w.Month == i+1)
				 .GroupBy(c => new { c.Month, c.AccountId })
				 .Select(m => new
				 {
					 Month = m.Key.Month,
					 Acct = m.Key.AccountId,
					 TotalAmt = m.Sum(g => g.TotalAmt)
				 });

			decimal loanLossRsrvPayments = 0;

			foreach (var item in aroBonusPaid)
			{
				loanLossRsrvPayments = (decimal) -item.TotalAmt;
			}

			return loanLossRsrvPayments;
		}


		private  List<AroBonusesPaid> GetLBonusPaidByType(List<IExpenses> expenses, string bonusType)
		{
			var aroBonusPaid = expenses
				.Where(w => w.AccountId == bonusType)
				 .GroupBy(c => c.Month)
				 .Select(m => new
				 {
					 Month = m.Key,
					 TotalAmt = m.Sum(g => g.TotalAmt)
				 });

			List<AroBonusesPaid> bonusPaids = new List<AroBonusesPaid>();

			foreach (var item in aroBonusPaid)
			{
				AroBonusesPaid bonusPaid = new AroBonusesPaid();
				bonusPaid.Month = item.Month;
				bonusPaid.TotalAmt = item.TotalAmt;

				bonusPaids.Add(bonusPaid);
			}

			return bonusPaids;
		}


		private  List<OperatingIncome> GetOperatingIncome(int i, List<IExpenses> expenses, List<IRevenue> revenue)
		{
			List<OperatingIncome> opIncs = new List<OperatingIncome>();
			if (RptType == "ARO")
			{
				opIncs = GetOperatingIncomeAros(expenses, revenue);
			}
		   else if (RptType == "PL")
		   {
				opIncs = GetOperatingIncomePl(i, expenses);
			}

			return opIncs;
		}



		private  List<OperatingIncome> GetOperatingIncomeAros(List<IExpenses> expenses, List<IRevenue> revenue)
		{
			// revenue
			var assignedRevenue = revenue.Select(r => new { RevenueAmt = r.TotalAmt, r.Month });

			// Get Cost of Goods Sold
			List<Cogs> cogs = GetCogs(expenses);

			var operatingIncome = from c in cogs
								  join a in assignedRevenue
								  on c.Month equals a.Month
								  select new { c.TotalCogsAmt, a.Month, a.RevenueAmt };

			List<OperatingIncome> opIncs = new List<OperatingIncome>();

			foreach (var item in operatingIncome)
			{
				OperatingIncome opInc = new OperatingIncome();
				opInc.Month = item.Month;
				opInc.RevenueAmt = item.RevenueAmt;
				opInc.TotalCogsAmt = item.TotalCogsAmt;

				opIncs.Add(opInc);
			}

			return opIncs;
		}


		
		private  List<OperatingIncome> GetOperatingIncomePl(int i, List<IExpenses> expenses){
		   
				// revenue 
				// get total revenue from PLRevenueAcntMap to include in RevenueAmt in operatingIncome
				Dictionary<string, int> acctRows = ExpenseAcctMap.GetAccountRows(RevenueIncMapPath);

			var revenueTotal = expenses
						   .Where(c => acctRows.ContainsKey(c.AccountId) && c.Month==i+1)
						   .GroupBy(c => c.Month)
							   .Select(m => new
							   {
								   TotalRevenueAmt = m.Sum(c => c.TotalAmt)
							   }).FirstOrDefault();
			
			// Get Cost of Goods Sold
			List<Cogs> cogs = GetCogs(expenses);
			var cogsTotal =  cogs.Where(c => c.Month == i + 1).FirstOrDefault();

			List<OperatingIncome> opIncs = new List<OperatingIncome>();

			OperatingIncome opInc = new OperatingIncome();

			opInc.Month = i + 1;


			if (revenueTotal != null)
			{
				opInc.RevenueAmt = revenueTotal.TotalRevenueAmt;
			}
			else
			{
				opInc.RevenueAmt = 0;

			}

			if (cogsTotal != null)
			{
				opInc.TotalCogsAmt = cogsTotal.TotalCogsAmt;
			}
			else
			{
				opInc.TotalCogsAmt = 0;

			}

			opIncs.Add(opInc);
			

			return opIncs;
		}


        decimal GetARRate(int i)
        {
            decimal aRRate;
            // branch history based on year indicated
            List<IBranchHistory> branchHist = BranchHistory.GetBranchHistory(Year, BranchId);

            //use AR Rate for the specific month (2015+ only)
            aRRate = branchHist.Where(b => b.MonthNum == i+1).Select(b => b.ARRate).FirstOrDefault();

            return aRRate;
        }


        internal  List<Cogs> GetCogs(List<IExpenses> expenses)
		{
			// get a list of Cost of Good Sold (cogs) account numbers  
			List<string> acctNums = ExpenseAcctMap.GetAccounts(CogsMapPath).ToList();

			// get 
			var cogExpenses = expenses
						   .Where(c => acctNums.Contains(c.AccountId))
						   .GroupBy(c => c.Month)
							   .Select(m => new
							   {
								   Month = m.Key,
								   TotalCogsAmt = m.Sum(c => c.TotalAmt)
							   });

			// get total cogs per month
			List<Cogs> cogs = new List<Cogs>();

			foreach (var item in cogExpenses)
			{
				Cogs cog = new Cogs();
				cog.Month = item.Month;
				cog.TotalCogsAmt = item.TotalCogsAmt;

				cogs.Add(cog);
			}

			return cogs;
		}
		#endregion


		internal class Cogs
		{
			public int Month { get; set; }

			public double TotalCogsAmt;
		}


		internal class OperatingIncome
		{
			public int Month { get; set; }

			public double TotalCogsAmt;

			public double RevenueAmt;
		}


		internal class AroBonusesPaid
		{
			public int Month { get; set; }

			public double TotalAmt;

			public string AcctId;
		}

	   public class AllocationsReserves 
		{
			private decimal[] _branchLoanLossReserveIncrease = new decimal[12];
			private decimal[] _paidBackToCorporate = new decimal[12];
			private decimal[] _corpInvestAcctBal = new decimal[12];
			private decimal[] _paidIntoAccountfromBranch = new decimal[12];
			private decimal[] _borrowedFromReservAccounts = new decimal[12];
			private decimal[] _adjustmentsToReserves = new decimal[12];
			private decimal[] _totalRsrvAccts = new decimal[12];
			private decimal[] _totalExpensesAndAllocationsToCapital = new decimal[12];
			private decimal[] _netIncomeToReserveAccount = new decimal[12];

			private decimal[] _loanLossReserveBeginning = new decimal[12];
			private decimal[] _loanLossReserveContribution = new decimal[12];
			private decimal[] _loanLossReserveUtilized = new decimal[12];
			private decimal[] _earlyPayoff = new decimal[12];
			private decimal[] _earlyPaymentDefault = new decimal[12];
			private decimal[] _loanLossReserveEndingbalance = new decimal[12];
			private decimal[] _operatingLoss = new decimal[12];
			private decimal[] _paidbacktocorporate = new decimal[12];
			private decimal[] _advanceFromCorporatetoBranch = new decimal[12];
			private decimal[] _endingCorpCapAccount = new decimal[12];
			private decimal[] _branchReserveBeginning = new decimal[12];
			private decimal[] _paidintoaccountfromBranch = new decimal[12];
			private decimal[] _appliedtoCoverLoss = new decimal[12];
			private decimal[] _branchReserveEnding = new decimal[12];
			private decimal[] _availableFundsToMgrBeg = new decimal[12];
			private decimal[] _availableFundsToManagerIncreases = new decimal[12];
			private decimal[] _appliedtoCoverLosses = new decimal[12];
			private decimal[] _arOverridePaid = new decimal[12];
			private decimal[] _endingAroAvailableforDraws = new decimal[12];
			private decimal[] _availableFundsToMgrEnd = new decimal[12];
			private decimal[] _nonProducingMgrDraw = new decimal[12];


			public decimal[] BranchLoanLossReserveIncrease
		   {
			   get { return _branchLoanLossReserveIncrease; } set { _branchLoanLossReserveIncrease = value; }
		   }

		   public decimal[] PaidBackToCorporate
		   {
				get { return _paidBackToCorporate   ; }
				set { _paidBackToCorporate = value; }
			}

		   public decimal[] CorpInvestAcctBal
		   {
				get { return _corpInvestAcctBal; }
				set { _corpInvestAcctBal = value; }
			}

			public decimal[] PaidIntoAccountfromBranch
			 {
				get { return _paidIntoAccountfromBranch; }
				set { _paidIntoAccountfromBranch = value; }
			}

		   public decimal[] AdjustmentsToReserves
		   {
				get { return _adjustmentsToReserves; }
				set { _adjustmentsToReserves = value; }
			}

			public decimal[] BorrowedFromReservAccounts
			{
				get { return _borrowedFromReservAccounts; }
				set { _borrowedFromReservAccounts = value; }
			}

			public decimal[] TotalRsrvAccts
			{
				get { return _totalRsrvAccts; }
				set { _totalRsrvAccts = value; }
			}

			public decimal[] TotalExpensesAndAllocationsToCapital
			{
				get { return _totalExpensesAndAllocationsToCapital; }
				set { _totalExpensesAndAllocationsToCapital = value; }
			}

			public decimal[] NetIncomeToReserveAccount
			{
				get { return _netIncomeToReserveAccount; }
				set { _netIncomeToReserveAccount = value; }
			}

			public decimal[] LoanLossReserveBeginning { get { return _loanLossReserveBeginning; } set { _loanLossReserveBeginning = value; } }
			public decimal[] LoanLossReserveContribution { get { return _loanLossReserveContribution; } set { _loanLossReserveContribution = value; } }
			public decimal[] LoanLossReserveUtilized { get { return _loanLossReserveUtilized; } set { _loanLossReserveUtilized = value; } }
			public decimal[] EarlyPayoff { get { return _earlyPayoff; } set { _earlyPayoff = value; } }
			public decimal[] EarlyPaymentDefault { get { return _earlyPaymentDefault; } set { _earlyPaymentDefault = value; } }
			public decimal[] LoanLossReserveEndingbalance { get { return _loanLossReserveEndingbalance; } set { _loanLossReserveEndingbalance = value; } }
			public decimal[] OperatingLoss { get { return _operatingLoss; } set { _operatingLoss = value; } }
			public decimal[] Paidbacktocorporate { get { return _paidbacktocorporate; } set { _paidbacktocorporate = value; } }
			public decimal[] AdvanceFromCorporatetoBranch { get { return _advanceFromCorporatetoBranch; } set { _advanceFromCorporatetoBranch = value; } }
			public decimal[] EndingCorpCapAccount { get { return _endingCorpCapAccount; } set { _endingCorpCapAccount = value; } }
			public decimal[] BranchReserveBeginning { get { return _branchReserveBeginning; } set { _branchReserveBeginning = value; } }
			public decimal[] PaidintoaccountfromBranch { get { return _paidintoaccountfromBranch; } set { _paidintoaccountfromBranch = value; } }
			public decimal[] AppliedtoCoverLoss { get { return _appliedtoCoverLoss; } set { _appliedtoCoverLoss = value; } }
			public decimal[] BranchReserveEnding { get { return _branchReserveEnding; } set { _branchReserveEnding = value; } }
			public decimal[] AvailableFundsToMgrBeg { get { return _availableFundsToMgrBeg; } set { _availableFundsToMgrBeg = value; } }
			public decimal[] AvailableFundsToManagerIncreases { get { return _availableFundsToManagerIncreases; } set { _availableFundsToManagerIncreases = value; } }
			public decimal[] AppliedtoCoverLosses { get { return _appliedtoCoverLosses; } set { _appliedtoCoverLosses = value; } }

			// "Non Producing Managers draw" in P & L
			public decimal[] ArOverridePaid { get { return _arOverridePaid; } set { _arOverridePaid = value; } }
			public decimal[] EndingAroAvailableforDraws { get { return _endingAroAvailableforDraws; } set { _endingAroAvailableforDraws = value; } }
			public decimal[] AvailableFundsToMgrEnd { get { return _availableFundsToMgrEnd; } set { _availableFundsToMgrEnd = value; } }
		   public decimal[] NonProducingMgrDraw { get { return _nonProducingMgrDraw; } set { _nonProducingMgrDraw = value; } }

		}

	}
}
