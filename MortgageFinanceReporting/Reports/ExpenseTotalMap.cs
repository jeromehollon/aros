﻿using System;
using System.IO;
using System.Linq;

namespace Reports
{

	// get an array of cell tokens ranges and total cell number 
	static class ExpenseTotalMap
	{
		public static string[] GetExpenseTotalMap(string mapPath)
		{
			string[] expenseAcctMap = File.ReadAllLines(mapPath);

			return expenseAcctMap;
		}
	}
}
