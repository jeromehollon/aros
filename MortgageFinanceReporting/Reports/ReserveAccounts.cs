﻿using System;
using System.Collections.Generic;
using System.Linq;
using BranchRepository.Interface;
using ExpenseRepository.Interface;
using Reports.Properties;
using RevenueRepository.Interface;

namespace Reports
{
    public class ReserveAccounts
    {
        #region properties
        public string TemplateFilePath { get; set; }

        public int Year { get; set; }

        public string BranchId { get; set; }

        public IBranch Branch { get; set; }

        public IBranchHistory BranchHistory { get; set; }

        public IExpenses Expenses { get; set; }

        public IRevenue Revenue { get; set; }

        public string CogsMapPath { get; set; }

        public string BonusPaidAcct { get; set; }

        public string BonusVolAcct { get; set; }

        public string LoanLossRsrvUtilized { get; set; }
        public string ExpenseAcctsPath { get; set; }
        #endregion


        #region constants
        // todo: read these three from a text file?        
        const int LoanLossUtilizedTotCell = 5;
        const int LoanLossUtilizedCell = 800;
        const int EarlyPayoffCell = 805;
        const int EarlyPaymentCell = 810;
        const int RsrvBeginCell = 6;
        const int RsrvDepositCell = 7;
        const int RsrvEndCell = 8;
        const int CorpCapContribCell = 11;
        const int RsrvBeginBalCell = 18;
        const int BranchRsrveReqForOpsCell = 24;
        const int BeginAroAccountBalCell = 26;
        const int AroBonusPaidCell = 28;
        const int VolBonusPaidCell = 30;
        const int ReserveRequiredCapCell = 0;
        const int LoanLossReserveCapCell = 1;
        #endregion


        #region public
        public ReserveAccounts(string templateFilePath, IBranch branch, IBranchHistory branchHist, IExpenses expenses, IRevenue revenue, string cogsMapPath)
        {
            // composition root constructor injection
            TemplateFilePath = templateFilePath;
            templateFilePath = TemplateFilePath;

            Branch = branch;
            BranchHistory = branchHist;
            Expenses = expenses;
            Revenue = revenue;
            CogsMapPath = cogsMapPath;
        }

        /// <summary>
        /// Initializes a new instance of the AroReport class.
        /// </summary>
        public ReserveAccounts(string templateFilePath, string cogsMapPath, int year)
        {
            // default to value in configuration file when not explicitly set
            TemplateFilePath = templateFilePath;
            CogsMapPath = cogsMapPath;
            Year = year;
            BranchHistory = new BranchRepository.Sql.BranchHistories();

            // default composition root    
            if (Year == DateTime.Now.Year)
            {
                Branch = new BranchRepository.Sql.Branches();                
                Revenue = new RevenueRepository.Sql.Pipeline();
            }
            else
            {
                Branch = new BranchRepository.Sql.Archive.Branches();
                Revenue = new RevenueRepository.Sql.Archive.Pipeline();
            }

            if (Year > 2014)
            {
                Expenses = new ExpenseRepository.Sql.Expenses();
            }
            else
            {               
                Expenses = new ExpenseRepository.Sql.Archive.Expenses();
            }
        }

        /// <summary>
        /// Initializes a new instance of the AroReport class.
        /// </summary>
        public ReserveAccounts()
        {
            // assume latest year when called with this constructor

            // default to value in configuration file when not explicitly set
            TemplateFilePath = Settings.Default.ReserveAcctTemplateFilePath;

            CogsMapPath = Settings.Default.ReserveAcctTemplateFilePath;

            // default composition root 
            Branch = new BranchRepository.Sql.Branches();

            BranchHistory = new BranchRepository.Sql.BranchHistories();

            Expenses = new ExpenseRepository.Sql.Expenses();

            Revenue = new RevenueRepository.Sql.Pipeline();
        }


        public string GetReserveAcctReport()
        {
            // get template
            string reserveAcctRpt = Common.GetRptTemplate(TemplateFilePath);

            // add date stamp
            reserveAcctRpt = AddDateStamps(reserveAcctRpt);

            // branch info
            IBranch branchRepository = Branch;

            Branch.Year = Year;
            IBranch branch = branchRepository.GetBranchData(BranchId);
            List<IBranchHistory> branchHist = BranchHistory.GetBranchHistory(Year, BranchId);
            reserveAcctRpt = BranchInfo.AddBranchToReserveAcctRpt(reserveAcctRpt, branch);

            // expenses            
            List<IExpenses> expenses = Expenses.GetSummaryByBranch(BranchId, Year);            

            reserveAcctRpt = AddLoanLossReserve(expenses, BranchId, branch, reserveAcctRpt, Year);

            reserveAcctRpt = AddReserveRequiredCap(branch, reserveAcctRpt);

            reserveAcctRpt = AddLoanLossReserveCap(branch, reserveAcctRpt);

            reserveAcctRpt = AddBranchRsrveReqForOps(branch, reserveAcctRpt);

            return reserveAcctRpt;
        }

        
        public string GetAllocationsReport()
        {
            string reserveAcctRpt = Common.GetRptTemplate(TemplateFilePath);

            // branch info
            Branch.Year = Year;
            IBranch branch = Branch.GetBranchData(BranchId);
            List<IExpenses> expenses = Expenses.GetSummaryByBranch(BranchId, Year);
            Revenue.Year = Year;
            List<IRevenue> revenue = Reports.Revenue.GetRevenueData(BranchId, Revenue);

            Allocations allocations = new Allocations();
            allocations.CogsMapPath = CogsMapPath;
            allocations.ExpenseAcctsPath = ExpenseAcctsPath;
            allocations.RptType = "ARO";
            allocations.BonusPaidAcct = "2316";
            allocations.BranchId = BranchId;
            allocations.Year = Year;

            Allocations.AllocationsReserves rsrvs = allocations.Main(branch, 2016, revenue, expenses);

            // add date stamp
            reserveAcctRpt = AddDateStamps(reserveAcctRpt);
            reserveAcctRpt = AddLoanLossReserve(expenses, BranchId, branch, reserveAcctRpt, Year);
            reserveAcctRpt = AddReserveRequiredCap(branch, reserveAcctRpt);
            reserveAcctRpt = AddLoanLossReserveCap(branch, reserveAcctRpt);

            reserveAcctRpt = AddBranchRsrveReqForOps(branch, reserveAcctRpt);

            // get month array
            string[] monthCols = Common.GetMonthCols();

            int i = 0;
            do
            {
                // loan loss reserve
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$6", Common.GetDecimalAmtTotStr(rsrvs.LoanLossReserveBeginning[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$7", Common.GetDecimalAmtTotStr(rsrvs.LoanLossReserveContribution[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$800", Common.GetDecimalAmtTotStr(rsrvs.LoanLossReserveUtilized[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$805", Common.GetDecimalAmtTotStr(rsrvs.EarlyPayoff[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$810", Common.GetDecimalAmtTotStr(rsrvs.EarlyPaymentDefault[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$8", Common.GetDecimalAmtTotStr(rsrvs.LoanLossReserveEndingbalance[i]));

                // Corporate Investment Account (aka Operating Deficit
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$11", Common.GetDecimalAmtTotStr(rsrvs.OperatingLoss[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$12", Common.GetDecimalAmtTotStr(rsrvs.PaidBackToCorporate[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$14", Common.GetDecimalAmtTotStr(rsrvs.AdvanceFromCorporatetoBranch[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$15", Common.GetDecimalAmtTotStr(rsrvs.CorpInvestAcctBal[i]));

                // Branch Reserves
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$18", Common.GetDecimalAmtTotStr(rsrvs.BranchReserveBeginning[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$19", Common.GetDecimalAmtTotStr(rsrvs.PaidIntoAccountfromBranch[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$21", Common.GetDecimalAmtTotStr(rsrvs.AppliedtoCoverLoss[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$22", Common.GetDecimalAmtTotStr(rsrvs.BranchReserveEnding[i]));

                // Available Funds To Manager
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$26", Common.GetDecimalAmtTotStr(rsrvs.AvailableFundsToMgrBeg[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$27", Common.GetDecimalAmtTotStr(rsrvs.AvailableFundsToManagerIncreases[i]));
                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$25", Common.GetDecimalAmtTotStr(rsrvs.AppliedtoCoverLosses[i]));

                reserveAcctRpt = reserveAcctRpt.Replace("$" + monthCols[i] + "$29", Common.GetDecimalAmtTotStr(rsrvs.AvailableFundsToMgrEnd[i]));

                i++;
            } while (i < 12);

            // order counts here because of the token wildcard
            reserveAcctRpt = reserveAcctRpt.Replace("$G$11", Common.GetDecimalAmtTotStr(rsrvs.OperatingLoss[0]));
            reserveAcctRpt = reserveAcctRpt.Replace("$G$18", Common.GetDecimalAmtTotStr(rsrvs.BranchReserveBeginning[0]));
            reserveAcctRpt = reserveAcctRpt.Replace("$G$1", Common.GetDecimalAmtTotStr(rsrvs.LoanLossReserveBeginning[0]));
            reserveAcctRpt = reserveAcctRpt.Replace("$G$26", Common.GetDecimalAmtTotStr(rsrvs.AvailableFundsToMgrBeg[0]));

            reserveAcctRpt = BranchInfo.AddBranchToReserveAcctRpt(reserveAcctRpt, branch);

            return reserveAcctRpt;
        }

        #endregion


        #region income

        private bool HasPostARROperatingIncome(List<IExpenses> expenses)
        {
            List<double> opsIncomes = new List<double>();
            // Operating income only comes into play here because bonuses can't be paid unless there is operating income

            // revenue
            List<OperatingIncome> operatingIncome = GetOperatingIncome(expenses);
            // branch info
            IBranch branchRepository = Branch;
            IBranch branch = branchRepository.GetBranchData(BranchId);
            List<IBranchHistory> branchHist = BranchHistory.GetBranchHistory(Year, BranchId);

            foreach (var item in operatingIncome)
            {
                if (Year == 2014)
                {
                    // 2014 had a static AR Rate for the entire year
                    opsIncomes.Add((item.RevenueAmt * (double)branch.ARRate * .01) - item.TotalCogsAmt);
                }
                else
                {
                    //use AR Rate for the specific month (2015+ only)
                    opsIncomes.Add((item.RevenueAmt * (double)branchHist.Where(b => b.MonthNum == item.Month).Select(b => b.ARRate).FirstOrDefault() * .01) - item.TotalCogsAmt);
                }
            }

            if (opsIncomes.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        private List<OperatingIncome> GetOperatingIncome(List<IExpenses> expenses)
        {
            // revenue
            List<IRevenue> revenue = Reports.Revenue.GetRevenueData(BranchId, Revenue);
            var assignedRevenue = revenue.Select(r => new { RevenueAmt = r.TotalAmt, r.Month });

            // Get Cost of Goods Sold
            List<Cogs> cogs = GetCogs(expenses);

            var operatingIncome = from c in cogs
                                  join a in assignedRevenue
                                  on c.Month equals a.Month
                                  select new { c.TotalCogsAmt, a.Month, a.RevenueAmt };

            List<OperatingIncome> opIncs = new List<OperatingIncome>();

            foreach (var item in operatingIncome)
            {
                OperatingIncome opInc = new OperatingIncome();
                opInc.Month = item.Month;
                opInc.RevenueAmt = item.RevenueAmt;
                opInc.TotalCogsAmt = item.TotalCogsAmt;

                opIncs.Add(opInc);
            }

            return opIncs;
        }
        #endregion


        #region expenses
        private List<Cogs> GetCogs(List<IExpenses> expenses)
        {
            // get a list of Cost of Good Sold (cogs) account numbers  
            List<string> acctNums = ExpenseAcctMap.GetAccounts(CogsMapPath).ToList();

            // get 
            var cogExpenses = expenses
                           .Where(c => acctNums.Contains(c.AccountId))
                           .GroupBy(c => c.Month)
                               .Select(m => new
                               {
                                   Month = m.Key,
                                   TotalCogsAmt = m.Sum(c => c.TotalAmt)
                               });

            // get total cogs per month
            List<Cogs> cogs = new List<Cogs>();

            foreach (var item in cogExpenses)
            {
                Cogs cog = new Cogs();
                cog.Month = item.Month;
                cog.TotalCogsAmt = item.TotalCogsAmt;

                cogs.Add(cog);
            }

            return cogs;
        }
        #endregion

        #region dates
        private string AddDateStamps(string reserveAcctRpt)
        {
            Expenses.Year = Year;
            Revenue.Year = Year;

            // date stamp of last expense update
            DateTime dateRefreshed = Expenses.DateRefreshed;
            reserveAcctRpt = AddLastRefreshDate(reserveAcctRpt, dateRefreshed);

            // date stamp of accounting close
            DateTime acctCloseDate = Expenses.AcctClosingDate;
            reserveAcctRpt = AddAcctCloseDate(reserveAcctRpt, acctCloseDate);

            return reserveAcctRpt;
        }


        private static string AddLastRefreshDate(string rptTemplate, DateTime dateRefreshed)
        {
            rptTemplate = rptTemplate.Replace("#LastUpdated", dateRefreshed.ToShortDateString());

            return rptTemplate;
        }


        private static string AddAcctCloseDate(string rptTemplate, DateTime acctCloseDate)
        {
            rptTemplate = rptTemplate.Replace("#AcctCloseDate", acctCloseDate.ToShortDateString());

            return rptTemplate;
        }
        #endregion


        #region branchdata
        private static string AddCorpCapBeginning(IBranch branch, string rptTemplate)
        {
            rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}</span", "G", CorpCapContribCell), String.Format(">{0}</span", Common.GetAmtTotStr(double.Parse(branch.CorpInvestBalDueRate.ToString()))));

            rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}</span", "H", CorpCapContribCell), String.Format(">{0}</span", Common.GetAmtTotStr(double.Parse(branch.CorpInvestBalDueRate.ToString()))));

            return rptTemplate;
        }

        // TODO: Dup?
        private static string AddCorpCapContrib(IBranch branch, string rptTemplate)
        {
            rptTemplate = rptTemplate.Replace(String.Format(">$G${0}</span", CorpCapContribCell), String.Format(">{0}</span", Common.GetAmtTotStr(double.Parse(branch.CorpInvestBalDueRate.ToString()))));

            return rptTemplate;
        }

        private static string AddReserveRequiredCap(IBranch branch, string rptTemplate)
        {
            rptTemplate = rptTemplate.Replace(String.Format(">$G${0}</span", ReserveRequiredCapCell), String.Format(">{0}</span", Common.GetAmtTotStr(double.Parse(branch.ReserveRequiredCap.ToString()))));

            return rptTemplate;
        }


        private static string AddLoanLossReserveCap(IBranch branch, string rptTemplate)
        {
           rptTemplate = rptTemplate.Replace(String.Format(">$G${0}</span", LoanLossReserveCapCell), String.Format(">{0}</span", Common.GetAmtTotStr(double.Parse(branch.LoanLossReserveCap.ToString()))));

            return rptTemplate;
        }

        // TODO: get rid of this by consolidating G$0 with RAG$24
        // this populates the Reserve Required Cap in the hidden 'Total Cap Accounts' row
        private static string AddBranchRsrveReqForOps(IBranch branch, string rptTemplate)
        {
            rptTemplate = rptTemplate.Replace(String.Format(">$G${0}</span", BranchRsrveReqForOpsCell), String.Format(">{0}</span", Common.GetAmtTotStr(double.Parse(branch.ReserveRequiredCap.ToString()))));

            return rptTemplate;
        }


        private string AddLoanLossReserve(List<IExpenses> expenses, string branchId, IBranch branch, string rptTemplate, int year)
        {
            // corp investment balance due
            rptTemplate = AddCorpCapContrib(branch, rptTemplate);
            rptTemplate = AddCorpCapBeginning(branch, rptTemplate);
                        
            // beginning balances
            if (year > 2014)
            {
                rptTemplate = AddBegBranchReserveBal(branch, rptTemplate);
                rptTemplate = AddBegLoanLossReserveBal(branch, rptTemplate);
                rptTemplate = AddBegAroAccountBal(branch, rptTemplate);
            }
            else
            {
                rptTemplate = AddBranchRsrveBalanceArchive(branch, rptTemplate);
            }

            // add token/amount to the dictionary for each month by cycling through 0-based array of columns that correspond to months

            // get month array
            string[] monthCols = Common.GetMonthCols();

            // ARO Bonuses Paid
            List<AroBonusesPaid> lAroBonusPaid = GetLBonusPaidByType(expenses, BonusPaidAcct);

            // Volume Bonuses Paid
            List<AroBonusesPaid> lVolBonusPaid = GetLBonusPaidByType(expenses, BonusVolAcct);
            
            // Loan Loss Reserve Utilized
            // todo: do this for each account?
            List<AroBonusesPaid> lLoanLossRsrvUtilized = GetLoanLossRsrvPayments(expenses);

            // add bonus data and links
            rptTemplate = AddBonuses(expenses, lAroBonusPaid, branchId, monthCols, year, lVolBonusPaid, lLoanLossRsrvUtilized, rptTemplate);

            return rptTemplate;
        }

        private string AddBonuses(List<IExpenses> expenses, List<AroBonusesPaid> lAroBonusPaid, string branchId, string[] monthCols, int year, List<AroBonusesPaid> lVolBonusPaid, List<AroBonusesPaid> lLoanLossRsrvUtilized,string rptTemplate)
        {
            int i = 0;
            int monthNum = 1;            

            // add bonus data and links when operating income exists for the month
            do
            {
                if (HasPostARROperatingIncome(expenses) == true)
                {
                    // ARO Balance Paid
                    string aroBonusPaidStr = Common.GetAmtTotStr(lAroBonusPaid.Where(b => b.Month == i + 1).Select(b => b.TotalAmt).FirstOrDefault());
                    string link = GetBonusDetailLink(aroBonusPaidStr, branchId, monthCols, i, year, AroBonusPaidCell);

                    rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}</span", monthCols[i], AroBonusPaidCell), String.Format(">{0}</span", link));

                    // Volume Balance Paid (used in 2014)
                    string volBonusPaidStr = Common.GetAmtTotStr(lVolBonusPaid.Where(b => b.Month == i + 1).Select(b => b.TotalAmt).FirstOrDefault());
                    link = GetBonusDetailLink(volBonusPaidStr, branchId, monthCols, i, year, VolBonusPaidCell);

                    rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}</span", monthCols[i], VolBonusPaidCell), String.Format(">{0}</span", link));

                    // Loan Loss Utilized - make it negative for the report
                    double loanLossUtilTotal = 0;
                    // todo: make this block a function
                    double loanLossUtil = -1 * lLoanLossRsrvUtilized.Where(b => b.Month == i + 1 && b.AcctId == "2706").Select(b => b.TotalAmt).FirstOrDefault();
                    string loanLossUtilStr = Common.GetAmtTotStr(loanLossUtil);
                    loanLossUtilTotal = loanLossUtil;
                    link = GetBonusDetailLink(loanLossUtilStr, branchId, monthCols, i, year, LoanLossUtilizedCell);
                    rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}<", monthCols[i], LoanLossUtilizedCell), String.Format(">{0}<", link));

                    // Loan Loss Utilized - make it negative for the report
                    loanLossUtil = -1 * lLoanLossRsrvUtilized.Where(b => b.Month == i + 1 && b.AcctId == "2707").Select(b => b.TotalAmt).FirstOrDefault();
                    loanLossUtilStr = Common.GetAmtTotStr(loanLossUtil);
                    loanLossUtilTotal += loanLossUtil;
                    link = GetBonusDetailLink(loanLossUtilStr, branchId, monthCols, i, year, EarlyPayoffCell);
                    rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}<", monthCols[i], EarlyPayoffCell), String.Format(">{0}<", link));

                    // Loan Loss Utilized - make it negative for the report                   
                    loanLossUtil = -1 * lLoanLossRsrvUtilized.Where(b => b.Month == i + 1 && b.AcctId == "2708").Select(b => b.TotalAmt).FirstOrDefault();
                    loanLossUtilStr = Common.GetAmtTotStr(loanLossUtil);
                    loanLossUtilTotal += loanLossUtil;
                    link = GetBonusDetailLink(loanLossUtilStr, branchId, monthCols, i, year, EarlyPaymentCell);
                    rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}<", monthCols[i], EarlyPaymentCell), String.Format(">{0}<", link));

                    // hidden total loan loss reserve will be used in client-side script
                    rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}<", monthCols[i], LoanLossUtilizedTotCell), String.Format(">{0}<", loanLossUtilTotal));
                }
                else
                {
                    rptTemplate = ReplaceMissingVals(monthCols[i], rptTemplate);
                }

                i += 1;
                monthNum += 1;
            }
            while (i < 12);

            return rptTemplate;
        }

        #endregion

        #region bonues
        private static string GetBonusDetailLink(string aroBonusPaidStr, string branchId, string[] monthCols, int i, int year, int acctRow)
        {

            string link;

            if (aroBonusPaidStr != "-")
                link = String.Format("<a id='{0}' href='../ExpenseDetail?id={0}&branchId=" + branchId + "&year=" + year + "' target='_blank'>{1}</a>", monthCols[i] + acctRow, aroBonusPaidStr);
            else
                link = "";

            return link;
        }


         private static List<AroBonusesPaid> GetLoanLossRsrvPayments(List<IExpenses> expenses)
        {
            List<string> acctNums = new List<string> { "2706", "2707", "2708" }.ToList();

            var aroBonusPaid = expenses
                 .Where(w => acctNums.Contains(w.AccountId))
                 .GroupBy(c => new { c.Month, c.AccountId})
                 .Select(m => new
                 {                     
                     Month = m.Key.Month,
                     Acct = m.Key.AccountId,
                     TotalAmt = m.Sum(g => g.TotalAmt)
                 });

            List<AroBonusesPaid> bonusPaids = new List<AroBonusesPaid>();

            foreach (var item in aroBonusPaid)
            {
                AroBonusesPaid bonusPaid = new AroBonusesPaid();
                bonusPaid.AcctId = item.Acct;                
                bonusPaid.Month = Convert.ToInt32(item.Month);
                bonusPaid.TotalAmt = item.TotalAmt;

                bonusPaids.Add(bonusPaid);
            }

            return bonusPaids;
        }


        private static List<AroBonusesPaid> GetLBonusPaidByType(List<IExpenses> expenses, string bonusType)
        {
            var aroBonusPaid = expenses
                .Where(w => w.AccountId == bonusType)
                 .GroupBy(c => c.Month)
                 .Select(m => new
                 {
                     Month = m.Key,
                     TotalAmt = m.Sum(g => g.TotalAmt)
                 });

            List<AroBonusesPaid> bonusPaids = new List<AroBonusesPaid>();

            foreach (var item in aroBonusPaid)
            {
                AroBonusesPaid bonusPaid = new AroBonusesPaid();
                bonusPaid.Month = item.Month;
                bonusPaid.TotalAmt = item.TotalAmt;

                bonusPaids.Add(bonusPaid);
            }

            return bonusPaids;
        }
        #endregion


        #region beginningBalances
        private static string AddBegLoanLossReserveBal(IBranch branch, string reserveAcctRpt)
        {
            if (branch.BegLoanLossReserveBal != null)
            {
                reserveAcctRpt = reserveAcctRpt.Replace("$H$" + RsrvBeginCell, Common.GetAmtTotStr(double.Parse(branch.BegLoanLossReserveBal.ToString())));
            }
            else
            {
                reserveAcctRpt = reserveAcctRpt.Replace("$H$" + RsrvBeginCell, "-");
            }

            return reserveAcctRpt;
        }

       
        private static string AddBranchRsrveBalanceArchive(IBranch branch, string reserveAcctRpt) {
             const int BranchRsvrCapCell = 18;

            reserveAcctRpt = reserveAcctRpt.Replace(String.Format(">$G${0}</span", BranchRsvrCapCell), String.Format(">{0}</span", Common.GetAmtTotStr(double.Parse(branch.BranchReserveBalanceRate.ToString()))));
            
            return reserveAcctRpt;
        }

        private static string AddBegBranchReserveBal(IBranch branch, string reserveAcctRpt)
        {
            if (branch.BegBranchReserveBal != null)
            {
                reserveAcctRpt = reserveAcctRpt.Replace("$G$" + RsrvBeginBalCell, Common.GetAmtTotStr(double.Parse(branch.BegBranchReserveBal.ToString())));
            }
            else
            {
                reserveAcctRpt = reserveAcctRpt.Replace("$G$" + RsrvBeginBalCell, Common.GetAmtTotStr(0));
            }

            return reserveAcctRpt;
        }


        private static string AddBegAroAccountBal(IBranch branch, string reserveAcctRpt)
        {
            if (branch.BegAroAccountBal != null)
            {
                reserveAcctRpt = reserveAcctRpt.Replace("$G$" + BeginAroAccountBalCell, Common.GetAmtTotStr(double.Parse(branch.BegAroAccountBal.ToString())));
            }
            else
            {
                reserveAcctRpt = reserveAcctRpt.Replace("$G$" + BeginAroAccountBalCell, "-");
            }

            return reserveAcctRpt;
        }
        #endregion


        #region utilities
        private static string ReplaceMissingVals(string monthCol, string rptTemplate)
        {
            // no data for this month
            rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}</span", monthCol, RsrvBeginCell), String.Format(">{0}</span", "-"));
            rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}</span", monthCol, RsrvDepositCell), String.Format(">{0}</span", "-"));
            rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}</span", monthCol, RsrvEndCell), String.Format(">{0}</span", "-"));
            rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}</span", monthCol, AroBonusPaidCell), String.Format(">{0}</span", "-"));
            rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}</span", monthCol, VolBonusPaidCell), String.Format(">{0}</span", "-"));

            rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}<", monthCol, LoanLossUtilizedCell), String.Format(">{0}<", "-"));
            rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}<", monthCol, EarlyPayoffCell), String.Format(">{0}<", "-"));
            rptTemplate = rptTemplate.Replace(String.Format(">${0}${1}<", monthCol, EarlyPaymentCell), String.Format(">{0}<", "-"));
            
            return rptTemplate;
        }

        #endregion

        internal class Cogs
        {
            public int Month { get; set; }

            public double TotalCogsAmt;
        }


        internal class OperatingIncome
        {
            public int Month { get; set; }

            public double TotalCogsAmt;

            public double RevenueAmt;
        }


        internal class AroBonusesPaid
        {
            public int Month { get; set; }

            public double TotalAmt;

            public string AcctId;
        }
    }
}
