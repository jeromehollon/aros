﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using RevenueExtractor.Interface;
using RevenueExtractor.Service.AuthService;
using RevenueExtractor.Service.ReportingService;

namespace RevenueExtractor.Service
{
    public class MonthlyClosing : IRevenueExtractor
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public List<LoanDetail> Loans { get; set; }

        private static string GetAuthTicket(string login, string password)
        {
            AuthServiceSoapClient x = new AuthServiceSoapClient();
            string authTicket = x.GetUserAuthTicket(login, password);

            return authTicket;
        }

        
        public List<LoanDetail> GetClosingRpt2014()
        {
            string authTicket = GetAuthTicket(Login, Password);

            ReportingSoapClient client = new ReportingSoapClient();
            string report = client.RetrieveCustomReport(authTicket, "CLOSING - YTD Volume & Units Report (AROS 2014)", true);

            List<LoanDetail> loanDetails = ParseClosingRpt(report);
                             
            var loanDtlsClean = loanDetails.Where(p => p.BorrowerFirstName != null);


            return loanDtlsClean.ToList();
        }


        public List<LoanDetail> GetClosingRpt()
        {
            string authTicket = GetAuthTicket(Login, Password);

            ReportingSoapClient client = new ReportingSoapClient();
            string report = client.RetrieveCustomReport(authTicket, "CLOSING - YTD Volume & Units Report (AROS Post 2014)", true);

            List<LoanDetail> loanDetails = ParseClosingRpt(report);
                             
            var loanDtlsClean = loanDetails.Where(p => p.BorrowerFirstName != null);


            return loanDtlsClean.ToList();
        }

        private static void AssignValToFld(LoanDetail loanDtl, int fldNum, string fldStr)
        {
            switch (fldNum)
            {
                case 1:
                    // when venta closes a branch they use this convention in Lending QB
                    // we still need to access their data so change it here
                    loanDtl.BranchName = fldStr.ToUpper().Replace("Z - CLOSED - ", "").Replace("Z- CLOSED - ", "").Replace("Z -CLOSED - ", "");
                    break;
                case 2:
                    loanDtl.LoanNumber = fldStr;
                    break;
                case 3:
                    loanDtl.LoanPurposeStr = fldStr;
                    break;
                case 4:
                    loanDtl.LoanType = fldStr;
                    break;
                case 5:
                    loanDtl.BorrowerLastName = fldStr;
                    break;
                case 6:
                    loanDtl.BorrowerFirstName = fldStr;
                    break;
                case 7:
                    loanDtl.TotalLoanAmount = Decimal.Parse(fldStr.Replace("$", ""));
                    break;
                case 8:
                    loanDtl.FundedDate = DateTime.Parse(fldStr);
                    break;
                case 9:
                    loanDtl.AssignedLoanOfficerName = fldStr;
                    break;
                case 10:
                    loanDtl.LenderAccountExecutiveName = fldStr;
                    break;
                case 11:
                    loanDtl.InvestorRateLockInvestorName = fldStr;
                    break;
                case 12:
                    loanDtl.LoanCloserName = fldStr;
                    break;
                default:
                    // do nothing
                    break;
            }
        }


        private static bool FlagFldsWithUnwantedVals(string fld)
        {
            const string unwantedValStr = "Report:,Branch Name,Loan Number,Loan Purpose,Loan Type,Borr Last Name,Borr First Name,Total Loan Amount,Funded Date,Assigned Loan Officer Name,Lender Account Executive Name,Investor Rate Lock Investor Name,Loan Closer Name";
            string[] unwantedVals = unwantedValStr.Split(',');

            bool isUnwanted = false;

            foreach (string unwantedVal in unwantedVals)
            {
                if (fld.Contains("\"" + unwantedVal) || fld.Contains(unwantedVal))
                {
                    isUnwanted = true;
                }
            }

            return isUnwanted;
        }


        private static bool ProcessAllFldsInLoan(string[] flds, LoanDetail loanDtl)
        {
            int fldNum = 0;
            bool isUnwanted = false;
            foreach (string fld in flds)
            {
                if (string.IsNullOrEmpty(fld) == false)
                {
                    if (fld != "," && fld.Substring(1, 1) != " " && fld.Substring(1, 1) != "-" && fld.Substring(1, 1) != "[")
                    {

                        isUnwanted = FlagFldsWithUnwantedVals(fld);

                        if (isUnwanted == false)
                        {
                            string fldStr = fld.Replace("\"", "");

                            if (fldStr.Contains("Tot =") == false)
                            {
                                fldNum += 1;

                                AssignValToFld(loanDtl, fldNum, fldStr);
                            }
                        }
                    }
                }
            }
            return isUnwanted;
        }


        private static bool LineHasSumVals(string loan)
        {
            bool hasSumVals;

            if (loan.Contains("Tot =") || loan.Contains("Avg ="))
                hasSumVals = true;
            else
                hasSumVals = false;
            return hasSumVals;
        }


        List<LoanDetail> ParseClosingRpt(string report)
        {
            // replace field delimiter so valid commas are counted in the field value
            report = report.Replace("\",", "^");

            List<LoanDetail> loanDetails = new List<LoanDetail>();           
            
            // convert the report to an array of lines
            string[] loans = Regex.Split(report, "\n");            
           
            // process each line of the report (loans)
            foreach (string loan in loans)
            {  
                // split line into field array 
                string[] flds = loan.Split('^');

                // don't include the cumulative lines at the bottom of the report
                if (loan.Contains("Cumulative Results")) {
                    break;
                }

                // don't include sub totals
                if (LineHasSumVals(loan) == false)
                {
                    LoanDetail loanDtl = new LoanDetail();

                    // assign field value to loan detail object
                    bool isUnwanted = ProcessAllFldsInLoan(flds, loanDtl);

                    if (isUnwanted == false && string.IsNullOrWhiteSpace(loanDtl.BranchName) == false)
                    {
                        loanDtl.DateCreated = DateTime.UtcNow;

                        // add loan item to collection
                        loanDetails.Add(loanDtl);
                    }
                }
            }

            return loanDetails;
        }
    }
}
