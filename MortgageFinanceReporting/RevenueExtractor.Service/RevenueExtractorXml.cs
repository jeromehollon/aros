﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.Serialization;
using RevenueExtractor.Interface;
using RevenueExtractor.Service.AuthService;
using RevenueExtractor.Service.Loan;

namespace RevenueExtractor.Service
{
    public class RevenueExtractorXml : IRevenueExtractor
    {
        public string Login { get; set; }

        public string Password { get; set; }

        public List<LoanDetail> Loans { get; set; }

        private static string GetAuthTicket(string login, string password)
        {
            AuthServiceSoapClient x = new AuthServiceSoapClient();
            string authTicket = x.GetUserAuthTicket(login, password);

            return authTicket;
        }

        public List<LoanDetail> GetLoans()
        {
            string authTicket = GetAuthTicket(Login, Password);

            LoanSoapClient loan = new LoanSoapClient();
            var modifiedLoans = loan.ListModifiedLoans(authTicket);

            XDocument xDoc = XDocument.Parse(modifiedLoans);

            var loanItems = from l in xDoc.Descendants("modified_loans").Descendants("loan")
                            select new
                            {
                                loanId = l.Attribute("name")
                            };

            // lo_xmlquery_blank.xml must be in same directory as the executable.
            string queryFlds = LoadFromFile("lo_xmlquery_blank.xml");

            List<LoanDetail> loanDetails = new List<LoanDetail>();

            foreach (var loanItem in loanItems)
            {
                LoanSoapClient loanService = new LoanSoapClient();
                string result = loanService.Load(authTicket, loanItem.loanId.Value.Trim(), queryFlds, 0);

                XDocument xDocDtl = XDocument.Parse(result);

                var x = from d in xDocDtl.Descendants("loan")
                        select d;

                // create a new loan detail item
                LoanDetail loanDtl = new LoanDetail();                 //
               
                //
                foreach (var comNm in x.Descendants("field"))
                {   
                    LendingQbFieldMap(loanDtl, comNm);
                }

                loanDtl.DateCreated = DateTime.UtcNow;

                // add loan item to collection
                loanDetails.Add(loanDtl);
            }

            return loanDetails;
        }
        public List<LoanDetail> GetClosing()
        {
            // TODO: remove this module and update interface
            throw new NotImplementedException();
        }

        private static void SetFundedDate(LoanDetail loanDtl, XElement ComNm)
        {
            var fundedDate = default(DateTime);
            bool isFundedDateValid = DateTime.TryParse(ComNm.Value, out fundedDate);

            if (isFundedDateValid == true)
            {
                loanDtl.FundedDate = fundedDate;
            }
        }

        private static void SetLoanPurpose(LoanDetail loanDtl, XElement ComNm)
        {
            short loanPurposeId = 0;
            bool isLoanPurposeValid = short.TryParse(ComNm.Value, out loanPurposeId);

            if (isLoanPurposeValid == true)
            {
                loanDtl.LoanPurpose = loanPurposeId;
            }
            else
            {
                throw new InvalidDataException(loanPurposeId.ToString());
            }
        }
       
        private static void LendingQbFieldMap(LoanDetail loanDtl, XElement lendingQbValue)
        {
            switch (lendingQbValue.Attribute("id").Value)
            {
                case "aBFirstNm":
                    loanDtl.BorrowerFirstName = lendingQbValue.Value;
                    break;
                case "aBLastNm":
                    loanDtl.BorrowerLastName = lendingQbValue.Value;
                    break;
                case "BranchNm":
                    // Branch Name
                    loanDtl.BranchName = lendingQbValue.Value;
                    break;
                case "sLPurposeT":
                    SetLoanPurpose(loanDtl, lendingQbValue);
                    break;
                // loan purpose
                case "sFundD":
                    // Funded Date
                    SetFundedDate(loanDtl, lendingQbValue);
                    break;
                case "sEmployeeLoanRepName":
                    loanDtl.AssignedLoanOfficerName = lendingQbValue.Value;
                    break;
                // Loan Officer Name
                case "sEmployeeLenderAccExecName":
                    loanDtl.LenderAccountExecutiveName = lendingQbValue.Value;
                    break;
                // Account Executive
                case "sInvestorLockLpInvestorNm":
                    loanDtl.InvestorRateLockInvestorName = lendingQbValue.Value;
                    break;
                // Investor Rate Lock Investor Name
                case "sEmployeeCloserName":
                    loanDtl.LoanCloserName = lendingQbValue.Value;
                    break;
                // Closer Name
                case "sLNm":
                    loanDtl.LoanNumber = lendingQbValue.Value;
                    break;
                // Loan Number
                case "sLT":
                    loanDtl.LoanType = lendingQbValue.Value;
                    break;
                // Loan Type
                case "sFinalLAmt":
                    SetLoanAmt(loanDtl, lendingQbValue);
                    break;
                // Total Loan Amount     
                default:
                    break;
            }
        }

        private static void SetLoanAmt(LoanDetail loanDtl, XElement ComNm)
        {
            var totalLoanAmt = 0M;
            bool isTotalLoanAmtValid = Decimal.TryParse(ComNm.Value.Replace("$", ""), out totalLoanAmt);

            if (isTotalLoanAmtValid == true)
            {
                loanDtl.TotalLoanAmount = totalLoanAmt;
            }
            else
            {
                throw new InvalidDataException(totalLoanAmt.ToString());
            }
        }

        private static string LoadFromFile(string fileName)
        {
            using (StreamReader reader = File.OpenText(fileName))
            {
                return reader.ReadToEnd();
            }
        }
    }
}
