﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using RevenueExtractor.Interface;
using RevenueRepository.Interface;

namespace RevenueRepository.Sql.Archive
{
    // Pipeline table is the source of revenue
    public class Pipeline : IRevenue
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public double TotalAmt { get; set; }
        public int TotalLoans { get; set; }
        public string BranchName { get; set; }


      
        private static void DeleteAllRecsFromTable()
        {
            using (RevenueArchiveEntities context = new RevenueArchiveEntities())
            {
                context.Database.ExecuteSqlCommand("DELETE dbo.PipelineLoans2014");
            }
        }

        public void AddLoans(List<LoanDetail> loans)
        {
            DeleteAllRecsFromTable();

            RevenueArchiveEntities context = new RevenueArchiveEntities();
            
            foreach (var loan in loans)
            {
                PipelineLoans2014 pipelineLoan = new PipelineLoans2014();

                pipelineLoan.AssignedLoanOfficerName = loan.AssignedLoanOfficerName;
                pipelineLoan.BorrowerFirstName = loan.BorrowerFirstName;
                pipelineLoan.BorrowerLastName = loan.BorrowerLastName;
                pipelineLoan.BranchName = loan.BranchName;
                pipelineLoan.FundedDate = loan.FundedDate;
                pipelineLoan.InvestorRateLockInvestorName = loan.InvestorRateLockInvestorName;
                pipelineLoan.LenderAccountExecutiveName = loan.LenderAccountExecutiveName;
                pipelineLoan.LoanCloserName = loan.LoanCloserName;
                pipelineLoan.LoanNumber = loan.LoanNumber;
                pipelineLoan.LoanPurpose = loan.LoanPurpose;
                pipelineLoan.LoanType = loan.LoanType;
                pipelineLoan.TotalLoanAmount = loan.TotalLoanAmount;
                pipelineLoan.DateCreated = loan.DateCreated;
                pipelineLoan.DateChanged = DateTime.Now;
               
                context.PipelineLoans2014.Add(pipelineLoan);
                context.SaveChanges();
            }               
        }
        

         List<IRevenue> IRevenue.GetSummary(string branch)
        {           
            List<IRevenue> summaryList = new List<IRevenue>();

            SqlDataReader summaries = GetRevenueSumByBranchYr(branch, Year);

            while (summaries.Read())
            {
                Pipeline summary = new Pipeline();

                summary.TotalLoans = summaries.GetInt32(0);
                summary.Month = summaries.GetInt32(3);
                summary.TotalAmt = (double)(summaries.GetDecimal(1));
                summary.BranchName = summaries.GetString(2);

                summaryList.Add(summary);
            }
           
            return summaryList;
        }

        private SqlDataReader GetRevenueSumByBranchYr(string branch, int year) {
           
            string connStr = ConfigurationManager.ConnectionStrings["RevenueEntitiesArchive"].ConnectionString;
            SqlConnection cn = new SqlConnection();
            cn.ConnectionString = connStr;
            cn.Open();

            using (SqlCommand dbCmd = new SqlCommand("uspRevenueSummaryByBranchYr", cn))
            {
            dbCmd.CommandType = System.Data.CommandType.StoredProcedure;
            SqlParameter param1 = new SqlParameter();

            param1.ParameterName = "@BranchId";            
            param1.Value = branch;
            dbCmd.Parameters.Add(param1);

            SqlParameter param2 = new SqlParameter();
            param2.ParameterName = "@Yr";
            param2.Value = year;
            dbCmd.Parameters.Add(param2);  

            SqlDataReader dr = (SqlDataReader)dbCmd.ExecuteReader();
            return dr;
            } 
        }
    }
}
