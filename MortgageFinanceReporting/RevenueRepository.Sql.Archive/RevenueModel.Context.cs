﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RevenueRepository.Sql.Archive
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class RevenueEntitiesArchive : DbContext
    {
        public RevenueEntitiesArchive()
            : base("name=RevenueEntitiesArchive")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<PipelineLoans2014> PipelineLoans2014 { get; set; }
    
        public virtual ObjectResult<uspRevenueSummaryByBranchYr_Result> uspRevenueSummaryByBranchYr(string branchId, Nullable<int> yr)
        {
            var branchIdParameter = branchId != null ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(string));
    
            var yrParameter = yr.HasValue ?
                new ObjectParameter("Yr", yr) :
                new ObjectParameter("Yr", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<uspRevenueSummaryByBranchYr_Result>("RevenueEntitiesArchive.uspRevenueSummaryByBranchYr", branchIdParameter, yrParameter);
        }
    
        public virtual ObjectResult<GetRevenueSumByBranchYr_Result> GetRevenueSumByBranchYr(string branchId, Nullable<int> yr)
        {
            var branchIdParameter = branchId != null ?
                new ObjectParameter("BranchId", branchId) :
                new ObjectParameter("BranchId", typeof(string));
    
            var yrParameter = yr.HasValue ?
                new ObjectParameter("Yr", yr) :
                new ObjectParameter("Yr", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetRevenueSumByBranchYr_Result>("RevenueEntitiesArchive.GetRevenueSumByBranchYr", branchIdParameter, yrParameter);
        }
    }
}
