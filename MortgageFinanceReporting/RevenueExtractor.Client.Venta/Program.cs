﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.Unity;
using _5XUtilities;
using System.IO;
using dotNetTips.Utility.Logging;
using System.Diagnostics;
using System.Data.Entity.Validation;

namespace RevenueExtractor.Client.Venta
{
    class Program
    {
        static void Main()
        {
            LoggingHelper.WriteEntry("Revenue Extractor Started", TraceEventType.Information);

            string uriPath = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(Program)).CodeBase) + "/ErrLog.txt";
            string localPath = new Uri(uriPath).LocalPath;
            IExceptionManagement exceptionMgt = new ExceptionManagement(ExceptionManagement.AlertMethod.File, localPath, "", "", "");

            try
            {
                // this is where the extract starts for Venta

                // setup DI object
                var container = new UnityContainer();
                container.RegisterType<RevenueExtractor.Interface.IRevenueExtractor, RevenueExtractor.Service.MonthlyClosing>();

                var extractor = container.Resolve<RevenueExtractor.Service.MonthlyClosing>();
             
                // get from configuration file
                extractor.Login = Venta.Properties.Settings.Default.LendingQbLogin;
                extractor.Password = Venta.Properties.Settings.Default.LendingQbPassword;

                List<RevenueExtractor.Interface.LoanDetail> loanDtls = extractor.GetClosingRpt();

                // save the loans to a repository
                RevenueRepository.Sql.Pipeline pipeLine = new RevenueRepository.Sql.Pipeline();                             
               
                pipeLine.AddLoans(loanDtls);

                if (Venta.Properties.Settings.Default.RunArchive == "true")
                {
                    // archive revenue from previous years
                    List<RevenueExtractor.Interface.LoanDetail> loanDtlsArchive = extractor.GetClosingRpt2014();

                    // save the loans to a repository
                    RevenueRepository.Sql.Archive.Pipeline pipeLineArchive = new RevenueRepository.Sql.Archive.Pipeline();

                    pipeLineArchive.AddLoans(loanDtlsArchive);
                }
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    string exMsg = "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";

                    exceptionMgt.logException(ex, exMsg, "", "Revenue Extractor", "");
                    LoggingHelper.WriteEntry("Error: " + ex.Message, TraceEventType.Error);                      
                    
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            catch (Exception ex)
            {
                exceptionMgt.logException(ex, "Venta", "", "Revenue Extractor", "");
                LoggingHelper.WriteEntry("Error: " + ex.Message, TraceEventType.Error);
            }

            LoggingHelper.WriteEntry("Revenue Extractor Ended", TraceEventType.Information);

        }
    }
}
