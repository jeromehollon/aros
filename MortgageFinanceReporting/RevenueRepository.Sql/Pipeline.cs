﻿using System;
using System.Collections.Generic;
using System.Linq;
using RevenueExtractor.Interface;
using RevenueRepository.Interface;

namespace RevenueRepository.Sql
{
    // Pipeline table is the source of revenue
    public class Pipeline : IRevenue
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public double TotalAmt { get; set; }
        public int TotalLoans { get; set; }
        public string BranchName { get; set; }


        private static void DeleteAllRecsFromTable()
        {
            using (RevenueEntities context = new RevenueEntities())
            {
                context.Database.ExecuteSqlCommand("DELETE dbo.PipelineLoans");
            }
        }

        public void AddLoans(List<LoanDetail> loans)
        {
            DeleteAllRecsFromTable();

            RevenueEntities context = new RevenueEntities();
            
            foreach (var loan in loans)
            {
                PipelineLoan pipelineLoan = new PipelineLoan();

                pipelineLoan.AssignedLoanOfficerName = loan.AssignedLoanOfficerName;
                pipelineLoan.BorrowerFirstName = loan.BorrowerFirstName;
                pipelineLoan.BorrowerLastName = loan.BorrowerLastName;
                pipelineLoan.BranchName = loan.BranchName;
                pipelineLoan.FundedDate = loan.FundedDate;
                pipelineLoan.InvestorRateLockInvestorName = loan.InvestorRateLockInvestorName;
                pipelineLoan.LenderAccountExecutiveName = loan.LenderAccountExecutiveName;
                pipelineLoan.LoanCloserName = loan.LoanCloserName;
                pipelineLoan.LoanNumber = loan.LoanNumber;
                pipelineLoan.LoanPurpose = loan.LoanPurpose;
                pipelineLoan.LoanType = loan.LoanType;
                pipelineLoan.TotalLoanAmount = loan.TotalLoanAmount;
                pipelineLoan.DateCreated = loan.DateCreated;
                pipelineLoan.DateChanged = DateTime.Now;
               
                context.PipelineLoans.Add(pipelineLoan);
                context.SaveChanges();
            }               
        }


         List<IRevenue> IRevenue.GetSummary(string branch)
        {
            RevenueEntities db = new RevenueEntities();
            var summaries = db.GetRevenueSumByBranch(branch);

            List<IRevenue> summaryList = new List<IRevenue>();

            foreach (var item in summaries)
            {
                Pipeline summary = new Pipeline();

                summary.TotalLoans = (int)item.TotalClosed;
                summary.Month = (int)item.Mnth;
                summary.TotalAmt = (double)item.TotalAmt;
                summary.BranchName = item.BranchName;

                summaryList.Add(summary);
            }

            return summaryList;
        }
    }
}
