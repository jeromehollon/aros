﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RevenueRepository.Interface;

namespace RevenueRepository.Sql
{
    public class Detail : IDetail
    {
        public int Month
        { get; set; }

        public string BranchName
        { get; set; }

        public string LoanNumber
        { get; set; }

        public string LoanType
        { get; set; }

        public string BorrowerLastName
        { get; set; }

        public string BorrowerFirstName
        { get; set; }

        public decimal TotalLoanAmount
        { get; set; }

        public DateTime FundedDate
        { get; set; }

        public string AssignedLoanOfficerName
        { get; set; }

        public string LenderAccountExecutiveName
        { get; set; }

        public string InvestorRateLockInvestorName
        { get; set; }

        public string LoanCloserName
        { get; set; }

        public string LoanPurpose
        { get; set; }

        List<IDetail> IDetail.GetDetails(string branch, int month, int year)
        {
            RevenueEntities context = new RevenueEntities();

            var details = context.GetRevenueDetail(branch, month, year);               

            List<IDetail> detailList = new List<IDetail>();

            foreach (var item in details)
            {
                IDetail detail = new Detail();

                detail.AssignedLoanOfficerName = item.AssignedLoanOfficerName;
                detail.BorrowerFirstName = item.BorrowerFirstName;
                detail.BorrowerLastName = item.BorrowerLastName;
                detail.BranchName = item.BranchName;
                detail.FundedDate = (DateTime)item.FundedDate;
                detail.InvestorRateLockInvestorName = item.InvestorRateLockInvestorName;
                detail.LenderAccountExecutiveName = item.LenderAccountExecutiveName;
                detail.LoanCloserName = item.LoanCloserName;
                detail.LoanNumber = item.LoanNumber;
                detail.LoanPurpose = item.LoanPurpose;
                detail.LoanType = item.LoanType;
                detail.TotalLoanAmount = item.TotalLoanAmount; 

                detailList.Add(detail);
            }

            return detailList;
        }

    }
}
