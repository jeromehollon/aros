﻿// ***********************************************************************
// Assembly         : ExpenseTransferAMBToAzure
// Author           : Ron de Frates (5X Solutions)
// Created          : 10-29-2014
//
// Last Modified By : Ron de Frates
// Last Modified On : 3-18-2015
// ***********************************************************************
// <copyright file="Program.cs" company="Venta Mortgage">
//     Copyright 2015 (c) . All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Diagnostics;
using dotNetTips.Utility.Logging;
using System.Data.Entity.Validation;
using System.IO;
using _5XUtilities;

namespace ExpenseTransferAMBToAzure
{
    /// <summary>
    /// Class Program.
    /// </summary>
    class Program
    {
        #region public
        /// <summary>
        /// Defines the entry point of the application.
        /// </summary>
        static void Main()
        {
            LoggingHelper.WriteEntry("Starting", TraceEventType.Information);

            string uriPath = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(Program)).CodeBase) + "/ErrLog.txt";
            string localPath = new Uri(uriPath).LocalPath;
            IExceptionManagement exceptionMgt = new ExceptionManagement(ExceptionManagement.AlertMethod.File, localPath, "", "", "");

            try
            { 
            string targetConnStr = ConfigurationManager.ConnectionStrings["Target"].ConnectionString;

            int origCount = GetOldRecordCount(targetConnStr);
            LoggingHelper.WriteEntry("Original Target Table record count = " + origCount, TraceEventType.Information);
            Console.WriteLine("Original Target Table record count = " + origCount);
            
            Console.WriteLine("Getting Expenses From AMB");
            DataSet ds = GetAmbExpenses();
            Console.WriteLine("Finished Getting Expenses");

            int newCount = ds.Tables[0].Rows.Count;
            bool refreshCompleted = false;

            // when the new records are fewer than the ones they are replacing ask the user if they want to proceed
            // the 1st of the year will always be less since the reports are YTD
            if (IsNewCountMoreThanOld(newCount, origCount))
            {
                refreshCompleted = RefreshExpenseTable(targetConnStr, ds, origCount, newCount);
            }
            else
            {
                string lowCountMsg = String.Format("Refresh suspended. New records {0} are fewer than the old count {1}.", newCount, origCount);
                bool proceed = AskUserToContinue(lowCountMsg);

                if (proceed)
                {
                    refreshCompleted = RefreshExpenseTable(targetConnStr, ds, origCount, newCount);
                }
                {
                    LoggingHelper.WriteEntry("User declined option to proceed.", TraceEventType.Warning);
                }
            }

            if (refreshCompleted)
            {
                LoggingHelper.WriteEntry("Success! Refresh complete", TraceEventType.Information);
                Console.WriteLine("Success! Refresh complete");
            }
        }
            catch (DbEntityValidationException ex)
            {
                foreach (var eve in ex.EntityValidationErrors)
                {
                    string exMsg = "Entity of type " + eve.Entry.Entity.GetType().Name + " in state " + eve.Entry.State + " has the following validation errors:";

                    exceptionMgt.logException(ex, exMsg, "", "Expense Transfer to AMB");
                    LoggingHelper.WriteEntry("Error: " + ex.Message, TraceEventType.Error);                      
                    
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine($"- Property: \"{ve.PropertyName}\", Error: \"{ve.ErrorMessage}\"");
                    }
}
                throw;
            }
            catch (Exception ex)
            {
                exceptionMgt.logException(ex, "Venta", "", "Expense Transfer to AMB");
                LoggingHelper.WriteEntry("Error: " + ex.Message, TraceEventType.Error);
            }

            LoggingHelper.WriteEntry("Ended", TraceEventType.Information);

            Console.WriteLine("_________________________________________");
        }
        #endregion


        #region private
        /// <summary>
        /// Refreshes the expense table.
        /// </summary>
        /// <param name="targetConnStr">The target connection string.</param>
        /// <param name="ds">The ds.</param>
        /// <param name="origCount">The original count.</param>
        /// <param name="newCount">The new count.</param>
        /// <returns><c>true</c> if successful, <c>false</c> otherwise.</returns>
        private static bool RefreshExpenseTable(string targetConnStr, DataSet ds, int origCount, int newCount)
        {
            Console.WriteLine("Create Temp Table -  Start");
            bool createTempTblStatus;
            bool saveToTarget;
            bool renameTempTblStatus;

            Console.WriteLine("Create Temp Table -  Start");
            createTempTblStatus = CreateTmpTable(targetConnStr);
            if (createTempTblStatus)
            {
                LoggingHelper.WriteEntry("Temp Table Created.", TraceEventType.Information);
            }

            Console.WriteLine("Save Expenses To Temp Table -  Start");
            saveToTarget = SaveToTargetDb("ExpensesTemp", targetConnStr, ds);
            if (saveToTarget)
            {
                LoggingHelper.WriteEntry("Target table added " + newCount + " records", TraceEventType.Information);
                RecordLastDate(targetConnStr, newCount);
                LoggingHelper.WriteEntry("Update timestamp added", TraceEventType.Information);
            }

            Console.WriteLine("Rename Temp Table -  Start");
            renameTempTblStatus = RenameTmpTable(targetConnStr);
            if (renameTempTblStatus)
            {
                LoggingHelper.WriteEntry("Temp Table & PK Renamed", TraceEventType.Information);
            }

            return true;
        }

        /// <summary>
        /// Asks the user to continue.
        /// </summary>
        /// <param name="lowCountMsg">The low count MSG.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        private static bool AskUserToContinue(string lowCountMsg)
        {
            Console.WriteLine(lowCountMsg);
            Console.WriteLine("Type 'y' to proceed; 'n' to end");

            string proceed = Console.ReadLine();

            if (proceed == "y")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Gets the amb expenses.
        /// </summary>
        /// <returns>DataSet.</returns>
        private static DataSet GetAmbExpenses()
        {
            string connStr = ConfigurationManager.ConnectionStrings["AMB"].ConnectionString;

            using (OracleConnection connection = new OracleConnection(connStr))
            {
                connection.Open();
            }

            OracleConnection cn = new OracleConnection() { ConnectionString = connStr };
            cn.Open();

            DataSet ds = new DataSet();

            using (OracleCommand dbCmd = new OracleCommand("select ACCTNUMB, POSTDATE, VENDNAME, ACCTDESC, COSTCTR, PRNDESC, AMOUNT from GLSTANDARDEXTRACT where costctr is not null and acctnumb is not null and length(trim(acctnumb)) >= 4 and length(trim(costctr)) >= 4", cn))
            {
                OracleDataAdapter adapter = new OracleDataAdapter(dbCmd);
                adapter.Fill(ds);
            }

            return ds;
        }


        /// <summary>
        /// Determines whether [is new count more than old] [the specified new count].
        /// </summary>
        /// <param name="newCount">The new count.</param>
        /// <param name="origCount">The original count.</param>
        /// <returns><c>true</c> if [is new count more than old] [the specified new count]; otherwise, <c>false</c>.</returns>
        private static bool IsNewCountMoreThanOld(int newCount, int origCount)
        {
            // flag if new count is more than 10% smaller than the original
            if (origCount <= (newCount * 1.1))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Gets the old record count.
        /// </summary>
        /// <param name="targetConnStr">The target connection string.</param>
        /// <returns>System.Int32.</returns>
        private static int GetOldRecordCount(string targetConnStr)
        {
            int oldRecCntNum;

            using (SqlConnection cn = new SqlConnection(targetConnStr))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("SELECT COUNT(*) AS OldCnt FROM dbo.Expenses", cn))
                {
                    var oldRecCnt = cmd.ExecuteScalar();

                    oldRecCntNum = (int)oldRecCnt;
                }
                cn.Close();
            }

            return oldRecCntNum;
        }


        /// <summary>
        /// Saves to target database.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="targetConnStr">The target connection string.</param>
        /// <param name="ds">The ds.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        private static bool SaveToTargetDb(string tableName, string targetConnStr, DataSet ds)
        {
            DataTable dt = ds.Tables[0];

            /* bulk copy data table to sql table */
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(
                targetConnStr,
                SqlBulkCopyOptions.KeepIdentity & SqlBulkCopyOptions.KeepNulls
                )
                {
                    DestinationTableName = tableName
                }
            )
            {
                bulkCopy.BatchSize = 5000;
                bulkCopy.EnableStreaming = true;
                bulkCopy.BulkCopyTimeout = 8000;

                bulkCopy.ColumnMappings.Clear();

                bulkCopy.ColumnMappings.Add("ACCTNUMB", "AcctNumb");
                bulkCopy.ColumnMappings.Add("POSTDATE", "PostDate");
                bulkCopy.ColumnMappings.Add("VENDNAME", "VendName");
                bulkCopy.ColumnMappings.Add("ACCTDESC", "AcctDesc");
                bulkCopy.ColumnMappings.Add("COSTCTR", "CostCtr");
                bulkCopy.ColumnMappings.Add("AMOUNT", "Amount");
                bulkCopy.ColumnMappings.Add("PRNDESC", "Memo");

                bulkCopy.WriteToServer(dt);
            }

            return true;
        }


        /// <summary>
        /// Records the last date.
        /// </summary>
        /// <param name="targetConnStr">The target connection string.</param>
        /// <param name="newCount">The new count.</param>
        static private void RecordLastDate(string targetConnStr, int newCount)
        {
            using (SqlConnection cn = new SqlConnection(targetConnStr))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand(String.Format("INSERT INTO dbo.ProfitAndLossDetailLastUpdate (LastUpdate, RowsCopied) VALUES(getDate(), {0})", newCount), cn))
                {
                    cmd.ExecuteScalar();
                }
                cn.Close();
            }
        }

        static private bool CreateTmpTable(string targetConnStr)
        {
            using (SqlConnection cn = new SqlConnection(targetConnStr))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("uspExpensesTmpTblCreate", cn))
                {
                    cmd.ExecuteScalar();
                }
                cn.Close();
            }

            return true;
        }

        static private bool RenameTmpTable(string targetConnStr)
        {
            using (SqlConnection cn = new SqlConnection(targetConnStr))
            {
                cn.Open();
                using (SqlCommand cmd = new SqlCommand("uspExpensesTmpTblRename", cn))
                {
                    cmd.ExecuteScalar();
                }
                cn.Close();
            }

            return true;
        }
        #endregion
    }
}