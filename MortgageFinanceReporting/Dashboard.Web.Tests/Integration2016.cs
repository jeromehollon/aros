﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dashboard.Web.Tests
{
    [TestClass]
    public class Integration2016
    {
        
        readonly string profitAndLossReserveRptFresno = "";

        #region utilities
        public Integration2016()
        {
            Utilities utl = new Utilities();
           
            if (utl.CheckIfRptExists("ProfitAndLossReserveRptFresno2016") == false)
            {
                profitAndLossReserveRptFresno = Reports.profitAndLossReserveRpt("2016", "CA 402 Fresno");
                utl.SaveRptToDisk("ProfitAndLossReserveRptFresno2016", profitAndLossReserveRptFresno);
            }
            else
            {
                profitAndLossReserveRptFresno = utl.GetRptFromDisk("ProfitAndLossReserveRptFresno2016");
            }
        }
        #endregion



        [STAThread]
        public void PLRsvrEndingAroAvailBalanceFresnoJun2016()
        {
            Assert.IsTrue(profitAndLossReserveRptFresno.Contains("<span id=\"RAM29\">19,078.22</"));
        }


        [TestMethod]
        [STAThread]
        public void PLRsvrEndingLoanLossFresnoJun2016()
        {
            Assert.IsTrue(profitAndLossReserveRptFresno.Contains("<span id=\"RAM8\"><span class=\"negative\">-10,000.00</"));
        }


        [TestMethod]
        [STAThread]
        public void PLRsvrEndingCorpInvFresnoJun2016()
        {
            Assert.IsTrue(profitAndLossReserveRptFresno.Contains("<span id=\"RAM15\">-</"));
        }


        [TestMethod]
        [STAThread]
        public void PLRsvrEndingBranchRsvrFresnoJun2016()
        {
            Assert.IsTrue(profitAndLossReserveRptFresno.Contains("<span id=\"RAM22\">25,000.00</"));
        }
      
    }
}
