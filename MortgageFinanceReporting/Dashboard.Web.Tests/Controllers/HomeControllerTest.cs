﻿using System;
using System.Data.Entity;
using System.IO;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Dashboard.Web.Controllers;
using Dashboard.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Telerik.JustMock;
using Telerik.JustMock.Helpers;

namespace Dashboard.Web.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        public HttpContextBase MockContext { get; set; }
        public HttpRequestBase MockRequest { get; set; }
        public HttpResponseBase MockResponse { get; set; }
        public HttpSessionStateBase MockSession { get; set; }
        public HttpServerUtilityBase MockServer { get; set; }
        public IPrincipal MockUser { get; set; }
        public IIdentity MockIdentity { get; set; }


        public HttpContextBase SetupCurrentUser(string username, bool isLoggedin, string role = "")
        {
            //Context.User
            MockContext = Mock.Create<HttpContextBase>();
            MockRequest = Mock.Create<HttpRequestBase>();
            MockResponse = Mock.Create<HttpResponseBase>();
            var mockUser = Mock.Create<IPrincipal>();

            var mockIdentity = Mock.Create<IIdentity>();
            Mock.Arrange(() => MockContext.User).Returns(mockUser);
            Mock.Arrange(() => mockUser.Identity).Returns(mockIdentity);
            Mock.Arrange(() => mockUser.Identity.Name).Returns(username);
            Mock.Arrange(() => mockUser.Identity.IsAuthenticated).Returns(isLoggedin);
            Mock.Arrange(() => mockUser.IsInRole(role)).Returns(true);

            //Guid userId = Guid.NewGuid();

            //Mock.Arrange(() => mockUser.Identity.GetUserId()).Returns(1);
            Mock.Arrange(() => MockRequest.InputStream).Returns(new MemoryStream());

            //Response
            Mock.Arrange(() => MockResponse.OutputStream).Returns(new MemoryStream());
            return MockContext;
        }


        [TestMethod]
        public void Index()
        {
            HomeControllerTest helper = new HomeControllerTest();

            HttpContextBase user = helper.SetupCurrentUser("rkdefrates", true, "Admin");
            user.User.IsInRole("Region");

            // Arrange
            HomeController target = new HomeController();

            target.ControllerContext = new ControllerContext(user, new RouteData(), target);


            // Act
            ViewResult result = target.Index() as ViewResult;

            // Assert - index is password protected so expect the view to be null 
            Assert.IsNotNull(result);
        }



        [TestMethod]
        public void PreApprovedUser_Should_GoToPreApprovedView()
        {
            // Arrange
            HomeController target = new HomeController();

            HomeControllerTest helper = new HomeControllerTest();
            target.ControllerContext = new ControllerContext(helper.SetupCurrentUser("XXXX", false), new RouteData(), target);


            // Act
            ViewResult result = target.Index() as ViewResult;

            // Assert - index is password protected so expect the view to be null 
            Assert.IsTrue(result.ViewName == "PreApproved");
        }


        [TestMethod]
        public void About()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.About() as ViewResult;

            // Assert
            Assert.AreEqual("This is a reporting application for Alterra Home Loans", result.ViewBag.Message);
        }

        [TestMethod]
        public void Contact()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Contact() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
