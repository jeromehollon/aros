﻿using System;
using System.Threading;
using WatiN.Core;


namespace Dashboard.Web.Tests
{
    class Reports
    {
        public static string ArosRpt(string year, string branch)
        {
            string result;
            using (var browser = new IE("http://arosdev.azurewebsites.net"))
            {
                browser.TextField(Find.ById("UserName")).TypeText("admin");
                browser.TextField(Find.ById("Password")).TypeText("red1dog");
                browser.Button(Find.ByValue("Log in")).Focus();
                browser.Button(Find.ByValue("Log in")).ClickNoWait();
                browser.WaitForComplete();
                Thread.Sleep(1000);

                browser.Link(link => link.Text == "Producing Branch Manager Report »").Click();
                Thread.Sleep(1000);

                var yearList = browser.SelectList(Find.ById("lstYear"));
                yearList.Select(year);

                var branchList = browser.SelectList(Find.ById("lstBranchId"));
                branchList.Select(branch);

                browser.WaitForComplete();
                browser.Button(Find.ByValue("Ok")).ClickNoWait();
                browser.WaitForComplete();
                Thread.Sleep(1000);

                result = browser.Body.OuterHtml;

                // scrub inert cells that are hidden and will never have a token replacement
                result = result.Replace("\"RAH13\">$H$13", ">");
                result = result.Replace("\"RAH20\">$H$20", ">");
            }
            return result;
        }


        public static string ReserveReport(string year, string branch)
        {
            string result;

            using (var browser = new IE("http://arosdev.azurewebsites.net"))
            {
                browser.TextField(Find.ById("UserName")).TypeText("admin");
                browser.TextField(Find.ById("Password")).TypeText("red1dog");
                browser.Button(Find.ByValue("Log in")).Focus();
                browser.Button(Find.ByValue("Log in")).ClickNoWait();
                browser.WaitForComplete();
                Thread.Sleep(1000);

                browser.Link(link => link.Text == "View Reserve Accounts Report »").Click();
                Thread.Sleep(1000);

                var yearList = browser.SelectList(Find.ById("lstYear"));
                yearList.Select(year);

                var branchList = browser.SelectList(Find.ById("lstBranchId"));
                branchList.Select(branch);

                browser.WaitForComplete();
                browser.Button(Find.ByValue("Ok")).ClickNoWait();
                browser.WaitForComplete();
                Thread.Sleep(1000);

                result = browser.Body.OuterHtml;

                // scrub inert cells that are hidden and will never have a token replacement
                result = result.Replace("\"RAH13\">$H$13", ">");
                result = result.Replace("\"RAH20\">$H$20", ">");
            }

            return result;
        }

        
         public static string ProfitAndLossRpt(string year, string branch)
        {
            string result;

            using (var browser = new IE("http://arosdev.azurewebsites.net"))
            {
                browser.TextField(Find.ById("UserName")).TypeText("admin");
                browser.TextField(Find.ById("Password")).TypeText("red1dog");
                browser.Button(Find.ByValue("Log in")).Focus();
                browser.Button(Find.ByValue("Log in")).ClickNoWait();
                browser.WaitForComplete();
                Thread.Sleep(1000);

                browser.Link(link => link.Text == "Non-Producing Manager Report »").Click();
                Thread.Sleep(1000);

                var yearList = browser.SelectList(Find.ById("lstYear"));
                yearList.Select(year);

                var branchList = browser.SelectList(Find.ById("lstBranchId"));
                branchList.Select(branch);

                browser.WaitForComplete();
                browser.Button(Find.ByValue("Ok")).ClickNoWait();
                browser.WaitForComplete();
                Thread.Sleep(1000);

                result = browser.Body.OuterHtml;

                // scrub inert cells that are hidden and will never have a token replacement
                result = result.Replace("\"RAH13\">$H$13", ">");
                result = result.Replace("\"RAH20\">$H$20", ">");
            }

            return result;
        }


        public static string profitAndLossReserveRpt(string year, string branch)
        {
            string result;

            using (var browser = new IE("http://arosdev.azurewebsites.net"))
            {
                browser.TextField(Find.ById("UserName")).TypeText("admin");
                browser.TextField(Find.ById("Password")).TypeText("red1dog");
                browser.Button(Find.ByValue("Log in")).Focus();
                browser.Button(Find.ByValue("Log in")).ClickNoWait();
                browser.WaitForComplete();
                Thread.Sleep(1000);

                Uri siteUri = new Uri("http://arosdev.azurewebsites.net/Reports/ProfitAndLossReserveRpt");
                browser.NativeBrowser.NavigateTo(siteUri);
                Thread.Sleep(1000);

                var yearList = browser.SelectList(Find.ById("lstYear"));
                yearList.Select(year);

                var branchList = browser.SelectList(Find.ById("lstBranchId"));
                branchList.Select(branch);

                browser.WaitForComplete();
                browser.Button(Find.ByValue("Ok")).ClickNoWait();
                browser.WaitForComplete();
                Thread.Sleep(1000);

                result = browser.Body.OuterHtml;

                // scrub inert cells that are hidden and will never have a token replacement
                result = result.Replace("\"RAH13\">$H$13", ">");
                result = result.Replace("\"RAH20\">$H$20", ">");
            }

            return result;
        }
    }
}
