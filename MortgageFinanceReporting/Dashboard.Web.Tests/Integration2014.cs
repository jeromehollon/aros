﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dashboard.Web.Tests
{
    [TestClass]
    public class Integration2014
    {
        string aroCA402Result = "";
        string rsrvCA402Result = "";

        string aroCA612Result = "";
        string rsrvCA612Result = "";

        public Integration2014()
        {
            Utilities utl = new Utilities();

            if (utl.CheckIfRptExists("ARO2014") == false)
            {
                aroCA402Result = Reports.ArosRpt("2014", "CA 402 Fresno");
                utl.SaveRptToDisk("ARO2014", aroCA402Result);
            }
            else
            {
                aroCA402Result = utl.GetRptFromDisk("ARO2014");
            }

            if (utl.CheckIfRptExists("AROCA6122014") == false)
            {
                aroCA612Result = Reports.ArosRpt("2014", "CA 612 Bakersfield");
                utl.SaveRptToDisk("AROCA6122014", aroCA612Result);
            }
            else
            {
                aroCA612Result = utl.GetRptFromDisk("AROCA6122014");
            }

            if (utl.CheckIfRptExists("RsrvRpt2014") == false)
            {
                rsrvCA402Result = Reports.ReserveReport("2014", "CA 402 Fresno");
                utl.SaveRptToDisk("RsrvRpt2014", rsrvCA402Result);
            }
            else
            {
                rsrvCA402Result = utl.GetRptFromDisk("RsrvRpt2014");
            }

            if (utl.CheckIfRptExists("RsrvRptCA6122014") == false)
            {
                rsrvCA612Result = Reports.ReserveReport("2014", "CA 612 Bakersfield");
                utl.SaveRptToDisk("RsrvRptCA6122014", rsrvCA612Result);
            }
            else
            {
                rsrvCA612Result = utl.GetRptFromDisk("RsrvRptCA6122014");
            }
        }


        [TestMethod]
        [STAThread]
        public void AROTokensReplace2014()
        {
            Assert.IsFalse(aroCA402Result.Contains("$H$"));
        }


        [TestMethod]
        [STAThread]
        public void AROTokensReplaceArchive2014()
        {
            string result = "";
            Assert.IsFalse(result.Contains("$H$"));
        }


        [TestMethod]
        [STAThread]
        public void ReserveRptTokensReplaceArchive2014()
        {
            Assert.IsFalse(aroCA402Result.Contains("$H$"));
        }

        [TestMethod]
        [STAThread]
        public void AroExpenseSubTotCA402Nov2014()
        {
            Assert.IsTrue(aroCA402Result.Contains("<span id=\"R115\" style=\"font-weight: bold;\">73,154.03</span>"));
        }


        [TestMethod]
        [STAThread]
        public void NetAROToReserveAccountCA402_2014()
        {
            Assert.IsTrue(aroCA402Result.Contains("<span id=\"T130\">215,734.35</span>"));
        }

        [TestMethod]
        [STAThread]
        public void NetAROToReserveAccountCA612_2014()
        {
            Assert.IsTrue(aroCA612Result.Contains("<span id=\"T130\">11,370.92</span>"));
        }

        [TestMethod]
        [STAThread]
        public void ReserveRptEndingAROAvailableForDrawsDecCA402_2014()
        {
            Assert.IsTrue(rsrvCA402Result.Contains("<span id=\"RAS29\">215,734.35</span>"));
        }


        [TestMethod]
        [STAThread]
        public void PaidIntoBranchReservesSepCA612()
        {
            Assert.IsTrue(aroCA612Result.Contains("<span id=\"P123\">2,078.13</span>"));
        }
    }
}
