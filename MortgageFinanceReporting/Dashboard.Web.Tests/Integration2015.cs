﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Dashboard.Web.Tests
{
    [TestClass]
    public class Integration2015
    {
        readonly string aroPalmdaleRpt = "";
        readonly string aroRsrvPalmdaleRpt = "";
        readonly string profitAndLossPalmdaleRpt = "";
        readonly string profitAndLossReserveRptPlamdale = "";

        #region utilities
        public Integration2015()
        {
            Utilities utl = new Utilities();

            if (utl.CheckIfRptExists("ARO") == false)
            {
                aroPalmdaleRpt = Reports.ArosRpt("2015", "CA 902 Palmdale");
                utl.SaveRptToDisk("ARO", aroPalmdaleRpt);
            }
            else
            {
                aroPalmdaleRpt = utl.GetRptFromDisk("ARO");
            }


            if (utl.CheckIfRptExists("RsrvRpt") == false)
            {
                aroRsrvPalmdaleRpt = Reports.ReserveReport("2015", "CA 902 Palmdale");
                utl.SaveRptToDisk("RsrvRpt", aroRsrvPalmdaleRpt);
            }
            else
            {
                aroRsrvPalmdaleRpt = utl.GetRptFromDisk("RsrvRpt");
            }

             
            if (utl.CheckIfRptExists("ProfitAndLossRpt") == false)
            {
                profitAndLossPalmdaleRpt = Reports.ProfitAndLossRpt("2015", "CA 902 Palmdale");
                utl.SaveRptToDisk("ProfitAndLossRpt", profitAndLossPalmdaleRpt);
            }
            else
            {
                profitAndLossPalmdaleRpt = utl.GetRptFromDisk("ProfitAndLossRpt");
            }


            if (utl.CheckIfRptExists("ProfitAndLossReserveRpt") == false)
            {
                profitAndLossReserveRptPlamdale = Reports.profitAndLossReserveRpt("2015", "CA 902 Palmdale");
                utl.SaveRptToDisk("ProfitAndLossReserveRpt", profitAndLossReserveRptPlamdale);
            }
            else
            {
                profitAndLossReserveRptPlamdale = utl.GetRptFromDisk("ProfitAndLossReserveRpt");
            }
        }
        #endregion



        [TestMethod]
        [STAThread]
        public void AroTokensReplace2015Palmdale()
        {
            Assert.IsFalse(aroPalmdaleRpt.Contains("$H$"));
        }
  

        [TestMethod]
        [STAThread]
        public void AroTokensReplaceTotal2015Palmdale()
        {
            Assert.IsFalse(aroPalmdaleRpt.Contains("$T$"));
        }


        [TestMethod]
        [STAThread]
        public void AroTokensReplaceAvgDollar2015Palmdale()
        {
            Assert.IsFalse(aroPalmdaleRpt.Contains("$U$"));
        }
        

        [TestMethod]
        [STAThread]
        public void AroTokensReplaceAvgPct2015Palmdale()
        {
            Assert.IsFalse(aroPalmdaleRpt.Contains("$V$"));
        }


        [TestMethod]
        [STAThread]
        public void ReserveRptTokensReplacePalmdale()
        {
            Assert.IsFalse(aroRsrvPalmdaleRpt.Contains("$H$"));
        }

        [TestMethod]
        [STAThread]
        public void AssignedRevenueRatePalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("\"H8\">4.250%</span>"));
        }


        [TestMethod]
        [STAThread]
        public void AroOperatingIncomeRatePalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("\"G20\">2.61%</"));
        }

        [TestMethod]
        [STAThread]
        public void AroOperatingIncomeJanPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("\"H20\">50,391.50</"));
        }


        [TestMethod]
        [STAThread]
        public void AroOperatingIncomeFebPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("\"I20\">100,182.12</"));
        }

        [TestMethod]
        [STAThread]
        public void AroOperatingIncomeMarPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("\"J20\">12,060.50</"));
        }

        [TestMethod]
        [STAThread]
        public void AroOperatingIncomeAprPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("\"K20\">119,906.84</span>"));
        }


        [TestMethod]
        [STAThread]
        public void AroExpenseSubTotJanPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("<span id=\"H15\" style=\"font-weight: bold;\">20,574.10</span>"));
        }

        [TestMethod]
        [STAThread]
        public void AroExpenseSubTotFebPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("<span id=\"I15\" style=\"font-weight: bold;\">42,312.52</span>"));
        }

        [TestMethod]
        [STAThread]
        public void AroExpenseSubTotMarPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("<span id=\"J15\" style=\"font-weight: bold;\">26,130.88</span>"));
        }

        [TestMethod]
        [STAThread]
        public void AroExpenseSubTotAprPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("<span id=\"K15\" style=\"font-weight: bold;\">26,718.99</span>"));
        }

        [TestMethod]
        [STAThread]
        public void AroNetResearveJanPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("<span id=\"H30\">3,333.57</span>"));
        }


        [TestMethod]
        [STAThread]
        public void AroNetResearveFebPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("<span id=\"I30\">28,114.80</span>"));
        }


        [TestMethod]
        [STAThread]
        public void AroNetResearveMarPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("<span id=\"J30\">-</span>"));
        }

        [TestMethod]
        [STAThread]
        public void AroNetResearveAprPalmdale()
        {
            Assert.IsTrue(aroPalmdaleRpt.Contains("<span id=\"K30\">72,575.61</span>"));
        }


        #region RsrvRptTesting
        [TestMethod]
        [STAThread]
        public void RsrvEndingAROAvailableforDrawsJanPalmdale()
        {
            Assert.IsTrue(aroRsrvPalmdaleRpt.Contains("-20,955.16"));
        }

        [TestMethod]
        [STAThread]
        public void RsrvEndingAROAvailableforDrawsCapFebPalmdale()
        {
            Assert.IsTrue(aroRsrvPalmdaleRpt.Contains("<span id=\"RAI29\">7,159.64</span>"));
        }

        [TestMethod]
        [STAThread]
        public void LoanLossRsrvEndBalFebPalmdale()
        {
            Assert.IsTrue(aroRsrvPalmdaleRpt.Contains("<span id=\"RAI8\">14,526.94</span>"));
        }
        	

        [TestMethod]
        [STAThread]
        public void RsrvEndingAROAvailableforDrawsCapMarPalmdale()
        {
            Assert.IsTrue(aroRsrvPalmdaleRpt.Contains("<span id=\"RAJ29\">7,159.64</span>"));
        }

        [TestMethod]
        [STAThread]
        public void RsrvEndingAROAvailableforDrawsCapAprPalmdale()
        {
            Assert.IsTrue(aroRsrvPalmdaleRpt.Contains("<span id=\"RAK29\">72,575.61</span>"));
        }
        #endregion

        #region Profit and Loss

        [TestMethod]
        [STAThread]
        public void PLNetIncomeAprPalmdale()
        {
            Assert.IsTrue(profitAndLossPalmdaleRpt.Contains("<span id=\"K17\"><span style=\"color: red;\">-84,008.95</"));
        }


        [TestMethod]
        [STAThread]
        public void PLTokensReplaceTotal2015Palmdale()
        {
            Assert.IsFalse(profitAndLossPalmdaleRpt.Contains("$T$"));
        }


        [TestMethod]
        [STAThread]
        public void PLTokensReplaceAvgDollar2015Palmdale()
        {
            Assert.IsFalse(profitAndLossPalmdaleRpt.Contains("$U$"));
        }


        [TestMethod]
        [STAThread]
        public void PLTokensReplaceAvgPct2015Palmdale()
        {
            Assert.IsFalse(profitAndLossPalmdaleRpt.Contains("$V$"));
        }


        #endregion

        #region Profit and Loss Reserve
        [TestMethod]
        [STAThread]
        public void PLRsvrEndingAroBalanceMarPalmdale()
        {
            // todo: change "style=\"color: red\" to class=\"negative\" when 2015 allocations are added
            Assert.IsTrue(profitAndLossReserveRptPlamdale.Contains("<span id=\"RAJ29\"><span style=\"color: red;\">-24,288.73</"));
        }
                        

        [TestMethod]
        [STAThread]
        public void PLRsvrEndingAroBalanceAprPalmdale()
        {
            // todo: change "style=\"color: red\" to class=\"negative\" when 2015 allocations are added
            Assert.IsTrue(profitAndLossReserveRptPlamdale.Contains("<span id=\"RAK29\"><span style=\"color: red;\">-24,288.73</"));
        }

        #endregion
    }
}
