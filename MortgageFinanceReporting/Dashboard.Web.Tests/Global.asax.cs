﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Dashboard.Web.Models;
using Microsoft.AspNet.Identity;
using System.Collections;
using Microsoft.AspNet.Identity.EntityFramework;
using Dashboard.Web.Controllers;
using System.Web.Security;
using System.Data.Entity;

namespace Dashboard.Web.Tests
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {  
            // this is to prevent migrations in PROD - better to use migration scripts from SQL Source Control
            Database.SetInitializer<ApplicationDbContext>(null);

           }
    }
}
