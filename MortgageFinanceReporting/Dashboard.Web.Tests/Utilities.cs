﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Dashboard.Web.Tests
{
	class Utilities
	{

		
		internal void SaveRptToDisk(string reportType, string reportResult)
		{
			string filePath = "e:\\Temp\\" + reportType + ".txt";

			FileStream fs;    
			fs = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.Write);
			StreamWriter sw = new StreamWriter(fs);
			sw.Write(reportResult);

			sw.Close();
			fs.Close();
		}


		internal string GetRptFromDisk(string reportType)
		{
			string filePath = "e:\\Temp\\" + reportType + ".txt";
			string content;

			using (StreamReader sr = new StreamReader(filePath))
			{
				content = sr.ReadToEnd();
			}

			return content;
		}


        internal bool CheckIfRptExists(string reportType)
        {
            string filePath = "E:\\Temp\\" + reportType + ".txt";

            return File.Exists(filePath);
        }


        internal void DeleteRpt(string reportType)
        {
            string filePath = "E:\\Temp\\" + reportType + ".txt";

            if (CheckIfRptExists(reportType) == true)
            {
                File.Delete(filePath);
            }
        }
	}
}
