﻿using System.Collections.Generic;
using System.Linq;
using BranchRepository.Interface;

namespace BranchRepository.Sql
{
    public class Branches : IBranch
    {
        #region Properties

        public int BranchId { get; set; }

        public int Year { get; set; }

        public string BranchName { get; set; }

        public decimal ManagerSalaryRate { get; set; }

        public decimal ManagerSalaryCap { get; set; }

        public decimal ARRate { get; set; }

        public decimal ARCap { get; set; }

        public decimal VolumeOverrideRate { get; set; }

        public decimal VolumeOverrideCap { get; set; }

        public decimal QrtBonusRate { get; set; }

        public decimal QrtBonusCap { get; set; }

        public decimal LoanVolChangeMetricRate { get; set; }

        public decimal LoanVolChangeMetricCap { get; set; }

        public decimal MonthlyClosingGoalRate { get; set; }

        public decimal MonthlyClosingGoalCap { get; set; }

        public decimal TotalMonthlyBudgetRate { get; set; }

        public decimal TotalMonthlyBudgetCap { get; set; }

        public decimal StartUpCostRate { get; set; }

        public decimal StartUpCostCap { get; set; }

        public decimal LoanLossReserveRate { get; set; }

        public decimal LoanLossReserveCap { get; set; }

        public decimal ReserveRequiredRate { get; set; }

        public decimal ReserveRequiredCap { get; set; }

        public decimal BranchReserveBalanceRate { get; set; }

        public decimal BranchReserveBalanceCap { get; set; }

        public decimal CorpInvestBalDueRate { get; set; }

        public decimal CorpInvestBalDueCap { get; set; }

        public decimal? BegLoanLossReserveBal { get; set; }

        public decimal? BegBranchReserveBal { get; set; }

        public decimal? BegAroAccountBal { get; set; }

        public decimal? BranchMargin { get; set; }

        public decimal? LoMargin { get; set; }

        public decimal? ProcessingFee { get; set; }
        #endregion

        public IBranch GetBranchData(string branchId)
        {
            BranchEntities db = new BranchEntities();

            var branches = (from b in db.Branches
                            where b.BranchName.Replace(" ", "").Contains(branchId)
                            select new
                            {
                                b.BranchName,
                                b.ARCap,
                                b.ARRate,
                                b.BegAroAccountBal,
                                b.BegBranchReserveBal,
                                b.BegLoanLossReserveBal,
                                b.BranchReserveBalanceCap,
                                b.BranchReserveBalanceRate,
                                b.CorpInvestBalDueCap,
                                b.CorpInvestBalDueRate,
                                b.LoanLossReserveCap,
                                b.LoanLossReserveRate,
                                b.LoanVolChangeMetricCap,
                                b.LoanVolChangeMetricRate,
                                b.ManagerSalaryCap,
                                b.ManagerSalaryRate,
                                b.MonthlyClosingGoalCap,
                                b.MonthlyClosingGoalRate,
                                b.QrtBonusCap,
                                b.QrtBonusRate,
                                b.ReserveRequiredCap,
                                b.ReserveRequiredRate,
                                b.StartUpCostCap,
                                b.StartUpCostRate,
                                b.TotalMonthlyBudgetCap,
                                b.TotalMonthlyBudgetRate,
                                b.VolumeOverrideCap,
                                b.VolumeOverrideRate,
                                b.BranchMargin,
                                b.LoMargin,
                                b.ProcessingFee
                            }).FirstOrDefault();

            IBranch branch = new Branches();

            if (branches != null)
            {
                branch.BranchName = branches.BranchName;
                branch.ARCap = branches.ARCap;
                branch.ARRate = branches.ARRate;
                branch.BegAroAccountBal = branches.BegAroAccountBal;
                branch.BegBranchReserveBal = branches.BegBranchReserveBal;
                branch.BegLoanLossReserveBal = branches.BegLoanLossReserveBal;
                branch.BranchReserveBalanceCap = branches.BranchReserveBalanceCap;
                branch.BranchReserveBalanceRate = branches.BranchReserveBalanceRate;
                branch.CorpInvestBalDueCap = branches.CorpInvestBalDueCap;
                branch.CorpInvestBalDueRate = branches.CorpInvestBalDueRate;
                branch.LoanLossReserveCap = branches.LoanLossReserveCap;
                branch.LoanLossReserveRate = branches.LoanLossReserveRate;
                branch.LoanVolChangeMetricCap = branches.LoanVolChangeMetricCap;
                branch.LoanVolChangeMetricRate = branches.LoanVolChangeMetricRate;
                branch.ManagerSalaryCap = branches.ManagerSalaryCap;
                branch.ManagerSalaryRate = branches.ManagerSalaryRate;
                branch.MonthlyClosingGoalCap = branches.MonthlyClosingGoalCap;
                branch.MonthlyClosingGoalRate = branches.MonthlyClosingGoalRate;
                branch.QrtBonusCap = branches.QrtBonusCap;
                branch.QrtBonusRate = branches.QrtBonusRate;
                branch.ReserveRequiredCap = branches.ReserveRequiredCap;
                branch.ReserveRequiredRate = branches.ReserveRequiredRate;
                branch.StartUpCostCap = branches.StartUpCostCap;
                branch.StartUpCostRate = branches.StartUpCostRate;
                branch.TotalMonthlyBudgetCap = branches.TotalMonthlyBudgetCap;
                branch.TotalMonthlyBudgetRate = branches.TotalMonthlyBudgetRate;
                branch.VolumeOverrideCap = branches.VolumeOverrideCap;
                branch.VolumeOverrideRate = branches.VolumeOverrideRate;
                branch.BranchMargin = branches.BranchMargin;
                branch.LoMargin = branches.LoMargin;
                branch.ProcessingFee = branches.ProcessingFee;
            }

            return branch;
        }

        public static List<IBranch> GetBranches(int year)
        {
            BranchEntities db = new BranchEntities();

            var branches = (from b in db.Branches
                            select new
                            {
                                b.BranchId,
                                b.BranchName,
                                b.ARCap,
                                b.ARRate,
                                b.BranchReserveBalanceCap,
                                b.BranchReserveBalanceRate,
                                b.CorpInvestBalDueCap,
                                b.CorpInvestBalDueRate,
                                b.LoanLossReserveCap,
                                b.LoanLossReserveRate,
                                b.LoanVolChangeMetricCap,
                                b.LoanVolChangeMetricRate,
                                b.ManagerSalaryCap,
                                b.ManagerSalaryRate,
                                b.MonthlyClosingGoalCap,
                                b.MonthlyClosingGoalRate,
                                b.QrtBonusCap,
                                b.QrtBonusRate,
                                b.ReserveRequiredCap,
                                b.ReserveRequiredRate,
                                b.StartUpCostCap,
                                b.StartUpCostRate,
                                b.TotalMonthlyBudgetCap,
                                b.TotalMonthlyBudgetRate,
                                b.VolumeOverrideCap,
                                b.VolumeOverrideRate
                            }).ToList();

            List<IBranch> branchList = new List<IBranch>();

            foreach (var item in branches)
            {
                IBranch branch = new Branches();

                branch.BranchId = item.BranchId;
                branch.BranchName = item.BranchName;

                branchList.Add(branch);
            }

            return branchList;
        }
    }
}
