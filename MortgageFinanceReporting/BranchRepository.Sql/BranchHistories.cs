﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BranchRepository.Interface;

namespace BranchRepository.Sql
{
    public class BranchHistories : IBranchHistory
    {
        public decimal ARRate { get; set; }
    
        public string BranchId { get; set; }

        public int EffectiveMonth { get; set; }

        public int EffectiveYear { get; set; }

        public int MonthNum { get; set; }


    public List<IBranchHistory> GetBranchHistory(int Year, string branchId)
        {
            BranchEntities db = new BranchEntities();
            var branches = db.GetBranchHistory(Year, branchId).ToList();

            List<IBranchHistory> historyList = new List<IBranchHistory>();

            foreach (var item in branches)
            {
                IBranchHistory history = new BranchHistories();

                history.BranchId = item.BranchId;
                history.ARRate = item.ARRate;
                history.EffectiveMonth = (int)item.EffectiveMonth;
                history.EffectiveYear = (int)item.EffectiveYear;
                history.MonthNum = (int)item.MonthNum;

                historyList.Add(history);
            }

            return historyList;
        }
    }
}
