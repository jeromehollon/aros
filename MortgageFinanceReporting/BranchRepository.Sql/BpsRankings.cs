﻿using System;
using System.Collections.Generic;
using System.Linq;
using BranchRepository.Interface;

namespace BranchRepository.Sql
{
    public class BpsRankings : IBpsRankings
    {
        #region Properties

        public int BranchRankId { get; set; }

        public int? ParentAcct { get; set; }

        public int Mnth { get; set; }

        public int Yr { get; set; }

        public double TotalAmt { get; set; }

        public int TotalVol { get; set; }

        public double TotAmt { get; set; }

        public string Branch { get; set; }

        public string BranchId { get; set; }

        public decimal? Bps { get; set; }

        public decimal TotalBps { get; set; }

        public int BpsRank { get; set; }

        public int TotalBranches { get; set; }

        public decimal BpsGoal { get; set; }

        public decimal YrAmt { get; set; }

        public int YrTotalVol { get; set; }

        #endregion


        public IBpsRankings GetBpsRankingForBranch(string branchId)
        {
            BranchEntities db = new BranchEntities();

            var branchRanking = (from r in db.BranchBpsRankings
                           where r.BranchId == branchId
                           select new
                           {                               
                               r.TotalBps,
                               r.BpsRank,
                               r.TotalBranches
                           }).FirstOrDefault();

            IBpsRankings bpsRanking = new BpsRankings();

            if (branchRanking != null)
            {
                bpsRanking.BpsRank = branchRanking.BpsRank;
                bpsRanking.TotalBps = branchRanking.TotalBps;
                bpsRanking.TotalBranches = branchRanking.TotalBranches;
            }

            return bpsRanking;
        }


        
        public List<IBpsRankings> GetBpsRankings()
        {
            BranchEntities db = new BranchEntities();

            var branchRanking = (from r in db.BranchBpsRankings
                                 select new
                                 {
                                     r.BranchRankId,
                                     r.ParentAcct,
                                     r.Mnth,
                                     r.Yr,
                                     r.TotalAmt,
                                     r.TotalVol,
                                     r.Tot_Amt,
                                     r.Branch,
                                     r.BranchId,
                                     r.Bps,
                                     r.TotalBps,
                                     r.BpsRank,
                                     r.TotalBranches,
                                     r.YrAmt,
                                     r.YrTotalVol,
                                     r.BpsGoal
                                 }).ToList();

            List<IBpsRankings> bpsRankings = new List<IBpsRankings>();
            
            if (branchRanking != null)
            {
                foreach (var item in branchRanking)
                {
                    IBpsRankings bpsRanking = new BpsRankings();

                    bpsRanking.BranchId = item.BranchId;
                    bpsRanking.Branch = item.Branch;
                    bpsRanking.ParentAcct = item.ParentAcct;
                    bpsRanking.Mnth = item.Mnth;
                    bpsRanking.Yr = item.Yr;
                    bpsRanking.TotalAmt = (double)item.TotalAmt;
                    bpsRanking.TotalVol = (int)item.TotalVol;
                    bpsRanking.TotAmt = (double)item.Tot_Amt;
                    bpsRanking.Bps = item.Bps;
                    bpsRanking.TotalBps = item.TotalBps;
                    bpsRanking.BpsRank = item.BpsRank;
                    bpsRanking.YrAmt = item.YrAmt;
                    bpsRanking.YrTotalVol = item.YrTotalVol;
                    bpsRanking.BpsGoal = item.BpsGoal;

                    bpsRankings.Add(bpsRanking);
                }
            }

            return bpsRankings;
        }


        

        
        public List<IBpsRankings> GetBpsRankings(int regionId)
        {
            BranchEntities db = new BranchEntities();

            var branchRanking = (from r in db.BranchBpsRankings
                                 join b in db.RegionBranches 
                                    on r.BranchId equals b.Branch.BranchName.Substring(0, 6).Replace(" ", "")
                                where b.Region.RegionId == regionId
                                 select new
                                 {
                                     r.BranchRankId,
                                     r.ParentAcct,
                                     r.Mnth,
                                     r.Yr,
                                     r.TotalAmt,
                                     r.TotalVol,
                                     r.Tot_Amt,
                                     r.Branch,
                                     r.BranchId,
                                     r.Bps,
                                     r.TotalBps,
                                     r.BpsRank,
                                     r.TotalBranches,
                                     r.YrAmt,
                                     r.YrTotalVol,
                                     r.BpsGoal
                                 }).ToList();

            List<IBpsRankings> bpsRankings = new List<IBpsRankings>();
            
            if (branchRanking != null)
            {
                foreach (var item in branchRanking)
                {
                    IBpsRankings bpsRanking = new BpsRankings();

                    bpsRanking.BranchId = item.BranchId;
                    bpsRanking.Branch = item.Branch;
                    bpsRanking.ParentAcct = item.ParentAcct;
                    bpsRanking.Mnth = item.Mnth;
                    bpsRanking.Yr = item.Yr;
                    bpsRanking.TotalAmt = (double)item.TotalAmt;
                    bpsRanking.TotalVol = (int)item.TotalVol;
                    bpsRanking.TotAmt = (double)item.Tot_Amt;
                    bpsRanking.Bps = item.Bps;
                    bpsRanking.TotalBps = item.TotalBps;
                    bpsRanking.BpsRank = item.BpsRank;
                    bpsRanking.YrAmt = item.YrAmt;
                    bpsRanking.YrTotalVol = item.YrTotalVol;
                    bpsRanking.BpsGoal = item.BpsGoal;

                    bpsRankings.Add(bpsRanking);
                }
            }

            return bpsRankings;
        }
    }
}