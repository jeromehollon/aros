﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExpenseRepository.Interface;

namespace ExpenseRepository.Sql
{
    public class Expenses : IExpenses
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public string AccountId { get; set; }
        public double TotalAmt { get; set; }
        public string BranchName { get; set; }

        List<IExpenses> IExpenses.GetSummaryByBranch(string branchId, int year)
        {
            VentaEntities db = new VentaEntities();
            var summaries = db.vwExpenseSummaries
                .Where(e => e.CostCtr == branchId && e.Yr == year
                )
               .Select(e => new { AcctId = e.AcctId, Mnth = e.Mnth, Yr = e.Yr, Amount = e.Amount, CostCtr = e.CostCtr })
                .ToList(); 

             List<IExpenses> summaryList = new List<IExpenses>();

            foreach (var item in summaries)
            {
                Expenses summary = new Expenses();

                summary.AccountId = item.AcctId;
                summary.Month = (int)item.Mnth;
                summary.Year = (int)item.Yr;
                summary.TotalAmt = (double)item.Amount;
                summary.BranchName = item.CostCtr;

                summaryList.Add(summary);
            }

            return summaryList;
        }

        
     
        DateTime IExpenses.DateRefreshed
        {
            get
            {               
                VentaEntities db = new VentaEntities();
                var lastUpdate = db.ProfitAndLossDetailLastUpdates.Max(d => d.LastUpdate);
                
                DateTime dateRefreshed = lastUpdate;
              
                return dateRefreshed;
            }
        }


        
        DateTime IExpenses.AcctClosingDate
        {
            get
            {               
                VentaEntities db = new VentaEntities();
                var lastUpdate = db.AcctClosings.Where(d => d.Year == null).Max(d => d.AcctClosingDate);
                
                DateTime dateRefreshed = lastUpdate;
              
                return dateRefreshed;
            }
        }
    }
}
