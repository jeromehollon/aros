//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExpenseRepository.Sql
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwExpense
    {
        public int ExpenseId { get; set; }
        public string AcctId { get; set; }
        public Nullable<System.DateTime> PostDate { get; set; }
        public string VendName { get; set; }
        public string AcctDesc { get; set; }
        public string CostCtr { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public Nullable<int> Mnth { get; set; }
        public Nullable<int> Yr { get; set; }
        public string Memo { get; set; }
    }
}
