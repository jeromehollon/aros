﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using ExpenseRepository.Interface;

namespace ExpenseRepository.Sql
{
    public class Detail : IDetail
    {
        public string AccountId
        { get; set; }
        

       List<IDetail> IDetail.GetDetails(string branch, int month, int year, string accountId)
        {
            VentaEntities db = new VentaEntities();

            var details = db.vwExpenses.Where(e => e.CostCtr == branch && e.AcctId == accountId && e.Mnth == month && e.Yr == year).ToList();

            List<IDetail> detailList = new List<IDetail>();

            foreach (var item in details)
            {
                IDetail detail = new Detail();

                detail.AccountId = item.AcctId;
                detail.AccountName = item.AcctDesc;
                detail.Amount = double.Parse(item.Amount.ToString(), NumberStyles.Currency);
                detail.Payee = item.VendName;
                detail.Memo = item.Memo;
                detail.TransactionDate = DateTime.Parse(item.PostDate.ToString());

                detailList.Add(detail);
            }

            return detailList;
        }

        public int Month
        { get; set; }

        public int Year
        { get; set; }

        public DateTime TransactionDate
        { get; set; }

        public double Amount
        { get; set; }

        public string AccountName
        { get; set; }
        
        public string Payee
        { get; set; }

        public string Memo
        { get; set; }
    }
}
