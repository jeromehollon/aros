﻿using System;
using System.IO;
using System.Linq;

namespace RemoveTempPages
{
	// To learn more about Microsoft Azure WebJobs, please see http://go.microsoft.com/fwlink/?LinkID=401557
	class Program
	{
		static void Main()
		{
			string filePath = "d:\\home\\site\\wwwroot\\TempPages";
			DirectoryInfo di = new DirectoryInfo(filePath);

			FileInfo[] files = di.GetFiles();            
			foreach (FileInfo file in files) {
				if (file.Name.Contains("Protected") == false) {
					file.Delete();
				}
			}
		}
	}
}
