﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RevenueExtractor.Interface;

namespace RevenueRepository.Interface
{
    public interface IRevenue
    {              
        int Month { get; set; }
        int Year { get; set; }
        int TotalLoans { get; set; }
        double TotalAmt { get; set; }
        string BranchName { get; set; }

        void AddLoans(List<LoanDetail> loans);
        List<IRevenue> GetSummary(string branchId);
    }
}
