﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevenueRepository.Interface
{
    public interface IDetail
    {
      int Month { get; set; }
       string BranchName { get; set; }
       string LoanNumber { get; set; }
      string LoanType { get; set; }
      string BorrowerLastName { get; set; }
      string BorrowerFirstName { get; set; }
      decimal TotalLoanAmount { get; set; }
      DateTime FundedDate { get; set; }
      string AssignedLoanOfficerName { get; set; }
      string LenderAccountExecutiveName { get; set; }
      string InvestorRateLockInvestorName { get; set; }
      string LoanCloserName { get; set; }      
      string LoanPurpose { get; set; }
      System.Collections.Generic.List<IDetail> GetDetails(string branch, int month, int year);
    }
}
