﻿using System;
using System.Collections.Generic;
using System.Linq;
using RevenueExtractor.Interface;
using RevenueRepository.Interface;
using RevenueRepository.Sql;

namespace RevenueRepository.Sql._2014
{
    // Pipeline table is the source of revenue
    public class Pipeline : IRevenue
    {
        public int Month { get; set; }
        public double TotalAmt { get; set; }
        public int TotalLoans { get; set; }
        public string BranchName { get; set; }


        public void AddLoans(List<LoanDetail> loans)
        {
            // This will never be implemented - leave it empty
        }
        

         List<IRevenue> IRevenue.GetSummary(string branch)
        {
            RevenueEntities db = new RevenueEntities();
            var summaries = db.GetRevenueSumByBranch(branch);

            List<IRevenue> summaryList = new List<IRevenue>();

            foreach (var item in summaries)
            {
                Pipeline summary = new Pipeline();

                summary.TotalLoans = (int)item.TotalClosed;
                summary.Month = (int)item.Mnth;
                summary.TotalAmt = (double)item.TotalAmt;
                summary.BranchName = item.BranchName;

                summaryList.Add(summary);
            }

            return summaryList;
        }
    }
}
