﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using _5XUtilities;
using dotNetTips.Utility.Logging;

namespace BranchBpsFillRankingTbl
{
    class Program
    {
        static void Main(string[] args)
        {
            // log starting event
            LoggingHelper.WriteEntry("Branch Bps Fill Ranking Tbl Started", TraceEventType.Information);

            try { 
                string targetConnStr = ConfigurationManager.ConnectionStrings["Target"].ConnectionString;
                // populate the ranking table - persisting it in a table speeds load time in web app
                FillBpsRankingTbl(targetConnStr);
            }
            catch (Exception ex)
            {
                string uriPath = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(Program)).CodeBase) + "/ErrLog.txt";
                string localPath = new Uri(uriPath).LocalPath;
                IExceptionManagement exceptionMgt = new ExceptionManagement(ExceptionManagement.AlertMethod.File, localPath, "", "", "");

                exceptionMgt.logException(ex, "Venta", "", "Branch Bps Fill Ranking Tbl Started", "");
                LoggingHelper.WriteEntry("Error: " + ex.Message, TraceEventType.Error);
            }

            // log ending event
            LoggingHelper.WriteEntry("Branch Bps Fill Ranking Tbl Ended", TraceEventType.Information);
        }

        
        static private void FillBpsRankingTbl(string connStr)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                connection.Open();

                SqlCommand cmdProc = new SqlCommand("dbo.uspFillBranchBpsTbl", connection);
                cmdProc.CommandTimeout = 4000;
                cmdProc.ExecuteNonQuery();
            }
        }
    }
}
