﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Dashboard.Web.Models;
using Microsoft.AspNet.Identity;
using System.Collections;
using Microsoft.AspNet.Identity.EntityFramework;
using Dashboard.Web.Controllers;
using System.Web.Security;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using Elmah;

namespace Dashboard.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Session_OnStart()
        {
            // todo: record IPs in a db for a week before cutting people off
            try
            {
                string ipAddr = _5XUtilities.UserBrowserInfo.GetIPAddress();

                if (ipAddr.Length > 0)
                {
                    WebClient client = new WebClient();
                    string json = client.DownloadString("https://freegeoip.net/json/" + ipAddr);

                    var geoIPInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<GeoIPInfo>(json);

                    
                    Session["Country"] = geoIPInfo.country_code;
                    RestrictNonUsa(Session["Country"]);
                }
                else
                {
                    RestrictNonUsa(null);
                }
            }
            catch (Exception ex)
            {
                ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        protected void Application_Start()
        {  
            // this is to prevent migrations in PROD - better to use migration scripts from SQL Source Control
            Database.SetInitializer<ApplicationDbContext>(null);

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // add roles and admin account here
            //CreateRoles();
            // AddAdmin("admin", "Admin");
        }


        //public void AddAdmin(string userName, string roleName)
        //{            
        //    ApplicationDbContext context = new ApplicationDbContext();

        //    ApplicationUser user = context.Users.Where(u => u.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
        //    using (var account = new AccountController())
        //    {
        //        if (Roles.GetRolesForUser(user.UserName).Count() == 0)
        //        {
        //            account.UserManager.AddToRole(user.Id, roleName);
        //        }
        //        else if (Roles.GetRolesForUser(user.UserName)[0] != roleName)
        //        {
        //            account.UserManager.AddToRole(user.Id, roleName);
        //        }

        //    }

        //}

        private void RestrictNonUsa(object country)
        {
            if (country == null || (string)country != "US")
            {
                ErrorSignal.FromCurrentContext().Raise(new System.ApplicationException("Non US IP Address: " + country.ToString()));
                // non-USA ips get 404 error
                Response.StatusCode = 404;
                Response.SuppressContent = true;
                Response.End();
            }
        }


        public class GeoIPInfo
        {
            public string ip { get; set; }
            public string country_code { get; set; }
            public string country_name { get; set; }
            public string region_code { get; set; }
            public string region_name { get; set; }
            public string city { get; set; }
            public string zip_code { get; set; }
            public string time_zone { get; set; }
            public double latitude { get; set; }
            public double longitude { get; set; }
            public int metro_code { get; set; }
        }
    }
}
