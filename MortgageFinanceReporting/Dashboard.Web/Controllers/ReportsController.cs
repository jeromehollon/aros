﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Dashboard.Web.Models;
using Dashboard.Web.Shared;
using Reports;


namespace Dashboard.Web.Controllers
{
#if !DEBUG
		[RequireHttps] //apply to all actions in controller
#endif
	[Authorize()]
	public class ReportsController : Controller
	{
		readonly ApplicationDbContext db = new ApplicationDbContext();
		Entities ba = new Entities();

		#region public
		[Authorize(Roles = "Admin, Global, Division Mgr, Region, Branch")]
		public ActionResult AroReport()
		{
			// branch code can come from list box (Admin or Region) or be assigned to the user (Branch)
			string branchCode = GetBranchCodeForRpt("AroReport");

			string aroRpt = "";

			if (IsFormPosted(branchCode) == true)
			{
				int yearSelected = GetSelectedYear();

				aroRpt = AroReportString(branchCode, yearSelected);
			}

			// branches & years
			GetBranchListBoxForRole();
			// year list box
			YearListBox();


			ViewBag.BranchId = branchCode;
			ViewBag.AroRpt = aroRpt;

			return View();
		}

		[Authorize(Roles = "Admin, Global, Division Mgr, Region, Branch, Non-Producing Branch Mgr")]
		public ActionResult ReportToPdf(string branchCode, int yearSelected, string rptType = "")
		{
			// get the report based on the branch id
			if (IsFormPosted(branchCode) == true)
			{
				string printableReport = "";

				if (rptType == "P&L")
				{
					string pAndLRpt = ProfitAndLossRptString(branchCode, yearSelected);
					printableReport = GetPrintablePAndLReport(pAndLRpt);
				}
				else
				{
					// ARO Report				
					string aroRpt = AroReportString(branchCode, yearSelected);

					// Reserve Accounts Report - select by year				
					string reserveAcctRpt = ReserveAccountsString(branchCode, yearSelected);

					printableReport = GetPrintableReport(aroRpt, reserveAcctRpt);
				}
				ViewBag.Report = printableReport;

				string fileName = PdfPrint.GetPdfFileName(printableReport, Server.MapPath("~"));

				// note: this won't work until the app is deployed to a remote server since the api can't access 'localhost'

				Response.Redirect("https://do.convertapi.com/Web2Pdf?APIKey=635362747&PageOrientation=landscape&CUrl=https://" + ConfigurationManager.AppSettings["url"] + "/TempPages/" + fileName);
				return null;
			}
			else
			{
				return null;
			}
		}

		[Authorize(Roles = "Admin, Global, Division Mgr, Region, Branch")]
		public ActionResult ReserveAccounts()
		{
			// branch code can come from list box (Admin) or be assigned to the user (Branch)
			string branchCode = GetBranchCodeForRpt("ReserveAccounts");

			if (IsFormPosted(branchCode) == true)
			{
				int yearSelected = GetSelectedYear();
				ViewBag.YearSelected = yearSelected;
				ViewBag.ReserveAcctRpt = ReserveAccountsString(branchCode, yearSelected);
				Session["BranchId"] = branchCode;
			}

			// list box for branches
			GetBranchListBoxForRole();
			// year list box
			YearListBox();


			return View();
		}


		[Authorize(Roles = "Admin, Global, Division Mgr, Region, Branch, Non-Producing Branch Mgr")]
		public ActionResult BpsRankings()
		{
			AroReport aro = new AroReport();
			ViewBag.BpsRankings = aro.GetBpsRankingReport();

			return View();
		}

		[Authorize(Roles = "Admin, Global, Division Mgr, Region, Non-Producing Branch Mgr")]
		public ActionResult ProfitAndLossRpt()
		{
			// branch code can come from list box (Admin) or be assigned to the user (Branch)
			string branchCode = GetBranchCodeForRpt("ProfitAndLossRpt");

			if (IsFormPosted(branchCode) == true)
			{
				int yearSelected = GetSelectedYear();
				ViewBag.YearSelected = yearSelected;

				ViewBag.ProfitAndLossRpt = ProfitAndLossRptString(branchCode, yearSelected);
			}

			// list box for branches & years
			YearListBox(2015);
			GetBranchListBoxForRole();

			return View();
		}


		[Authorize(Roles = "Admin, Global, Division Mgr, Region, Non-Producing Branch Mgr")]
		public ActionResult ProfitAndLossReserveRpt()
		{
			// branch code can come from list box (Admin) or be assigned to the user (Branch)
			string branchCode = GetBranchCodeForRpt("ProfitAndLossReserveRpt");

			if (IsFormPosted(branchCode) == true)
			{
				int yearSelected = GetSelectedYear();

				ViewBag.ProfitAndLossReserveRpt = ProfitAndLossReserveRptString(branchCode, yearSelected);
				ViewBag.IsPosted = true;

				if (yearSelected == 2015)
				{
					ViewBag.PLDetailRpt = AroReportString(branchCode, yearSelected);
				}
				else
				{
					ViewBag.PLDetailRpt = ProfitAndLossRptString(branchCode, yearSelected);
				}
				Session["BranchId"] = branchCode;
			}

			// list box for branches & years
			YearListBox(2015);
			GetBranchListBoxForRole();

			return View();
		}
		#endregion


		#region private
		private string GetSelectedBranchCode()
		{
			string branchName = "";
			string branchCode = "";

			try
			{
				if (Request.Form.HasKeys())
				{
					branchName = Request.Form.Get("lstBranchId");

					if (string.IsNullOrWhiteSpace(branchName) == false)
					{
						branchCode = BranchUtils.GetBranchCode(branchName);
					}
				}
			}
			catch { }
			return branchCode;
		}


		private string GetRegionBranchCodes(string rptType)
		{
			// get region associated with user
			ApplicationUser user = db.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
			int userRegionId = user.RegionId.Value;

			// branch list box for region
			RegionBranch regionBranch = new RegionBranch();
			List<Branch> producingBranches;

			producingBranches = GetBranchesForRptType(rptType, user, userRegionId, regionBranch);

			BranchListBoxRegions(producingBranches);

			YearListBox();

			// region name
			GetRegionName(userRegionId);

			// selected branch code
			string branchCode = GetSelectedBranchCodeRegion(producingBranches);

			return branchCode;
		}

		private List<Branch> GetBranchesForRptType(string rptType, ApplicationUser user, int userRegionId, RegionBranch regionBranch)
		{
			
			List<Branch> producingBranches;
			if (BranchUtils.prodBranchRptFlag(User.IsInRole("Division Mgr"), rptType))
			{
				int userBranchId = user.BranchId;
				// Divisional users can't see their own branch
				List<Branch> regionBranches = regionBranch.GetDivisionBranches(userRegionId, userBranchId, db);

				vwProducingBranch branchNonProducing = new vwProducingBranch();
				producingBranches = branchNonProducing.GetProducingBranches(userRegionId, userBranchId, regionBranches, db);

				ViewBag.ProdBranchRpt = true;
			}
			else if (BranchUtils.prodBranchRptFlag(User.IsInRole("Region"), rptType))
			{
				int userBranchId = user.BranchId;
				List<Branch> regionBranches = regionBranch.GetRegionBranches(userRegionId, db);

				vwProducingBranch branchNonProducing = new vwProducingBranch();
				producingBranches = branchNonProducing.GetProducingBranches(userRegionId, userBranchId, regionBranches, db);

				ViewBag.ProdBranchRpt = true;
			}
			else // show all branches for Division and Regional users for the P&L reports)
			{
				producingBranches = regionBranch.GetRegionBranches(userRegionId, db);
			}

			return producingBranches;
		}

		private void GetRegionName(int userRegionId)
		{
			string regionName = db.Regions.Where(r => r.RegionId == userRegionId).Select(r => r.RegionName).FirstOrDefault();
			ViewBag.RegionName = regionName;
		}


		private string GetSelectedBranchCodeRegion(List<Branch> regionBranches)
		{
			string branchCode = GetSelectedBranchCode();

			// compare selected branch to user's list of region's branches to ensure that it really belongs to them - avoid spoofing
			branchCode = GetPermittedBranches(regionBranches, branchCode);
			return branchCode;
		}


		private string GetPermittedBranches(List<Branch> branches, string branchCode)
		{
			// compare selected branch to user's list of region's branches to ensure that it really belongs to them - avoid spoofing
			var permittedBranches = branches.Where(x => x.BranchName.Substring(0, 6).Replace(" ", "") == branchCode).FirstOrDefault();

			if (permittedBranches != null)
			{
				branchCode = BranchUtils.GetBranchCode(permittedBranches.BranchName);
			}
			else
			{
				branchCode = null;
			}
			return branchCode;
		}


		private string GetBranchCodeForRpt(string rptType)
		{
			string branchCode = "";

			if (User.IsInRole("Admin") || User.IsInRole("Global"))
			{
				branchCode = GetSelectedBranchCode();
			}
			else if (User.IsInRole("Region") || User.IsInRole("Division Mgr"))
			{
				// region user logic
				branchCode = GetRegionBranchCodes(rptType);

			}
			else if (User.IsInRole("Branch") || User.IsInRole("Non-Producing Branch Mgr"))
			{
				// Get the branch code for this users branch - no choices allowed
				string branchName = BranchUtils.GetBranchName(db, User);
				branchCode = BranchUtils.GetBranchCode(branchName);
			}

			return branchCode;
		}


		private static void SetExpenseMapPath(AroReport aro)
		{
			if (aro.Year == 2014)
			{
				aro.ExpenseMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\ExpenseAcntMap2014.txt";
				aro.BonusMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\BonusAcntMap2014.txt";
			}
			else
			{
				aro.ExpenseMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\ExpenseAcntMap.txt";
				aro.BonusMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\BonusAcntMap.txt";
			}
		}


		private static void SetExpenseTotalPath(AroReport aro)
		{
			if (aro.Year == 2014)
			{
				aro.ExpenseTotalPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\ExpenseTotalMap2014.txt";
			}
			else
			{
				aro.ExpenseTotalPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\ExpenseTotalMap.txt";
			}
		}


		private static string GetCogsPath(int year)
		{
			string cogsMapPath = "";

			if (year == 2014)
			{
				cogsMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\CogsAcntMap2014.txt";
			}
			else
			{
				cogsMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\CogsAcntMap.txt";
			}

			return cogsMapPath;
		}

		
		private static string GetTemplateRsrvAcctPath(int yearSelected)
		{
			string templateRsrvAcctPath;
			// get template of Reserve Acct Rpt by selected by year
			if (yearSelected == 2014)
			{
				templateRsrvAcctPath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\ReserveAccts2014.htm";
			}
			else if(yearSelected == 2015)

			{
				templateRsrvAcctPath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\ReserveAccounts2015.htm";
			}
			else
			{
				templateRsrvAcctPath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\ReserveAccounts.htm";
			}

			return templateRsrvAcctPath;
		}


		private static void SetBonusAcctNum(int yearSelected, ReserveAccounts reserveAcct)
		{
			// todo: move this to an external map that is set by the client
			if (yearSelected == 2014)
			{
				reserveAcct.BonusPaidAcct = "2343";
				reserveAcct.BonusVolAcct = "6024";
			}
			else
			{				
				reserveAcct.BonusVolAcct = "6024";
				reserveAcct.LoanLossRsrvUtilized = "2706";

				// kludge: 
				string path = System.Web.HttpContext.Current.Request.Url.AbsolutePath;
				System.IO.FileInfo info = new System.IO.FileInfo(path);

				if (info.Name == "ProfitAndLossReserveRpt")
				{
					reserveAcct.BonusPaidAcct = "6025"; // non-producing managers draw
				}
				else
				{
					reserveAcct.BonusPaidAcct = "2316"; // AR Override Paid
				}
			}
		}


		private string AroReportString(string branchCode, int yearSelected)
		{
			string aroRpt;
			string templateAroPath = GetTemplatePath(yearSelected);

			AroReport aro = new AroReport(templateAroPath, yearSelected);

			aro.Year = yearSelected;
			aro.BranchId = branchCode;

			// set row mapping file path
			SetExpenseMapPath(aro);
			SetExpenseTotalPath(aro);

			aroRpt = aro.GetAroReport();
			string templateRsrvAcctPath = GetTemplateRsrvAcctPath(yearSelected);

			string cogsMapPath = GetCogsPath(yearSelected);

			ReserveAccounts reserveAcct = new ReserveAccounts(templateRsrvAcctPath, cogsMapPath, yearSelected);

			// reserve report is included in the AROS report but hidden for calc purpose and visa-versa
			reserveAcct.BranchId = branchCode;
			reserveAcct.Year = yearSelected;

			SetBonusAcctNum(yearSelected, reserveAcct);
			ViewBag.ReserveAcctRpt = reserveAcct.GetReserveAcctReport();
			ViewBag.YearSelected = yearSelected;

			return aroRpt;
		}


		private static string GetTemplatePath(int yearSelected)
		{
			string templatePath;
			if (yearSelected == 2014)
			{
				templatePath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\ARO2014.htm";
			}
			else if (yearSelected == 2015)
			{
				templatePath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\ARO2015.htm";
			}
			else
			{
				templatePath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\ARO.htm";
			}
			return templatePath;
		}


		private void BranchListBoxRegions(List<Branch> regionBranches)
		{
			// list box for branches
			var branches = regionBranches.OrderBy(x => x.BranchName);
			ViewBag.BranchListBox = new SelectList(branches, "BranchName", "BranchName", Request.Form.Get("lstBranchId"));
		}


		private bool IsFormPosted(string branchCode)
		{
			if (string.IsNullOrEmpty(branchCode) == false)
			{
				return true;
			}
			else
			{
				return false;
			}
		}


		private void GetBranchListBoxForRole()
		{
			if (!User.IsInRole("Region") && !User.IsInRole("Division Mgr"))
			{
				// region list box set GetBranchCodeForRpt()
				int year;
				if (string.IsNullOrEmpty(Request.Form.Get("lstYear")) == false)
					year = int.Parse(Request.Form.Get("lstYear"));
				else
					year = DateTime.Now.Year;

				BranchesListBox(year);
			}
		}


		private void YearListBox(int startYear = 2014)
		{
			// year list box
			List<int> years = new List<int>();
			string branch = BranchUtils.GetBranchName(db, User);

			for (int year = DateTime.Now.Year; year >= startYear; year--)
			{
				// don't display 2014 if user is Bakersfield or Visalia
				// todo: this is temporary until we come up with a programmatic solution
				if (year == 2014 && branch != "CA 612 Bakersfield" && branch != "CA 222 Visalia")
				{
					years.Add(year);
				}
				else if (year != 2014)
				{
					years.Add(year);
				}
			}

			if (string.IsNullOrEmpty(Request.Form.Get("lstYear")) == false)
			{
				int yearSelected = GetSelectedYear();
				ViewBag.YearListBox = new SelectList(years, yearSelected);
				ViewBag.YearSelected = yearSelected;
			}
			else
			{
				ViewBag.YearListBox = new SelectList(years);
			}
		}


		private void YearListBoxCurrent()
		{
			// year list box
			List<int> years = new List<int>();

			years.Add(DateTime.Now.Year);

			if (string.IsNullOrEmpty(Request.Form.Get("lstYear")) == false)
			{
				int yearSelected = GetSelectedYear();
				ViewBag.YearListBox = new SelectList(years, yearSelected);
				ViewBag.YearSelected = yearSelected;
			}
			else
			{
				ViewBag.YearListBox = new SelectList(years);
			}
		}


		private void BranchesListBox(int year)
		{
			if (year == 0)
			{
				year = DateTime.Now.Year;
			}

			// list box for branches
			if (year == DateTime.Now.Year)
			{
				// current year
				ViewBag.BranchListBox = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchName", "BranchName", Request.Form.Get("lstBranchId"));
			}
			else
			{
				// past years
				var branchArchives = (from b in ba.BranchesArchives select new { b.BranchName, b.Year }).Where(b => b.Year == year).OrderBy(b => b.BranchName);

				ViewBag.BranchListBox = new SelectList(branchArchives, "BranchName", "BranchName", Request.Form.Get("lstBranchId"));
			}
		}


		private int GetSelectedYear()
		{
			int yearSelected = DateTime.Now.Year;

			if (Request.Form.HasKeys())
			{
				if (Request.Form.Get("lstYear") != null)
				{
					int.TryParse(Request.Form.Get("lstYear"), out yearSelected);
				}
			}

			return yearSelected;
		}


		private static string GetPrintableReport(string aroRpt, string reserveAcctRpt)
		{
			// render the view first - clean-up the width so it looks right on paper

			// combine the aro with the reserve report
			string printableReport = aroRpt + "<div style='display: none'>" + reserveAcctRpt + "</div>";
			printableReport = printableReport.Replace("width:960pt", "");
			printableReport = printableReport.Replace("width:2191pt", "");
			printableReport = printableReport.Replace("width=1280", "");
			printableReport = printableReport.Replace("width=2925", "");
            printableReport = printableReport.Replace("collapsible", "");
            printableReport = printableReport.Replace("collapsed", "");
            printableReport = printableReport.Replace("closed", "");
            printableReport = "<html><body>" + printableReport + "</body></html>";

			return printableReport;
		}


		private static string GetPrintablePAndLReport(string pAndLRpt)
		{
			// render the view first - clean-up the width so it looks right on paper

			// combine the aro with the reserve report
			string printableReport = pAndLRpt;
			printableReport = printableReport.Replace("width:960pt", "");
			printableReport = printableReport.Replace("width:2191pt", "");
			printableReport = printableReport.Replace("width=1280", "");
			printableReport = printableReport.Replace("width=2925", "");
			printableReport = printableReport.Replace("collapsible", "");
            printableReport = printableReport.Replace("collapsed", "");
            printableReport = printableReport.Replace("closed", "");

            printableReport = "<html><body>" + printableReport + "</body></html>";

			return printableReport;
		}

		private string ReserveAccountsString(string branchCode, int yearSelected)
		{
			string reserveAcctRpt = "";

			// Reserve Accounts Report - select by year
			string templateRsrvAcctPath = GetTemplateRsrvAcctPath(yearSelected);

			string cogsMapPath = GetCogsPath(yearSelected);

			ReserveAccounts reserveAcct = new ReserveAccounts(templateRsrvAcctPath, cogsMapPath, yearSelected);

			reserveAcct.BranchId = branchCode;
			reserveAcct.Year = yearSelected;

			//
			reserveAcct.CogsMapPath = cogsMapPath;
			reserveAcct.ExpenseAcctsPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\Expenses.txt";
			SetBonusAcctNum(yearSelected, reserveAcct);
			//
			SetBonusAcctNum(yearSelected, reserveAcct);
		   
			if (yearSelected <= 2015)
			{
				// 2014 & 2015 uses different rules from 2016 (server-side allocation model) so use javascript version
				reserveAcctRpt = reserveAcct.GetReserveAcctReport();
				ViewBag.AroRpt = AroReportString(branchCode, yearSelected);
			}
			else
			{
				reserveAcctRpt = reserveAcct.GetAllocationsReport();
				ViewBag.AroRpt = "";
			}

			ViewBag.BranchId = branchCode;

			ViewBag.YearSelected = yearSelected;

			return reserveAcctRpt;
		}


		private string ProfitAndLossRptString(string branchCode, int yearSelected)
		{
			string plRpt;
			string templatePLPath;
			
			if (yearSelected == 2015)
			{
				// 2015 uses different rules so use javascript version
				templatePLPath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\ProfitAndLoss2015.htm";
			}
			else
			{
				templatePLPath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\ProfitAndLoss.htm";
			}

			ProfitAndLossReport profitAndLoss = new ProfitAndLossReport(templatePLPath, yearSelected);

			profitAndLoss.Year = yearSelected;
			profitAndLoss.BranchId = branchCode;

			// set row mapping file path
			profitAndLoss.ExpenseMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\ExpenseAcntMap.txt";
			profitAndLoss.BonusMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\PLRevneueBonusAcntMap.txt";
			profitAndLoss.ExpenseTotalPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\ExpenseTotalMap.txt";
			profitAndLoss.RevenueIncMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\PLRevneueAcntMap.txt";
			profitAndLoss.RevenueIncTotalMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\PLRevneueTotalMap.txt";

			plRpt = profitAndLoss.GetProfitAndLossReport();
			ViewBag.YearSelected = yearSelected;
			ViewBag.BranchId = branchCode;
			ViewBag.RptType = "P&L";

			return plRpt;
		}


		private string ProfitAndLossReserveRptString(string branchCode, int yearSelected)
		{
			string plRpt;
			string templatePLPath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\ProfitLossReserveAccounts.htm";
			
			string cogsMapPath = GetCogsPath(yearSelected);

			ProfitAndLossRsrvReport reserveAcct = new ProfitAndLossRsrvReport(templatePLPath,  yearSelected);
			reserveAcct.BranchId = branchCode;
			reserveAcct.Year = yearSelected;
			reserveAcct.CogsMapPath = cogsMapPath;
			reserveAcct.RevenueIncMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\PLRevneueAcntMap.txt";
			reserveAcct.RevenueIncTotalMapPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\PLRevneueTotalMap.txt";
			reserveAcct.ExpenseAcctsPath = AppDomain.CurrentDomain.BaseDirectory + "\\App_Code\\Expenses.txt";
			SetBonusAcctNum(yearSelected, reserveAcct);

			if (yearSelected == 2015)
			{
				// 2015 uses different rules so use javascript version
				reserveAcct.TemplateFilePath = AppDomain.CurrentDomain.BaseDirectory + "Templates\\ProfitLossReserveAccounts2015.htm";
				plRpt = reserveAcct.GetProfitAndLossReport();
			}
			else
			{
				plRpt = reserveAcct.GetAllocationsReport();
			}

			plRpt = plRpt.Replace("�", " ");
			
			return plRpt;
		}

		
		#endregion
		}
	}