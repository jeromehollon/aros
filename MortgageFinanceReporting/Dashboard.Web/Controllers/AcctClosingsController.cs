﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dashboard.Web.Models;

namespace Dashboard.Web.Controllers
{
    [Authorize(Roles = "Admin, Global")]
    public class AcctClosingsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: AcctClosings
        public ActionResult Index()
        {
            return View(db.AcctClosings.ToList());
        }

        // GET: AcctClosings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AcctClosing acctClosing = db.AcctClosings.Find(id);
            if (acctClosing == null)
            {
                return HttpNotFound();
            }
            return View(acctClosing);
        }

        // GET: AcctClosings/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: AcctClosings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "AcctClosingId,AcctClosingDate")] AcctClosing acctClosing)
        {
            if (ModelState.IsValid)
            {
                db.AcctClosings.Add(acctClosing);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(acctClosing);
        }

        // GET: AcctClosings/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AcctClosing acctClosing = db.AcctClosings.Find(id);
            if (acctClosing == null)
            {
                return HttpNotFound();
            }
            return View(acctClosing);
        }

        // POST: AcctClosings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "AcctClosingId,AcctClosingDate")] AcctClosing acctClosing)
        {
            if (ModelState.IsValid)
            {
                db.Entry(acctClosing).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(acctClosing);
        }

        // GET: AcctClosings/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AcctClosing acctClosing = db.AcctClosings.Find(id);
            if (acctClosing == null)
            {
                return HttpNotFound();
            }
            return View(acctClosing);
        }

        // POST: AcctClosings/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AcctClosing acctClosing = db.AcctClosings.Find(id);
            db.AcctClosings.Remove(acctClosing);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
