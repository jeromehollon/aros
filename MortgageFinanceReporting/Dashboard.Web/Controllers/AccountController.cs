﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Dashboard.Web.Models;
using Dashboard.Web.Shared;
using Elmah;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;

namespace Dashboard.Web.Controllers
{
#if !DEBUG
    [RequireHttps] //apply to all actions in controller
#endif
    [Authorize]
    public class AccountController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                    await SignInAsync(user, model.RememberMe);

                    // todo: move these to a new seed function                    
                    var roleManager =
                        new RoleManager<Microsoft.AspNet.Identity.EntityFramework.IdentityRole>(
                            new RoleStore<IdentityRole>(new ApplicationDbContext()));

                    if (!roleManager.RoleExists("Non-Producing Branch Mgr"))
                    {
                        var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                        role.Name = "Non-Producing Branch Mgr";
                        roleManager.Create(role);
                    }

                    if (!roleManager.RoleExists("Division Mgr"))
                    {
                        var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                        role.Name = "Division Mgr";
                        roleManager.Create(role);
                    }

                    if (!roleManager.RoleExists("Global"))
                    {
                        var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                        role.Name = "Global";
                        roleManager.Create(role);
                    }

                    // todo: record IPs in a db for a week before cutting people off
                    try
                    {
                        RecordIpAddress(model.UserName);
                    }
                    catch (Exception ex) {
                        ErrorSignal.FromCurrentContext().Raise(ex);
                    }

                return RedirectToLocal(returnUrl);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        private static void RecordIpAddress(string userName)
        {
            // record user's IP address to establish trusted access
            string ipAddr = _5XUtilities.UserBrowserInfo.GetIPAddress();
            Entities db = new Entities();

            UserIpAddr userIpAddr = new UserIpAddr();
            userIpAddr.IpAddr = ipAddr;
            userIpAddr.UserName = userName;

            db.UserIpAddrs.Add(userIpAddr);
            db.SaveChanges();
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            // drop down list boxes             
            ViewBag.BranchId = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchId", "BranchName");
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser() { UserName = model.UserName, BranchId = model.BranchId, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName };

                try
                {
                    var result = await UserManager.CreateAsync(user, model.Password);


                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);

                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        AddErrors(result);
                        // drop down list boxes             
                        ViewBag.BranchId = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchId", "BranchName");
                        ViewBag.RegionList = new SelectList(db.Regions.OrderBy(r => r.RegionName), "RegionId", "RegionName");
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", GetErrorMsg(ex));

                    // drop down list boxes             
                    ViewBag.BranchId = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchId", "BranchName");
                    ViewBag.RegionList = new SelectList(db.Regions.OrderBy(r => r.RegionName), "RegionId", "RegionName");

                    return View(model);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Users()
        {

            var users = Membership.GetAllUsers();
            return View(users);
        }
        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2213:DisposableFieldsShouldBeDisposed", MessageId = "db")]
        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region LostPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(string userName)
        {
            //check user existence
            ApplicationUser user = db.Users.Where(u => u.UserName.ToUpper() == userName.ToUpper()).FirstOrDefault();

            if (user == null)
            {
                TempData["Message"] = "Sorry, we can't find that username.";
            }
            else
            {
                //generate password token
                string token = GetRandomToken();

                user.PasswordVerificationToken = token;

                db.SaveChanges();

                //create url with above token
                var resetLink = String.Format("<a href='{0}'>Reset Password</a>", Url.Action("ResetPassword", "Account", new { un = userName, rt = token }, "http"));
                //get user email

                var email = (from i in db.Users
                             where i.UserName == userName
                             select i.Email).FirstOrDefault();
                //send mail
                const string Subject = "AROS Account Password Reset Token";
                string body = String.Format("Dear AROS User,<br /><br />You lost your password so we found a new one for you. Please click to {0}<br /><br />The AROS Support Team", resetLink);
                try
                {
                    ICommunications comService = new Communications();
                    comService.SendEMail(email, Subject, body);
                    TempData["Message"] = "Mail Sent.";
                }
                catch (Exception ex)
                {
                    // log exception
                    TempData["Message"] = "Error occurred while sending email." + ex.Message;

                    // create an instance of the event
                    LogEvent eee = new LogEvent(String.Format("{0}: To: {1}: Message: {2}", ex, email, body), this);
                    eee.Raise();

                    TempData["Message"] = String.Format("Sorry, you encountered an error ({0})", ex.Message);
                }
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string un, string rt)
        {
            // check the username and token matching and then perform following
            //get user id of received username
            //var userId = User.Identity.GetUserId();

            //check user id and token matches
            bool any = (from j in db.Users
                        where (j.UserName == un)
                        && (j.PasswordVerificationToken == rt)
                        select j).Any();

            if (any == true)
            {
                //generate random password
                string newpassword = GenerateRandomPassword(8);

                //reset password                
                bool response = true;

                try
                {
                    ApplicationUser user = db.Users.Where(u => u.UserName.ToUpper() == un.ToUpper()).FirstOrDefault();

                    String hashedNewPassword = UserManager.PasswordHasher.HashPassword(newpassword);
                    using (UserStore<ApplicationUser> store = new UserStore<ApplicationUser>())
                    {
                        store.SetPasswordHashAsync(user, hashedNewPassword);
                        store.UpdateAsync(user);
                    }

                    user.PasswordHash = hashedNewPassword;
                    db.SaveChanges();
                }
                catch
                {
                    response = false;
                    throw;
                }


                if (response == true)
                {
                    //get user email to send password
                    var email = (from i in db.Users
                                 where i.UserName == un
                                 select i.Email).FirstOrDefault();
                    //send email
                    const string Subject = "New Password";
                    string body = String.Format("Here is the new password you requested:<br/>{0}<p>Please login and change your password as soon as possible", newpassword); //edit it
                    try
                    {
                        ICommunications comService = new Communications();
                        comService.SendEMail(email, Subject, body);
                        TempData["Message"] = "Mail Sent.";
                    }
                    catch (Exception ex)
                    {
                        TempData["Message"] = "Error occurred while sending email." + ex.Message;
                        // create an instance of the event
                        LogEvent eee = new LogEvent(String.Format("{0}: To: {1}: Message: {2}", ex, email, body), this);
                        eee.Raise();
                    }

                    //display message
                    TempData["Message"] = "Success! Check your email. It contains your new password. It may take a few minutes for you to receive it so be patient.";
                }
                else
                {
                    TempData["Message"] = "Hey, avoid random request on this page.";
                }
            }
            else
            {
                TempData["Message"] = "Username and token did not match.";
            }

            return View();
        }

        // added for password recovery
        private static string GenerateRandomPassword(int length)
        {
            const string AllowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-*&#+";
            char[] chars = new char[length];
            Random rd = new Random();
            for (int i = 0; i < length; i++)
            {
                chars[i] = AllowedChars[rd.Next(0, AllowedChars.Length)];
            }
            return new string(chars);
        }

        private static string GetRandomToken()
        {
            using (RandomNumberGenerator rng = new RNGCryptoServiceProvider())
            {
                byte[] tokenData = new byte[32];
                rng.GetBytes(tokenData);

                string token = Convert.ToBase64String(tokenData);

                return token;
            }
        }

        #endregion

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        private static string GetErrorMsg(Exception e)
        {
            string errorMsg = "";
            if (e.InnerException.ToString().Contains("IX_AspNetUsers_Email"))
            {
                errorMsg = "The email you entered is already taken. Please us a new one or recover the password from the login page.";
            }
            else
            {
                errorMsg = e.Message;
            }
            return errorMsg;
        }
        #endregion
    }
}