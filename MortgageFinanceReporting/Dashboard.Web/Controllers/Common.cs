﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using Dashboard.Web.Models;
using Microsoft.AspNet.Identity;

namespace Dashboard.Web.Controlers
{
    public class Common
    {

		public static string GetBranchName(ApplicationDbContext db, IPrincipal currentUser)
		{            
			var userStore = new Microsoft.AspNet.Identity.EntityFramework.UserStore<ApplicationUser>();
			var userManager = new UserManager<ApplicationUser>(userStore);

			var user = userManager.FindById(currentUser.Identity.GetUserId());

			string branchName = "";

			if (user != null)
			{
				Branch branch = db.Branches.Find(user.BranchId);

				branchName = branch.BranchName.ToString();               
			}

			return branchName;
		}


		public static string GetBranchCode(string branchName)
		{
			string branchCode = "";

			if (string.IsNullOrWhiteSpace(branchName) == false)
			{
				if (branchName.Length > 5)
				{
					if (branchName.IndexOf(" ", 5) > 0)
					{
						branchCode = branchName.Substring(0, branchName.IndexOf(" ", 5)).Replace(" ", "");
					}
				}
			}

			return branchCode;
		}
    }
}