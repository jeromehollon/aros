﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dashboard.Web.Models;
using System.IO;
using System.Collections;

namespace Dashboard.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BranchPostingsJobsController : Controller
    {
        private Entities db = new Entities();

        // GET: BranchPostingsJobs
        public ActionResult Index()
        {
            var postingJobs = db.BranchPostingsJobs.ToList().LastOrDefault();

            if (postingJobs == null)
            {
                return RedirectToAction("Create", "BranchPostingsJobs");
            }
            else
            {
                ViewBag.CompleteDate = postingJobs.CompleteDate;
                ViewBag.JobId = postingJobs.JobId;

                return View(postingJobs);
            }
        }

        // GET: BranchPostingsJobs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BranchPostingsJob branchPostingsJob = db.BranchPostingsJobs.Find(id);
            if (branchPostingsJob == null)
            {
                return HttpNotFound();
            }
            return View(branchPostingsJob);
        }

        // GET: BranchPostingsJobs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BranchPostingsJobs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RequestDate, Month, Year")] BranchPostingsJob branchPostingsJob)
        {
            if (ModelState.IsValid)
            {
                db.BranchPostingsJobs.Add(branchPostingsJob);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(branchPostingsJob);
        }

        // GET: BranchPostingsJobs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BranchPostingsJob branchPostingsJob = db.BranchPostingsJobs.Find(id);
            if (branchPostingsJob == null)
            {
                return HttpNotFound();
            }
            return View(branchPostingsJob);
        }

        // POST: BranchPostingsJobs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "JobId,RequestDate,Month,Year,CompleteDate")] BranchPostingsJob branchPostingsJob)
        {
            if (ModelState.IsValid)
            {
                db.Entry(branchPostingsJob).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(branchPostingsJob);
        }

        // GET: BranchPostingsJobs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BranchPostingsJob branchPostingsJob = db.BranchPostingsJobs.Find(id);
            if (branchPostingsJob == null)
            {
                return HttpNotFound();
            }
            return View(branchPostingsJob);
        }

        // POST: BranchPostingsJobs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BranchPostingsJob branchPostingsJob = db.BranchPostingsJobs.Find(id);
            db.BranchPostingsJobs.Remove(branchPostingsJob);
            db.SaveChanges();
            return RedirectToAction("Index");
        }


       
        public ActionResult CtrlRptLLR(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.JobId = id;

            return View(db.vwBranchPostingsLoanLosses.ToList().FindAll(b => b.JobId == id).ToList());
        }


        public ActionResult DownloadLLR(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Response.AddHeader("Content-Type", "text/csv");
            Response.AddHeader("charset", "utf-8");
            Response.AddHeader("Content-Disposition", "attachment; filename=LoanLossRsrv.csv");

            return View(db.vwBranchPostingsLoanLosses.ToList().FindAll(b => b.JobId == id).ToList());
        }


        public ActionResult CtrlRptARO(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.JobId = id;
            return View(db.vwBranchPostingsAROes.ToList().FindAll(b => b.JobId == id).ToList());
        }


        public ActionResult DownloadARO(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Response.AddHeader("Content-Type", "text/csv");
            Response.AddHeader("charset", "utf-8");
            Response.AddHeader("Content-Disposition", "attachment; filename=AROOveride.csv");

            return View(db.vwBranchPostingsAROes.ToList().FindAll(b => b.JobId == id).ToList());
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
