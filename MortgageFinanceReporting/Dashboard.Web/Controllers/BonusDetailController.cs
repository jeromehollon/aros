﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Reports;

// TODO: remove this
namespace Dashboard.Web.Controllers
{
    public class BonusDetailController : Controller
    {
        // GET: BonusDetail
      		public ActionResult Index(string id, string branchId, int year)
		{
			AroReport aro = new AroReport();

			string monthLetter = id.Substring(0, 1);
			
			string acctNum = id.Replace(monthLetter, "");

			string expenseDetail = aro.GetExpenseDetailRpt(branchId, monthLetter, year, acctNum);
			ViewBag.DetailRpt = expenseDetail;
			ViewBag.id = id;
			ViewBag.branchId = branchId;

			return View();
		}
    }
}