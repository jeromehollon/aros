﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using Dashboard.Web.Shared;
using Reports;

namespace Dashboard.Web.Controllers
{
	#if !DEBUG
		[RequireHttps] //apply to all actions in controller
	#endif
	[Authorize()]
	public class RevenueDetailController : Controller
	{  
		//
		// GET: /RevenueDetail/
		public ActionResult Index(string id, string branchId, int year)
		{
			AroReport aro = new AroReport("", year);

			string monthLetter = id.Substring(0, 1);			

			string revenueDetail = aro.GetRevenueDetailRpt(branchId, monthLetter, year);
			ViewBag.DetailRpt = revenueDetail;
			ViewBag.id = id;
			ViewBag.branchId = branchId;
			ViewBag.Year = year;

			return View();
		}


		public ActionResult RevenueDetailToPdf(string id, string branchId, int year)
		{           
			 AroReport aro = new AroReport("", year);
			
			string monthLetter= id.Substring(0, 1);
			
			string revenueDetail = aro.GetRevenueDetailRpt(branchId, monthLetter, year);
			ViewBag.DetailRpt = revenueDetail;

			// send the request to the pdf server
			string fileName = PdfPrint.GetPdfFileName(revenueDetail, Server.MapPath("~"));

			Response.Redirect("https://do.convertapi.com/Web2Pdf?APIKey=635362747&PageOrientation=landscape&CUrl=https://" + ConfigurationManager.AppSettings["url"] + "/TempPages/" + fileName);
			return null;           
		}
	}
}