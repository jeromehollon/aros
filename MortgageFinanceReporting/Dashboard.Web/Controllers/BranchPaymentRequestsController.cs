﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Dashboard.Web.Models;
using Dashboard.Web.Shared;
using Elmah;

namespace Dashboard.Web.Controllers
{
	#if !DEBUG
			[RequireHttps] //apply to all actions in controller
	#endif
	[Authorize()]
	public class BranchPaymentRequestsController : Controller
	{
		private Entities db = new Entities();

		// GET: BranchPaymentRequests
		public ActionResult IndexForNonAdmin()
		{
			var branchPaymentRequests = db.vwBranchPaymentRequests;
			ApplicationDbContext dbApp = new ApplicationDbContext();
			ApplicationUser user = dbApp.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

            if (HttpContext.Request.QueryString["maxAvailForDraws"] != null)
            {
                Session["MaxAvailForDraws"] = HttpContext.Request.QueryString["maxAvailForDraws"].Replace("'", "");
            }
            else
            {
                Session["MaxAvailForDraws"] = 0;
            }

            ViewBag.MaxAvailForDraws = Session["MaxAvailForDraws"];

			if (User.IsInRole("Admin") || User.IsInRole("Global"))
			{    
				// admin and multi-branch managers should see the request that corresponds the branch in their report - session val is set in the report controller            
				string branchCode = Session["BranchId"].ToString();
				return View(branchPaymentRequests.Where(b => b.BranchName.Substring(0, 6).Replace(" ", "") == branchCode).ToList());
			}
			else if (User.IsInRole("Region") || User.IsInRole("Division Mgr"))
			{
				// admin and multi-branch managers should see the request that corresponds the branch in their report - session val is set in the report controller            
				string branchCode = Session["BranchId"].ToString();

				// get pending request so we know if we should allow the user to add a request 
				var pendingRequest = branchPaymentRequests.Where(b => b.BranchId == user.BranchId && (b.StatusType == null || b.StatusType == "Pending Review"));
				ViewBag.PendingRequest = pendingRequest.Count();

				return View(branchPaymentRequests.Where(b => b.BranchName.Substring(0, 6).Replace(" ", "") == branchCode).ToList());
			}
			else
			{
				// branch manager - return their branch - get pending request so we know if we should allow the user to add a request       
				var pendingRequest = branchPaymentRequests.Where(b => b.BranchId == user.BranchId && (b.StatusType == null || b.StatusType == "Pending Review"));
				ViewBag.PendingRequest = pendingRequest.Count();

				return View(branchPaymentRequests.Where(b => b.BranchId == user.BranchId).ToList());
			}
		}
		public ActionResult Index()
		{
			var branchPaymentRequests = db.vwBranchPaymentRequests;
			if (User.IsInRole("Admin"))
			{
				return View(branchPaymentRequests.ToList());
			}
			else
			{
				ApplicationDbContext dbApp = new ApplicationDbContext();
				ApplicationUser user = dbApp.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
				return View(branchPaymentRequests.Where(b => b.BranchId == user.BranchId).ToList());
			}
		}
	

		// GET: BranchPaymentRequests/Request
		public ActionResult Request()
		{
			ViewBag.MaxAvailForDraws = HttpContext.Request.QueryString["maxAvailForDraws"].Replace("'", ""); ;
			return View();
		}


		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Request([Bind(Include = "BranchPaymentRequestId,AmtRequestd,Comment")] BranchPaymentRequest branchPaymentRequest)
		{

			var errors = new List<string>();
			StringBuilder sb = new StringBuilder();

			if (ModelState.IsValid)
			{
				try
				{
					// set values based the user's profile
					ApplicationDbContext dbApp = new ApplicationDbContext();
					ApplicationUser user = dbApp.Users.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();

                    branchPaymentRequest.Requestor = User.Identity.Name;
					// this should be the selected branch - not just the one associated w/ the user when a regional or divisional requests payment
					if (Session["BranchId"] == null)
					{
						branchPaymentRequest.BranchId = user.BranchId;
					}
					else
					{
						branchPaymentRequest.BranchId = BranchUtils.GetBranchId(dbApp, Session["BranchId"].ToString());
					}
					branchPaymentRequest.DateRequested = DateTime.Now;

					db.BranchPaymentRequests.Add(branchPaymentRequest);

					db.SaveChanges();


					//send mail
					string Subject = "AROS - Request For Payment From " + User.Identity.Name;
					string body = "AROS Admin,<br /><br />" + BranchUtils.GetBranchName(dbApp, branchPaymentRequest.BranchId) + 
						" has requested a payment for " + 
						String.Format("{0:c}", Convert.ToDecimal(branchPaymentRequest.AmtRequestd) +
                        ". Please got to https://aros.goalterra.com//branchpaymentrequests<br /><br />The AROS Support Team");
					string email = ConfigurationManager.AppSettings["RequestPaymentEmail"];

                    try
					{
						ICommunications comService = new Communications();
						comService.SendEMail(email, Subject, body);
						TempData["Message"] = "Mail Sent.";
					}
					catch (Exception ex)
					{
						// log exception
						TempData["Message"] = "Error occurred while sending email." + ex.Message;

						// create an instance of the event
						LogEvent eee = new LogEvent(String.Format("{0}: To: {1}: Message: {2}", ex, email, body), this);
						eee.Raise();

						TempData["Message"] = String.Format("Sorry, you encountered an error ({0})", ex.Message);
					}

					string maxAvailForDraws = HttpContext.Request.QueryString["maxAvailForDraws"];
					return RedirectToAction("IndexForNonAdmin", new { maxAvailForDraws = maxAvailForDraws });
				}
				catch (DbEntityValidationException v)
				{
					foreach (var error in v.EntityValidationErrors)
					{
						foreach (var msg in error.ValidationErrors)
						{
							errors.Add(msg.ErrorMessage);
							sb.Append(msg.ErrorMessage + "<br />");
						}
					}

					Exception e = new Exception("Validation Error " + errors);
					ViewBag.ErrorMsg = sb.ToString();
					ErrorSignal.FromCurrentContext().Raise(e);
				}
			}
			else
			{
				foreach (var state in ModelState)
				{
					foreach (var error in state.Value.Errors)
					{
						errors.Add(error.ErrorMessage);
						sb.Append(error.ErrorMessage + "<br />");
					}
				}

				Exception e = new Exception("Validation Error " + errors);
				ViewBag.ErrorMsg = sb.ToString();
				ErrorSignal.FromCurrentContext().Raise(e);
			}           

			ViewBag.BranchId = new SelectList(db.Branches, "BranchId", "BranchName", branchPaymentRequest.BranchId);
			return View(branchPaymentRequest);
		}


		// GET: BranchPaymentRequests/Create
		public ActionResult Create()
		{
			if ((User.IsInRole("Admin") || User.IsInRole("Global") == false)) {
				Redirect("~/BranchPaymentRequests/Request");
			}
			ViewBag.BranchId = new SelectList(db.Branches, "BranchId", "BranchName");
			return View();
		}

		// POST: BranchPaymentRequests/Create
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Create([Bind(Include = "BranchPaymentRequestId,BranchId,DateRequested,Requestor,AmtRequestd,IsApproved,Comment")] BranchPaymentRequest branchPaymentRequest)
		{

			var errors = new List<string>();
			StringBuilder sb = new StringBuilder();

			if (ModelState.IsValid)
			{
				try { 
					db.BranchPaymentRequests.Add(branchPaymentRequest);
					db.SaveChanges();
					return RedirectToAction("Index");
				}
				catch (DbEntityValidationException v)
				{                    
					foreach( var error in v.EntityValidationErrors)
					{
						foreach (var msg in error.ValidationErrors)
						{
							errors.Add(msg.ErrorMessage);
							sb.Append(msg.ErrorMessage + "<br />");
						}
					}

					Exception e = new Exception("Validation Error " + errors);
					ViewBag.ErrorMsg = sb.ToString();
					ErrorSignal.FromCurrentContext().Raise(e);
				}
			}
			else
			{
				foreach (var state in ModelState)
				{
					foreach (var error in state.Value.Errors)
					{
						errors.Add(error.ErrorMessage);
						sb.Append(error.ErrorMessage + "<br />");
					}
				}

				Exception e = new Exception("Validation Error " + errors);
				ViewBag.ErrorMsg = sb.ToString();
				ErrorSignal.FromCurrentContext().Raise(e);
			}


			ViewBag.BranchId = new SelectList(db.Branches, "BranchId", "BranchName", branchPaymentRequest.BranchId);
			return View(branchPaymentRequest);
		}

		// GET: BranchPaymentRequests/Edit/5
		public ActionResult Edit(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			BranchPaymentRequest branchPaymentRequest = db.BranchPaymentRequests.Find(id);
			if (branchPaymentRequest == null)
			{
				return HttpNotFound();
			}
			ViewBag.BranchId = new SelectList(db.Branches, "BranchId", "BranchName", branchPaymentRequest.BranchId);
			ViewBag.StatusId = branchPaymentRequest.IsApproved;

			return View(branchPaymentRequest);
		}

		// POST: BranchPaymentRequests/Edit/5
		// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
		// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit([Bind(Include = "BranchPaymentRequestId,BranchId,DateRequested,Requestor,AmtRequestd,IsApproved,Comment")] BranchPaymentRequest branchPaymentRequest)
		{
			if (ModelState.IsValid)
			{
				db.Entry(branchPaymentRequest).State = EntityState.Modified;
				db.SaveChanges();
				return RedirectToAction("Index");
			}
			ViewBag.BranchId = new SelectList(db.Branches, "BranchId", "BranchName", branchPaymentRequest.BranchId);
			return View(branchPaymentRequest);
		}

		// GET: BranchPaymentRequests/Delete/5
		public ActionResult Delete(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			BranchPaymentRequest branchPaymentRequest = db.BranchPaymentRequests.Find(id);
			if (branchPaymentRequest == null)
			{
				return HttpNotFound();
			}
			return View(branchPaymentRequest);
		}

		// GET: BranchPaymentRequests/DeleteForNonAdmin/5
		public ActionResult DeleteForNonAdmin(int? id)
		{
			if (id == null)
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
			}
			BranchPaymentRequest branchPaymentRequest = db.BranchPaymentRequests.Find(id);
			if (branchPaymentRequest == null)
			{
				return HttpNotFound();
			}
		   
			db.BranchPaymentRequests.Remove(branchPaymentRequest);
			db.SaveChanges();

			ViewBag.MaxAvailForDraws = Session["maxAvailForDraws"];

			return RedirectToAction("IndexForNonAdmin", new { maxAvailForDraws = ViewBag.MaxAvailForDraws });
		}

		// POST: BranchPaymentRequests/Delete/5
		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			BranchPaymentRequest branchPaymentRequest = db.BranchPaymentRequests.Find(id);
			db.BranchPaymentRequests.Remove(branchPaymentRequest);
			db.SaveChanges();
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}
