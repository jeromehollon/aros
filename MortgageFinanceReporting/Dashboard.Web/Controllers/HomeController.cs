﻿using System.Web.Mvc;
using Dashboard.Web.Models;
using Dashboard.Web.Shared;
using Reports;

namespace Dashboard.Web.Controllers
{
    public class HomeController : Controller
    {
        [Authorize()]
        public ActionResult Index()
        {
            if (User.IsInRole("Admin") || User.IsInRole("Global") || User.IsInRole("Branch") || User.IsInRole("Region") || User.IsInRole("Non-Producing Branch Mgr") || User.IsInRole("Division Mgr"))
            {
                AroReport aro = new AroReport();
                ViewBag.BpsRankings = aro.GetBpsRankingReport();
               
                return View();
            }
            else
            {
                return View("PreApproved");
            }
        }


        public ActionResult About()
        {
            ViewBag.Message = "This is a reporting application for Alterra Home Loans";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Contact Alterra Home Loans";

            return View();
        }
    }
}