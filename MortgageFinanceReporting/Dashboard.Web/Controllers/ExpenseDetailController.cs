﻿// ***********************************************************************
// Assembly         : Dashboard.Web
// Author           : Ron de Frates
// Created          : 07-11-2014
//
// Last Modified By : Ron de Frates
// Last Modified On : 10-04-2014
// ***********************************************************************
// <copyright file="ExpenseDetailController.cs" company="5X Solutions">
//     Copyright (c)5X Solutions, Inc. All rights reserved.
// </copyright>
// <summary></summary>
// ***********************************************************************
using System;
using System.Configuration;
using System.Web.Mvc;
using Dashboard.Web.Shared;
using Reports;

/// <summary>
/// The Controllers namespace.
/// </summary>
namespace Dashboard.Web.Controllers
{
#if !DEBUG
		[RequireHttps] //apply to all actions in controller
#endif
    /// <summary>
    /// Class ExpenseDetailController.
    /// </summary>
    [Authorize()]
    public class ExpenseDetailController : Controller
    {
        //
        // GET: /ExpenseDetail/
        /// <summary>
        /// Indexes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="branchId">The branch identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="isBonus">if set to <c>true</c> [is bonus].</param>
        /// <param name="rptType">Type of the RPT.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult Index(string id, string branchId, int year, bool isBonus = false, string rptType = "")
        {
            AroReport aro = new AroReport("", year);
            aro.Year = year;

            string monthLetter = id.Substring(0, 1);

            string acctNum = id.Replace(monthLetter, "");

            string expenseMapPath = SetExpenseMapPath(rptType, isBonus, aro);

            string expenseDetail = aro.GetExpenseDetailRpt(branchId, monthLetter, year, acctNum, expenseMapPath);

            ViewBag.DetailRpt = expenseDetail;
            ViewBag.id = id;
            ViewBag.branchId = branchId;
            ViewBag.Year = year;
            ViewBag.IsBonus = isBonus;
            ViewBag.rptType = rptType;

            return View();
        }

        /// <summary>
        /// Expenses the detail to PDF.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="branchId">The branch identifier.</param>
        /// <param name="year">The year.</param>
        /// <param name="isBonus">if set to <c>true</c> [is bonus].</param>
        /// <param name="rptType">Type of the RPT.</param>
        /// <returns>ActionResult.</returns>
        public ActionResult ExpenseDetailToPdf(string id, string branchId, int year, bool isBonus = false, string rptType = "")
        {
            // generate the details page
            AroReport aro = new AroReport("", year);
            aro.Year = year;

            string monthLetter = id.Substring(0, 1);

            string acctNum = id.Replace(monthLetter, "");

            string expenseMapPath = SetExpenseMapPath(rptType, isBonus, aro);

            string expenseDetail = aro.GetExpenseDetailRpt(branchId, monthLetter, year, acctNum, expenseMapPath);

            ViewBag.DetailRpt = expenseDetail;

            // send the request to the pdf server
            string fileName = PdfPrint.GetPdfFileName(expenseDetail, Server.MapPath("~"));

            Response.Redirect("https://do.convertapi.com/Web2Pdf?APIKey=635362747&PageOrientation=landscape&CUrl=https://" + ConfigurationManager.AppSettings["url"] + "/TempPages/" + fileName);

            return null;
        }

        #region private


        /// <summary>
        /// Sets the expense map path.
        /// </summary>
        /// <param name="rptType">Type of the RPT.</param>
        /// <param name="isBonus">if set to <c>true</c> [is bonus].</param>
        /// <param name="aro">The aro.</param>
        /// <returns>System.String.</returns>
        private string SetExpenseMapPath(string rptType, bool isBonus, AroReport aro)
        {
            string expenseMapPath = "";
           
            // todo: use case statement or break apart
            if (rptType == "PL")
            {
                expenseMapPath = AppDomain.CurrentDomain.BaseDirectory + "/App_Code/PLRevneueAcntMap.txt";
            }
            else if (isBonus == true)
            {
                if (aro.Year == 2014)
                {
                    expenseMapPath = AppDomain.CurrentDomain.BaseDirectory + "/App_Code/BonusAcntMap2014.txt";
                }
                else
                {
                    expenseMapPath = AppDomain.CurrentDomain.BaseDirectory + "/App_Code/BonusAcntMap.txt";
                }
            }
            else
            {
                // kludge: 
                //string path = System.Web.HttpContext.Current.Request.UrlReferrer.AbsolutePath;
                //System.IO.FileInfo info = new System.IO.FileInfo(path);

                if (aro.Year == 2014)
                {
                    expenseMapPath = AppDomain.CurrentDomain.BaseDirectory + "/App_Code/ExpenseAcntMap2014.txt";
                }
                else if (rptType == "PLRsrv")
                {
                    expenseMapPath = AppDomain.CurrentDomain.BaseDirectory + "/App_Code/PLRevneueBonusAcntMap.txt";
                }
                else
                {
                   expenseMapPath = AppDomain.CurrentDomain.BaseDirectory + "/App_Code/ExpenseAcntMap.txt";
                }
            }
            return expenseMapPath;
        }



        #endregion
    }
}