﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Dashboard.Web.Models;

namespace Dashboard.Web.Controllers
{     
    [Authorize(Roles = "Admin, Global")]
    public class RegionBranchController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: Regions
        public ActionResult Index()
        {
            return View(db.RegionBranches.ToList());
        }

        // GET: Regions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegionBranch regionBranches = db.RegionBranches.Find(id);
            if (regionBranches == null)
            {
                return HttpNotFound();
            }
            return View(regionBranches);
        }

        // GET: Regions/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            var viewModel = new RegionBranch();
            // drop down list boxes 
            ViewBag.BranchList = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchId", "BranchName");
            ViewBag.RegionList = new SelectList(db.Regions.OrderBy(r => r.RegionName), "RegionId", "RegionName");

            return View(viewModel);
        }

        // POST: Regions/Create
        [Authorize(Roles = "Admin")]
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RegionBranch regionBranches)
        {
            if (ModelState.IsValid)
            {
                db.RegionBranches.Add(regionBranches);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
              // drop down list boxes 
            ViewBag.BranchList = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchId", "BranchName");
            ViewBag.RegionList = new SelectList(db.Regions.OrderBy(r => r.RegionName), "RegionId", "RegionName");

            return View(regionBranches);
        }

        // GET: Regions/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

             // drop down list boxes 
            ViewBag.BranchList = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchId", "BranchName");
            ViewBag.RegionList = new SelectList(db.Regions.OrderBy(r => r.RegionName), "RegionId", "RegionName");

            RegionBranch regionBranches = db.RegionBranches.Find(id);
            if (regionBranches == null)
            {
                return HttpNotFound();
            }
            return View(regionBranches);
        }

        // POST: Regions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RegionBranchId,RegionId,BranchId")] RegionBranch regionBranches)
        {
            if (ModelState.IsValid)
            {
                db.Entry(regionBranches).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

              // drop down list boxes 
            ViewBag.BranchList = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchId", "BranchName");
            ViewBag.RegionList = new SelectList(db.Regions.OrderBy(r => r.RegionName), "RegionId", "RegionName");

            return View(regionBranches);
        }

        // GET: Regions/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegionBranch regionBranches = db.RegionBranches.Find(id);
            if (regionBranches == null)
            {
                return HttpNotFound();
            }
            return View(regionBranches);
        }

        // POST: Regions/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RegionBranch regionBranches = db.RegionBranches.Find(id);
            db.RegionBranches.Remove(regionBranches);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
