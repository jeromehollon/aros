﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Security;
using Dashboard.Web.Models;
using Dashboard.Web.Shared;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Dashboard.Web.Controllers
{
#if !DEBUG
        [RequireHttps] //apply to all actions in controller
#endif
    [Authorize(Roles = "Admin, Global")]
    public class UsersAdminController : Controller
    {
        private readonly ApplicationDbContext db = new ApplicationDbContext();

        public UsersAdminController()
        {
            Context = new ApplicationDbContext();
            UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(Context));
            RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(Context));
        }

        public UsersAdminController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }
        public RoleManager<IdentityRole> RoleManager { get; private set; }
        public ApplicationDbContext Context { get; private set; }

        //
        // GET: /Users/       
        public async Task<ActionResult> Index()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            ViewBag.BranchNames = db.Branches.OrderBy(b => b.BranchName).Select(b => new { b.BranchName, b.BranchId }).ToList();

            return View(await Context.Users.ToListAsync());
        }

        // GET: /Users/
        public async Task<ActionResult> AllUsers()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            ViewBag.BranchNames = db.Branches.OrderBy(b => b.BranchName).Select(b => new { b.BranchName, b.BranchId }).ToList();

            return View("UserList", await Context.Users.OrderBy(u => u.UserName).ToListAsync());
        }

        // GET: /Users/
        public async Task<ActionResult> PendingUsers()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            ViewBag.BranchNames = db.Branches.OrderBy(b => b.BranchName).Select(b => new { b.BranchName, b.BranchId }).ToList();

            return View("UserListPending", await Context.Users.Where(u => u.Roles.Count == 0).OrderBy(u => u.UserName).ToListAsync());
        }
        //
        // GET: /Users/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);
            return View(user);
        }
       
        //
        // POST: /Users/Create
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel userViewModel, string RoleId)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser();
                user.UserName = userViewModel.UserName;
                user.BranchId = userViewModel.BranchId;
                user.Email = userViewModel.Email;
                var adminresult = await UserManager.CreateAsync(user, userViewModel.Password);

                //Add User Admin to Role Admin
                if (adminresult.Succeeded)
                {
                    if (!String.IsNullOrEmpty(RoleId))
                    {
                        //Find Role Admin
                        var role = await RoleManager.FindByIdAsync(RoleId);
                        var result = await UserManager.AddToRoleAsync(user.Id, role.Name);
                        if (!result.Succeeded)
                        {
                            ModelState.AddModelError("", result.Errors.First().ToString());
                            return View();
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", adminresult.Errors.First().ToString());
                    return View();

                }
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        //
        // GET: /Users/Edit/1
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            // get user role
            if (string.IsNullOrWhiteSpace(UserRoles.GetUserRole(user.UserName)) == false)
            {
                ViewBag.Role = UserRoles.GetUserRole(user.UserName);
            }
            else
            {
                ViewBag.Role = "";
            }

            // drop down list boxes 
            ViewBag.BranchList = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchId", "BranchName");
            ViewBag.RegionList = new SelectList(db.Regions.OrderBy(r => r.RegionName), "RegionId", "RegionName");

            return View(user);
        }

        //
        // POST: /Users/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "UserName,Id,BranchId,RegionId,Email,FirstName,LastName")] ApplicationUser formuser, string id, string RoleId, string RoleName)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var user = await UserManager.FindByIdAsync(id);
            user.UserName = formuser.UserName;
            user.BranchId = formuser.BranchId;
            user.RegionId = formuser.RegionId;
            user.Email = formuser.Email;
            user.FirstName = formuser.FirstName;
            user.LastName = formuser.LastName;

            if (ModelState.IsValid)
            {
                //Update the user details
                await UserManager.UpdateAsync(user);

                //If user has existing Role then remove the user from the role
                // This also accounts for the case when the Admin selected Empty from the drop-down and
                // this means that all roles for the user must be removed
                var rolesForUser = await UserManager.GetRolesAsync(id);
                if (rolesForUser.Count() > 0)
                {
                    foreach (var item in rolesForUser)
                    {
                        var result = await UserManager.RemoveFromRoleAsync(id, item);
                    }
                }

                if (!String.IsNullOrEmpty(RoleId))
                {
                    //Find Role
                    var role = await RoleManager.FindByIdAsync(RoleId);
                    //Add user to new role
                    var result = await UserManager.AddToRoleAsync(id, role.Name);
                    if (!result.Succeeded)
                    {
                        ModelState.AddModelError("", result.Errors.First().ToString());

                        // drop down list boxes 
                        ViewBag.BranchId = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchId", "BranchName", user.Branch);
                        ViewBag.RegionList = new SelectList(db.Regions.OrderBy(r => r.RegionName), "RegionId", "RegionName");

                        return View();
                    }
                }

                if (string.IsNullOrEmpty(RoleName) == false)
                {
                    if (Roles.GetRolesForUser(user.UserName).Count() == 0)
                    {
                        AddUserToRole(user.UserName, RoleName);
                    }
                    else if (Roles.GetRolesForUser(user.UserName)[0] != RoleName)
                    {
                        AddUserToRole(user.UserName, RoleName);
                    }
                }

                return RedirectToAction("Index");
            }
            else
            {
                // drop down list boxes 
                ViewBag.BranchId = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchId", "BranchName", user.Branch);
                ViewBag.RegionList = new SelectList(db.Regions.OrderBy(r => r.RegionName), "RegionId", "RegionName");

                return View();
            }
        }

        //
        // GET: /Users/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = await UserManager.FindByIdAsync(id);

            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        //
        // POST: /Users/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            if (ModelState.IsValid)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var user = await UserManager.FindByIdAsync(id);

                var logins = UserManager.GetLogins(id);
                foreach (var login in logins)
                {
                    await UserManager.RemoveLoginAsync(id, login);
                }

                var rolesForUser = UserManager.GetRoles(id);
                if (rolesForUser.Count() > 0)
                {
                    foreach (var item in rolesForUser)
                    {
                        var result = await UserManager.RemoveFromRoleAsync(user.Id, item);
                    }
                }
                Context.Users.Remove(user);
                await Context.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }


        #region Utilities
        [Authorize(Roles = "Admin")]
        public void AddUserToRole(string userName, string roleName)
        {
            ApplicationDbContext context = new ApplicationDbContext();

            ApplicationUser user = context.Users.Where(u => u.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            using (var account = new AccountController())
            {
                account.UserManager.AddToRole(user.Id, roleName);
            }

        }
        #endregion
    }
}
