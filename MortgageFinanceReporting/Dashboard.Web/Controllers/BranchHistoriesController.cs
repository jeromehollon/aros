﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Dashboard.Web.Models;

namespace Dashboard.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BranchHistoriesController : Controller
    {
        private Entities db = new Entities();

        // GET: BranchHistories
        public ActionResult Index()
        {
            var branchHistories = db.BranchHistories.Include(b => b.Branch).OrderBy(b => b.Branch.BranchName);
            return View(branchHistories.ToList());
        }

        // GET: BranchHistories/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BranchHistory branchHistory = db.BranchHistories.Find(id);
            if (branchHistory == null)
            {
                return HttpNotFound();
            }
            return View(branchHistory);
        }

        // GET: BranchHistories/Create
        public ActionResult Create()
        {
            List<DateTime> effectiveDates = new List<DateTime>();
            effectiveDates.Add(Convert.ToDateTime("1/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("2/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("3/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("4/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("5/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("6/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("7/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("8/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("9/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("10/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("11/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("12/1/" + DateTime.Now.Year));

            ViewBag.EffectiveDates = new SelectList(effectiveDates, "EffectiveDate");
            ViewBag.BranchId = new SelectList(db.Branches.OrderBy(b => b.BranchName), "BranchId", "BranchName");
            return View();
        }

        // POST: BranchHistories/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BranchHistoryId,BranchId,ARRate,DateChanged,EffectiveDate,UserName")] BranchHistory branchHistory)
        {
            if (ModelState.IsValid)
            {
                // track who made the change
                branchHistory.UserName = User.Identity.Name;
                branchHistory.DateChanged = DateTime.Now.Date;

                db.BranchHistories.Add(branchHistory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BranchId = new SelectList(db.Branches, "BranchId", "BranchName", branchHistory.BranchId);
            return View(branchHistory);
        }

        // GET: BranchHistories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BranchHistory branchHistory = db.BranchHistories.Find(id);
            if (branchHistory == null)
            {
                return HttpNotFound();
            }

            List<DateTime> effectiveDates = new List<DateTime>();
            effectiveDates.Add(Convert.ToDateTime("1/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("2/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("3/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("4/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("5/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("6/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("7/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("8/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("9/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("10/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("11/1/" + DateTime.Now.Year));
            effectiveDates.Add(Convert.ToDateTime("12/1/" + DateTime.Now.Year));

            ViewBag.EffectiveDates = new SelectList(effectiveDates, "EffectiveDate");
            ViewBag.BranchId = new SelectList(db.Branches, "BranchId", "BranchName", branchHistory.BranchId);
            return View(branchHistory);
        }

        // POST: BranchHistories/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BranchHistoryId,BranchId,ARRate,DateChanged,EffectiveDate,UserName")] BranchHistory branchHistory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(branchHistory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BranchId = new SelectList(db.Branches, "BranchId", "BranchName", branchHistory.BranchId);
            return View(branchHistory);
        }

        // GET: BranchHistories/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BranchHistory branchHistory = db.BranchHistories.Find(id);
            if (branchHistory == null)
            {
                return HttpNotFound();
            }
            return View(branchHistory);
        }

        // POST: BranchHistories/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BranchHistory branchHistory = db.BranchHistories.Find(id);
            db.BranchHistories.Remove(branchHistory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
