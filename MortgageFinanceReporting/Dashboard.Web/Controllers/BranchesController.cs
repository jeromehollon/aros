﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;
using Dashboard.Web.Models;
using Elmah;

namespace Dashboard.Web.Controllers
{
    [Authorize(Roles = "Admin, Global")]
    public class BranchesController : Controller
    {
        private Entities db = new Entities();

        // GET: Branches
        public ActionResult Index()
        {
            return View(db.Branches.OrderBy(b => b.BranchName).ToList());
        }

        // GET: Branches/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            return View(branch);
        }

        // GET: Branches/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Branches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BranchName,BranchManager,ManagerSalaryRate,ManagerSalaryCap,ARRate,ARCap,VolumeOverrideRate,VolumeOverrideCap,QrtBonusRate,QrtBonusCap,LoanVolChangeMetricRate,LoanVolChangeMetricCap,MonthlyClosingGoalRate,MonthlyClosingGoalCap,TotalMonthlyBudgetRate,TotalMonthlyBudgetCap,StartUpCostRate,StartUpCostCap,LoanLossReserveRate,LoanLossReserveCap,ReserveRequiredRate,ReserveRequiredCap,BranchReserveBalanceRate,BranchReserveBalanceCap,CorpInvestBalDueRate,CorpInvestBalDueCap,ActivationDate,BegLoanLossReserveBal,BegBranchReserveBal,BegAroAccountBal,BranchMargin,LoMargin,ProcessingFee,BpsGoal")] Branch branch)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    db.Branches.Add(branch);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbEntityValidationException e)
                {
                    ErrorSignal.FromCurrentContext().Raise(e);
                }
            }
            else
            {
                var errors = new List<string>();
                StringBuilder sb = new StringBuilder();

                foreach (var state in ModelState)
                {
                    foreach (var error in state.Value.Errors)
                    {
                        errors.Add(error.ErrorMessage);
                        sb.Append(error.ErrorMessage + "<br />");
                    }
                }

                Exception e = new Exception("Validation Error " + errors);
                ViewBag.ErrorMsg = sb.ToString();
                ErrorSignal.FromCurrentContext().Raise(e);
            }
            return View(branch);

        }

        // GET: Branches/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            return View(branch);
        }

        // POST: Branches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BranchId,BranchName,BranchManager,ManagerSalaryRate,ARRate,VolumeOverrideRate,VolumeOverrideCap,QrtBonusRate,LoanVolChangeMetricRate,MonthlyClosingGoalRate,TotalMonthlyBudgetRate,StartUpCostRate,LoanLossReserveRate,LoanLossReserveCap,ReserveRequiredRate,ReserveRequiredCap,BranchReserveBalanceRate,BranchReserveBalanceCap,CorpInvestBalDueRate,ActivationDate,BegLoanLossReserveBal,BegBranchReserveBal,BegAroAccountBal,BranchMargin,LoMargin,ProcessingFee,BpsGoal")] Branch branch)
        {
            if (ModelState.IsValid)
            {
                db.Entry(branch).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(branch);
        }

        // GET: Branches/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Branch branch = db.Branches.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            return View(branch);
        }

        // POST: Branches/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Branch branch = db.Branches.Find(id);
            db.Branches.Remove(branch);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
