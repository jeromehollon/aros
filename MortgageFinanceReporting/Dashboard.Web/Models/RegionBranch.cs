﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Dashboard.Web.Models;

namespace Dashboard.Web.Models
{
    public class RegionBranch
    {
        public List<Branch> GetRegionBranches(int userRegionId, ApplicationDbContext db)
        {
            // get list of branches associated with this region
            var regionBranches = db.RegionBranches
            	.Where(b => b.Region.RegionId == userRegionId)
            	.Select(b => new
            	{
            		b.Branch.BranchId,
            		b.Branch.BranchName
            	});

           List<Branch> branches = new List<Branch>();

            foreach (var regionBranch in regionBranches)
            {
                Branch branch = new Branch();
                
                branch.BranchId = regionBranch.BranchId;
                branch.BranchName = regionBranch.BranchName;

                branches.Add(branch);
            }

            return branches;
        }

        public List<Branch> GetDivisionBranches(int userRegionId, int userBranchId, ApplicationDbContext db)
        {
            // get list of branches associated with this region
            var regionBranches = db.RegionBranches
            	.Where(b => b.Region.RegionId == userRegionId && b.BranchId != userBranchId)
            	.Select(b => new
            	{
            		b.Branch.BranchId,
            		b.Branch.BranchName
            	});

           List<Branch> branches = new List<Branch>();

            foreach (var regionBranch in regionBranches)
            {
                Branch branch = new Branch();
                
                branch.BranchId = regionBranch.BranchId;
                branch.BranchName = regionBranch.BranchName;

                branches.Add(branch);
            }

            return branches;
        }

        [Key]
        public int RegionBranchId { get; set; }

       [Required] 
       [Display(Name = "Region")]  
        virtual public int RegionId { get; set; }
        virtual public Region Region { get; set; }

       [Required]
       [Display(Name = "Branch")]
        virtual public int BranchId { get; set; } 
       virtual public Branch Branch { get; set; } 
    }
}