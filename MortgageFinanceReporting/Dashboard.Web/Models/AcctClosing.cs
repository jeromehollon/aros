﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Dashboard.Web.Models
{
    public class AcctClosing
    {
        [Key]
        public int AcctClosingId { get; set; }

        [Display(Name = "Accounting Close Through")]
        [DataType(DataType.DateTime)]
        public DateTime AcctClosingDate { get; set; }
    }
}