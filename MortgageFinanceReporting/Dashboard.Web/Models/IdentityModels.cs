﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Dashboard.Web.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        [Display(Name = "Branch Name")]
        public int BranchId { get; set; }
        public Branch Branch { get; set; }

        [Display(Name = "Region Name")]
        public Nullable<int> RegionId { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Display(Name = "Lockout EndDate Utc")]
        public DateTime? LockoutEndDateUtc { get; set; }

        [Display(Name = "Access Failed Count")]
        public int? AccessFailedCount { get; set; }

        [Display(Name = "Last Password Change")]
        public DateTime? LastPwChange { get; set; }

        public string PasswordVerificationToken { get; set; }
    }



    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }

        public System.Data.Entity.DbSet<Dashboard.Web.Models.Branch> Branches { get; set; }

        public System.Data.Entity.DbSet<Dashboard.Web.Models.AcctClosing> AcctClosings { get; set; }

        public System.Data.Entity.DbSet<Dashboard.Web.Models.Region> Regions { get; set; }

        public System.Data.Entity.DbSet<Dashboard.Web.Models.RegionBranch> RegionBranches { get; set; }

        public System.Data.Entity.DbSet<Dashboard.Web.Models.vwProducingBranch> vwProducingBranches { get; set; }
    }
}