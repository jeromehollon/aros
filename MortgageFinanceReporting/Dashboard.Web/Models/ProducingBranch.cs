﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Dashboard.Web.Models;

namespace Dashboard.Web.Models
{
    public class vwProducingBranch 
    {
       [Key]
       public int BranchId { get; set; }
       virtual public string BranchName { get; set; } 

        public List<Branch> GetProducingBranches(int userRegionId, int userBranchId, List<Branch> branchSuperSet, ApplicationDbContext db)
        {
            var producingBranches = from s in branchSuperSet
                   join n in db.vwProducingBranches on s.BranchId equals n.BranchId 
                    select new 
                    {
                        s.BranchId,
                        s.BranchName
                    };
           
           List<Branch> branches = new List<Branch>();

            foreach (var producingBranch in producingBranches)
            {
                Branch branch = new Branch();
                
                branch.BranchId = producingBranch.BranchId;
                branch.BranchName = producingBranch.BranchName;

                branches.Add(branch);
            }

            return branches;
        }
    }
}
