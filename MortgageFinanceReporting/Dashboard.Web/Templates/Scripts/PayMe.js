﻿$(document)
	.ready(function() {
		// show the request for payment UI
		var rptYear = getRptYear();
		if ($("#frmRequestForPayment") != null && rptYear >= 2016) {
			var maxAvailForDraws = getMaxAvailForDraw();
			document.getElementById("frmRequestForPayment").src = '../BranchPaymentRequests/IndexForNonAdmin?maxAvailForDraws=' + maxAvailForDraws;
		}

	});




function  getMaxAvailForDraw() {
	var closeDate = $('#closeDate').html();

	// get the amount for the last month preceeding the close date
	var maxAvailForDraws = getMonthAvailForDraws(closeDate);
 
	if (maxAvailForDraws.indexOf(">") > 0) {
		// bonuses have a drill-down link so derive the true number
		var maxAvailForDraws = maxAvailForDraws.substr(maxAvailForDraws.indexOf(">") + 1, 50);        
		maxAvailForDraws = convertToZero(maxAvailForDraws);
		if (maxAvailForDraws < 0) {
			maxAvailForDraws = 0;
		}
	}
	else {
		maxAvailForDraws = convertToZero(maxAvailForDraws);
		if (maxAvailForDraws < 0) {
			maxAvailForDraws = 0;
		}
	}
	maxAvailForDraws = Math.round(maxAvailForDraws);

	return maxAvailForDraws;
}


function getMonthAvailForDraws(closeDateStr) {
	var lastAvailForDraws = "0";

	var closeDate = new Date(closeDateStr);
	var year = new Date().getFullYear();
	var a = new Array("H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S");

	for (i = 0; i < a.length; i++) {  // start at January 
		if ($('#RA' + a[i] + '29').html().length > 1) {
			if (new Date(getMonthNum(a[i]) + "/1/" + year) <= closeDate) {
				lastAvailForDraws = $('#RA' + a[i] + '29').html();
			}
		}
	}
   
	return lastAvailForDraws;
}

