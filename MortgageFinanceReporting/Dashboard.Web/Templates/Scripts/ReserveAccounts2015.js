﻿// used for 2015 only - pre server-side Allocations logic for 2016+

	function getRptYear() {
		var year = $("#btnPdf").attr("href");

		if (year != null) {
			year = year.substr(year.indexOf("yearSelected") + 13, 4);
		}
		else if($('#lstYear').val() != null) {
			year = $('#lstYear').val();
		}
		else {
			today.getFullYear();
		}

		return year;
	}

	lastMonth = 0; // global var that is calculated once in missing value function
	var rptYear = getRptYear();

	$(document).ready(function () {

		a = new Array("H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S");

		//****************************** //
		// Jan Loan Loss Reserve         //
		// handled by server side script //
		//****************************** //

		var today = new Date();
		var todaysMnth = today.getMonth() + 1;
		var todaysYear = today.getFullYear();

		for (i = 0; i < a.length; i++) {

			//************************//
			// Branch Reserve Account //
			//************************//
			// loan loss reserve
			if (i >= 1) {
				// don't show value if in the future except for PDFs which have not lstYear                 
				if (todaysMnth >= i + 1 || todaysYear > $('#lstYear').val() || $('#lstYear').val() == null) {
					document.getElementById("RA" + a[i] + "6").innerHTML = accounting.formatNumber(convertToZero($("#RA" + a[i - 1] + "8").html()), 2);
				}
				else {
					document.getElementById("RA" + a[i] + "6").innerHTML = "-";
				}
			}
	  
			// Loan Loss Reserve Increase
			if (rptYear == 2015) {
				document.getElementById("RA" + a[i] + "7").innerHTML = getloanLossRsrvIncrease2015();
			}
			else {
				document.getElementById("RA" + a[i] + "7").innerHTML = getLoanLossRsrvIncrease();
			}

			// get loan loss reserve utilized value from hidden field that is totaled on server side            
			if ($("#RA" + a[i] + "5").html().indexOf("$5") > 0) {
				loanLossRsvrUtl = 0;
			}
			else {
				loanLossRsvrUtl = convertToZero($("#RA" + a[i] + "5").html());
			}


			document.getElementById("RA" + a[i] + "8").innerHTML = accounting.formatNumber(convertToZero($("#RA" + a[i] + "6").html()) +
				convertToZero($("#RA" + a[i] + "7").html()) +
				convertToZero($("#" + a[i] + "800").html()) +
				convertToZero($("#" + a[i] + "805").html()) +
				convertToZero($("#" + a[i] + "810").html()), 2);

			//NOTE: process branch reserve acct section before Corp Cap Acct section since it depends on branch reserve acct values
			//NOTE:  151 (RA18) - 1st pass is the branch reserve cap [C20] which is done server-side
			if (i >= 1) {
				document.getElementById("RA" + a[i] + "18").innerHTML = accounting.formatNumber(convertToZero($("#RA" + a[i - 1] + "22").html()), 2);

				if (convertToZero($("#" + a[i] + "15").html()) === 0) {
					// set branch reserve to 0 when expenses missing
					document.getElementById("RA" + a[i] + "18").innerHTML = "-"
				}
			}
			else {
				document.getElementById("RAH18").innerHTML = accounting.formatNumber(convertToZero($("#RAG18").html()), 2);
			}


			// Allocated Increase in Branch Operating Reserve
			corpContribBegin = convertToZero($("#RA" + a[i] + "11").html());

			if (corpContribBegin != convertToZero($("#" + a[i] + "22").html())) {
				document.getElementById(a[i] + "23").innerHTML = "-";
			}

			// Decrease in Branch Cap Account (for advance)
			document.getElementById("RA" + a[i] + "21").innerHTML = accounting.formatNumber(getAppliedToCoverLoss(), 2);
			//if (convertToZero($("#" + a[i] + "25").html()) + convertToZero($("#RA" + a[i] + "18").html()) > 0) {
			//	document.getElementById("RA" + a[i] + "21").innerHTML = accounting.formatNumber(convertToZero($("#" + a[i] + "25").html()), 2);
			//}
			//else {
			//	document.getElementById("RA" + a[i] + "21").innerHTML = accounting.formatNumber(convertToZero($("#RA" + a[i] + "18").html()) * -1, 2);
			//}


			//****************************** //
			// Corp Cap (Investment) Account //
			//****************************** //

			// don't show value if in the future except for PDFs which have not lstYear                 
			if (todaysMnth >= i + 1 || todaysYear > $('#lstYear').val() || $('#lstYear').val() == null) {
				document.getElementById("RA" + a[i] + "11").innerHTML = accounting.formatNumber(convertToZero($("#RA" + a[i - 1] + "15").html()), 2);
			}
			else {
				document.getElementById("RA" + a[i] + "11").innerHTML = "-";
			}

			// january is alway the beginning amount reflected in cell g
			document.getElementById("RAH11").innerHTML = accounting.formatNumber(convertToZero($("#RAG11").html()), 2);


			// Allocated Decrease in Corp Cap Acct
			priorAllocationMinusRsrv = convertToZero($("#" + a[i] + "17").html()) - convertToZero($("#" + a[i] + "21").html());
			corpContribBegin = convertToZero($("#RA" + a[i] + "11").html());

			if (priorAllocationMinusRsrv < 1) {
				document.getElementById("RA" + a[i] + "12").innerHTML = "-";
			}
			else {
				if (corpContribBegin > 0) {
					if (priorAllocationMinusRsrv > 0) {
						if (priorAllocationMinusRsrv < corpContribBegin) {
							document.getElementById("RA" + a[i] + "12").innerHTML = accounting.formatNumber(convertToZero($("#" + a[i] + "22").html()) * -1, 2);
						}
						else if (priorAllocationMinusRsrv > corpContribBegin) {
							document.getElementById("RA" + a[i] + "12").innerHTML = accounting.formatNumber(corpContribBegin * -1, 2);
						}
					}
					else {
						document.getElementById("RA" + a[i] + "12").innerHTML = "-";
					}
				}
				else {
					document.getElementById("RA" + a[i] + "12").innerHTML = "-";
				}
			}

			// go back and adjust value in ARO report
			priorAllocationDecreaseDiff = convertToZero($("#" + a[i] + "17").html()) - convertToZero($("#" + a[i] + "21").html());
			if (priorAllocationDecreaseDiff > 0) {
				if (i >= 1) {
					if (priorAllocationDecreaseDiff < convertToZero($("#RA" + a[i - 1] + "15").html())) {
						document.getElementById(a[i] + "22").innerHTML = priorAllocationDecreaseDiff;
					}
					else {
						document.getElementById(a[i] + "22").innerHTML = convertToZero($("#RA" + a[i - 1] + "15").html());
					}
				}
				else {
					document.getElementById(a[i] + "22").innerHTML = convertToZero($("#RA" + a[i] + "12").html());
				}
			}

			document.getElementById(a[i] + "22").innerHTML = accounting.formatNumber(Math.abs($("#" + a[i] + "22").html()), 2);
			
			// 7923	Paid into Branch Reserves
			// NOTE: this step is repeated from ARO2015.htm because 122 (Acct #7920) is redefined above - this is the 2nd sweep
			allocatedIncOpRsrv = convertToZero($("#" + a[i] + "17").html()) - convertToZero($("#" + a[i] + "21").html()) - convertToZero($("#" + a[i] + "22").html());

			 // for 2015
			if (rptYear == 2015) {
				if (allocatedIncOpRsrv > 1) {
					// if Corporate Investment Balance - 7920 - Paid Back to Corporate is zero
					if (convertToZero($("#RA" + a[i] + "11").html()) - convertToZero($("#" + a[i] + "22").html()) === 0) {
						// Corporate Cap Contribution (beginning) - Acct 7920 equal 0

						if (convertToZero($("#RA" + a[i] + "18").html()) <= convertToZero($("#G0").html())) {
							// if Branch Reserve Balance <= RESERVE REQUIRED FOR BRANCH CAP
							if ((allocatedIncOpRsrv / 2) < (convertToZero($("#G0").html()) - convertToZero($("#RA" + a[i] + "18").html()))) {
								// if (Allocated Increase in Branch Operating Reserve/2) < (Reserve Required Cap - Branch Reserve Balance)
								document.getElementById(a[i] + "23").innerHTML = accounting.formatNumber(allocatedIncOpRsrv / 2, 2); // (ARO PRIOR TO ALLOCATIONS - Acct 7910 - Acct 7920) divided by 2

							}
							else {
								document.getElementById(a[i] + "23").innerHTML = accounting.formatNumber(convertToZero($("#RAG24").html()) - convertToZero($("#RA" + a[i] + "18").html()), 2);
								// 7923	Paid into Branch Reserves = RESERVE REQUIRED FOR BRANCH - Branch Reserve Balance
							}
						}
						else {
							document.getElementById(a[i] + "23").innerHTML = "-";
						}
					}
					else {
						document.getElementById(a[i] + "23").innerHTML = "-";
					}
				}
				else {
					document.getElementById(a[i] + "23").innerHTML = "-";
				}

				document.getElementById("RA" + a[i] + "19").innerHTML = accounting.formatNumber(convertToZero($("#" + a[i] + "23").html()), 2);
				}
			else {
				// 2016+
				document.getElementById("RA" + a[i] + "19").innerHTML = getPaidIntoAcctFromBranch();
				// Paid into Branch Reserves = Paid Into Account From Branch
				document.getElementById(a[i] + "23").innerHTML = getPaidIntoAcctFromBranch(); 
			}

			// Ending Corp Inv Account
			document.getElementById("RA" + a[i] + "22").innerHTML =
			   accounting.formatNumber(
				convertToZero($("#RA" + a[i] + "18").html()) +
				convertToZero($("#RA" + a[i] + "19").html()) +
				convertToZero($("#RA" + a[i] + "21").html())
				, 2);

			// Increase in Corp Inv Account (for advance)
			if (convertToZero($("#RA" + a[i] + "21").html()) > convertToZero($("#" + a[i] + "25").html())) {
				incInCorpInvAcct = (convertToZero($("#" + a[i] + "25").html()) - convertToZero($("#RA" + a[i] + "21").html())) * -1;
			}
			
			 // for 2015
			if (rptYear == 2015) {
				document.getElementById("RA" + a[i] + "25").innerHTML = "-";
			}
			else {
				// Applied To Cover Losses Avail Funds To Mgr is new in 2016 
				if (i > 0) {
					document.getElementById("RA" + a[i] + "26").innerHTML =
					accounting.formatNumber(
						convertToZero($("#RA" + a[i - 1] + "29").html()),
					 2);
				}
				else {
					document.getElementById("RAH26").innerHTML =
					accounting.formatNumber(
						convertToZero($("#RAG26").html()),
					 2);
				}

				var bonusPaid = getBonusPaidVal();

				document.getElementById("RA" + a[i] + "29").innerHTML =
					accounting.formatNumber(
						convertToZero($("#RA" + a[i] + "25").html()) +
						convertToZero($("#RA" + a[i] + "26").html()) +
						convertToZero($("#RA" + a[i] + "27").html()) +
						convertToZero(bonusPaid), // bonusPaid is cell 28
					 2);

				document.getElementById("RA" + a[i] + "25").innerHTML = getAppliedToCoverLossesAvailFundsToMgr();
			}
			
			// advance from corp to branch
			 // for 2015
			if (rptYear == 2015) {
				if (convertToZero($("#RA" + a[i] + "21").html()) > convertToZero($("#" + a[i] + "25").html())) {
					document.getElementById("RA" + a[i] + "14").innerHTML = accounting.formatNumber(incInCorpInvAcct, 2);
				}
				else {
					document.getElementById("RA" + a[i] + "14").innerHTML = "-";
				}
			}
			else {
				// for 2016+
				document.getElementById("RA" + a[i] + "14").innerHTML = getAdvFromCorpToBranch();
			}

			// Ending Corp Inv Account
			document.getElementById("RA" + a[i] + "15").innerHTML =
			   accounting.formatNumber(
				convertToZero($("#RA" + a[i] + "11").html()) +
				convertToZero($("#RA" + a[i] + "12").html()) +
				convertToZero($("#RA" + a[i] + "14").html())
				, 2);						
		}

		
		// Total Cap Accounts - keep this even though it is hidden
		for (i = 0; i < a.length; i++) {
			document.getElementById("RA" + a[i] + "24").innerHTML =
				accounting.formatNumber(
					convertToZero($("#RA" + a[i] + "15").html()) +
					convertToZero($("#RA" + a[i] + "22").html())
					, 2);
		}

		// 126 is dependent on 123 so go back and calculate this
		rsrvAcctSubTot();
		netARO();

		indicateMissingForFutureRA();

		// show the request for payment UI
		if ($("#frmRequestForPayment") != null && rptYear >= 2016) {
			maxAvailForDraws = getMaxAvailForDraw();
			document.getElementById("frmRequestForPayment").src = '../BranchPaymentRequests/IndexForNonAdmin?maxAvailForDraws=' + maxAvailForDraws;
		}
	})


function netARO() {
	// net aro lives in both detail report and the reserve report
	for (i = 0; i < a.length; i++) {
		//document.getElementById(a[i] + "30").innerHTML =
		//	accounting.formatNumber(
		//		convertToZero($("#" + a[i] + "20").html())
		//		- convertToZero($("#" + a[i] + "28").html()),
		//	2);
		// -------------------------------------
		var AROPriortoAllocations = convertToZero($("#" + a[i] + "17").html());
		var BranchLoanLossReserveIncrease = convertToZero($("#" + a[i] + "21").html());
		var PaidBacktoCorporate = convertToZero($("#" + a[i] + "22").html());
		var PaidIntoBranchReserves = convertToZero($("#" +a[i]+ "23").html());

		var netAROToRsvrAcnt = 0;
		if (AROPriortoAllocations <= 0) {
			netAROToRsvrAcnt = 0;
		}
		else {
			netAROToRsvrAcnt = AROPriortoAllocations - BranchLoanLossReserveIncrease - PaidBacktoCorporate - PaidIntoBranchReserves;
		}

		
		if (netAROToRsvrAcnt < 0) {
			netAROToRsvrAcnt = 0;
		}
		document.getElementById(a[i] + "30").innerHTML = accounting.formatNumber(netAROToRsvrAcnt, 2);
// --------------------------------------

		// ARO Increases
		document.getElementById("RA" + a[i] + "27").innerHTML = $("#" + a[i] + "30").html();

		if (i > 0) {
			document.getElementById("RA" + a[i] + "26").innerHTML =
			accounting.formatNumber(
				convertToZero($("#RA" + a[i - 1] + "29").html()),
			 2);
		}
		else {
			document.getElementById("RAH26").innerHTML =
			accounting.formatNumber(
				convertToZero($("#RAG26").html()),
			 2);
		}

		// Bonuses are an expense in ARO details and should be a negative in the Reserve Report
		var bonusPaidStr = $("#RA" + a[i] + "28").html();
		if (bonusPaidStr.indexOf(">") > 0) {
			// bonuses have a drill-down link so derive the true number
			var bonusPaid = bonusPaidStr.substr(bonusPaidStr.indexOf(">") + 1, 50);
			bonusPaid = convertToZero(bonusPaid) * -1;
			bonusPaidStr = bonusPaidStr.replace(">", " style = 'color:red'>-");
		}
		else {
			bonusPaid = 0;
			bonusPaidStr = "-";
		}


		document.getElementById("RA" + a[i] + "28").innerHTML = bonusPaidStr;

		// Ending ARO Available for Draws
		document.getElementById("RA" + a[i] + "29").innerHTML =
			accounting.formatNumber(
				convertToZero($("#RA" + a[i] + "25").html()) +
				convertToZero($("#RA" + a[i] + "26").html()) +
				convertToZero($("#RA" + a[i] + "27").html()) +
				convertToZero(bonusPaid), // bonusPaid is cell 28
			 2);		
	}

	rowTotal(30);
	rowAvgValue(30);

	document.getElementById("G30").innerHTML =
		   accounting.formatNumber(
			   (
			   convertToZero($("#T30").html())
			   / convertToZero($("#T5").html())
			   ) * 100,
		   2) + "%";
}



function getBonusPaidVal() {
	// Bonuses are an expense in ARO details and should be a negative in the Reserve Report
	var bonusPaidStr = $("#RA" + a[i] + "28").html();
	if (bonusPaidStr.indexOf(">") > 0) {
		// bonuses have a drill-down link so derive the true number
		var bonusPaid = bonusPaidStr.substr(bonusPaidStr.indexOf(">") + 1, 50);
		bonusPaid = convertToZero(bonusPaid) * -1;
	}
	else {
		bonusPaid = 0;
	}

	return bonusPaid;
}

