(function () {

    var collapsible = document.getElementsByClassName('collapsible');
    collapsible = [].slice.call(collapsible, 0);

    collapsible.forEach(function (collapseTr) {

        // Find title td
        var tds = collapseTr.children;
        var titleTd;
        // How far in the collapsible is
        var indention = 0;
        for (var i = 0; i < tds.length; i++) {
            indention++;
            if (tds[i].innerHTML.trim() !== '') {
                titleTd = tds[i];
                break;
            }
        }

        // Create Arrow link
        var arrow = document.createElement('a');
        arrow.className = 'arrow';
        titleTd.innerHTML = arrow.outerHTML + titleTd.innerHTML;
        titleTd.className = 'title';

        // Set children
        var childrenTr = [];
        var currentTr = collapseTr;
        do {
            currentTr = currentTr.nextElementSibling;
            var childTds = currentTr.children;
            var curIndent = 0;
            for (var i = 0; i < childTds.length; i++) {
                curIndent++;
                if (childTds[i].innerHTML.trim() !== '') {
                    childTds[i].className += ' detail';
                    break;
                }
            }
            if (indention < curIndent) {
                childrenTr.push(currentTr);
            }

        } while (indention < curIndent);

        collapseTr.closeChildren = function () {
            childrenTr.forEach(function (tr) {
                $(tr).addClass('closed');
            });
        };

        // Open children but check make children collapsibles stay collapsed
        collapseTr.openChildren = function () {
            var queue = [];
            childrenTr.forEach(function (tr) {
                if ($(tr).hasClass('collapsed')) {
                    queue.push(tr.closeChildren);
                }
                $(tr).removeClass('closed');
            });

            queue.forEach(function (fn) { fn() });
        };

        // Create click event
        collapseTr.addEventListener('click', function () {
            $(collapseTr).toggleClass('collapsed');
            if ($(collapseTr).hasClass('collapsed')) {
                collapseTr.closeChildren();
            } else {
                collapseTr.openChildren();
            }
        });

    });

    function init() {
        collapsible.forEach(function (c) {
            if ($(c).hasClass('collapsed')) {
                c.closeChildren();
            } else {
                c.openChildren();
            }
        })
    }

    function collapseAll() {
        collapsible.forEach(function (c) {
            $(c).addClass('collapsed');
            c.closeChildren();
        })
    }
    function expandAll () {
        collapsible.forEach(function (c) {
            $(c).removeClass('collapsed');
            c.openChildren();
        })
    }

    init();

    // Global buttons
    var expandBtn = document.getElementById('expand');
    var collapseBtn = document.getElementById('collapse');

    expandBtn.addEventListener('click', expandAll);
    collapseBtn.addEventListener('click', collapseAll);

    // url get parameter
    var doExpand = !!(+location.search.split('expand=')[1]);
    if (doExpand) {
        expandAll();
    }

})();