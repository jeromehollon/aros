﻿lastMonth = 0; // global var that is calculated once in missing value function

function rsrvAcctSubTot() {
		for (i = 0; i < a.length; i++) {
			document.getElementById(a[i] + "26").innerHTML =
				accounting.formatNumber(
				convertToZero($("#" + a[i] + "21").html())
				- convertToZero($("#" + a[i] + "22").html())
				+ convertToZero($("#" + a[i] + "23").html())
				+ convertToZero($("#" + a[i] + "24").html())
				+ convertToZero($("#" + a[i] + "25").html()), 2
				);			

			// Hidden Total Expenses and Allocations to Capital	 = sub total of expenses + Total 7900 - Reserve Account
			document.getElementById(a[i] + "28").innerHTML =
				accounting.formatNumber(convertToZero($("#" + a[i] + "15").html())
				+ convertToZero($("#" + a[i] + "26").html()), 2);
		}
	}


function convertToZero(myNum) {
	if (!myNum) {
		return 0;
	}

	if (isNaN(myNum) === true) {
		if (myNum.indexOf("(") > 0) {
			// extract number that is displayed w/ parenthesis
			myNum = myNum.substring(myNum.indexOf("(") + 1, myNum.indexOf(")"))
		}
	}

	cleanNum = accounting.parse(myNum);

	if (isNaN(cleanNum) === true) {
		newNum = 0;
	}
	else {
		newNum = cleanNum;
	}

	return newNum;
}


function assignedRevenueAmt() {
	// Based on total closed volume and historic ARR for each month
	for (i = 0; i < a.length; i++) {
		if (convertToZero($("#" + a[i] + "5").html()) > 0) {
			document.getElementById(a[i] + "6").innerHTML = accounting.formatNumber(convertToZero($("#" + a[i] + "5").html()) * convertToZero($("#" + a[i] + "8").html()) * .01, 2);            
		}
	}
}


function assignedRevenue() {
	// Assigned Revenue for branch is simply repeated from the previous row
	for (i = 0; i < a.length; i++) {
		document.getElementById(a[i] + "7").innerHTML = accounting.formatNumber(convertToZero($("#" + a[i] + "6").html()), 2);
	}
}


function avgLoanAmt() {
	if (convertToZero(document.getElementById("T4").innerHTML) > 0 && convertToZero(document.getElementById("T5").innerHTML) > 0) {
		avgLoanAmt = convertToZero(document.getElementById("T5").innerHTML) / convertToZero(document.getElementById("T4").innerHTML);
		document.getElementById("U3").innerHTML = accounting.formatNumber(avgLoanAmt, 2);
	}
	else {
		document.getElementById("U3").innerHTML = "-";
	}
}


function rate(numerator, denominator, targetCell) {
	if (convertToZero(document.getElementById(numerator).innerHTML) > 0 && convertToZero(document.getElementById(denominator).innerHTML) > 0) {
		avgLoanAmt = convertToZero(document.getElementById(numerator).innerHTML) / convertToZero(document.getElementById(denominator).innerHTML);
		document.getElementById(targetCell).innerHTML = (accounting.formatNumber(avgLoanAmt * 100, 2)) + "%";
	}
	else {
		document.getElementById(targetCell).innerHTML = "-";
	}
}


function rowTotal(iRow) {
	var total = 0;

	for (i = 0; i < a.length; i++) {
		cellVal = $("#" + a[i] + iRow).html();

		cellVal = convertToZero(cellVal);

		total += Number(cellVal);        
	}

	// add column total for row
	if (iRow === 4) {
		// number of units - not currency format
		total = accounting.formatNumber(total, 0);
	}
	else {
		total = accounting.formatNumber(total, 2);
	}

	if (document.getElementById("T" + iRow) != null) {
		document.getElementById("T" + iRow).innerHTML = total;
	}
	else {
		console.log(iRow);
	}	
}


function totalCOGS() {
	var total = 0;

	for (i = 0; i < a.length; i++) {
		cogsTot = 0;
		for (k = 115; k <= 227; k++) {
			cogsTot = cogsTot + convertToZero($("#" + a[i] + k).html());
		}
		document.getElementById(a[i] + "18").innerHTML =
		  accounting.formatNumber(cogsTot, 2);

		cogs = convertToZero($("#" + a[i] + "18").html());
		assignedRevenue = convertToZero($("#" + a[i] + "7").html());

		cellVal = convertToZero(assignedRevenue - cogs);
		document.getElementById(a[i] + "20").innerHTML = accounting.formatNumber(cellVal, 2);

		total += Number(cellVal);
	}

	rowTotal(18);

	// add row total
	total = accounting.formatNumber(total, 2);
	document.getElementById("T20").innerHTML = total;
}


function expenseSubTot() {
	for (i = 0; i < a.length; i++) {
		document.getElementById(a[i] + "15").innerHTML =
			accounting.formatNumber(
			convertToZero($("#" + a[i] + "40").html())
			+ convertToZero($("#" + a[i] + "63").html())
			+ convertToZero($("#" + a[i] + "79").html())
			+ convertToZero($("#" + a[i] + "96").html())
			+ convertToZero($("#" + a[i] + "10").html())
			+ convertToZero($("#" + a[i] + "114").html())
			+ convertToZero($("#" + a[i] + "50").html()),
			2);
	}
}


function aroPriorToAllocations() {
	// Assigned Revenue Rate for branch
	for (i = 0; i < a.length; i++) {
		aroVal = convertToZero($("#" + a[i] + "20").html()) - convertToZero($("#" + a[i] + "15").html());

		aroVal = accounting.formatNumber(aroVal, 2);

		document.getElementById(a[i] + "17").innerHTML = aroVal;
	}
}


function breadCrumbs(i, j) {
	if (i === 4) {
		alert(j)
	}
}


function allocationToReserve() {
	//------------------------ //
	// ALLOCATIONS TO RESERVES //
	//------------------------ //
	for (i = 0; i < a.length; i++) {
		// 7910	Branch Loan Loss Reserve

		loanLossRsvrRate = convertToZero($("#G121").html()) * .01;
		closedVol = convertToZero($("#" + a[i] + "5").html());

		document.getElementById(a[i] + "21").innerHTML = accounting.formatNumber(loanLossRsvrRate * closedVol, 2);


		// 1st pass - dependent on Reserve Acct Calcs
		// 7920	Allocated Decrease in Corp Investment
		if (convertToZero($("#" + a[i] + "17").html()) - convertToZero($("#" + a[i] + "21").html()) < 1) {
			document.getElementById(a[i] + "22").innerHTML = "-";
		}
		else {
			document.getElementById(a[i] + "22").innerHTML = convertToZero($("#" + a[i] + "17").html()) - convertToZero($("#" + a[i] + "21").html());
		}

		// 7923	Allocated Increase in Branch Operating Reserve
		allocatedIncOpRsrv = convertToZero($("#" + a[i] + "17").html()) - convertToZero($("#" + a[i] + "21").html()) - convertToZero($("#" + a[i] + "22").html());


		// 7930	Reserve account + Advance or (-) Repayment HAS NO CALCS IN SPREAD SHEET
		document.getElementById(a[i] + "24").innerHTML = "-";

		// Advance from Reserve Accounts
		advFromRsrvAcct = convertToZero($("#" + a[i] + "17").html()) - convertToZero($("#" + a[i] + "21").html()) - convertToZero($("#" + a[i] + "22").html());

		if (advFromRsrvAcct < 0) {
			document.getElementById(a[i] + "25").innerHTML = accounting.formatNumber(advFromRsrvAcct, 2);
		}
		else {
			document.getElementById(a[i] + "25").innerHTML = "-";
		}
	}

	// Total 7900 - Reserve Account
	rsrvAcctSubTot();

	// allocations to capital total
	rowTotal(28);

	// allocations to capital rate
	document.getElementById("G28").innerHTML = accounting.formatNumber((convertToZero($("#T28").html()) / convertToZero($("#T5").html())) * 100, 2) + "%";

	// Net Assigned Revenue Override (ARO) is calculated in the Reserve Report
}


function getTotalMnths() {
	var firstMnth = 0;
	var lastMnth = 0;
	var totalMnths = 0;
	
	for (i = 0; i < a.length; i++) {
		// check for 1st month with sub total expenses
		if (convertToZero($("#" + a[i] + "15").html()) != 0 && firstMnth == 0) {
			firstMnth = i + 1;
		}

		if (convertToZero($("#" + a[i] + "15").html()) != 0) {
			lastMnth = i + 1;
		}
	}
	
	if (lastMnth > 0 && firstMnth > 0) {
		totalMnths = lastMnth - firstMnth + 1;
	}

	document.getElementById("$Mnth").innerHTML = totalMnths;

	return totalMnths;
}


function rowAvgValue(iRow) {
	// Avg $ = Total / (1st month number - last month number)
	// % of Revenue = Avg $ / Revenue Avg $
	var totalMnths = 0;
	var total = 0;	

	total = convertToZero($("#T" + iRow).html());
	
	totalMnths = getTotalMnths();
	
	avg = convertToZero(total / totalMnths);

	if (document.getElementById("U" + iRow) != null) {
		document.getElementById("U" + iRow).innerHTML = accounting.formatNumber(avg, 2);
	}
	else {
		console.log("missing avg row " + iRow);
	}

	// BPS %
	closeVol = convertToZero($("#T5").html());
	if (closeVol != 0) {
		bps = total / closeVol;
	}
	else {
		bps = 0;
	}

	if (document.getElementById("V" + iRow) != null) {
		document.getElementById("V" + iRow).innerHTML = accounting.formatNumber(bps * 10000, 0);		
	}
	else {
		console.log("missing avg row " +iRow);
	}
}