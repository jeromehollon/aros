﻿function indicateMissingForFutureRA() {
	// remove values for month before accounting close date - except beginning ARO balance
	for (i = 1; i < a.length; i++) {  // start at January - leave the 'G's          
		for (j = 1; j < 30; j++) {
		   indicateMissingRA(j, a[i]);
		}
	}
}


function indicateMissingRA(rowNum, mnthLetter) {
	if ($("#RA" + mnthLetter + rowNum).html() === "$" + mnthLetter + "$" + rowNum)  {
		document.getElementById("RA" + mnthLetter + rowNum).innerHTML = "-";
	}
	

	if (lastMonth == 0) {
		// call this just once - replace with current month or end-of-year
		rptYear = parseInt($("#lstYear").find(':selected').text(), 10);
		
		var d = new Date();
		if (rptYear < d.getFullYear()) {
			// report is from previous years so the last month is 12
			lastMonth = 12;
		}
		else {
			lastMonth = d.getMonth() + 1;
		}
	}

	var monthNum = getMonthNum(mnthLetter);   

	if (monthNum > lastMonth) {
		if ($("#RA" + mnthLetter + rowNum).length) {
			document.getElementById("RA" + mnthLetter + rowNum).innerHTML = "-";
		}
	}
}


function indicateMissingForFuture() {
	// remove values for month before accounting close date
	for (i = 0; i < a.length; i++) {            
		for (j = 1; j < 1000; j++) { // start at January - leave the 'G's 
			indicateMissing(j, a[i]);
		}
	}
}


function indicateMissing(rowNum, mnthLetter) {    
	if ($("#" + mnthLetter + rowNum).html() == "$" + mnthLetter + "$" + rowNum) {
		document.getElementById(mnthLetter + rowNum).innerHTML = "-";
	}
	
	if (lastMonth == 0) {
		// call this just once - replace with current month or end-of-year
		rptYear = parseInt($("#lstYear").find(':selected').text(), 10);
		
		var d = new Date();
		if (rptYear < d.getFullYear()) {
			// report is from previous years so the last month is 12
			lastMonth = 12;
		}
		else {
			lastMonth = d.getMonth() + 1;
		}
	}

	var monthNum = getMonthNum(mnthLetter);   

	if (monthNum > lastMonth) {
		if ($("#" + mnthLetter + rowNum).length) {
			document.getElementById(mnthLetter + rowNum).innerHTML = "-";
		}
	}
}


function getMonthNum(mnthLetter) {
	var monthNum = 0;

	 switch (mnthLetter) {
		case ("H"):
			monthNum = 1;
			break;
		case ("I"):
			monthNum = 2;
			break;
		 case ("J"):
			 monthNum = 3;
			 break;
		 case ("K"):
			 monthNum = 4;
			 break;
		 case ("L"):
			 monthNum = 5;
			 break;
		 case ("M"):
			 monthNum = 6;
			 break;
		 case ("N"):
			 monthNum = 7;
			 break;
		 case ("O"):
			 monthNum = 8;
			 break;
		 case ("P"):
			 monthNum = 9;
			 break;
		 case ("Q"):
			 monthNum = 10;
			 break;
		 case ("R"):
			 monthNum = 11;
			 break;
		 case ("S"):
			 monthNum = 12;
			 break;
	}

	return monthNum;
}


function getloanLossRsrvIncrease2015() {
	// note: this is currently in use
	// Test loan loss reserve balance against the cap to set the loan loss reserve increase
	var loanLossInc = 0;
	var loanLossRsvrRate = convertToZero($("#G121").html()) * .01;
	var closedVol = convertToZero($("#" + a[i] + "5").html());
	var loanLossRsrvReqCap = convertToZero($("#RAG1").html());
	var loanLossRsrvBal = convertToZero($("#RA" + a[i] + "6").html());

	if (loanLossRsrvBal >= loanLossRsrvReqCap) {
		loanLossInc = 0;
	}
	else if ((loanLossRsrvReqCap - loanLossRsrvBal) < (closedVol * loanLossRsvrRate)) {
		loanLossInc = loanLossRsrvReqCap - loanLossRsrvBal;
	}
	else {
		loanLossInc = closedVol * loanLossRsvrRate;
	}

	return accounting.formatNumber(loanLossInc, 2);
}


// -------------------------------------------------------------------------------------
function getLoanLossRsrvIncrease() {
	// Test loan loss reserve balance against the cap to set the loan loss reserve increase
	var loanLossRsrvIncr = 0;
   
	var incomeFromOps = convertToZero($("#" + a[i] + "17").html());
	var loanLossRsvrRate = convertToZero($("#G121").html()) * .01;
	var closedVol = convertToZero($("#" + a[i] + "5").html());
	var loanLossRsrvReqCap = convertToZero($("#RAG1").html());
	var loanLossRsrvBal = convertToZero($("#RA" + a[i] + "6").html());

	loanLossRsrvIncr = calcloanLossRsrvIncrease(incomeFromOps, loanLossRsvrRate, closedVol, loanLossRsrvReqCap, loanLossRsrvBal);

	return accounting.formatNumber(loanLossRsrvIncr, 2);
}


function calcloanLossRsrvIncrease(incomeFromOps, loanLossRsvrRate, closedVol, loanLossRsrvReqCap, loanLossRsrvBal) {
	// Test loan loss reserve balance against the cap to set the loan loss reserve increase
	var loanLossRsrvIncr = 0;
	var loanLossRsrvIncr1 = 0;
	var loanLossRsrvIncr2 = 0;

	if (loanLossRsrvBal < 0 && incomeFromOps > 0) {
		if (incomeFromOps < (- loanLossRsrvBal) + (closedVol * loanLossRsvrRate)) {
			loanLossRsrvIncr1 = incomeFromOps - (closedVol * loanLossRsvrRate);
		} 
		else {
			loanLossRsrvIncr1 = (-loanLossRsrvBal);
		}
	}
	else {
		loanLossRsrvIncr1 = 0;
	}

	if (loanLossRsrvBal >= 0 &&	loanLossRsrvReqCap <= loanLossRsrvBal) {
		loanLossRsrvIncr2 = 0;
	}
	else if ((loanLossRsrvReqCap - loanLossRsrvBal) > (closedVol * loanLossRsvrRate)) {
		loanLossRsrvIncr2 = (closedVol * loanLossRsvrRate);
	}
	else {
		loanLossRsrvIncr2 = (loanLossRsrvReqCap - loanLossRsrvBal);
	}

	loanLossRsrvIncr = loanLossRsrvIncr1 + loanLossRsrvIncr2;

	return accounting.formatNumber(loanLossRsrvIncr, 2);
}


// -------------------------------------------------------------------------------------
function getAppliedToCoverLossesAvailFundsToMgr() {
	var appliedToCoverLosses = 0;
   
	var borrowedFromRsrvAcct = convertToZero($("#" + a[i] + "25").html());
	var appliedToCoverLossBranchRsrv = convertToZero($("#RA" + a[i] + "21").html());

	if (i == 0) {
		 var begAROAcct = convertToZero($("#RAG26").html());
	}
	else {
		var begAROAcct = convertToZero($("#RA" + a[i - 1] + "29").html());
	}	
	
	appliedToCoverLosses = calcAppliedToCoverLossesAvailFundsToMgr(borrowedFromRsrvAcct, appliedToCoverLossBranchRsrv, begAROAcct);

	return accounting.formatNumber(appliedToCoverLosses, 2);
}


function calcAppliedToCoverLossesAvailFundsToMgr(borrowedFromRsrvAcct, appliedToCoverLossBranchRsrv, begAROAcct) {
	var appliedToCoverLosses = 0;
	
	if (borrowedFromRsrvAcct - appliedToCoverLossBranchRsrv >= 0 || begAROAcct <= 0) {
		appliedToCoverLoss = 0;
	}
	else if (((borrowedFromRsrvAcct * (-1)) + appliedToCoverLossBranchRsrv - begAROAcct) > 0) {
		appliedToCoverLoss = (begAROAcct * (-1));
	}
	else {
		appliedToCoverLoss = borrowedFromRsrvAcct - appliedToCoverLossBranchRsrv;
	}	

	return appliedToCoverLoss;
}
// -------------------------------------------------------------------------------------

function getAdvFromCorpToBranch() {
	var advFromCorpToBranch = 0;
   
	var borrowedFromRsrvAccts = convertToZero($("#" + a[i] + "25").html());
	var appliedToCoverLossAvailFundsToMgr = convertToZero($("#RA" + a[i] + "25").html());
	// todo: this is not picking up a value so I kludge in the calc function
	var appliedToCoverLossBranchRsrv = convertToZero($("#RA" + a[i] + "21").html());
	
	advFromCorpToBranch = calcAdvFromCorpToBranch(borrowedFromRsrvAccts, appliedToCoverLossAvailFundsToMgr, appliedToCoverLossBranchRsrv);

	return accounting.formatNumber(advFromCorpToBranch, 2);
}


function calcAdvFromCorpToBranch(borrowedFromRsrvAccts, appliedToCoverLossAvailFundsToMgr, appliedToCoverLossBranchRsrv) {
	var advFromCorpToBranch = 0;

	var appliedToCoverLossBranchRsrv = convertToZero($("#RA" + a[i] + "21").html());

	if (borrowedFromRsrvAccts >= 0) {
		advFromCorpToBranch = 0;
	}
	else if (appliedToCoverLossBranchRsrv + appliedToCoverLossAvailFundsToMgr > borrowedFromRsrvAccts) {
		advFromCorpToBranch = (borrowedFromRsrvAccts - appliedToCoverLossBranchRsrv - appliedToCoverLossAvailFundsToMgr) * -1;

	}
	else {
		advFromCorpToBranch = 0;
	}

	return advFromCorpToBranch;
}


// -------------------------------------------------------------------------------------
function getPaidIntoAcctFromBranch() {
	var paidIntoAcctFromBranch = 0;

	var incomeFromOps = convertToZero($("#" + a[i] + "17").html());
	var loanLossRsrvIncr = convertToZero($("#RA" + a[i] + "7").html());
	var paidBackToCorp = convertToZero($("#RA" + a[i] + "12").html());

	var begBranchRsrv = convertToZero($("#RA" + a[i] + "18").html());
	var branchRsrvCap = convertToZero($("#G0").html());

	paidIntoAcctFromBranch = calcPaidIntoAcctFromBranch(incomeFromOps, loanLossRsrvIncr, paidBackToCorp, begBranchRsrv, branchRsrvCap);
	return accounting.formatNumber(paidIntoAcctFromBranch, 2);
}


function calcPaidIntoAcctFromBranch(incomeFromOps, loanLossRsrvIncr, paidBackToCorp, begBranchRsrv, branchRsrvCap) {
	var paidIntoAcctFromBranch = 0;
	paidBackToCorp = paidBackToCorp * -1;

	if ((incomeFromOps - loanLossRsrvIncr - paidBackToCorp) <= 0) {
		paidIntoAcctFromBranch = 0;
	}
		else if((begBranchRsrv < branchRsrvCap) && 
			(incomeFromOps - loanLossRsrvIncr - paidBackToCorp) < (branchRsrvCap - begBranchRsrv)) {

			paidIntoAcctFromBranch = incomeFromOps - loanLossRsrvIncr - paidBackToCorp;
	}
	else {				
			paidIntoAcctFromBranch = branchRsrvCap - begBranchRsrv;			
	}

	return paidIntoAcctFromBranch;
}


function getAppliedToCoverLoss() {
	var borrowedFromRsrvAccts = convertToZero($("#" + a[i] + "25").html());
	var begBranchRsrv = convertToZero($("#RA" + a[i] + "18").html());

	if (borrowedFromRsrvAccts + begBranchRsrv > 0) {
		appliedToCoverLoss = borrowedFromRsrvAccts;
	}
	else {		
		appliedToCoverLoss = begBranchRsrv;
	}  
	
	if (appliedToCoverLoss > 0) {
		appliedToCoverLoss = appliedToCoverLoss * (-1);
	}
	

	return appliedToCoverLoss;
}

// -------------------------------------------------------------------------------------

function getParameterByName(name) {
	var url = window.location.href;
	url = url.toLowerCase(); // This is just to avoid case sensitiveness  
	name = name.replace(/[\[\]]/g, "\\$&").toLowerCase();// This is just to avoid case sensitiveness for query parameter name
	var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(url);
	if (!results) return null;

	return decodeURIComponent(results[2].replace(/\+/g, " "));
}


function getRptYear() {
	var year = $("#btnPdf").attr("href");

	if (year != null) {
		year = year.substr(year.indexOf("yearSelected") + 13, 4);
	}
	else if($('#lstYear').val() != null) {
		year = $('#lstYear').val();
	}
	else {
		today.getFullYear();
	}

	return year;
}



function convertToZero(myNum) {
	if (!myNum) {
		return 0;
	}

	if (isNaN(myNum) === true) {
		if (myNum.indexOf("(") > 0) {
			// extract number that is displayed w/ parenthesis
		    myNum = myNum.substring(myNum.indexOf("(") + 1, myNum.indexOf(")"));
		}
	}

	cleanNum = accounting.parse(myNum);

	if (isNaN(cleanNum) === true) {
		newNum = 0;
	}
	else {
		newNum = cleanNum;
	}

	return newNum;
}