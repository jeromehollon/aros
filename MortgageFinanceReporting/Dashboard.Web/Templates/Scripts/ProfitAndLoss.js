﻿lastMonth = 0; // global var that is calculated once in missing value function

$(document).ready(function () {
    a = new Array("H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S");
    
    indicateMissingForFuture();

    // revenue income from loans is unique to the P & L
    totalRevenueIncome();

    // account total calcs (sum over columns for a given row)
    rowTotal(40);
    rowTotal(63);
    rowTotal(79);
    rowTotal(96);
    rowTotal(10);
    rowTotal(114);
    rowTotal(50);

    // branch calcs
    rowTotal(3);
    rowTotal(4);
    rowTotal(5);
    rowTotal(7);
    avgLoanAmt();

    // COGS
    totalCOGSWithRevInc();

    // Rates
    rate("T18", "T5", "G18");
    rate("T20", "T5", "G20");
    rate("T40", "T5", "G40");
    rate("T63", "T5", "G63");
    rate("T79", "T5", "G79");
    rate("T96", "T5", "G96");
    rate("T10", "T5", "G10");
    rate("T114", "T5", "G114");
    rate("T50", "T5", "G50");

    // expense summaries
    expenseSubTot();
    rowTotal(15);

    totalNetIncome();

    rowTotal(17);

    // row totals
    rowTotal(700);
    rowTotal(705);
    rowTotal(710);
    rowTotal(715);
    rowTotal(720);
    rowTotal(725);
    rowTotal(730);
    rowTotal(732);
    rowTotal(735);
    rowTotal(740);
    rowTotal(7);
    rowTotal(100);
    rowTotal(105);
    rowTotal(115);
    rowTotal(130);
    rowTotal(155);
    rowTotal(165);
    rowTotal(180);
    rowTotal(182);
    rowTotal(185);
    rowTotal(190);
    rowTotal(195);
    rowTotal(205);
    rowTotal(207);
    rowTotal(210);
    rowTotal(215);
    rowTotal(220);
    rowTotal(225);
    rowTotal(50);
    rowTotal(15);
    rowTotal(60);

    rowTotal(550);
    rowTotal(555);
    rowTotal(556);
    rowTotal(557);
    rowTotal(558);
    rowTotal(561);
    rowTotal(562);
    rowTotal(563);
    rowTotal(503);
    rowTotal(505);
    rowTotal(508);
    rowTotal(510);
    rowTotal(511);
    rowTotal(512);
    rowTotal(513);
    rowTotal(515);
    rowTotal(520);
    rowTotal(580);
    rowTotal(585);
    rowTotal(590);
    rowTotal(565);
    rowTotal(570);
    rowTotal(525);
    rowTotal(530);
    rowTotal(535);
    rowTotal(96);
    rowTotal(245);
    rowTotal(250);
    rowTotal(255);
    rowTotal(260);
    rowTotal(265);
    rowTotal(270);
    rowTotal(275);
    rowTotal(282);
    rowTotal(280);
    rowTotal(285);
    rowTotal(290);
    rowTotal(295);
    rowTotal(300);
    rowTotal(305);
    rowTotal(310);
    rowTotal(315);
    rowTotal(320);
    rowTotal(325);
    rowTotal(335);
    rowTotal(40);
    rowTotal(385);
    rowTotal(340);
    rowTotal(345);
    rowTotal(350);
    rowTotal(355);
    rowTotal(360);
    rowTotal(365);
    rowTotal(370);
    rowTotal(375);
    rowTotal(378);
    rowTotal(380);
    rowTotal(63);
    rowTotal(450);
    rowTotal(395);
    rowTotal(400);
    rowTotal(405);
    rowTotal(410);
    rowTotal(415);
    rowTotal(420);
    rowTotal(425);
    rowTotal(430);
    rowTotal(435);
    rowTotal(440);
    rowTotal(445);
    rowTotal(455);
    rowTotal(460);
    rowTotal(465);
    rowTotal(485);
    rowTotal(490);
    rowTotal(495);
    rowTotal(500);
    rowTotal(79);
    rowTotal(595);
    rowTotal(600);
    rowTotal(610);
    rowTotal(605);
    rowTotal(612);
    rowTotal(615);
    rowTotal(10);
    rowTotal(620);
    rowTotal(625);
    rowTotal(630);
    rowTotal(635);
    rowTotal(640);
    rowTotal(645);
    rowTotal(114);
    rowTotal(655);
    rowTotal(660);
    rowTotal(665);
    rowTotal(670);
    rowTotal(227);
            
    rowAvgValue(5);
    rowAvgValue(7);
    rowAvgValue(700);
    rowAvgValue(705);
    rowAvgValue(710);
    rowAvgValue(715);
    rowAvgValue(720);
    rowAvgValue(725);
    rowAvgValue(730);
    rowAvgValue(732);
    rowAvgValue(735);
    rowAvgValue(740);    
    rowAvgValue(100);
    rowAvgValue(105);
    rowAvgValue(115);
    rowAvgValue(130);
    rowAvgValue(155);
    rowAvgValue(165);
    rowAvgValue(180);
    rowAvgValue(182);
    rowAvgValue(185);
    rowAvgValue(190);
    rowAvgValue(195);
    rowAvgValue(205);
    rowAvgValue(207);
    rowAvgValue(210);
    rowAvgValue(215);
    rowAvgValue(220);
    rowAvgValue(225);
    rowAvgValue(18);
    rowAvgValue(20);
    rowAvgValue(50);
    rowAvgValue(15);
    rowAvgValue(17);

    rowAvgValue(550);
    rowAvgValue(555);
    rowAvgValue(556);
    rowAvgValue(557);
    rowAvgValue(558);
    rowAvgValue(561);
    rowAvgValue(562);
    rowAvgValue(563);
    rowAvgValue(503);
    rowAvgValue(505);
    rowAvgValue(508);
    rowAvgValue(510);
    rowAvgValue(511);
    rowAvgValue(512);
    rowAvgValue(513);
    rowAvgValue(515);
    rowAvgValue(520);
    rowAvgValue(580);
    rowAvgValue(585);
    rowAvgValue(590);
    rowAvgValue(565);
    rowAvgValue(570);
    rowAvgValue(525);
    rowAvgValue(530);
    rowAvgValue(535);
    rowAvgValue(96);
    rowAvgValue(245);
    rowAvgValue(250);
    rowAvgValue(255);
    rowAvgValue(260);
    rowAvgValue(265);
    rowAvgValue(270);
    rowAvgValue(275);
    rowAvgValue(282);
    rowAvgValue(280);
    rowAvgValue(285);
    rowAvgValue(290);
    rowAvgValue(295);
    rowAvgValue(300);
    rowAvgValue(305);
    rowAvgValue(310);
    rowAvgValue(315);
    rowAvgValue(320);
    rowAvgValue(325);
    rowAvgValue(335);
    rowAvgValue(40);
    rowAvgValue(385);
    rowAvgValue(340);
    rowAvgValue(345);
    rowAvgValue(350);
    rowAvgValue(355);
    rowAvgValue(360);
    rowAvgValue(365);
    rowAvgValue(370);
    rowAvgValue(375);
    rowAvgValue(378);
    rowAvgValue(380);
    rowAvgValue(63);
    rowAvgValue(450);
    rowAvgValue(395);
    rowAvgValue(400);
    rowAvgValue(405);
    rowAvgValue(410);
    rowAvgValue(415);
    rowAvgValue(420);
    rowAvgValue(425);
    rowAvgValue(430);
    rowAvgValue(435);
    rowAvgValue(440);
    rowAvgValue(445);
    rowAvgValue(455);
    rowAvgValue(460);
    rowAvgValue(465);
    rowAvgValue(485);
    rowAvgValue(490);
    rowAvgValue(495);
    rowAvgValue(500);
    rowAvgValue(79);
    rowAvgValue(595);
    rowAvgValue(600);
    rowAvgValue(610);
    rowAvgValue(605);
    rowAvgValue(612);
    rowAvgValue(615);
    rowAvgValue(10);
    rowAvgValue(620);
    rowAvgValue(625);
    rowAvgValue(630);
    rowAvgValue(635);
    rowAvgValue(640);
    rowAvgValue(645);
    rowAvgValue(114);
    rowAvgValue(655);
    rowAvgValue(660);
    rowAvgValue(665);
    rowAvgValue(670);
    rowAvgValue(227);

});


function totalCOGSWithRevInc() {

    for (i = 0; i < a.length; i++) {
        cogsTot = 0;
        for (k = 115; k <= 235; k++) {
            cogsTot = cogsTot + convertToZero($("#" + a[i] + k).html());

        }
        document.getElementById(a[i] + "18").innerHTML =
                accounting.formatNumber(cogsTot, 2);

        cogs = convertToZero($("#" + a[i] + "18").html());

        // operating income = Total Revenue - COGS
        opsIncome = convertToZero($("#" + a[i] + "7").html()) - convertToZero($("#" + a[i] + "18").html());
        document.getElementById(a[i] + "20").innerHTML = accounting.formatNumber(opsIncome, 2);
    }

    // add row total
    rowTotal(18);
    rowTotal(20);
}

function totalNetIncome() {
    // Assigned Revenue Rate for branch
    for (i = 0; i < a.length; i++) {
        aroVal = convertToZero($("#" + a[i] + "20").html()) - convertToZero($("#" + a[i] + "15").html());

        aroVal = accounting.formatNumber(aroVal, 2);

        document.getElementById(a[i] + "17").innerHTML = aroVal;
    }
}


function totalRevenueIncome() {
    var grandTotal = 0;

    for (i = 0; i < a.length; i++) {
        tot = 0;

        tot = convertToZero($("#" + a[i] + "715").html()) + convertToZero($("#" + a[i] + "740").html());

        cellVal = convertToZero(tot);
        document.getElementById(a[i] + "7").innerHTML = accounting.formatNumber(cellVal, 2);

        grandTotal += Number(cellVal);
    }

    // add row total
    grandTotal = accounting.formatNumber(grandTotal, 2);
    document.getElementById("T7").innerHTML = grandTotal;
}

