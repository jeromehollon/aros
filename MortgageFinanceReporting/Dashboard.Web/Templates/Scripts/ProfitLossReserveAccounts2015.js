﻿// used for 2015 only - pre server-side Allocations logic for 2016+
lastMonth = 0; // global var that is calculated once in missing value function
var rptYear = getRptYear();

	$(document).ready(function () {
		a = new Array("H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S");

		//****************************** //
		// Jan Loan Loss Reserve         //
		// handled by server side script //
		//****************************** //

		var today = new Date();
		var todaysMnth = today.getMonth() + 1;
		var todaysYear = today.getFullYear();

		for (i = 0; i < a.length; i++) {			
			operatingIncome();            

			aroPriorToAllocations();           

			//************************//
			// Branch Reserve Account //
			//************************//
			// loan loss reserve
			if (i >= 1) {
				// don't show value if in the future except for PDFs which have no lstYear                 
				if (todaysMnth >= i + 1 || todaysYear > $('#lstYear').val() || $('#lstYear').val() == null) {
					document.getElementById("RA" + a[i] + "6").innerHTML = accounting.formatNumber(convertToZero($("#RA" + a[i - 1] + "8").html()), 2);
				}
				else {
					document.getElementById("RA" + a[i] + "6").innerHTML = "-";
				}
			}

			LoanLossRsrvContrib();

			// get loan loss reserve utilized value from hidden field that is totaled on server side
			if ($("#RA" + a[i] + "5").html().indexOf("$5") > 0) {
				loanLossRsvrUtl = 0;
			}
			else {
				loanLossRsvrUtl = convertToZero($("#RA" + a[i] + "5").html());
			}

			document.getElementById("RA" + a[i] + "8").innerHTML = accounting.formatNumber(convertToZero($("#RA" + a[i] + "6").html()) + convertToZero($("#RA" + a[i] + "7").html()) + loanLossRsvrUtl, 2);

			//NOTE: process branch reserve acct section before Corp Cap Acct section since it depends on branch reserve acct values
			//NOTE:  151 (RA18) - 1st pass is the branch reserve cap [C20] which is done server-side
			if (i >= 1) {
				document.getElementById("RA" + a[i] + "18").innerHTML = accounting.formatNumber(convertToZero($("#RA" + a[i - 1] + "22").html()), 2);

				if (convertToZero($("#" + a[i] + "15").html()) === 0) {
					// set branch reserve to 0 when expenses missing
					document.getElementById("RA" + a[i] + "18").innerHTML = "-"
				}
			}
			else {
				document.getElementById("RAH18").innerHTML = accounting.formatNumber(convertToZero($("#RAG18").html()), 2);
			}


			// Allocated Increase in Branch Operating Reserve
			corpContribBegin = convertToZero($("#RA" + a[i] + "11").html());

			if (corpContribBegin != convertToZero($("#" + a[i] + "22").html())) {
				document.getElementById(a[i] + "23").innerHTML = "-";
			}			
			
			//****************************** //
			// Corp Cap (Investment) Account //
			//****************************** //

			// don't show value if in the future except for PDFs which have not lstYear                 
			if (todaysMnth >= i + 1 || todaysYear > $('#lstYear').val() || $('#lstYear').val() == null) {
				document.getElementById("RA" + a[i] + "11").innerHTML = accounting.formatNumber(convertToZero($("#RA" + a[i - 1] + "15").html()), 2);
			}
			else {
				document.getElementById("RA" + a[i] + "11").innerHTML = "-";
			}

			// january is alway the beginning amount reflected in cell g
			document.getElementById("RAH11").innerHTML = accounting.formatNumber(convertToZero($("#RAG11").html()), 2);


			// Allocated Decrease in Corp Cap Acct
			priorAllocationMinusRsrv = convertToZero($("#" + a[i] + "17").html()) - convertToZero($("#" + a[i] + "21").html());
			corpContribBegin = convertToZero($("#RA" + a[i] + "11").html());		   
		  

			// new 'Paid Back to Corp' logic
			loanLossRsvrIncrease = convertToZero($("#RA" + a[i] + "7").html());
				
			totalRevenue = convertToZero($("#" + a[i] + "715").html()) + convertToZero($("#" + a[i] + "740").html());
			netIncomeFromOps = convertToZero((totalRevenue - convertToZero($("#" + a[i] + "18").html()) - convertToZero($("#" + a[i] + "15").html())));

			if (netIncomeFromOps - loanLossRsvrIncrease < 1) {
				document.getElementById("RA" + a[i] + "12").innerHTML = "-";
			}
			else {
				if (corpContribBegin > 0) {
					if (netIncomeFromOps - loanLossRsvrIncrease > 0) {
						if (netIncomeFromOps - loanLossRsvrIncrease < corpContribBegin) {
							document.getElementById("RA" + a[i] + "12").innerHTML = accounting.formatNumber((netIncomeFromOps - loanLossRsvrIncrease) * -1, 2);
						}
						else {
							document.getElementById("RA" + a[i] + "12").innerHTML = accounting.formatNumber(corpContribBegin * -1, 2);
						}
					}
					else {
						document.getElementById("RA" + a[i] + "12").innerHTML = "-";
					}
				}
				else {
					document.getElementById("RA" + a[i] + "12").innerHTML = "-";
				}
			}
		   
			// NOTE: this step is repeated from ARO2015.htm because 122 (Acct #7920) is redefined above - this is the 2nd sweep
			allocatedIncOpRsrv = (convertToZero($("#" + a[i] + "17").html()) - convertToZero($("#RA" + a[i] + "7").html()) - convertToZero($("#RA" + a[i] + "11").html()));
		   
			 // for 2015
			if (rptYear == 2015) {
				if (allocatedIncOpRsrv > 1) {
					// if Corporate Investment Balance - 7920 - Paid Back to Corporate is zero
					if (convertToZero($("#RA" + a[i] + "11").html()) + convertToZero($("#RA" + a[i] + "12").html()) === 0) {
						// Corporate Cap Contribution (beginning) - Acct 7920 equal 0

						if (convertToZero($("#RA" + a[i] + "18").html()) <= convertToZero($("#G0").html())) {
							// if Branch Reserve Balance <= RESERVE REQUIRED FOR BRANCH CAP
							if ((allocatedIncOpRsrv / 2) < (convertToZero($("#G0").html()) - convertToZero($("#RA" + a[i] + "18").html()))) {
								// if (Allocated Increase in Branch Operating Reserve/2) < (Reserve Required Cap - Branch Reserve Balance)
								document.getElementById(a[i] + "23").innerHTML = accounting.formatNumber(allocatedIncOpRsrv / 2, 2); // (ARO PRIOR TO ALLOCATIONS - Acct 7910 - Acct 7920) divided by 2							
							}
							else {
								document.getElementById(a[i] + "23").innerHTML = accounting.formatNumber(convertToZero($("#RAG24").html()) - convertToZero($("#RA" + a[i] + "18").html()), 2);
								// 7923	Paid into Branch Reserves = RESERVE REQUIRED FOR BRANCH - Branch Reserve Balance
							}
						}
						else {
							document.getElementById("RA" + a[i] + "14").innerHTML = "-";
							document.getElementById(a[i] + "23").innerHTML = "-";
						}
					}
					else {
						document.getElementById(a[i] + "23").innerHTML = "-";
					}
				}
				else {
					document.getElementById(a[i] + "23").innerHTML = "-";
				}

				// for 2015
				document.getElementById("RA" + a[i] + "19").innerHTML = accounting.formatNumber(convertToZero($("#" + a[i] + "23").html()), 2);
			}
			else {
				// 2016+
				document.getElementById("RA" + a[i] + "19").innerHTML = getPaidIntoAcctFromBranch();
				// Paid into Branch Reserves = Paid Into Account From Branch
				document.getElementById(a[i] + "23").innerHTML = getPaidIntoAcctFromBranch();
			}
			
			// Ending Reserve Account (1st pass)
			endingRsrvrAcct();
			
			// -------------------------------- //
			// new Advanced From Corp to Branch //
			// -------------------------------- //     
			getPaidBackToCorp();

			paidBackToCorp = convertToZero($("#" + a[i] + "22").html());
			
			paidIntoBranchRsrvs = convertToZero($("#RA" + a[i] + "19").html());

			borrowedFromRsvrAccts = convertToZero((netIncomeFromOps - loanLossRsvrIncrease + paidBackToCorp - paidIntoBranchRsrvs));
			borrowedFromRsvrAccts = Math.abs(borrowedFromRsvrAccts) * (-1);
			borrowedFromRsvrAccts =  Math.round(borrowedFromRsvrAccts * 100) / 100;
	
			branchRsrvBegBal = convertToZero($("#RA" + a[i] + "18").html());
			
			appliedToCoverLoss = getAppliedToCoverLoss();
			getBorrowedFromReserveForPL();
			
			document.getElementById("RA" + a[i] + "21").innerHTML = accounting.formatNumber(appliedToCoverLoss, 2);			    
			
			// Ending Corp Inv Account (2nd pass)
			endingRsrvrAcct(); 
			
			// for 2015
			if (rptYear == 2015) {
				document.getElementById("RA" + a[i] + "25").innerHTML = "-";
			}
			else {
				// Applied To Cover Losses Avail Funds To Mgr is new in 2016 but run it for 2015 - it will just be blank
				 if (i > 0) {
					document.getElementById("RA" + a[i] + "26").innerHTML =
					accounting.formatNumber(
						convertToZero($("#RA" + a[i - 1] + "29").html()),
					 2);
				}
				else {
					document.getElementById("RAH26").innerHTML =
					accounting.formatNumber(
						convertToZero($("#RAG26").html()),
					 2);
				}
			  
				 document.getElementById("RA" + a[i] + "25").innerHTML = getAppliedToCoverLossesAvailFundsToMgr();
							 
			}
			// todo: keep these next 2 line?
			//document.getElementById(a[i] + "28").innerHTML =
			//	accounting.formatNumber(convertToZero($("#" + a[i] + "15").html())
			//	+ convertToZero($("#" + a[i] + "26").html()), 2);
			aroIncreasesPerMnth();
 appliedToCoverLoss = getAppliedToCoverLoss();
 getBorrowedFromReserveForPL();
 document.getElementById("RA" + a[i] + "21").innerHTML = accounting.formatNumber(appliedToCoverLoss, 2);

 if (rptYear >= 2016) {
	document.getElementById("RA" + a[i] + "25").innerHTML = getAppliedToCoverLossesAvailFundsToMgr();
 }


// Ending Corp Inv Account (3rd pass)
 endingRsrvrAcct();

			
			
			// ADVANCE FROM CORP BRANCH
			// for 2015
			if (rptYear == 2015) {
				if (appliedToCoverLoss >= borrowedFromRsvrAccts) {			    
					document.getElementById("RA" + a[i] + "14").innerHTML = "-";
				}
				else {
					document.getElementById("RA" + a[i] + "14").innerHTML = accounting.formatNumber(Math.abs(borrowedFromRsvrAccts - appliedToCoverLoss), 2);
				}
			}
			// for 2016
			else {
				document.getElementById("RA" + a[i] + "14").innerHTML = getAdvFromCorpToBranch();
			}

			// -------------------------------- //
			// Ending Corp Inv Account          //
			// -------------------------------- //
			// Corporate Investment Balance + Paid Back To Corporate + Advanced From Corporate to Branch
			document.getElementById("RA" + a[i] + "15").innerHTML =
				accounting.formatNumber(
				convertToZero($("#RA" + a[i] + "11").html()) +
				convertToZero($("#RA" + a[i] + "12").html()) +
				convertToZero($("#RA" + a[i] + "14").html())
				, 2);
			}		
		

		// Total Cap Accounts - keep this even though it is hidden
		for (i = 0; i < a.length; i++) {
			document.getElementById("RA" + a[i] + "24").innerHTML =
				accounting.formatNumber(
					convertToZero($("#RA" + a[i] + "15").html()) +
					convertToZero($("#RA" + a[i] + "22").html())
					, 2);			
		}


		// 126 is dependent on 123 so go back and calculate this
		rsrvAcctSubTot();
// todo: remove this next line?
		//aroIncreases();

		allocationToReserveForPL();
		netARO();   
		
		indicateMissingForFutureRA();
		
		// show the request for payment UI
		if ($("#frmRequestForPayment") != null && rptYear >= 2016) {
			maxAvailForDraws = getMaxAvailForDraw();
			document.getElementById("frmRequestForPayment").src = '../BranchPaymentRequests/IndexForNonAdmin?maxAvailForDraws=' + maxAvailForDraws;
		}
	})


function aroIncreases() { 
	for (i = 0; i < a.length; i++) {       
		// operating income - Hidden Total Expenses and Allocations to Capital
		var netAROToRsvrAcnt = (convertToZero($("#" + a[i] + "20").html()) -				
				convertToZero($("#" + a[i] + "28").html()));		
		
		document.getElementById(a[i] + "30").innerHTML = accounting.formatNumber(netAROToRsvrAcnt, 2);

		document.getElementById("RA" + a[i] + "27").innerHTML = $("#" + a[i] + "30").html();

		  if (i > 0) {
			document.getElementById("RA" + a[i] + "26").innerHTML =
			accounting.formatNumber(
				convertToZero($("#RA" + a[i - 1] + "29").html()),
			 2);
		}
		else {
			document.getElementById("RAH26").innerHTML =
			accounting.formatNumber(
				convertToZero($("#RAG26").html()),
			 2);
		  }

		// Ending ARO Available for Draws = (ARO Avail For Draws Increase + Bonuses Paid)
		var bonusPaid = getBonusPaidVal();
		document.getElementById("RA" + a[i] + "29").innerHTML =
			accounting.formatNumber(
				convertToZero($("#RA" + a[i] + "25").html()) +
				convertToZero($("#RA" + a[i] + "26").html()) +
				convertToZero($("#RA" + a[i] + "27").html()) +
				convertToZero(bonusPaid), // bonusPaid is cell 28
			 2);
	}
}


function aroIncreasesPerMnth() {       
		// operating income - Hidden Total Expenses and Allocations to Capital
		//var netAROToRsvrAcnt = (convertToZero($("#" + a[i] + "20").html()) - convertToZero($("#" + a[i] + "28").html()));		

	var AROPriortoAllocations = convertToZero($("#" + a[i] + "17").html());
	var BranchLoanLossReserveIncrease = convertToZero($("#" + a[i] + "21").html());
	var PaidBacktoCorporate = convertToZero($("#" + a[i] + "22").html());
	var PaidIntoBranchReserves = convertToZero($("#" + a[i] + "23").html());

	var netAROToRsvrAcnt = 0;
	if (AROPriortoAllocations <= 0) {
		netAROToRsvrAcnt = 0;
	}
	else {
		netAROToRsvrAcnt = AROPriortoAllocations - BranchLoanLossReserveIncrease + PaidBacktoCorporate - PaidIntoBranchReserves;
	}

		
		if (netAROToRsvrAcnt < 0) {
			netAROToRsvrAcnt = 0;
		}
		document.getElementById(a[i] + "30").innerHTML = accounting.formatNumber(netAROToRsvrAcnt, 2);

		document.getElementById("RA" + a[i] + "27").innerHTML = $("#" + a[i] + "30").html();

		  if (i > 0) {
			document.getElementById("RA" + a[i] + "26").innerHTML =
			accounting.formatNumber(
				convertToZero($("#RA" + a[i - 1] + "29").html()),
			 2);
		}
		else {
			document.getElementById("RAH26").innerHTML =
			accounting.formatNumber(
				convertToZero($("#RAG26").html()),
			 2);
		  }

		// Ending ARO Available for Draws = (ARO Avail For Draws Increase + Bonuses Paid)
		var bonusPaid = getBonusPaidVal();
		document.getElementById("RA" + a[i] + "29").innerHTML =
			accounting.formatNumber(
				convertToZero($("#RA" + a[i] + "25").html()) +
				convertToZero($("#RA" + a[i] + "26").html()) +
				convertToZero($("#RA" + a[i] + "27").html()) +
				convertToZero(bonusPaid), // bonusPaid is cell 28
			 2);

}


function netARO() {
	// net aro lives in both detail report and the reserve report
	for (i = 0; i < a.length; i++) {       
		if (i > 0) {
			document.getElementById("RA" + a[i] + "26").innerHTML =
			accounting.formatNumber(
				convertToZero($("#RA" + a[i - 1] + "29").html()),
			 2);
		}
		else {
			document.getElementById("RAH26").innerHTML =
			accounting.formatNumber(
				convertToZero($("#RAG26").html()),
			 2);
		}

		
		var bonusPaidStr = getBonusPaidStr();
		var bonusPaid = getBonusPaidVal();

		document.getElementById("RA" + a[i] + "28").innerHTML = bonusPaidStr;
		var netAROToRsvrAcct = (convertToZero($("#" + a[i] + "20").html()) - convertToZero($("#" + a[i] + "28").html()));		
	}

	rowTotal(30);

	document.getElementById("G30").innerHTML =
		   accounting.formatNumber(
			   (
			   convertToZero($("#T30").html())
			   / convertToZero($("#T5").html())
			   ) * 100,
		   2) + "%";
}


function operatingIncome() {
	// Assigned Revenue Rate for branch    
	totalRevenue = convertToZero($("#" + a[i] + "715").html()) + convertToZero($("#" + a[i] + "740").html());
	var opIncVal = convertToZero($("#" + a[i] + "18").html()) - totalRevenue;

	opIncVal = accounting.formatNumber(opIncVal, 2);

	var cogs = convertToZero($("#" + a[i] + "18").html());
	var cellVal = convertToZero(totalRevenue - cogs);
	document.getElementById(a[i] + "20").innerHTML = accounting.formatNumber(cellVal, 2);
}

function aroPriorToAllocations() {
	var aroPrior = accounting.formatNumber(convertToZero($("#" + a[i] + "20").html()) - convertToZero($("#" + a[i] + "15").html()), 2);
   
	if (a[i] != null) {
		if (aroPrior != null && aroPrior != "-") {
			document.getElementById(a[i] + "17").innerHTML = aroPrior;
		}
		else {
			document.getElementById(a[i] + "17").innerHTML = "-";
		}
	}
}


function LoanLossRsrvContrib() {
	// Loan Loss Reserve Contribution from ARO Branch Loan Loss Reserve 
	if (rptYear == 2015) {
		document.getElementById("RA" + a[i] + "7").innerHTML = getloanLossRsrvIncrease2015();
	}
	else {
		document.getElementById("RA" + a[i] + "7").innerHTML = getLoanLossRsrvIncrease();
	}
}


function endingRsrvrAcct() {
	document.getElementById("RA" + a[i] + "22").innerHTML =
	   accounting.formatNumber(
		convertToZero($("#RA" + a[i] + "18").html()) +
		convertToZero($("#RA" + a[i] + "19").html()) +
		convertToZero($("#RA" + a[i] + "21").html())
		, 2);
}


function getPaidBackToCorp() {
		if (netIncomeFromOps - loanLossRsvrIncrease < 1) {
			document.getElementById(a[i] + "22").innerHTML = "-";
		}
		else {
			if (corpContribBegin > 0) {
				if (netIncomeFromOps - loanLossRsvrIncrease > 0) {
					if (netIncomeFromOps - loanLossRsvrIncrease < corpContribBegin) {
						document.getElementById(a[i] + "22").innerHTML = accounting.formatNumber((netIncomeFromOps - loanLossRsvrIncrease) * -1, 2);
						
					}
					else {
						document.getElementById(a[i] + "22").innerHTML = accounting.formatNumber(corpContribBegin * -1, 2);
						
					}
				}
				else {
					document.getElementById(a[i] + "22").innerHTML = "-";
				}
			}
			else {
				document.getElementById(a[i] + "22").innerHTML = "-";
			}
		}		
}


function allocationToReserveForPL() {
	//------------------------ //
	// ALLOCATIONS TO RESERVES //
	//------------------------ //
	for (i = 0; i < a.length; i++) {
		// 7910	Branch Loan Loss Reserve

		loanLossRsvrRate = convertToZero($("#G121").html()) * .01;
		closedVol = convertToZero($("#" + a[i] + "5").html());

		document.getElementById(a[i] + "21").innerHTML = getLoanLossRsrvIncrease();


		// Paid Back to Corporate removed from AROS code       

		// 7923	Allocated Increase in Branch Operating Reserve
		allocatedIncOpRsrv = convertToZero($("#" + a[i] + "17").html()) - convertToZero($("#" + a[i] + "21").html()) - convertToZero($("#" + a[i] + "22").html());


		// 7930	Reserve account + Advance or (-) Repayment HAS NOT CALCS IN SPREAD SHEET
		document.getElementById(a[i] + "24").innerHTML = "";		
	}

	// Total 7900 - Reserve Account
	rsrvAcctSubTot();

	// allocations to capital total
	rowTotal(28);

	// allocations to capital rate
	document.getElementById("G28").innerHTML = accounting.formatNumber((convertToZero($("#T28").html()) / convertToZero($("#T5").html())) * 100, 2) + "%";
}



function getBorrowedFromReserveForPL() {
	//------------------------ //
	// ALLOCATIONS TO RESERVES //
	//------------------------ //
	
		// 7910	Branch Loan Loss Reserve

		loanLossRsvrRate = convertToZero($("#G121").html()) * .01;
		closedVol = convertToZero($("#" + a[i] + "5").html());

		document.getElementById(a[i] + "21").innerHTML = getLoanLossRsrvIncrease();

		// Advance from Reserve Accounts
		// ARO PRIOR TO ALLOCATIONS - Branch Loan Loss Reserve Increase - Paid Back to Corporate

		advFromRsrvAcct = convertToZero($("#" + a[i] + "17").html()) - convertToZero($("#" + a[i] + "21").html()) - convertToZero($("#" + a[i] + "22").html());
		
		
		if (advFromRsrvAcct < 0) {
			document.getElementById(a[i] + "25").innerHTML = accounting.formatNumber(advFromRsrvAcct, 2);
		}
		else {
			document.getElementById(a[i] + "25").innerHTML = "-";
		}
	}


function getBonusPaidStr() {
	// Bonuses are an expense in ARO details and should be a negative in the Reserve Report
	var bonusPaidStr = $("#RA" + a[i] + "28").html();
	if (bonusPaidStr.indexOf(">") > 0) {
		// bonuses have a drill-down link so derive the true number		
		bonusPaidStr = bonusPaidStr.replace(">", " style = 'color:red'>-");
	}
	else {
		bonusPaidStr = "-";
	}

	return bonusPaidStr;
}


function getBonusPaidVal() {
	// Bonuses are an expense in ARO details and should be a negative in the Reserve Report
	var bonusPaidStr = $("#RA" + a[i] + "28").html();
	if (bonusPaidStr.indexOf(">") > 0) {
		// bonuses have a drill-down link so derive the true number
		var bonusPaid = bonusPaidStr.substr(bonusPaidStr.indexOf(">") + 1, 50);
		bonusPaid = convertToZero(bonusPaid) * -1;
	}
	else {
		bonusPaid = 0;
	}

	return bonusPaid;
}
