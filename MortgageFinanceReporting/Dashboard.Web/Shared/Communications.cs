﻿using System;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using Dashboard.Web.Models;
using SendGrid;


namespace Dashboard.Web.Shared
{
    public class Communications : ICommunications
    {     
        public void SendEMail(string toEmail, string subject, string body)
        {
            string fromAddr = "";

            fromAddr = ConfigurationManager.AppSettings["AdminEmail"];

            // Create the email object first, then add the properties.
            var message = new SendGridMessage();

            // Add the message properties.
            message.From = new MailAddress(fromAddr);       
            message.AddTo(toEmail);
            message.Subject = subject;
            message.Html = body;

            // Create credentials, specifying your user name and password.
            var credentials = new NetworkCredential("azure_d3bcb0cb938926fd8050103a1a28db89@azure.com", "FEq7KVF6DZwY864");
            // Create an SMTP transport for sending email.
            var transportSmtp = new SendGrid.Web(credentials);

           // Send the email.
            transportSmtp.Deliver(message);          
        }
    }
}