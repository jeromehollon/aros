﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Management;

namespace Dashboard.Web.Shared
{
    public class LogEvent : WebBaseEvent
    {
        // custom events get logged via the health monitoring (see web.config)
        // use this to augment the logging of un-handled errors with handled errors and business exceptions

        public const int LogEventCode = WebEventCodes.WebExtendedBase + 10;
        public LogEvent(string message, object eventSource)
            : base(message, eventSource, LogEventCode)
        { }
    }
}