using System;

namespace Dashboard.Web.Shared
{
    public interface ICommunications
    {
        void SendEMail(string toEmail, string subject, string body);
    }
}