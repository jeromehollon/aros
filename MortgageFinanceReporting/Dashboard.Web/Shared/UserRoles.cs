﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dashboard.Web.Controllers;
using Dashboard.Web.Models;
using Microsoft.AspNet.Identity;

namespace Dashboard.Web.Shared
{
    public static class UserRoles
    {
        public static string GetUserRole(string userName)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ApplicationUser user = context.Users.Where(u => u.UserName.Equals(userName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            var account = new AccountController();

            return account.UserManager.GetRoles(user.Id).FirstOrDefault();
        }
    }
}