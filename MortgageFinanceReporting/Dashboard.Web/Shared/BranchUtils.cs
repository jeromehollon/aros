﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using Dashboard.Web.Models;
using Microsoft.AspNet.Identity;

namespace Dashboard.Web.Shared
{
    internal static class BranchUtils
	{
		internal static string GetBranchName(ApplicationDbContext db, IPrincipal currentUser)
		{            
			var userStore = new Microsoft.AspNet.Identity.EntityFramework.UserStore<ApplicationUser>();
			var userManager = new UserManager<ApplicationUser>(userStore);

			var user = userManager.FindById(currentUser.Identity.GetUserId());

			string branchName = "";

			if (user != null)
			{
				Branch branch = db.Branches.Find(user.BranchId);

				branchName = branch.BranchName.ToString();               
			}

			return branchName;
		}


        internal static string GetBranchName(ApplicationDbContext db, int branchId)
        {           
            string branchName = "";            
            Branch branch = db.Branches.Find(branchId);

            branchName = branch.BranchName.ToString();           

            return branchName;
        }

        
        internal static int GetBranchId(ApplicationDbContext db, string branchCode)
        {
            int branchId = 0;
            var branches = db.Branches.Select(b => new { b.BranchId, b.BranchName });

            foreach(var branch in branches)
            {
                if (GetBranchCode(branch.BranchName) == branchCode)
                {
                    branchId = branch.BranchId;
                }
            }
            
            return branchId;
        }


        internal static string GetBranchCode(string branchName)
		{
			string branchCode = "";

			if (string.IsNullOrWhiteSpace(branchName) == false)
			{
				if (branchName.Length > 5)
				{
					if (branchName.IndexOf(" ", 5) > 0)
					{
						branchCode = branchName.Substring(0, branchName.IndexOf(" ", 5)).Replace(" ", "");
					}
				}
			}

			return branchCode;
		}


        internal static bool prodBranchRptFlag(bool isUserRole, string rptType)
        {
            return isUserRole && (rptType == "AroReport" || rptType == "ReserveAccounts");
        }      

	}

}