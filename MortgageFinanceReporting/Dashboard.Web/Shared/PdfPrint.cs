﻿using System;
using System.IO;
using System.Linq;
using System.Web;
using _5XUtilities.Common.UtilityClasses;

namespace Dashboard.Web.Shared
{
    public static class PdfPrint
    {
        public static string GetPdfFileName(string stringToPrint, string fileDirPath)
        {
            // save the report to a static html page that resides in the temp folder
            // this solves two problems:
            // 1) the javascript results only work with convertapi
            // 2) convertapi can't access the page when it is password protected (forms based)

            // make the file name based on the timestamp so they don't collide
            string fileName = Crypto.Encrypt(DateTime.Now.ToString("yyyyMMddHHmmss"), "boogie");

            // remove characters that may cause problems
            fileName = _5XUtilities.StringServices.CleanInput(fileName);
            fileName = fileName.Replace("-", "");
            fileName = fileName.Replace(",", "");
            fileName = fileName.Replace(".", "");
            fileName = fileName.Replace("@", "");

            fileName = fileName + ".html";

            FileStream fs;
            string filePath = String.Format("{0}//TempPages//{1}", fileDirPath, fileName);

            fs = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);

            sw.Write(stringToPrint);

            sw.Close();
            fs.Close();
            return fileName;
        }
    }
}