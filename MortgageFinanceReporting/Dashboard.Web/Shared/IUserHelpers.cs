using Dashboard.Web.Models;

namespace Dashboard.Web.Shared
{
    public interface IUserHelpers
    {
        int GetUserId(string userName);

        int GetOrgId(string userName);

        string GetOrgName(int orgId);
    }
}