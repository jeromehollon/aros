namespace Dashboard.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RegionBranches : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RegionBranches",
                c => new
                    {
                        RegionBranchId = c.Int(nullable: false, identity: true),
                        Branch_BranchId = c.Int(nullable: false),
                        Region_RegionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RegionBranchId)
                .ForeignKey("dbo.Branches", t => t.Branch_BranchId, cascadeDelete: true)
                .ForeignKey("dbo.Regions", t => t.Region_RegionId, cascadeDelete: true)
                .Index(t => t.Branch_BranchId)
                .Index(t => t.Region_RegionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RegionBranches", "Region_RegionId", "dbo.Regions");
            DropForeignKey("dbo.RegionBranches", "Branch_BranchId", "dbo.Branches");
            DropIndex("dbo.RegionBranches", new[] { "Region_RegionId" });
            DropIndex("dbo.RegionBranches", new[] { "Branch_BranchId" });
            DropTable("dbo.RegionBranches");
        }
    }
}
