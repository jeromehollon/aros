namespace Dashboard.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BeginBalNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Branches", "BegLoanLossReserveBal", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Branches", "BegBranchReserveBal", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Branches", "BegAroAccountBal", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Branches", "BegAroAccountBal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Branches", "BegBranchReserveBal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Branches", "BegLoanLossReserveBal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
