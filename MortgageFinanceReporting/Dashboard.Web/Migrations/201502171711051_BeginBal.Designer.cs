// <auto-generated />
namespace Dashboard.Web.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.0-20911")]
    public sealed partial class BeginBal : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(BeginBal));
        
        string IMigrationMetadata.Id
        {
            get { return "201502171711051_BeginBal"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
