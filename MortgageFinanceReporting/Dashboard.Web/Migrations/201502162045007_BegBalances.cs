namespace Dashboard.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BegBalances : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Branches", "BegLoanLossReserveBal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Branches", "BegBranchReserveBal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Branches", "BegAroAccountBal", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Branches", "BegAroAccountBal");
            DropColumn("dbo.Branches", "BegBranchReserveBal");
            DropColumn("dbo.Branches", "BegLoanLossReserveBal");
        }
    }
}
