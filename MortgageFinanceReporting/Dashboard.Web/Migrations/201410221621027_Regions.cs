namespace Dashboard.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Regions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        RegionId = c.Int(nullable: false, identity: true),
                        RegionName = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.RegionId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Regions");
        }
    }
}
