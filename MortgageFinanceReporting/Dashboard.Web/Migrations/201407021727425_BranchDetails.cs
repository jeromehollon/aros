namespace Dashboard.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BranchDetails : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.Branches", "ManagerSalaryRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "ManagerSalaryCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "ARRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "ARCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "VolumeOverrideRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "VolumeOverrideCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "QrtBonusRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "QrtBonusCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "LoanVolChangeMetricRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "LoanVolChangeMetricCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "MonthlyClosingGoalRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "MonthlyClosingGoalCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "TotalMonthlyBudgetRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "TotalMonthlyBudgetCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "StartUpCostRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "StartUpCostCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "LoanLossReserveRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "LoanLossReserveCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "ReserveRequiredRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "ReserveRequiredCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "BranchReserveBalanceRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "BranchReserveBalanceCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "CorpInvestBalDueRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            //AddColumn("dbo.Branches", "CorpInvestBalDueCap", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            //DropColumn("dbo.Branches", "CorpInvestBalDueCap");
            //DropColumn("dbo.Branches", "CorpInvestBalDueRate");
            //DropColumn("dbo.Branches", "BranchReserveBalanceCap");
            //DropColumn("dbo.Branches", "BranchReserveBalanceRate");
            //DropColumn("dbo.Branches", "ReserveRequiredCap");
            //DropColumn("dbo.Branches", "ReserveRequiredRate");
            //DropColumn("dbo.Branches", "LoanLossReserveCap");
            //DropColumn("dbo.Branches", "LoanLossReserveRate");
            //DropColumn("dbo.Branches", "StartUpCostCap");
            //DropColumn("dbo.Branches", "StartUpCostRate");
            //DropColumn("dbo.Branches", "TotalMonthlyBudgetCap");
            //DropColumn("dbo.Branches", "TotalMonthlyBudgetRate");
            //DropColumn("dbo.Branches", "MonthlyClosingGoalCap");
            //DropColumn("dbo.Branches", "MonthlyClosingGoalRate");
            //DropColumn("dbo.Branches", "LoanVolChangeMetricCap");
            //DropColumn("dbo.Branches", "LoanVolChangeMetricRate");
            //DropColumn("dbo.Branches", "QrtBonusCap");
            //DropColumn("dbo.Branches", "QrtBonusRate");
            //DropColumn("dbo.Branches", "VolumeOverrideCap");
            //DropColumn("dbo.Branches", "VolumeOverrideRate");
            //DropColumn("dbo.Branches", "ARCap");
            //DropColumn("dbo.Branches", "ARRate");
            //DropColumn("dbo.Branches", "ManagerSalaryCap");
            //DropColumn("dbo.Branches", "ManagerSalaryRate");
        }
    }
}
