namespace Dashboard.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserRegion : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "RegionId", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "RegionId");
        }
    }
}
