namespace Dashboard.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RegionBranchid : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.RegionBranches", name: "Branch_BranchId", newName: "BranchId");
            RenameColumn(table: "dbo.RegionBranches", name: "Region_RegionId", newName: "RegionId");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.RegionBranches", name: "RegionId", newName: "Region_RegionId");
            RenameColumn(table: "dbo.RegionBranches", name: "BranchId", newName: "Branch_BranchId");
        }
    }
}
