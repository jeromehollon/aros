﻿using System.Collections.Generic;

namespace BranchRepository.Interface
{
    public interface IBranch
    {
        IBranch GetBranchData(string branchId);

        int BranchId { get; set; }

        int Year { get; set; }

        string BranchName { get; set; }

        decimal ManagerSalaryRate { get; set; }

        decimal ManagerSalaryCap { get; set; }

        decimal ARRate { get; set; }

        decimal ARCap { get; set; }

        decimal VolumeOverrideRate { get; set; }

        decimal VolumeOverrideCap { get; set; }

        decimal QrtBonusRate { get; set; }

        decimal QrtBonusCap { get; set; }

        decimal LoanVolChangeMetricRate { get; set; }

        decimal LoanVolChangeMetricCap { get; set; }

        decimal MonthlyClosingGoalRate { get; set; }

        decimal MonthlyClosingGoalCap { get; set; }

        decimal TotalMonthlyBudgetRate { get; set; }

        decimal TotalMonthlyBudgetCap { get; set; }

        decimal StartUpCostRate { get; set; }

        decimal StartUpCostCap { get; set; }

        decimal LoanLossReserveRate { get; set; }

        decimal LoanLossReserveCap { get; set; }

        decimal ReserveRequiredRate { get; set; }

        decimal ReserveRequiredCap { get; set; }

        decimal BranchReserveBalanceRate { get; set; }

        decimal BranchReserveBalanceCap { get; set; }

        decimal CorpInvestBalDueRate { get; set; }

        decimal CorpInvestBalDueCap { get; set; }

        decimal? BegLoanLossReserveBal { get; set; }

        decimal? BegBranchReserveBal { get; set; }

        decimal? BegAroAccountBal { get; set; }

        decimal? BranchMargin { get; set; }

        decimal? LoMargin { get; set; }

        decimal? ProcessingFee { get; set; }
            
    }
}
