﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BranchRepository.Interface
{
    public interface IBpsRankings
    {
        int BranchRankId { get; set; }

        int? ParentAcct { get; set; }

        int Mnth { get; set; }

        int Yr { get; set; }

        double TotalAmt { get; set; }

        int TotalVol { get; set; }

        double TotAmt { get; set; }

        string Branch { get; set; }

        string BranchId { get; set; }

        decimal? Bps { get; set; }

        decimal TotalBps { get; set; }

        int BpsRank { get; set; }

        int TotalBranches { get; set; }

        decimal BpsGoal { get; set; }

        decimal YrAmt { get; set; }

        int YrTotalVol { get; set; }

        IBpsRankings GetBpsRankingForBranch(string branchId);

        List<IBpsRankings> GetBpsRankings();

        List<IBpsRankings> GetBpsRankings(int regionId);
    }
}
