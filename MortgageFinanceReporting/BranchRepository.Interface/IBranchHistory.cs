﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BranchRepository.Interface
{
    public interface IBranchHistory
    {
        List<IBranchHistory> GetBranchHistory(int Year, string branchId);
        
        string BranchId { get; set; }

        decimal ARRate { get; set; }

        int EffectiveYear { get; set; }

        int EffectiveMonth { get; set; }

        int MonthNum { get; set; }

    }
}
