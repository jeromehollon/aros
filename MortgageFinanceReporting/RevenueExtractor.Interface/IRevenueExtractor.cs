using System;
using System.Collections.Generic;
using System.Linq;

namespace RevenueExtractor.Interface
{
    public interface IRevenueExtractor
    {
        string Login { get; set; }
        string Password { get; set; }
        List<LoanDetail> Loans { get; set; }
        List<LoanDetail> GetClosingRpt();
    }
}
