using System;

namespace RevenueExtractor.Interface
{
    public class LoanDetail
    {
        public int LoanId { get; set; }

        public string BranchName { get; set; }

        public string LoanNumber { get; set; }

        public string LoanPurpose { get; set; }

        public string LoanPurposeStr { get; set; }

        public string LoanType { get; set; }

        public string BorrowerLastName { get; set; }

        public string BorrowerFirstName { get; set; }

        public decimal TotalLoanAmount { get; set; }

        public DateTime FundedDate { get; set; }

        public string AssignedLoanOfficerName { get; set; }

        public string LenderAccountExecutiveName { get; set; }

        public string InvestorRateLockInvestorName { get; set; }

        public string LoanCloserName { get; set; }

        public DateTime DateCreated { get; set; }
    }
}