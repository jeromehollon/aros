﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Telerik.JustMock;
using Telerik.JustMock.AutoMock;
using ExpenseRepository.Interface;
using ExpenseRepository.Sql;
using ExpenseRepository.Sql.Archive;

namespace ExpenseRepository.Test
{
    /// <summary>
    /// Summary description for JustMockTest
    /// </summary>
    [TestClass]
    public class JustMockTest
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void IntegrationTest2014()
        {
            IExpenses expenses = new ExpenseRepository.Sql.Archive.Expenses();
            expenses.Year = 2014;


            List<IExpenses> summaries = expenses.GetSummaryByBranch("CA902", 2014);

            Assert.IsTrue(summaries.Count > 0);
        }

           [TestMethod]
        public void IntegrationTest2015()
        {
            IExpenses expenses = new ExpenseRepository.Sql.Expenses();
            expenses.Year = 2015;


            List<IExpenses> summaries = expenses.GetSummaryByBranch("CA902", 2015);

            Assert.IsTrue(summaries.Count > 0);
        }

        [TestMethod]
        public void Test()
        {            
            var expenses = Mock.Create<IExpenses>();
            Mock.Arrange( () => expenses.GetSummaryByBranch("CA 404", 2014)).Returns(new List<IExpenses>() {
                new ExpenseRepository.Sql.Archive.Expenses { AccountId = "5030",  BranchName="CA 404 Test", Month=2, TotalAmt=100.00, Year=2014},
                new ExpenseRepository.Sql.Archive.Expenses { AccountId = "5050", BranchName="CA 404 Test", Month=2, TotalAmt=200.00, Year=2014}
            }).MustBeCalled();


        }
    }
}
