﻿using System;
namespace ExpenseRepository.Interface
{
    public interface IExpenses
    {
        string AccountId { get; set; }
        System.Collections.Generic.List<IExpenses> GetSummaryByBranch(string branch, int year);
        int Month { get; set; }
        int Year { get; set; }
        double TotalAmt { get; set; }
        string BranchName { get; set; }
       DateTime DateRefreshed { get; }
       DateTime AcctClosingDate { get; }
    }
}
