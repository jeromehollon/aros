﻿using System;
namespace ExpenseRepository.Interface
{
    public interface IDetail
    {      
        string AccountId { get; set; }
        System.Collections.Generic.List<IDetail> GetDetails(string branch, int month, int year, string accountId);
        int Month { get; set; }
        int Year { get; set; }
        DateTime TransactionDate { get; set; }
        double Amount { get; set; }
        string AccountName { get; set; }
        string Memo { get; set; }
        string Payee { get; set; }
    }
}
