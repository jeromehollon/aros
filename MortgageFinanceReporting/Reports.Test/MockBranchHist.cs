﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BranchRepository.Interface;

namespace Reports.Test
{
    class MockBranchHist : IBranchHistory
    {
        public decimal ARRate { get; set; }


        public string BranchId { get; set; }


        public int EffectiveMonth { get; set; }

        public int EffectiveYear { get; set; }

        public int MonthNum { get; set; }

       

        public List<IBranchHistory> GetBranchHistory(int Year, string branchId)
        {
            List<IBranchHistory> branchHist = new List<IBranchHistory>();
            if (branchId == "AROBranch")
            {
                {
                    IBranchHistory branchHist1 = new MockBranchHist();
                    branchHist1.ARRate = (decimal) 3.75;
                    branchHist1.BranchId = "AROBranch";
                    branchHist1.MonthNum = 1;
                    branchHist1.EffectiveMonth = 1;
                    branchHist1.EffectiveYear = 2016;
                    branchHist.Add(branchHist1);

                    IBranchHistory branchHist2 = new MockBranchHist();
                    branchHist2.ARRate = (decimal)3.75;
                    branchHist2.BranchId = "AROBranch";
                    branchHist2.MonthNum = 2;
                    branchHist2.EffectiveMonth = 1;
                    branchHist2.EffectiveYear = 2016;
                    branchHist.Add(branchHist2);

                    IBranchHistory branchHist3 = new MockBranchHist();
                    branchHist3.ARRate = (decimal)4.25;
                    branchHist3.BranchId = "AROBranch";
                    branchHist3.MonthNum = 3;
                    branchHist3.EffectiveMonth = 1;
                    branchHist3.EffectiveYear = 2016;
                    branchHist.Add(branchHist3);

                    IBranchHistory branchHist4 = new MockBranchHist();
                    branchHist4.ARRate = (decimal)4.25;
                    branchHist4.BranchId = "AROBranch";
                    branchHist4.MonthNum = 4;
                    branchHist4.EffectiveMonth = 1;
                    branchHist4.EffectiveYear = 2016;
                    branchHist.Add(branchHist4);

                    IBranchHistory branchHist5 = new MockBranchHist();
                    branchHist5.ARRate = 0;
                    branchHist5.BranchId = "AROBranch";
                    branchHist5.MonthNum = 5;
                    branchHist5.EffectiveMonth = 1;
                    branchHist5.EffectiveYear = 2016;
                    branchHist.Add(branchHist5);

                    IBranchHistory branchHist6 = new MockBranchHist();
                    branchHist6.ARRate = (decimal)0;
                    branchHist6.BranchId = "AROBranch";
                    branchHist6.MonthNum = 6;
                    branchHist6.EffectiveMonth = 1;
                    branchHist6.EffectiveYear = 2016;
                    branchHist.Add(branchHist6);

                    IBranchHistory branchHist7 = new MockBranchHist();
                    branchHist7.ARRate = 0;
                    branchHist7.BranchId = "AROBranch";
                    branchHist7.MonthNum = 7;
                    branchHist7.EffectiveMonth = 1;
                    branchHist7.EffectiveYear = 2016;
                    branchHist.Add(branchHist7);

                    IBranchHistory branchHist8 = new MockBranchHist();
                    branchHist8.ARRate = 0;
                    branchHist8.BranchId = "AROBranch";
                    branchHist8.MonthNum = 8;
                    branchHist8.EffectiveMonth = 1;
                    branchHist8.EffectiveYear = 2016;
                    branchHist.Add(branchHist8);

                    IBranchHistory branchHist9 = new MockBranchHist();
                    branchHist9.ARRate = 0;
                    branchHist9.BranchId = "AROBranch";
                    branchHist9.MonthNum = 9;
                    branchHist9.EffectiveMonth = 1;
                    branchHist9.EffectiveYear = 2016;
                    branchHist.Add(branchHist9);

                    IBranchHistory branchHist10 = new MockBranchHist();
                    branchHist10.ARRate = 0;
                    branchHist10.BranchId = "AROBranch";
                    branchHist10.MonthNum = 10;
                    branchHist10.EffectiveMonth = 1;
                    branchHist10.EffectiveYear = 2016;
                    branchHist.Add(branchHist10);

                    IBranchHistory branchHist11 = new MockBranchHist();
                    branchHist11.ARRate = 0;
                    branchHist11.BranchId = "AROBranch";
                    branchHist11.MonthNum = 10;
                    branchHist11.EffectiveMonth = 1;
                    branchHist11.EffectiveYear = 2016;
                    branchHist.Add(branchHist11);

                    IBranchHistory branchHist12 = new MockBranchHist();
                    branchHist12.ARRate = 0;
                    branchHist12.BranchId = "AROBranch";
                    branchHist12.MonthNum = 10;
                    branchHist12.EffectiveMonth = 1;
                    branchHist12.EffectiveYear = 2016;
                    branchHist.Add(branchHist12);
                    // todo: finish
                }
            }
            return branchHist;
        }
    }
}