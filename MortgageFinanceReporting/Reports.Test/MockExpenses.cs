﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExpenseRepository.Interface;

namespace Reports.Test
{
    class MockExpenses : IExpenses
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public string AccountId { get; set; }
        public double TotalAmt { get; set; }
        public string BranchName { get; set; }

        List<IExpenses> summaryList = new List<IExpenses>();

        void addExpense(string accountId, int month, double totalAmt, string branch)
        {            
            MockExpenses summary = new MockExpenses();

            summary.AccountId = accountId; summary.Month = month; summary.TotalAmt = totalAmt; summary.BranchName = "branch"; 
            summaryList.Add(summary);
        }  

        List<IExpenses> IExpenses.GetSummaryByBranch(string branch, int year)
        {
            if (summaryList.Count <= 0)
            {
                addExpense("6110", 1, 2327.24, branch);
                addExpense("6120", 1, 13529.57, branch);
                addExpense("6460", 1, 67.1, branch);
                addExpense("6480", 1, 41.68, branch);
                addExpense("6490", 1, 365.63, branch);
                addExpense("6540", 1, 283.24, branch);
                addExpense("6570", 1, 1398, branch);
                addExpense("6580", 1, 45, branch);
                addExpense("6600", 1, 860.79, branch);
                addExpense("6610", 1, 184.94, branch);
                addExpense("6740", 1, 710, branch);
                addExpense("6760", 1, 1541.2, branch);
                addExpense("6830", 1, 87.5, branch);
                addExpense("6840", 1, 40.25, branch);
                addExpense("6850", 1, 184, branch);
                addExpense("6980", 1, 180.12, branch);
                addExpense("7010", 1, 219.74, branch);
                addExpense("7030", 1, 250, branch);
                addExpense("7140", 1, 65.4, branch);
                addExpense("7150", 1, 69, branch);
                addExpense("7185", 1, 23.35, branch);
                addExpense("7190", 1, 50, branch);
                addExpense("7200", 1, 4000, branch);
                addExpense("7210", 1, 2500, branch);
                addExpense("7220", 1, 15.8, branch);
                addExpense("7450", 1, 2967.64, branch);
                addExpense("7490", 1, -1371.37, branch);
                addExpense("7500", 1, 873.01, branch);
                addExpense("7510", 1, 5738, branch);
                addExpense("7520", 1, 700, branch);
                addExpense("7640", 1, 44.96, branch);
                addExpense("7660", 1, 718.25, branch);
                addExpense("7685", 1, 163.82, branch);
                addExpense("7850", 1, 120, branch);
                addExpense("6110", 2, 1133.32, branch);
                addExpense("6120", 2, 34277.68, branch);
                addExpense("6460", 2, 33.49, branch);
                addExpense("6490", 2, 293.74, branch);
                addExpense("6530", 2, 111.66, branch);
                addExpense("6540", 2, 400.42, branch);
                addExpense("6560", 2, 18, branch);
                addExpense("6570", 2, 1398, branch);
                addExpense("6580", 2, 180, branch);
                addExpense("6600", 2, 625.81, branch);
                addExpense("6610", 2, 167.95, branch);
                addExpense("6720", 2, 300, branch);
                addExpense("6740", 2, 990, branch);
                addExpense("6760", 2, 3143.86, branch);
                addExpense("6780", 2, 149.6, branch);
                addExpense("6830", 2, 163.5, branch);
                addExpense("6840", 2, 15, branch);
                addExpense("6850", 2, 178.5, branch);
                addExpense("7150", 2, 79, branch);
                addExpense("7200", 2, 4000, branch);
                addExpense("7450", 2, 2934.32, branch);
                addExpense("7460", 2, 708, branch);
                addExpense("7490", 2, -1363.04, branch);
                addExpense("7500", 2, 598.17, branch);
                addExpense("7510", 2, 3500, branch);
                addExpense("7520", 2, 500, branch);
                addExpense("7850", 2, 170, branch);
                addExpense("6110", 3, 4214.55, branch);
                addExpense("6120", 3, 29004.97, branch);
                addExpense("6430", 3, 65, branch);
                addExpense("6460", 3, 33.49, branch);
                addExpense("6480", 3, 8.71, branch);
                addExpense("6490", 3, 289.28, branch);
                addExpense("6540", 3, 911.53, branch);
                addExpense("6560", 3, 745.04, branch);
                addExpense("6570", 3, 1398, branch);
                addExpense("6580", 3, 90, branch);
                addExpense("6600", 3, 631.53, branch);
                addExpense("6610", 3, 185.07, branch);
                addExpense("7300", 3, 128.91, branch);
                addExpense("6720", 3, 345, branch);
                addExpense("6740", 3, 1750, branch);
                addExpense("6760", 3, 3341.72, branch);
                addExpense("6780", 3, 110, branch);
                addExpense("6830", 3, 143.8, branch);
                addExpense("6840", 3, 75, branch);
                addExpense("6850", 3, 170.5, branch);
                addExpense("6960", 3, 265.69, branch);
                addExpense("6980", 3, 368.95, branch);
                addExpense("7010", 3, 97.45, branch);
                addExpense("7120", 3, 786.59, branch);
                addExpense("7150", 3, 189.42, branch);
                addExpense("7200", 3, 3189.95, branch);
                addExpense("7100", 3, 109, branch);
                addExpense("7440", 3, 2000, branch);
                addExpense("7450", 3, 608, branch);
                addExpense("7460", 3, 2323.8, branch);
                addExpense("7471", 3, 6000, branch);
                addExpense("7490", 3, -1752.8, branch);
                addExpense("7500", 3, 693.53, branch);
                addExpense("7510", 3, 3803.8, branch);
                addExpense("7520", 3, 1000, branch);
                addExpense("7660", 3, 330, branch);
                addExpense("7685", 3, 265.89, branch);
                addExpense("7850", 3, 250, branch);
                addExpense("5068", 4, 55, branch);
                addExpense("6110", 4, 496.03, branch);
                addExpense("6120", 4, 50827.21, branch);
                addExpense("6460", 4, 33.55, branch);
                addExpense("6490", 4, 403.47, branch);
                addExpense("6530", 4, 11.57, branch);
                addExpense("6540", 4, 302.27, branch);
                addExpense("6570", 4, 1398, branch);
                addExpense("6580", 4, 90, branch);
                addExpense("6600", 4, 1777.06, branch);
                addExpense("6610", 4, 193.56, branch);
                addExpense("6720", 4, -150, branch);
                addExpense("6740", 4, 1805, branch);
                addExpense("6760", 4, 3765.5, branch);
                addExpense("6780", 4, 108.5, branch);
                addExpense("6830", 4, 322.5, branch);
                addExpense("6840", 4, 131.25, branch);
                addExpense("6850", 4, 425, branch);
                addExpense("6960", 4, 162.65, branch);
                addExpense("6980", 4, 120.6, branch);
                addExpense("7200", 4, 2259.6, branch);
                addExpense("7210", 4, 2500, branch);
                addExpense("7270", 4, 1500, branch);
                addExpense("7440", 4, 4000, branch);
                addExpense("7450", 4, 2445.9, branch);
                addExpense("7460", 4, 2479.2, branch);
                addExpense("7490", 4, -1197.04, branch);
                addExpense("7500", 4, 2733.74, branch);
                addExpense("7510", 4, 4774.46, branch);
                addExpense("7850", 4, 240, branch);
            }

            return summaryList;
        }

        public DateTime DateRefreshed
        {
            get; set;       
        }


        public DateTime AcctClosingDate
        {
            get
            {
                return DateTime.Now;
            }
        }
    }
}
