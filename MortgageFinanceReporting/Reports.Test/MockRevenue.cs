﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RevenueRepository.Interface;

namespace Reports.Test
{
    internal class MockRevenue : IRevenue
    {
       public int Month { get; set; }
       public int Year { get; set; }
        public double TotalAmt { get; set; }
        public int TotalLoans { get; set; }
        public string BranchName { get; set; }

        public void AddLoans(List<RevenueExtractor.Interface.LoanDetail> loans)
        {
            throw new NotImplementedException();
        }

        public List<IRevenue> GetSummary(string branchId)
        {
            List<IRevenue> summaryList = new List<IRevenue>();

            IRevenue summary1 = new MockRevenue();
            summary1.TotalLoans = 5;
            summary1.Month = 1;
            summary1.TotalAmt = 782165;
            summary1.BranchName = "Test Branch CA Palmdale";
            summaryList.Add(summary1);            

            IRevenue summary2 = new MockRevenue();
            summary2.TotalLoans = 10;
            summary2.Month = 2;
            summary2.TotalAmt =  1640003;
            summary2.BranchName = "Test Branch CA Palmdale";
            summaryList.Add(summary2);   

            IRevenue summary3 = new MockRevenue();
            summary3.TotalLoans = 9;
            summary3.Month = 3;
            summary3.TotalAmt =  1364362.00;
            summary3.BranchName = "Test Branch CA Palmdale";
            summaryList.Add(summary3);

            IRevenue summary4 = new MockRevenue();
            summary4.TotalLoans = 14;
            summary4.Month = 4;
            summary4.TotalAmt =  2362624.00;
            summary4.BranchName = "Test Branch CA Palmdale";
            summaryList.Add(summary4);   


            return summaryList;
        }
    }
}
