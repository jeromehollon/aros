﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using BranchRepository.Interface;
using ExpenseRepository.Interface;
using Reports.Test;
using RevenueRepository.Interface;

namespace Reports.Tests
{
    [TestClass()]
    public class AllocationsTests
    {
        #region LoanLossRsrvInc
        //private decimal getLoanLossRsrvInc(int i, decimal loanLossReserveBal)
        //{
        //    string branchId = "PLBranch";
        //    IExpenses mockExpenses = new MockExpenseAllocations();
        //    List<IExpenses> expenses = mockExpenses.GetSummaryByBranch(branchId, 2016);

        //    IRevenue mockRevenuerevenue = new MockRevenueAllocations();
        //    List<IRevenue> revenue = mockRevenuerevenue.GetSummary(branchId);

        //    IBranch mockBranch = new MockBranch(); 
        //    IBranch branch = mockBranch.GetBranchData(branchId);


        //    Allocations.CogsMapPath = "E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\CogsAcntMap.txt";
        //    Allocations.ExpenseMapPath = "E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\ExpenseAcntMap.txt";
        //    Allocations.BonusPaidAcct = "";
        //    Allocations.RevenueIncMapPath = "E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\PLRevneueAcntMap.txt";
        //    Allocations.RptType = "PL";

        //    return Allocations.LoanLossReserveIncrease(i, loanLossReserveBal, branch, 2016, revenue, expenses);
        //}


        //[TestMethod()]
        //public void LoanLossReserveIncreaseTestJan()
        //{
        //    decimal loanLossRsrvIncs;
        //    loanLossRsrvIncs = getLoanLossRsrvInc(0, (decimal) 12604.77);
        //    Assert.IsTrue(loanLossRsrvIncs.ToString(("#.##")) == "1507.8");
        //}


        //[TestMethod()]
        //public void LoanLossReserveIncreaseTestFeb()
        //{
        //    decimal loanLossRsrvIncs;
        //    loanLossRsrvIncs = getLoanLossRsrvInc(1, (decimal)14112.57);
        //    Assert.IsTrue(loanLossRsrvIncs.ToString(("#.##")) == "887.43");
        //}

        //[TestMethod()]
        //public void LoanLossReserveIncreaseTestMar()
        //{
        //    decimal loanLossRsrvIncs;
        //    loanLossRsrvIncs = getLoanLossRsrvInc(2, (decimal)14500);
        //    Assert.IsTrue(loanLossRsrvIncs == (decimal) 500.00);
        //}

        #endregion

        #region PaidBackToCorp
        //! no longer needed since this is covered in MainTest()


        //private decimal getPaidBackToCorp(int i, decimal loanLossRsrvInc, decimal corpInvestAcctBalPrev)
        //{
        //    string branchId = "PLBranch";
        //    IExpenses mockExpenses = new MockExpenseAllocations();
        //    List<IExpenses> expenses = mockExpenses.GetSummaryByBranch(branchId, 2016);

        //    IRevenue mockRevenuerevenue = new MockRevenueAllocations();
        //    List<IRevenue> revenue = mockRevenuerevenue.GetSummary(branchId);

        //    IBranch mockBranch = new MockBranch(); ;
        //    IBranch branch = mockBranch.GetBranchData(branchId);


        //    Allocations.CogsMapPath = "E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\CogsAcntMap.txt";
        //    Allocations.ExpenseMapPath = "E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\ExpenseAcntMap.txt";
        //    Allocations.RevenueIncMapPath = "E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\PLRevneueAcntMap.txt";
        //    Allocations.BonusPaidAcct = "";
        //    Allocations.RptType = "PL";


        //    return Allocations.PaidBackToCorp(i, loanLossRsrvInc, corpInvestAcctBalPrev, branch, 2016, revenue, expenses);

        //}


        //[TestMethod()]
        //public void PaidBackToCorpTestJan()
        //{
        //    decimal paidBackToCorp;
        //    decimal loanLossRsrvInc = (decimal)1507.8;

        //    paidBackToCorp = getPaidBackToCorp(0, loanLossRsrvInc, new decimal(0));

        //    Assert.IsTrue(paidBackToCorp.ToString(("#.##")) == "");
        //}


        //[TestMethod()]
        //public void PaidBackToCorpTestFeb()
        //{
        //    decimal paidBackToCorp;
        //    decimal loanLossRsrvInc = (decimal)887.43;

        //    paidBackToCorp = getPaidBackToCorp(1, loanLossRsrvInc, new decimal(3804.56));

        //    Assert.IsTrue(paidBackToCorp.ToString(("#.##")) == "");
        //}


        //[TestMethod()]
        //public void PaidBackToCorpTestMar()
        //{
        //    decimal paidBackToCorp;
        //    decimal loanLossRsrvInc = (decimal)500;

        //    paidBackToCorp = getPaidBackToCorp(2, loanLossRsrvInc, new decimal(28002.93));

        //    Assert.IsTrue(paidBackToCorp.ToString(("#.##")) == "28002.93");
        //}
        #endregion

        [TestMethod()]
        public void PLRsrvRptTest()
        {
            // based on 'P&L Report' worksheet in AROS_Rev_3.0_FRESNO.xlxs' in the KANBAN (copied to Rev. 3.0 in RKD's local folder)
            string branchId = "PLBranch";
            IExpenses mockExpenses = new MockExpenseAllocations();
            List<IExpenses> expenses = mockExpenses.GetSummaryByBranch(branchId, 2016);

            IRevenue mockRevenuerevenue = new MockRevenueAllocations();
            List<IRevenue> revenue = mockRevenuerevenue.GetSummary(branchId);

            IBranch mockBranch = new MockBranch(); ;
            IBranch branch = mockBranch.GetBranchData(branchId);

            Allocations allocations = new Allocations();


            allocations.CogsMapPath = "E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\CogsAcntMap.txt";
            allocations.ExpenseAcctsPath = "E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\Expenses.txt";
            allocations.RevenueIncMapPath = "E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\PLRevneueAcntMap.txt";
            allocations.RptType = "PL";
            allocations.BonusPaidAcct = "6025";

            Allocations.AllocationsReserves rsrvs = allocations.Main(branch, 2016,revenue,expenses);

            // allocations
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[0] == (decimal)1507.7995);
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[1] == (decimal)887.4305);
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[2] == (decimal)500.00);
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[3] == 0);
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[4] == 0);
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[5] == 0);

            Assert.IsTrue(rsrvs.PaidBackToCorporate[0] == 0);
            Assert.IsTrue(rsrvs.PaidBackToCorporate[1] == 0);
            Assert.IsTrue(rsrvs.PaidBackToCorporate[2].ToString("#.##") == "28002.93");
            Assert.IsTrue(rsrvs.PaidBackToCorporate[3] == 0);
            Assert.IsTrue(rsrvs.PaidBackToCorporate[4] == (decimal)5378.54);
            Assert.IsTrue(rsrvs.PaidBackToCorporate[5] == 0);

            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[0] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[1] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[2].ToString("#.##") == "3966.3");
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[3] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[4].ToString("#.##") == "10046.46");
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[5].ToString("####.##") == "14953.54");

            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[0].ToString("#.##") == "-43804.56");
            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[1].ToString("#.##") == "-24198.37");
            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[2] == 0);
            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[3].ToString("#.##") == "-9344.84");
            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[4] == 0);
            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[5] == 0);

            Assert.IsTrue(rsrvs.TotalRsrvAccts[0].ToString("#.##") == "-42296.76");
            Assert.IsTrue(rsrvs.TotalRsrvAccts[1].ToString("#.##") == "-23310.94");
            Assert.IsTrue(rsrvs.TotalRsrvAccts[2].ToString("#.##") ==  "32469.23");
            Assert.IsTrue(rsrvs.TotalRsrvAccts[3].ToString("#.##") == "-9344.84");
            Assert.IsTrue(rsrvs.TotalRsrvAccts[4].ToString("#.##") == "15425");
            Assert.IsTrue(rsrvs.TotalRsrvAccts[5].ToString("#.##") == "14953.54");

            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[0] == 0);
            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[1] == 0);
            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[2] == 0);
            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[3] == 0);
            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[4] == 0);
            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[5].ToString("#.##") == "24078.22");


            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[0].ToString("#.##") == "55622.01");
            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[1].ToString("#.##") == "49151.56");
            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[2].ToString("#.##") == "113020.42");
            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[3].ToString("#.##") == "77910.17");
            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[4].ToString("#.##") == "80955.93");
            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[5].ToString("#.##") == "89269.47");

            // details - loan loss reserve
            Assert.IsTrue(rsrvs.LoanLossReserveBeginning[0].ToString("#.##") == "12604.77");
            Assert.IsTrue(rsrvs.LoanLossReserveBeginning[1].ToString("#.##") == "14112.57");
            Assert.IsTrue(rsrvs.LoanLossReserveBeginning[2].ToString("#.##") == "14500");
            Assert.IsTrue(rsrvs.LoanLossReserveBeginning[3].ToString("#.##") == "15000");
            Assert.IsTrue(rsrvs.LoanLossReserveBeginning[4].ToString("#.##") == "15000");
            Assert.IsTrue(rsrvs.LoanLossReserveBeginning[5].ToString("#.##") == "15000");

            Assert.IsTrue(rsrvs.LoanLossReserveContribution[0] == (decimal)1507.7995);
            Assert.IsTrue(rsrvs.LoanLossReserveContribution[1] == (decimal)887.4305);
            Assert.IsTrue(rsrvs.LoanLossReserveContribution[2] == (decimal)500.00);
            Assert.IsTrue(rsrvs.LoanLossReserveContribution[3] == 0);
            Assert.IsTrue(rsrvs.LoanLossReserveContribution[4] == 0);

            // acct 2706 - Loan Loss Utilized
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[0] == 0);
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[1] == (decimal)-500);
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[2] == 0);
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[3] == 0);
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[4] == 0);
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[5] == 0);
           
            // 2707 - Early Payoff
            Assert.IsTrue(rsrvs.EarlyPayoff[0] == 0);
            Assert.IsTrue(rsrvs.EarlyPayoff[1] == 0);
            Assert.IsTrue(rsrvs.EarlyPayoff[2] == 0);
            Assert.IsTrue(rsrvs.EarlyPayoff[3] == 0);
            Assert.IsTrue(rsrvs.EarlyPayoff[4] == 0);
            Assert.IsTrue(rsrvs.EarlyPayoff[5] == 0);

            // 2708 - Early Payment Default
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[0] == 0);
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[1] == 0);
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[2] == 0);
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[3] == 0);
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[4] == 0);
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[5].ToString("#.##") == "-25000");

            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[0].ToString("#.##") == "14112.57");
            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[1].ToString("#.##") == "14500");
            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[2].ToString("#.##") == "15000");
            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[3].ToString("#.##") == "15000");
            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[4].ToString("#.##") == "15000");
            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[5].ToString("#.##") == "-10000");

            // details - corp investment account
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[0].ToString("#.##") == "3804.56");
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[1].ToString("#.##") == "28002.93");
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[2] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[3].ToString("#.##") == "5378.54");
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[4] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[5] == 0);

            Assert.IsTrue(rsrvs.OperatingLoss[0] == 0);
            Assert.IsTrue(rsrvs.OperatingLoss[1].ToString("####.##") == "3804.56");
            Assert.IsTrue(rsrvs.OperatingLoss[2].ToString("####.##") == "28002.93");
            Assert.IsTrue(rsrvs.OperatingLoss[3] == 0);
            Assert.IsTrue(rsrvs.OperatingLoss[4].ToString("####.##") == "5378.54");
            Assert.IsTrue(rsrvs.OperatingLoss[5] == 0);

            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[0].ToString("####.##") == "3804.56");
            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[1].ToString("####.##") == "24198.37");
            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[2] == 0);
            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[3].ToString("####.##") == "5378.54");
            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[4] == 0);
            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[5] == 0);

            Assert.IsTrue(rsrvs.CorpInvestAcctBal[0].ToString("####.##") == "3804.56");
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[1].ToString("####.##") == "28002.93");
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[2] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[3].ToString("####.##") == "5378.54");
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[4] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[5] == 0);

            // details - Branch Reserve Account
            Assert.IsTrue(rsrvs.BranchReserveBeginning[0].ToString("####.##") == "25000");
            Assert.IsTrue(rsrvs.BranchReserveBeginning[1] == 0);
            Assert.IsTrue(rsrvs.BranchReserveBeginning[2] == 0);
            Assert.IsTrue(rsrvs.BranchReserveBeginning[3].ToString("####.##") == "3966.3");
            Assert.IsTrue(rsrvs.BranchReserveBeginning[4] == 0);
            Assert.IsTrue(rsrvs.BranchReserveBeginning[5].ToString("####.##") == "10046.46");

            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[0] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[1] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[2].ToString("####.##") == "3966.3");
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[3] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[4].ToString("####.##") == "10046.46");
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[5].ToString("####.##") == "14953.54");

            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[0].ToString("####.##") == "-25000");
            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[1] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[2] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[3].ToString("####.##") == "-3966.3");
            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[4] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[5] == 0);

            Assert.IsTrue(rsrvs.BranchReserveEnding[0] == 0);
            Assert.IsTrue(rsrvs.BranchReserveEnding[1] == 0);
            Assert.IsTrue(rsrvs.BranchReserveEnding[2].ToString("####.##") == "3966.3");
            Assert.IsTrue(rsrvs.BranchReserveEnding[3] == 0);
            Assert.IsTrue(rsrvs.BranchReserveEnding[4].ToString("####.##") == "10046.46");
            Assert.IsTrue(rsrvs.BranchReserveEnding[5].ToString("####.##") == "25000");

            // details: available funds to manager
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[0] == 0);
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[1] == 0);
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[2] == 0);
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[3] == 0);
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[4] == 0);
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[5].ToString("#.##") == "24078.22");

            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[0].ToString("####.##") == "-15000");
            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[1] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[2] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[3] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[4] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[5] == 0);

            Assert.IsTrue(rsrvs.ArOverridePaid[0].ToString("####.##") == "5000");
            Assert.IsTrue(rsrvs.ArOverridePaid[1] == 0);
            Assert.IsTrue(rsrvs.ArOverridePaid[2] == 0);
            Assert.IsTrue(rsrvs.ArOverridePaid[3] == 0);
            Assert.IsTrue(rsrvs.ArOverridePaid[4] == 0);
            Assert.IsTrue(rsrvs.ArOverridePaid[5] == 0);

            Assert.IsTrue(rsrvs.NonProducingMgrDraw[0].ToString("####.##") == "5000");
            Assert.IsTrue(rsrvs.NonProducingMgrDraw[1] == 0);
            Assert.IsTrue(rsrvs.NonProducingMgrDraw[2] == 0);
            Assert.IsTrue(rsrvs.NonProducingMgrDraw[3] == 0);
            Assert.IsTrue(rsrvs.NonProducingMgrDraw[4] == 0);
            Assert.IsTrue(rsrvs.NonProducingMgrDraw[5] == 0);

            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[0].ToString("#.##") == "-5000");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[1].ToString("#.##") == "-5000");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[2].ToString("#.##") == "-5000");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[3].ToString("#.##") == "-5000");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[4].ToString("#.##") == "-5000");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[5].ToString("#.##") == "19078.22");

            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[0].ToString("#.##") == "15000");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[1].ToString("#.##") == "-5000");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[2].ToString("#.##") == "-5000");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[3].ToString("#.##") == "-5000");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[4].ToString("#.##") == "-5000");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[5].ToString("#.##") == "-5000");

        }

        [TestMethod()]
        public void ARORsrvRptTest(){
            // based on 'P&L Report' worksheet in AROS_Rev_3.0_FRESNO.xlxs' in the KANBAN (copied to Rev. 3.0 in RKD's local folder)
            string branchId = "AROBranch";
            IExpenses mockExpenses = new MockExpenseAllocations();
            List<IExpenses> expenses = mockExpenses.GetSummaryByBranch(branchId, 2016);

            IRevenue mockRevenuerevenue = new MockRevenueAllocations();
            List<IRevenue> revenue = mockRevenuerevenue.GetSummary(branchId);

            IBranch mockBranch = new MockBranch(); ;
            IBranch branch = mockBranch.GetBranchData(branchId);

            IBranchHistory mockBranchHist = new MockBranchHist();

            Allocations allocations = new Allocations();

            allocations.BranchId = branchId;
            allocations.Year = 2016;

            allocations.CogsMapPath = "E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\CogsAcntMap.txt";
            allocations.ExpenseAcctsPath = "E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\Expenses.txt";
            allocations.RevenueIncMapPath = ""; //"E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\PLRevneueAcntMap.txt";
            allocations.RptType = "ARO";
            allocations.BonusPaidAcct = "6025";
            allocations.BranchHistory = mockBranchHist;

            Allocations.AllocationsReserves rsrvs = allocations.Main(branch, 2016,revenue,expenses);

            // allocations
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[0] == (decimal)1507.7995);
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[1] == (decimal)887.4305);
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[2] == (decimal)500);
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[3] == 0);
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[4] == 0);
            Assert.IsTrue(rsrvs.BranchLoanLossReserveIncrease[5] == 0);

            Assert.IsTrue(rsrvs.PaidBackToCorporate[0] == 0);
            Assert.IsTrue(rsrvs.PaidBackToCorporate[1] == 0);
            Assert.IsTrue(rsrvs.PaidBackToCorporate[2] == 0);
            Assert.IsTrue(rsrvs.PaidBackToCorporate[3] == 0);
            Assert.IsTrue(rsrvs.PaidBackToCorporate[4] == 0);
            Assert.IsTrue(rsrvs.PaidBackToCorporate[5] == 0);

            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[0].ToString("#.##") == "25000");
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[1] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[2] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[3] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[4] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[5] == 0);

            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[0] == 0);
            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[1] == 0);
            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[2] == 0);
            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[3] == 0);
            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[4] == 0);
            Assert.IsTrue(rsrvs.BorrowedFromReservAccounts[5] == 0);

            Assert.IsTrue(rsrvs.TotalRsrvAccts[0].ToString("#.##") == "26507.8");
            Assert.IsTrue(rsrvs.TotalRsrvAccts[1].ToString("#.##") == "887.43");
            Assert.IsTrue(rsrvs.TotalRsrvAccts[2] == (decimal)500);
            Assert.IsTrue(rsrvs.TotalRsrvAccts[3] == 0);
            Assert.IsTrue(rsrvs.TotalRsrvAccts[4] == 0);
            Assert.IsTrue(rsrvs.TotalRsrvAccts[5] == 0);

            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[0].ToString("#.##") == "49910.54");
            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[1].ToString("#.##") == "40582.13");
            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[2].ToString("#.##") == "209773.32");
            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[3].ToString("#.##") == "116814.09");
            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[4] == 0);
            Assert.IsTrue(rsrvs.NetIncomeToReserveAccount[5] == 0);

            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[0].ToString("#.##") == "47081.9");
            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[1].ToString("#.##") == "43199.95");
            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[2].ToString("#.##") == "26630.88");
            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[3].ToString("#.##") == "26718.99");
            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[4] == 0);
            Assert.IsTrue(rsrvs.TotalExpensesAndAllocationsToCapital[5] == 0);

            // details - loan loss reserve
            Assert.IsTrue(rsrvs.LoanLossReserveBeginning[0].ToString("#.##") == "12604.77");
            Assert.IsTrue(rsrvs.LoanLossReserveBeginning[1].ToString("#.##") == "14112.57");
            Assert.IsTrue(rsrvs.LoanLossReserveBeginning[2].ToString("#.##") == "14500");
            Assert.IsTrue(rsrvs.LoanLossReserveBeginning[3].ToString("#.##") == "15000");
            // todo: set to zero when missing revenue?
            // todo: change rule inn spread sheet that says there must be loan volume
            //Assert.IsTrue(rsrvs.LoanLossReserveBeginning[4] == 0);
            // Assert.IsTrue(rsrvs.LoanLossReserveBeginning[5] == 0);

            Assert.IsTrue(rsrvs.LoanLossReserveContribution[0] == (decimal)1507.7995);
            Assert.IsTrue(rsrvs.LoanLossReserveContribution[1] == (decimal)887.4305);
            Assert.IsTrue(rsrvs.LoanLossReserveContribution[2] == 500);
            Assert.IsTrue(rsrvs.LoanLossReserveContribution[3] == 0);
            Assert.IsTrue(rsrvs.LoanLossReserveContribution[4] == 0);

            // acct 2706 - Loan Loss Utilized
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[0] == 0);
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[1] == (decimal)-500);
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[2] == 0);
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[3] == 0);
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[4] == 0);
            Assert.IsTrue(rsrvs.LoanLossReserveUtilized[5] == 0);

            // 2707 - Early Payoff
            Assert.IsTrue(rsrvs.EarlyPayoff[0] == 0);
            Assert.IsTrue(rsrvs.EarlyPayoff[1] == 0);
            Assert.IsTrue(rsrvs.EarlyPayoff[2] == 0);
            Assert.IsTrue(rsrvs.EarlyPayoff[3] == 0);
            Assert.IsTrue(rsrvs.EarlyPayoff[4] == 0);
            Assert.IsTrue(rsrvs.EarlyPayoff[5] == 0);

            // 2708 - Early Payment Default
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[0] == 0);
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[1] == 0);
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[2] == 0);
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[3] == 0);
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[4] == 0);
            Assert.IsTrue(rsrvs.EarlyPaymentDefault[5] == 0);

            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[0].ToString("#.##") == "14112.57");
            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[1].ToString("#.##") == "14500");
            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[2].ToString("#.##") == "15000");
            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[3].ToString("#.##") == "15000");
            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[4].ToString("#.##") == "15000");
            Assert.IsTrue(rsrvs.LoanLossReserveEndingbalance[5].ToString("#.##") == "15000");

            // details - corp investment account
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[0] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[1] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[2] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[3] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[4] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[5] == 0);

            Assert.IsTrue(rsrvs.OperatingLoss[0] == 0);
            Assert.IsTrue(rsrvs.OperatingLoss[1] == 0);
            Assert.IsTrue(rsrvs.OperatingLoss[2] == 0);
            Assert.IsTrue(rsrvs.OperatingLoss[3] == 0);
            Assert.IsTrue(rsrvs.OperatingLoss[4] == 0);
            Assert.IsTrue(rsrvs.OperatingLoss[5] == 0);

            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[0] == 0);
            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[1] == 0);
            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[2] == 0);
            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[3] == 0);
            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[4] == 0);
            Assert.IsTrue(rsrvs.AdvanceFromCorporatetoBranch[5] == 0);

            Assert.IsTrue(rsrvs.CorpInvestAcctBal[0] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[1] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[2] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[3] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[4] == 0);
            Assert.IsTrue(rsrvs.CorpInvestAcctBal[5] == 0);

            // details - Branch Reserve Account
            Assert.IsTrue(rsrvs.BranchReserveBeginning[0] == 0);
            Assert.IsTrue(rsrvs.BranchReserveBeginning[1].ToString("####.##") == "25000");
            Assert.IsTrue(rsrvs.BranchReserveBeginning[2].ToString("####.##") == "25000");
            Assert.IsTrue(rsrvs.BranchReserveBeginning[3].ToString("####.##") == "25000");
            // todo: change rule inn spread sheet that says there must be loan volume
            //Assert.IsTrue(rsrvs.BranchReserveBeginning[4] == 0);
            //Assert.IsTrue(rsrvs.BranchReserveBeginning[5] == 0);

            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[0].ToString("####.##") == "25000");
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[1] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[2] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[3] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[4] == 0);
            Assert.IsTrue(rsrvs.PaidIntoAccountfromBranch[5] == 0);

            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[0] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[1] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[2] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[3] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[4] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLoss[5] == 0);

            Assert.IsTrue(rsrvs.BranchReserveEnding[0].ToString("####.##") == "25000");
            Assert.IsTrue(rsrvs.BranchReserveEnding[1].ToString("####.##") == "25000");
            Assert.IsTrue(rsrvs.BranchReserveEnding[2].ToString("####.##") == "25000");
            Assert.IsTrue(rsrvs.BranchReserveEnding[3].ToString("####.##") == "25000");
            // todo: change rule inn spread sheet that says there must be loan volume
            //Assert.IsTrue(rsrvs.BranchReserveEnding[4] == 0);
            //Assert.IsTrue(rsrvs.BranchReserveEnding[5] == 0);

            // details: available funds to manager
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[0].ToString("####.##") == "49910.54");
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[1].ToString("####.##") == "40582.13");
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[2].ToString("####.##") == "209773.32");
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[3].ToString("####.##") == "116814.09");
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[4] == 0);
            Assert.IsTrue(rsrvs.AvailableFundsToManagerIncreases[5] == 0);

            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[0] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[1] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[2] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[3] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[4] == 0);
            Assert.IsTrue(rsrvs.AppliedtoCoverLosses[5] == 0);

            Assert.IsTrue(rsrvs.ArOverridePaid[0] == 0);
            Assert.IsTrue(rsrvs.ArOverridePaid[1] == 0);
            Assert.IsTrue(rsrvs.ArOverridePaid[2] == 0);
            Assert.IsTrue(rsrvs.ArOverridePaid[3] == 0);
            Assert.IsTrue(rsrvs.ArOverridePaid[4] == 0);
            Assert.IsTrue(rsrvs.ArOverridePaid[5] == 0);

            //Assert.IsTrue(rsrvs.NonProducingMgrDraw[0].ToString("####.##") == "5000");
            //Assert.IsTrue(rsrvs.NonProducingMgrDraw[1] == 0);
            //Assert.IsTrue(rsrvs.NonProducingMgrDraw[2] == 0);
            //Assert.IsTrue(rsrvs.NonProducingMgrDraw[3] == 0);
            //Assert.IsTrue(rsrvs.NonProducingMgrDraw[4] == 0);
            //Assert.IsTrue(rsrvs.NonProducingMgrDraw[5] == 0);

                         

            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[0].ToString("#.##") == "5000");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[1].ToString("#.##") == "54910.54");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[2].ToString("#.##") == "95492.68");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[3].ToString("#.##") == "305265.99");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[4].ToString("#.##") == "422080.08");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrBeg[5].ToString("#.##") == "422080.08");

            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[0].ToString("#.##") == "54910.54");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[1].ToString("#.##") == "95492.68");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[2].ToString("#.##") == "305265.99");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[3].ToString("#.##") == "422080.08");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[4].ToString("#.##") == "422080.08");
            Assert.IsTrue(rsrvs.AvailableFundsToMgrEnd[5].ToString("#.##") == "422080.08");
        }
    }
}