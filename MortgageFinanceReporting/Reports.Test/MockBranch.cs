using System;
using BranchRepository.Interface;

namespace Reports.Test
{
    public class MockBranch : IBranch
    { 
        #region Properties
        
   
        public int BranchId { get; set; }

        public int Year { get; set; }
        
        public string BranchName { get; set; }
        
        public decimal ManagerSalaryRate { get; set; }
        
        public decimal ManagerSalaryCap { get; set; }
        
        public decimal ARRate { get; set; }
        
        public decimal ARCap { get; set; }
        
        public decimal VolumeOverrideRate { get; set; }
        
        public decimal VolumeOverrideCap { get; set; }
        
        public decimal QrtBonusRate { get; set; }
        
        public decimal QrtBonusCap { get; set; }
        
        public decimal LoanVolChangeMetricRate { get; set; }
        
        public decimal LoanVolChangeMetricCap { get; set; }
        
        public decimal MonthlyClosingGoalRate { get; set; }
        
        public decimal MonthlyClosingGoalCap { get; set; }
        
        public decimal TotalMonthlyBudgetRate { get; set; }
        
        public decimal TotalMonthlyBudgetCap { get; set; }
        
        public decimal StartUpCostRate { get; set; }
        
        public decimal StartUpCostCap { get; set; }
        
        public decimal LoanLossReserveRate { get; set; }
        
        public decimal LoanLossReserveCap { get; set; }
        
        public decimal ReserveRequiredRate { get; set; }
        
        public decimal ReserveRequiredCap { get; set; }
        
        public decimal BranchReserveBalanceRate { get; set; }
        
        public decimal BranchReserveBalanceCap { get; set; }
        
        public decimal CorpInvestBalDueRate { get; set; }
        
        public decimal CorpInvestBalDueCap { get; set; }
        
        public decimal? BegLoanLossReserveBal { get; set; }

        public decimal? BegBranchReserveBal { get; set; }

        public decimal? BegAroAccountBal { get; set; } 
        
        public decimal? BranchMargin { get; set; }

        public decimal? LoMargin { get; set; }

        public decimal? ProcessingFee { get; set; }

        public decimal? LoanLossReserveUtilized { get; set; }
        #endregion
        

         public IBranch GetBranchData(string branchId)
        {
            IBranch branch = new MockBranch();
            // todo: add Available Funds To Manager

            if (branchId == "CA402")
             {
                 branch.BranchName = "Test Branch1";
                 branch.BranchId = 1;
                 branch.Year = 2014;
                 branch.ARRate = 4;
             }
             else if(branchId == "AROBranch") {
                 {
                     branch.BranchName = "AROBranch";
                     branch.BranchId = 1;
                     branch.Year = 2016;
                     branch.ARRate = new decimal(3.75);
                     branch.BegLoanLossReserveBal = (decimal?) 12604.77;
                     branch.LoanLossReserveCap = 15000;
                     branch.LoanLossReserveRate = new decimal(.05);
                     branch.BegBranchReserveBal = 0;
                    branch.ReserveRequiredCap = 25000;
                    branch.BegAroAccountBal = 5000;
                    //branch.BegBranchReserveBal
                 }
             }
            else if (branchId == "PLBranch")
            {
                {
                    branch.BranchName = "PLBranch";
                    branch.BranchId = 1;
                    branch.Year = 2016;
                    branch.ARRate = new decimal(3.75);
                    branch.BegLoanLossReserveBal = (decimal?)12604.77;
                    branch.LoanLossReserveCap = 15000;
                    branch.LoanLossReserveRate = new decimal(.05);
                    branch.BegBranchReserveBal = 25000;
                    branch.ReserveRequiredCap = 25000;
                    branch.BegAroAccountBal = 15000;
                }
            }

            return branch;
        }
    }
}