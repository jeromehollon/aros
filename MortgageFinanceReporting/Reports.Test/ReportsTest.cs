﻿// Example header text. Can be configured in the options.
using System;
using System.Collections.Generic;
using ExpenseRepository.Interface;
// todo: move this
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BranchRepository.Interface;
using RevenueRepository.Interface;

namespace Reports.Test
{
	[TestClass]
	public class ReportsTest
	{
		private static string GetAroRptStr2014()
		{
			AroReport aro = new AroReport();
			aro.Year = 2014;

			aro.BranchId = "CA902";
			aro.TemplateFilePath = @"E:\Clients\Active\5X\Venta\Code\MortgageFinanceReporting\Dashboard.Web\Templates\ARO2014.htm";
			aro.ExpenseMapPath = @"E:\Clients\Active\5X\Venta\Code\MortgageFinanceReporting\Dashboard.Web\/App_Code/ExpenseAcntMap2014.txt";
			aro.ExpenseTotalPath = @"E:\Clients\Active\5X\Venta\Code\MortgageFinanceReporting\Dashboard.Web\/App_Code/ExpenseTotalMap2014.txt";

			IBranch branch = new MockBranch();

			aro.Branch = branch.GetBranchData("CA902");

			string aroRptStr = aro.GetAroReport();
			return aroRptStr;
		}


		private static string GetAroRptStr2015()
		{
			AroReport aro = new AroReport();
			aro.Year = 2015;

			aro.BranchId = "CA902";
			aro.TemplateFilePath = @"E:\Clients\Active\5X\Venta\Code\MortgageFinanceReporting\Dashboard.Web\Templates\ARO2015.htm";
			aro.ExpenseMapPath = @"E:\Clients\Active\5X\Venta\Code\MortgageFinanceReporting\Dashboard.Web\App_Code\ExpenseAcntMap.txt";
			aro.ExpenseTotalPath = @"E:\Clients\Active\5X\Venta\Code\MortgageFinanceReporting\Dashboard.Web\App_Code\ExpenseTotalMap.txt";

			IBranch branch = new MockBranch();

			aro.Branch = branch.GetBranchData("CA902");

			string aroRptStr = aro.GetAroReport();
			return aroRptStr;
		}


			[TestMethod]
		public void AROIntegrationTest2014()
		{
			string aroRptStr = GetAroRptStr2014();

			Assert.IsFalse(aroRptStr.Contains("$H$3"));
			Assert.IsFalse(aroRptStr.Contains("$H$4"));
			Assert.IsFalse(aroRptStr.Contains("$H$5"));
			Assert.IsFalse(aroRptStr.Contains("$G$6"));
			Assert.IsFalse(aroRptStr.Contains("$H$17"));
			Assert.IsFalse(aroRptStr.Contains("$H$40"));
			Assert.IsFalse(aroRptStr.Contains("$H$52"));
			Assert.IsFalse(aroRptStr.Contains("$H$63"));
		}


		  [TestMethod]
		public void AroCalcTest()
		{
			String rpt = GetRpt();
			Assert.IsFalse(rpt.Contains("$H$92"));
		}


			  [TestMethod]
		public void AroCalcSumTest()
		{
			String rpt = GetRpt();
			Assert.IsFalse(rpt.Contains("<span id=\"H128\">11,518.97</span>"));
		}
		

		private static String GetRpt()
		{
			//IExpenses expenses = new ExpenseRepository.Sql.Expenses();
			IExpenses expenses = new MockExpenses();

			AroReport aro = new AroReport();

			aro.BranchId = "CA902";

			aro.TemplateFilePath = @"E:\Clients\Active\5X\Venta\Code\MortgageFinanceReporting\Dashboard.Web\Templates\ARO2014.htm";
			aro.ExpenseMapPath = @"E:\Clients\Active\5X\Venta\Code\MortgageFinanceReporting\Dashboard.Web\/App_Code/ExpenseAcntMap2014.txt";
			aro.ExpenseTotalPath = @"E:\Clients\Active\5X\Venta\Code\MortgageFinanceReporting\Dashboard.Web\/App_Code/ExpenseTotalMap2014.txt";
			aro.Expenses = expenses;

			IRevenue revenue = new MockRevenue();
			aro.Revenue = revenue;

			string aroRptStr = aro.GetAroReport();

			ReserveAccounts reserveAcct = new ReserveAccounts();
			reserveAcct.BranchId = aro.BranchId;
			reserveAcct.Year = 2014;
			reserveAcct.Expenses = expenses;
			reserveAcct.Revenue = revenue;

			String reserveAcctStr = reserveAcct.GetReserveAcctReport();

			String rpt = aroRptStr + reserveAcctStr;
			return rpt;
		}

		[TestMethod]
		public void GetExpenseAcctRowsReturnsCorrect()
		{
			Dictionary<string, int> acctRows = ExpenseAcctMap.GetAccountRows("E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\ExpenseAcntMap2014.txt");

			Assert.IsTrue(acctRows.Count > 0);
		}

				[TestMethod]
		public void GetExpenseTotalMapReturnsCorrect()
		{
			Dictionary<string, int> acctRows = ExpenseAcctMap.GetAccountRows("E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\ExpenseTotalMap.txt");

			Assert.IsTrue(acctRows.Count > 0);
		}
		
				[TestMethod]
		public void GetBonusMapReturnsCorrect()
		{
			Dictionary<string, int> acctRows = ExpenseAcctMap.GetAccountRows("E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\BonusAcntMap2014.txt");

			Assert.IsTrue(acctRows.Count > 0);
		}
		  
				[TestMethod]
		public void GetCogsMapReturnsCorrect()
		{
			string[] accts = ExpenseAcctMap.GetAccounts("E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\CogsAcntMap.txt");

			Assert.IsTrue(accts.Length > 0);
		}

			[TestMethod]
		public void GetExpenseAcctRowsReturnsCorrect2014()
		{
			Dictionary<string, int> acctRows = ExpenseAcctMap.GetAccountRows("E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\ExpenseAcntMap2014.txt");

			Assert.IsTrue(acctRows.Count > 0);
		}

				[TestMethod]
		public void GetExpenseTotalMapReturnsCorrect2014()
		{
			Dictionary<string, int> acctRows = ExpenseAcctMap.GetAccountRows("E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\ExpenseTotalMap2014.txt");

			Assert.IsTrue(acctRows.Count > 0);
		}
		
				[TestMethod]
		public void GetBonusTotalMapReturnsCorrect2014()
		{
			Dictionary<string, int> acctRows = ExpenseAcctMap.GetAccountRows("E:\\Clients\\Active\\5X\\Venta\\Code\\MortgageFinanceReporting\\Dashboard.Web\\App_Code\\BonusAcntMap2014.txt");

			Assert.IsTrue(acctRows.Count > 0);
		}
			   //[TestMethod]
		//public void GetRowsForTotalCalcsReturnsCorrect()
		//{
		//    Dictionary<string, int> acctRows = GetAccountRows();
		//    Dictionary<string, int> rowsToSum = AroReport.GetRowsForTotalCalcs(10, 15, acctRows);
			
		//    Assert.AreEqual(6, rowsToSum.Count);
		//}

		//[TestMethod]
		//public void GetAcctTotalsReturnCorrectSum()
		//{
		//    Dictionary<string, int> acctRows = GetAccountRows();
		//    List<IExpenses> summary = GetSummaryData();
		//    double acctTotal = AroReport.GetAcctTotals(1, 10, 15, acctRows, summary);

		//    Assert.AreEqual(600d, acctTotal);
		//}

		//[TestMethod()]
		//public void AddColumnTotalsTest()
		//{
		//    string output = AroReport.AddColumnTotals("18", summary, 
		//    Assert.Fail();
		//}

		//private List<IExpenses> GetSummaryData()
		//{            
		//    List<IExpenses> summaryList = new List<IExpenses>();
		   
		//    Expenses summary1 = new Expenses();
		//    summary1.AccountId = "5030";
		//    summary1.Month = 1;
		//    summary1.TotalAmt = 100;
		//    summaryList.Add(summary1);

		//    Expenses summary2 = new Expenses();
		//    summary2.AccountId = "5060";
		//    summary2.Month = 1;
		//    summary2.TotalAmt = 200;
		//    summaryList.Add(summary2);

		//    Expenses summary3 = new Expenses();
		//    summary3.AccountId = "5068";
		//    summary3.Month = 1;
		//    summary3.TotalAmt = 300;
		//    summaryList.Add(summary3);

		//    return summaryList;
		//}

		//static Dictionary<string, int> GetAccountRows()
		//{
		//    // map account id to row number
		//    Dictionary<string, int> acctRows = new Dictionary<string, int>();

		//    acctRows.Add("7870", 113);
		//    acctRows.Add("7860", 112);
		//    acctRows.Add("7850", 111);
		//    acctRows.Add("7830", 110);
		//    acctRows.Add("7820", 109);
		//    acctRows.Add("7810", 108);
		//    acctRows.Add("7685", 104);
		//    acctRows.Add("7660", 103);
		//    acctRows.Add("7650", 102);
		//    acctRows.Add("7640", 101);
		//    acctRows.Add("7615", 99);
		//    //acctRows.Add("7615", 99);
		//    acctRows.Add("7520", 94);
		//    // acctRows.Add("7400", 94);
		//    acctRows.Add("7510", 93);
		//    acctRows.Add("7500", 92);
		//    acctRows.Add("7490", 91);
		//    acctRows.Add("7473", 90);
		//    acctRows.Add("7472", 89);
		//    acctRows.Add("7471", 88);
		//    acctRows.Add("7470", 87);
		//    acctRows.Add("7460", 86);
		//    acctRows.Add("7455", 85);
		//    acctRows.Add("7450", 84);
		//    acctRows.Add("7445", 83);
		//    acctRows.Add("7440", 82);
		//    acctRows.Add("7100", 78);
		//    acctRows.Add("7290", 77);
		//    acctRows.Add("7270", 76);
		//    acctRows.Add("7250", 75);
		//    acctRows.Add("7230", 74);
		//    acctRows.Add("7220", 73);
		//    acctRows.Add("7210", 72);
		//    acctRows.Add("7200", 71);
		//    acctRows.Add("7190", 70);
		//    acctRows.Add("7185", 69);
		//    acctRows.Add("7150", 68);
		//    acctRows.Add("7140", 67);
		//    acctRows.Add("7120", 66);
		//    acctRows.Add("7030", 62);
		//    acctRows.Add("7010", 61);
		//    acctRows.Add("7005", 60);
		//    acctRows.Add("7000", 59);
		//    acctRows.Add("6980", 58);
		//    acctRows.Add("6960", 57);
		//    acctRows.Add("6930", 56);
		//    acctRows.Add("6910", 55);
		//    acctRows.Add("6860", 51);
		//    acctRows.Add("6850", 50);
		//    acctRows.Add("6840", 49);
		//    acctRows.Add("6830", 48);
		//    acctRows.Add("6825", 47);
		//    acctRows.Add("6780", 46);
		//    acctRows.Add("6760", 45);
		//    acctRows.Add("6740", 44);
		//    acctRows.Add("6720", 43);
		//    acctRows.Add("7300", 39);
		//    acctRows.Add("6610", 38);
		//    acctRows.Add("6600", 37);
		//    acctRows.Add("6590", 36);
		//    acctRows.Add("6580", 35);
		//    acctRows.Add("6570", 34);
		//    acctRows.Add("6560", 33);
		//    acctRows.Add("6540", 32);
		//    acctRows.Add("6530", 31);
		//    acctRows.Add("6500", 30);
		//    acctRows.Add("6490", 29);
		//    acctRows.Add("6480", 28);
		//    acctRows.Add("6460", 27);
		//    acctRows.Add("6430", 26);
		//    acctRows.Add("6410", 25);
		//    acctRows.Add("6350", 24);
		//    acctRows.Add("6120", 15);
		//    acctRows.Add("6110", 14);
		//    acctRows.Add("6100", 13);
		//    acctRows.Add("5068", 12);
		//    acctRows.Add("5060", 11);
		//    acctRows.Add("5030", 10);

		//    return acctRows;
		//}        
	}
}