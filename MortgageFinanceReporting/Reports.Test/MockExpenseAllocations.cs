﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ExpenseRepository.Interface;

namespace Reports.Test
{
    class MockExpenseAllocations : IExpenses
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public string AccountId { get; set; }
        public double TotalAmt { get; set; }
        public string BranchName { get; set; }

        List<IExpenses> summaryList = new List<IExpenses>();

        void addExpense(string accountId, int month, double totalAmt, string branch)
        {            
            MockExpenses summary = new MockExpenses();

            summary.AccountId = accountId; summary.Month = month; summary.TotalAmt = totalAmt; summary.BranchName = "branch"; 
            summaryList.Add(summary);
        }  

        List<IExpenses> IExpenses.GetSummaryByBranch(string branch, int year)
        {
            if (branch == "AROBranch")
            {
                // I just need sum of expenses for this test
                if (summaryList.Count <= 0)
                {
                    // EXPENSES
                    addExpense("6020", 1, 20574.10, branch);
                    addExpense("6020", 2, 42312.52, branch);
                    addExpense("6020", 3, 26130.88, branch);
                    addExpense("6020", 4, 26718.99, branch);

                    // COGS
                    addExpense("5020", 1, 16092.52, branch);
                    addExpense("5020", 2, 39218.14, branch);
                    addExpense("5020", 3, 35022.99, branch);
                    addExpense("5020", 4, 57289.96, branch);

                    //
                    addExpense("2706", 2, 500, branch);
                }
            }
            else if (branch == "PLBranch")
            {
                // EXPENSES
                //addExpense("6020", 1, 92918.77, branch); // 97918.77 - 5000 (bonus)
                addExpense("6020", 1, 97918.77, branch);
                addExpense("6020", 2, 72462.5, branch);
                addExpense("6020", 3, 80551.19, branch);
                addExpense("6020", 4, 87255.01, branch);
                addExpense("6020", 5, 65530.93, branch);
                addExpense("6020", 6, 74315.93, branch);

                addExpense("4020", 1, 130792.69, branch); // 130792.69 - 5000 (bonus) = 125792.69
                addExpense("4020", 2,  115017.59, branch);
                addExpense("4020", 3, 245947.33, branch);
                addExpense("4020", 4, 168476.45, branch);
                addExpense("4020", 5, 178030.05, branch);
                addExpense("4020", 6, 229011.36, branch);

                // COGS
                addExpense("5020", 1, 75170.68, branch);
                addExpense("5020", 2, 65866.03, branch);
                addExpense("5020", 3, 132926.91, branch);
                addExpense("5020", 4, 90566.28, branch);
                addExpense("5020", 5, 97074.12, branch);
                addExpense("5020", 6, 115663.67, branch); 

                addExpense("2706", 2, 500, branch);
                addExpense("2708", 6, 25000, branch);

                // BONUS
                // todo: this breaks other tests but is needed for mgr draws
                addExpense("6025", 1, 5000, branch);
            }

            return summaryList;
        }

        public DateTime DateRefreshed
        {
            get; set;       
        }


        public DateTime AcctClosingDate
        {
            get
            {
                return DateTime.Now;
            }
        }
    }
}
