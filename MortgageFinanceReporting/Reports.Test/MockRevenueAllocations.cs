﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RevenueRepository.Interface;

namespace Reports.Test
{
    internal class MockRevenueAllocations : IRevenue
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public double TotalAmt { get; set; }
        public int TotalLoans { get; set; }
        public string BranchName { get; set; }

        public void AddLoans(List<RevenueExtractor.Interface.LoanDetail> loans)
        {
            throw new NotImplementedException();
        }

        public List<IRevenue> GetSummary(string branchId)
        {
            List<IRevenue> summaryList = new List<IRevenue>();

            if (branchId == "AROBranch")
            {
                IRevenue summary1 = new MockRevenue();
                summary1.TotalLoans = 15;
                summary1.Month = 1;
                summary1.TotalAmt = 3015599;
                summary1.BranchName = "Test Branch CA Palmdale";
                summaryList.Add(summary1);

                IRevenue summary2 = new MockRevenue();
                summary2.TotalLoans = 20;
                summary2.Month = 2;
                summary2.TotalAmt = 3280006;
                summary2.BranchName = "Test Branch CA Palmdale";
                summaryList.Add(summary2);

                IRevenue summary3 = new MockRevenue();
                summary3.TotalLoans = 33;
                summary3.Month = 3;
                summary3.TotalAmt = 6386522;
                summary3.BranchName = "Test Branch CA Palmdale";
                summaryList.Add(summary3);

                IRevenue summary4 = new MockRevenue();
                summary4.TotalLoans = 28;
                summary4.Month = 4;
                summary4.TotalAmt = 4725248
                    ;
                summary4.BranchName = "Test Branch CA Palmdale";
                summaryList.Add(summary4);
            }
            else if (branchId == "PLBranch")
            {
                IRevenue summary1 = new MockRevenue();
                summary1.TotalLoans = 15;
                summary1.Month = 1;
                summary1.TotalAmt = 3015599;

                summary1.BranchName = "Test Branch CA Palmdale";
                summaryList.Add(summary1);

                IRevenue summary2 = new MockRevenue();
                summary2.TotalLoans = 20;
                summary2.Month = 2;
                summary2.TotalAmt = 3280006;
                summary2.BranchName = "Test Branch CA Palmdale";
                summaryList.Add(summary2);

                IRevenue summary3 = new MockRevenue();
                summary3.TotalLoans = 33;
                summary3.Month = 3;
                summary3.TotalAmt = 6386522;
                summary3.BranchName = "Test Branch CA Palmdale";
                summaryList.Add(summary3);

                IRevenue summary4 = new MockRevenue();
                summary4.TotalLoans = 28;
                summary4.Month = 4;
                summary4.TotalAmt = 4725248;

                summary4.BranchName = "Test Branch CA Palmdale";
                summaryList.Add(summary4);

                IRevenue summary5 = new MockRevenue();
                summary5.TotalLoans = 22;
                summary5.Month = 5;
                summary5.TotalAmt = 4848873;

                summary5.BranchName = "Test Branch CA Palmdale";
                summaryList.Add(summary5);


                IRevenue summary6 = new MockRevenue();
                summary6.TotalLoans = 31;
                summary6.Month = 6;
                summary6.TotalAmt = 5598432;

                summary6.BranchName = "Test Branch CA Palmdale";
                summaryList.Add(summary6);
            }

            return summaryList;
        }

    }
}