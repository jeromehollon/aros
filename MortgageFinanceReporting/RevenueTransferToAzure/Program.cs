﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using _5XUtilities;
using dotNetTips.Utility.Logging;

namespace ExpenseTransferToAzure
{
    class Program
    {  
        // notes:
        // split transactions in QB are only available via OpenSync when run for each branch individually
        // this pgm combines the resulting tables into on table 
        // it does this by as follows:
        //  OpenSync runs a report for each branch and saves it in a remote SQL db (OpenSync does not support Azure)
        //  This pgm creates a view that includes all report tables dynamically (report tables vary so the view must be dynamic)
         // It uses SQLBulkInsert to copy the tables.
        //  The source and the target must have the same structure

        static void Main()
        {      
            LoggingHelper.WriteEntry("Expense Transfer to Azure Started", TraceEventType.Information);
            
            string sourceConnStr = ConfigurationManager.ConnectionStrings["Source"].ConnectionString;

            try
            {
                PrepareSourceView(sourceConnStr);

                
                DataSet ds = GetSourceDataSet(sourceConnStr, "dbo.vwprofitandlossdetail", "profitandlossdetail");

                int newCount = ds.Tables[0].Rows.Count;

                // log the record count of the source table to ensure all records are counted
                LoggingHelper.WriteEntry("Source Table record count = " + newCount, TraceEventType.Information);

                int origCount = GetOrigCount();                

                // when the new records are fewer than the ones they are replacing ask the user if they want to proceed
                // the 1st of the year will always be less since the reports are YTD
                if (IsNewCountMoreThanOld(newCount, origCount) == true)                   
                {
                    CompleteExtraction(sourceConnStr, ds);
                }
                else
                {
                    string lowCountMsg = String.Format("Refresh suspended. Source records {0} are fewer than the old count {1}.", origCount, newCount);
                    AskUserToContinue(sourceConnStr, ds, lowCountMsg);
                }

                // todo: move this to a new class
                // sub ledger
                string targetConnStr = ConfigurationManager.ConnectionStrings["Target"].ConnectionString;
                DataSet dsSubLeger = GetSourceDataSet(sourceConnStr, "subledger", "subledger");

                SaveToTargetDb("subledger", targetConnStr, dsSubLeger);
                DropSourceTable(sourceConnStr, "subledger");

                // ARO Bonus Paid
                DataSet dsAroBonusPaid = GetSourceDataSet(sourceConnStr, "arobonuspaid", "arobonuspaid");

                SaveToTargetDb("arobonuspaid", targetConnStr, dsAroBonusPaid);
                DropSourceTable(sourceConnStr, "arobonuspaid");
            }
            catch (Exception ex)
            {
                IExceptionManagement exceptionMgt = GetExceptionMgt();

                exceptionMgt.logException(ex, "Venta", "", "Expense Transfer", "");
                LoggingHelper.WriteEntry("Error: " + ex.Message, TraceEventType.Error);
            }            
            
            LoggingHelper.WriteEntry("Expense Transfer to Azure Ended", TraceEventType.Information);
            Console.WriteLine("Expense Transfer to Azure Ended");
        }


        #region Private Main
        static private void PrepareSourceView(string connStr)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                connection.Open();

                SqlCommand cmdProc = new SqlCommand("uspGenerateProfitAndLossDetailView", connection);
                cmdProc.ExecuteNonQuery();
            }
        }
        

        static private DataSet GetSourceDataSet(string connStr, string srcTbl, string trgtTbl)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                //Create a SqlDataAdapter
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {

                    // A table mapping names the DataTable.
                    adapter.TableMappings.Add("Table", trgtTbl);

                    // Open the connection.
                    connection.Open();
                    Console.WriteLine("The SqlConnection is open.");

                    // Create a SqlCommand to retrieve expense data.
                    SqlCommand command = new SqlCommand(
                        "SELECT * FROM " + srcTbl + ";",
                        connection);
                    command.CommandType = CommandType.Text;

                    // Set the SqlDataAdapter's SelectCommand.
                    adapter.SelectCommand = command;

                    // Fill the DataSet.
                    DataSet dataSet = new DataSet(trgtTbl);
                    adapter.Fill(dataSet);

                    return dataSet;
                }
            }
        }


        private static bool IsNewCountMoreThanOld(int newCount, int origCount)
        {
            // flag if new count is more than 10% smaller than the original
            if (origCount <= (newCount * 1.1) && DateTime.Now.Month > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void CompleteExtraction(string sourceConnStr, DataSet ds)
        {
            EmptyTargetTables();

            string targetConnStr = ConfigurationManager.ConnectionStrings["Target"].ConnectionString;
            SaveToTargetDb("profitandlossdetail", targetConnStr, ds);
            
            LastUpdate();

            // delete source data
            DropExpenseSourceTables(sourceConnStr);

            // populate the ranking table - persisting it in a table speeds load time in web app
            FillBpsRankingTbl(targetConnStr);
        }


        static private void EmptyTargetTables()
        {
            using (TargetEntities context = new TargetEntities())
            {
                // log the record count of the source table to ensure all records are counted
                var origCount = context.ProfitAndLossDetails.Count();

                string statusMsg = "Original Target Table record count = " + origCount;

                context.Database.ExecuteSqlCommand("DELETE dbo.profitandlossdetail");

                context.Database.ExecuteSqlCommand("DELETE dbo.subledger");

                context.Database.ExecuteSqlCommand("DELETE dbo.arobonuspaid");

                LoggingHelper.WriteEntry(statusMsg, TraceEventType.Information);

                Console.WriteLine(statusMsg);
            }
        }


        static private void SaveToTargetDb(string tableName, string targetConnStr, DataSet ds)
        {
            DataTable dt = ds.Tables[0];

            /* bulk copy data table to sql table */
            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(targetConnStr) { DestinationTableName = tableName })
            {
                bulkCopy.BatchSize = 100;
                bulkCopy.EnableStreaming = true;
                bulkCopy.BulkCopyTimeout = 4000;
                bulkCopy.WriteToServer(dt);
            }
        }


        static private void DropExpenseSourceTables(string connStr)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                connection.Open();
               
                for (int i = 1; i < 40; i++)
                    {
                    try
                    {                   
                            SqlCommand cmd = new SqlCommand("DROP table dbo.profitandlossdetail" + i, connection);
                            cmd.ExecuteNonQuery();
                        }
                    catch
                    {
                        // swallow these errors since we don't know if the actual table even exists - just degrade gracefully
                    }
                }
                
            }
        }


        static private void DropSourceTable(string connStr, string tableName)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                connection.Open();               
               
                try
                {                   
                    SqlCommand cmd = new SqlCommand("DROP table " + tableName, connection);
                    cmd.ExecuteNonQuery();
                }
                catch
                {
                    // swallow these errors since we don't know if the actual table even exists - just degrade gracefully
                }  
            }
        }


        static private void FillBpsRankingTbl(string connStr)
        {
            using (SqlConnection connection = new SqlConnection(connStr))
            {
                connection.Open();

                SqlCommand cmdProc = new SqlCommand("dbo.uspFillBranchBpsTbl", connection);
                cmdProc.CommandTimeout = 4000;
                cmdProc.ExecuteNonQuery();
            }
        }

        #endregion


        #region Private
        private static int GetOrigCount()
        {
            TargetEntities context = new TargetEntities();
            var origCount = context.ProfitAndLossDetails.Count();
            return origCount;
        }



        private static IExceptionManagement GetExceptionMgt()
        {
            string uriPath = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(Program)).CodeBase) + "/ErrLog.txt";
            string localPath = new Uri(uriPath).LocalPath;
            IExceptionManagement exceptionMgt = new ExceptionManagement(ExceptionManagement.AlertMethod.File, localPath, "", "", "");
            return exceptionMgt;
        }


        private static void AskUserToContinue(string sourceConnStr, DataSet ds, string lowCountMsg)
        {
            Console.WriteLine(lowCountMsg);
            Console.WriteLine("Type 'y' to proceed; 'n' to end");

            string proceed = Console.ReadLine();
            if (proceed == "y")
            {
                CompleteExtraction(sourceConnStr, ds);
            }
            else
            {
                LoggingHelper.WriteEntry("User declined option to proceed.");
            }
        }   
       

            static private void LastUpdate()
        { 
            int rowsCopied = 0;

            using (TargetEntities context = new TargetEntities())
            {   
                 // log the record count of the source table to ensure all records are counted
                rowsCopied = context.ProfitAndLossDetails.Count();

                  // log the record count of the source table to ensure all records are counted
               string statusMsg = "Updated Target Table record count = " + rowsCopied;

                LoggingHelper.WriteEntry(statusMsg, TraceEventType.Information);

                Console.WriteLine(statusMsg);

                context.Database.ExecuteSqlCommand(String.Format("INSERT INTO dbo.ProfitAndLossDetailLastUpdate (LastUpdate, RowsCopied) VALUES(getDate(), {0})", rowsCopied));              
            }
        }

#endregion
    }
}
