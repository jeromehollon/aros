﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using _5XUtilities;
using BranchRepository.Interface;
using BranchReservesPostings;
using dotNetTips.Utility.Logging;
using WatiN.Core;

namespace BranchReserves
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            // log starting event
            LoggingHelper.WriteEntry("Branch Reserves Postings Started", TraceEventType.Information);

            string uriPath = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(Program)).CodeBase) + "/ErrLog.txt";
            string localPath = new Uri(uriPath).LocalPath;
            IExceptionManagement exceptionMgt = new ExceptionManagement(ExceptionManagement.AlertMethod.File, localPath, "", "", "");
            string branchName = "";

            int jobId;
            string yearStr;
            int monthNum;

            try
            {                
                GetJobParameters(out jobId, out yearStr, out monthNum);

                // run when pending job request exists (no complete date)
                if (jobId > 0)
                {
                    // Get Branches for selected year
                    List<IBranch> branches = GetBranchList(yearStr);

                    // create web session
                    string arosUrl = BranchReservesPostings.Properties.Settings.Default.ArosUrl.ToString();

                    using (var browser = new IE(arosUrl, true))
                    {
                        // login and setup page the 1st time
                        SetupPage(yearStr, browser);

                        foreach (var branch in branches)
                        {
                            branchName = branch.BranchName;
                            IBranch br = new BranchRepository.Sql.Branches();
                            br.BranchId = branch.BranchId;
                            br.BranchName = branch.BranchName;

                            IBranchReserves branchReserves = ReserveReport(browser, yearStr, monthNum, br);

                            Console.WriteLine(branchName + ": " + branchReserves.LoanLossReserveIncrease + ": " + branchReserves.LoanLossReserveBal);
                            Console.WriteLine(branchName + ": " + branchReserves.AroOverride + ": " + branchReserves.AroBalance);

                            LoggingHelper.WriteEntry(branchName + ": " + branchReserves.LoanLossReserveIncrease + ": " + branchReserves.LoanLossReserveBal + "ARO: " + branchReserves.AroOverride + ": " + branchReserves.AroBalance, TraceEventType.Information);

                            // save the results to a db table
                            SaveBranchReserves(jobId, branch, branchReserves);
                        }
                    }
                    UpdateJobResolutionDate(jobId);
                }
            }
            catch (Exception ex)
            {
                exceptionMgt.logException(ex, "Venta", "", "Branch Reserves Postings", branchName);
                LoggingHelper.WriteEntry("Error: " + ex.Message + ex.InnerException, TraceEventType.Error);
            }

            LoggingHelper.WriteEntry("Branch Reserves Postings Ended", TraceEventType.Information);
        }

        private static void SetupPage(string yearStr, IE browser)
        {
            browser.TextField(Find.ById("UserName")).TypeText("admin");
            browser.TextField(Find.ById("Password")).TypeText(BranchReservesPostings.Properties.Settings.Default.AdminPw.ToString());
            browser.Button(Find.ByValue("Log in")).Focus();
            browser.Button(Find.ByValue("Log in")).ClickNoWait();
            browser.WaitForComplete();
            Thread.Sleep(1000);

            browser.Link(link => link.Text == "View Reserve Accounts Report »").Click();
            Thread.Sleep(1000);

            var yearList = browser.SelectList(Find.ById("lstYear"));
            yearList.Select(yearStr);
            Thread.Sleep(5000);
        }

        private static void SaveBranchReserves(int jobId, IBranch branch, IBranchReserves branchReserves)
        {
            using (var context = new AROSReportsEntities())
            {
                var branchPosting = new BranchReservesPostings.BranchPosting();
                var branchPostings = context.BranchPostings;

                branchPosting.JobId = jobId;
                branchPosting.BranchId = branch.BranchId;
                branchPosting.LoanLossReserveIncrease = branchReserves.LoanLossReserveIncrease;
                branchPosting.LoanLossReserveBal = branchReserves.LoanLossReserveBal;
                branchPosting.AroOverride = branchReserves.AroOverride;
                branchPosting.AroBalance = branchReserves.AroBalance;

                branchPostings.Add(branchPosting);
                context.SaveChanges();
            }
        }


        private static List<IBranch> GetBranchList(string yearStr)
        {
            List<IBranch> branches = new List<IBranch>();

            if (yearStr == DateTime.Now.Year.ToString())
            {
                branches = BranchRepository.Sql.Branches.GetBranches(DateTime.Now.Year);
            }
            else
            {
                branches = BranchRepository.Sql.Archive.Branches.GetBranches(int.Parse(yearStr));
            }

            return branches;
        }

        private static void GetJobParameters(out int jobId, out string yearStr, out int monthNum)
        {
            AROSReportsEntities db = new AROSReportsEntities();
            var postingJobs = db.BranchPostingsJobs.Where(j => j.IsComplete == false || j.CompleteDate == null).OrderByDescending(j => j.JobId).FirstOrDefault();

            if (postingJobs != null)
            {
                using (var postingJob = new AROSReportsEntities())
                {
                    // get requested year string 
                    yearStr = postingJobs.Year.ToString();
                    // get requested month
                    monthNum = postingJobs.Month;

                    jobId = postingJobs.JobId;
                }
            }
            else
            {
                jobId = -1;
                yearStr = "";
                monthNum = -1;
            }
        }

        private static void UpdateJobResolutionDate(int jobId)
        {
            using (var context = new AROSReportsEntities())
            {
                var branchPostingJob = context.BranchPostingsJobs.Where(j => j.JobId == jobId).FirstOrDefault();

                
                    branchPostingJob.CompleteDate = DateTime.Now;
                    branchPostingJob.IsComplete = true;
                    context.SaveChanges();
              
            }


            }

        public static IBranchReserves ReserveReport(IE browser, string year, int monthNum, IBranch branch)
        {
            IBranchReserves branchRsrv = new BranchReserves();
           
            //using (browser )
            //{              

                var branchList = browser.SelectList(Find.ById("lstBranchId"));
                branchList.Select(branch.BranchName);

                browser.WaitForComplete();
                browser.Button(Find.ByValue("Ok")).ClickNoWait();
                browser.WaitForComplete();
                Thread.Sleep(1000);

                string[] monthCols = GetMonthCols();
                string monthLetter = monthCols[monthNum-1]; // zero based array so subtract 1 

                branchRsrv.BranchName = branch.BranchName;
                if (browser.Body.Element("RA" + monthLetter + "7").InnerHtml == "-")
                    {
                        branchRsrv.LoanLossReserveIncrease = 0;
                    }
                    else
                    {
                    branchRsrv.LoanLossReserveIncrease = decimal.Parse(browser.Body.Element("RA" + monthLetter + "7").Text);
                    }
                
                    if (browser.Body.Element("RA" + monthLetter + "8").InnerHtml == "-")
                    {
                        branchRsrv.LoanLossReserveBal = 0;
                    }
                    else
                    {
                    branchRsrv.LoanLossReserveBal = decimal.Parse(browser.Body.Element("RA" + monthLetter + "8").Text);
                    }

                    if (browser.Body.Element("RA" + monthLetter + "27").InnerHtml == "-")
                    {
                        branchRsrv.AroOverride = 0;
                    }
                    else
                    {
                        branchRsrv.AroOverride = decimal.Parse(browser.Body.Element("RA" + monthLetter + "27").Text);
                    }

                if (browser.Body.Element("RA" + monthLetter + "29").InnerHtml == "-")
                    {
                        branchRsrv.AroBalance = 0;
                    }
                    else
                    {
                    branchRsrv.AroBalance = decimal.Parse(browser.Body.Element("RA" + monthLetter + "29").Text);
                    }                
            //}

            return branchRsrv;
        }

        
        private static string[] GetMonthCols()
        {
            // month array of column letters
            string[] monthCols = new string[] { "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S" };
            return monthCols;
        }
    }
}
