﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BranchRepository.Interface;
using BranchRepository.Sql;

namespace BranchReserves
{
    public interface IBranchReserves : IBranch
    {
        decimal LoanLossReserveIncrease { get; set; }
        decimal LoanLossReserveBal { get; set; }
        decimal AroOverride { get; set; }
        decimal AroBalance { get; set; }
    }

    public class BranchReserves : IBranchReserves
    {
        public decimal LoanLossReserveIncrease { get; set; }
        public decimal LoanLossReserveBal { get; set; }
        public decimal AroOverride { get; set; }
        public decimal AroBalance { get; set; }
        public int BranchId { get; set; }

        public int Year { get; set; }

        public string BranchName { get; set; }

        public decimal ManagerSalaryRate { get; set; }

        public decimal ManagerSalaryCap { get; set; }

        public decimal ARRate { get; set; }

        public decimal ARCap { get; set; }

        public decimal VolumeOverrideRate { get; set; }

        public decimal VolumeOverrideCap { get; set; }

        public decimal QrtBonusRate { get; set; }

        public decimal QrtBonusCap { get; set; }

        public decimal LoanVolChangeMetricRate { get; set; }

        public decimal LoanVolChangeMetricCap { get; set; }

        public decimal MonthlyClosingGoalRate { get; set; }

        public decimal MonthlyClosingGoalCap { get; set; }

        public decimal TotalMonthlyBudgetRate { get; set; }

        public decimal TotalMonthlyBudgetCap { get; set; }

        public decimal StartUpCostRate { get; set; }

        public decimal StartUpCostCap { get; set; }

        public decimal LoanLossReserveRate { get; set; }

        public decimal LoanLossReserveCap { get; set; }

        public decimal ReserveRequiredRate { get; set; }

        public decimal ReserveRequiredCap { get; set; }

        public decimal BranchReserveBalanceRate { get; set; }

        public decimal BranchReserveBalanceCap { get; set; }

        public decimal CorpInvestBalDueRate { get; set; }

        public decimal CorpInvestBalDueCap { get; set; }

        public decimal? BegLoanLossReserveBal { get; set; }

        public decimal? BegBranchReserveBal { get; set; }

        public decimal? BegAroAccountBal { get; set; }

        public decimal? BranchMargin { get; set; }

        public decimal? LoMargin { get; set; }

        public decimal? ProcessingFee { get; set; }


        public BranchReserves()
        {

        }

        public IBranch GetBranchData(string branchId)
        {
            IBranch branch = new BranchReserves();
            return branch;
        }

        static List<IBranchReserves> GetBranches(int year)
        {
            List<IBranchReserves> branchList = new List<IBranchReserves>();
            return branchList;
        }
    }
}